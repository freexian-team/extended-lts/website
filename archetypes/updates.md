---
title: "ELA-$DSAID $PACKAGE $UPDATEINFO"
package: $PACKAGE
version: $VERSIONS
version_map: $VERSION_MAP
description: ""
date: {{ .Date }}
draft: false
type: updates
tags:
- update
cvelist:
$CVELIST
---

This is the text of the security advisory.
