---
title: "ELA-530-1 systemd bug fix"
package: systemd
version: 215-17+deb8u14
distribution: "Debian 8 jessie"
description: "systemd bugfix update"
date: 2021-12-27T01:28:58+05:30
draft: false
type: updates
cvelist:

---

`systemd-shutdown` is run after the network is stopped, so remounting
a network filesystem read-only can hang. A simple umount is the most
useful thing that can be done for a network filesystem once the
network is down.
