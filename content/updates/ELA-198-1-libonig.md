---
title: "ELA-198-1 libonig security update"
package: libonig
version: 5.9.1-1+deb7u4
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2019-12-04T10:54:33+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-19012
  - CVE-2019-19204
  - CVE-2019-19246

---

Several vulnerabilities were discovered in the Oniguruma regular
expressions library, notably used in PHP mbstring.

* CVE-2019-19012

    An integer overflow in the search_in_range function in regexec.c
    leads to an out-of-bounds read, in which the offset of this read
    is under the control of an attacker. (This only affects the 32-bit
    compiled version). Remote attackers can cause a denial-of-service
    or information disclosure, or possibly have unspecified other
    impact, via a crafted regular expression.

* CVE-2019-19204

    In the function fetch_range_quantifier in regparse.c, PFETCH is
    called without checking PEND. This leads to a heap-based buffer
    over-read and lead to denial-of-service via a crafted regular
    expression.

* CVE-2019-19246

    Heap-based buffer over-read in str_lower_case_match in regexec.c
    can lead to denial-of-service via a crafted regular expression.
