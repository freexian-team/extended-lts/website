---
title: "ELA-1097-1 libreoffice security update"
package: libreoffice
version: 1:6.1.5-3~deb9u3 (stretch)
version_map: {"9 stretch": "1:6.1.5-3~deb9u3"}
description: "unchecked script execution"
date: 2024-05-26T13:56:58Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-3044

---

Unchecked script execution in Graphic on-click binding in affected LibreOffice versions allows an attacker to create a document which without prompt will execute scripts built-into LibreOffice on clicking a graphic. Such scripts were previously deemed trusted but are now deemed untrusted.
