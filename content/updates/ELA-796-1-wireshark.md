---
title: "ELA-796-1 wireshark security update"
package: wireshark
version: 2.6.20-0+deb9u4 (stretch)
version_map: {"9 stretch": "2.6.20-0+deb9u4"}
description: "denial of service"
date: 2023-02-08T22:02:38+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-4345
  - CVE-2023-0411
  - CVE-2023-0412
  - CVE-2023-0413
  - CVE-2023-0415
  - CVE-2023-0417

---

Multiple security vulnerabilities have been discovered in Wireshark, a
network traffic analyzer. An attacker could cause a denial of service
(infinite loop or application crash) via packet injection or a crafted
capture file.

CVE-2022-4345

    Infinite loops in the BPv6, OpenFlow, and Kafka protocol dissectors in
    Wireshark 4.0.0 to 4.0.1 and 3.6.0 to 3.6.9 allows denial of service via
    packet injection or crafted capture file

CVE-2023-0411

    Excessive loops in multiple dissectors in Wireshark 4.0.0 to 4.0.2 and
    3.6.0 to 3.6.10 and allows denial of service via packet injection or
    crafted capture file

CVE-2023-0412

    TIPC dissector crash in Wireshark 4.0.0 to 4.0.2 and 3.6.0 to 3.6.10 and
    allows denial of service via packet injection or crafted capture file

CVE-2023-0413

    Dissection engine bug in Wireshark 4.0.0 to 4.0.2 and 3.6.0 to 3.6.10
    and allows denial of service via packet injection or crafted capture
    file

CVE-2023-0415

    iSCSI dissector crash in Wireshark 4.0.0 to 4.0.2 and 3.6.0 to 3.6.10
    and allows denial of service via packet injection or crafted capture
    file

CVE-2023-0417

    Memory leak in the NFS dissector in Wireshark 4.0.0 to 4.0.2 and 3.6.0
    to 3.6.10 and allows denial of service via packet injection or crafted
    capture file
