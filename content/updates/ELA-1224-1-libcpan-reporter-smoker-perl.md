---
title: "ELA-1224-1 libcpan-reporter-smoker-perl bug fix update"
package: libcpan-reporter-smoker-perl
version: 0.28-1+deb9u1 (stretch), 0.29-1+deb10u1 (buster)
version_map: {"9 stretch": "0.28-1+deb9u1", "10 buster": "0.29-1+deb10u1"}
description: "build fix"
date: 2024-11-01T22:22:16Z
draft: false
type: updates
tags:
- update
cvelist:

---

This update fixes the build of this package, which was preventing security
updates of perl to be tested.
