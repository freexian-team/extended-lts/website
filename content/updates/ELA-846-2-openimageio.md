---
title: "ELA-846-2 openimageio regression update"
package: openimageio
version: 1.4.14~dfsg0-1+deb8u2 (jessie)
version_map: {"8 jessie": "1.4.14~dfsg0-1+deb8u2"}
description: "armel binaries"
date: 2023-05-04T02:35:22+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

The previous security update of openimageio, issued as ELA-846-1, could not be
built on the armel computer platform for Debian 8 "Jessie". This update
disables the creation of openimageio's pdf documentation at build time on
armel. All other platforms are not affected and an upgrade is not required.
