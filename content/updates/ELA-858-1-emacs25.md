---
title: "ELA-858-1 emacs25 security update"
package: emacs25
version: 25.1+1-4+deb9u2 (stretch)
version_map: {"9 stretch": "25.1+1-4+deb9u2"}
description: "arbitrary shell command execution"
date: 2023-05-30T00:56:46+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-45939
  - CVE-2022-48337
  - CVE-2022-48339
  - CVE-2023-28617

---

Xi Lu discovered that missing input sanitizing in Emacs could result in the
execution of arbitrary shell commands.

