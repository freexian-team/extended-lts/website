---
title: "ELA-1228-1 openjdk-8 security update"
package: openjdk-8
version: 8u432-b06-2~deb8u1 (jessie), 8u432-b06-2~deb9u1 (stretch)
version_map: {"8 jessie": "8u432-b06-2~deb8u1", "9 stretch": "8u432-b06-2~deb9u1"}
description: "multiple vulnerabilities"
date: 2024-11-04T18:20:50+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-21208
  - CVE-2024-21210
  - CVE-2024-21217
  - CVE-2024-21235

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
which may result in denial of service, information disclosure or bypass
of Java sandbox restrictions.
