---
title: "ELA-908-1 netty-3.9 security update"
package: netty-3.9
version: 3.9.0.Final-1+deb8u2 (jessie)
version_map: {"8 jessie": "3.9.0.Final-1+deb8u2"}
description: "information disclosure"
date: 2023-07-31T18:34:23+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-21290

---

It was discovered that there was an insecure temporary file issue that could
have lead to disclosure of arbitrary local files.
