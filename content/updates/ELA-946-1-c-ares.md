---
title: "ELA-946-1 c-ares security update"
package: c-ares
version: 1.10.0-2+deb8u7 (jessie), 1.12.0-1+deb9u6 (stretch)
version_map: {"8 jessie": "1.10.0-2+deb8u7", "9 stretch": "1.12.0-1+deb9u6"}
description: "buffer overflow"
date: 2023-09-15T10:49:43+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-22217

---

A vulnerability has been identified in c-ares, an asynchronous name
resolver library:

CVE-2020-22217

    A buffer overflow vulnerability has been found in c-ares before
    via the function ares_parse_soa_reply in ares_parse_soa_reply.c.
    This vulnerability was discovered through fuzzing. Exploitation
    of this vulnerability may allow an attacker to execute arbitrary
    code or cause a denial of service condition.
