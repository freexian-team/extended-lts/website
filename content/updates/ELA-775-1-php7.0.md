---
title: "ELA-775-1 php7.0 security update"
package: php7.0
version: 7.0.33-0+deb9u13 (stretch)
version_map: {"9 stretch": "7.0.33-0+deb9u13"}
description: "multiple vulnerabilities"
date: 2023-01-24T09:44:22+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-21707
  - CVE-2022-31625
  - CVE-2022-31626
  - CVE-2022-31628
  - CVE-2022-31629

---

Multiple security issues were discovered in PHP, a widely-used open
source general purpose scripting language which could result in denial
of service, information disclosure, insecure cookie handling or
potentially the execution of arbitrary code.
