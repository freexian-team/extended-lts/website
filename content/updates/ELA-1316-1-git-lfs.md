---
title: "ELA-1316-1 git-lfs security update"
package: git-lfs
version: 2.7.1-1+deb10u2 (buster)
version_map: {"10 buster": "2.7.1-1+deb10u2"}
description: "information disclosure"
date: 2025-02-04T13:14:07+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-53263

---

* **CVE-2024-53263**

    When Git LFS requests credentials from Git for a remote host, it
    passes portions of the host's URL to the `git-credential(1)` command
    without checking for embedded line-ending control characters, and then
    sends any credentials it receives back from the Git credential helper
    to the remote host. By inserting URL-encoded control characters such
    as line feed (LF) or carriage return (CR) characters into the URL,
    an attacker may be able to retrieve a user's Git credentials.
