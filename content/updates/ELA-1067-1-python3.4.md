---
title: "ELA-1067-1 python3.4 security update"
package: python3.4
version: 3.4.2-1+deb8u17 (jessie)
version_map: {"8 jessie": "3.4.2-1+deb8u17"}
description: "quoted-overlap zipbomb DoS"
date: 2024-03-24T23:57:01+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-0450

---

The zipfile module was vulnerable to “quoted-overlap” zip-bombs
in the Python 3 interpreter.