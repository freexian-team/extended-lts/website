---
title: "ELA-498-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb8u16
distribution: "Debian 8 jessie"
description: "buffer overrun might result in DoS"
date: 2021-10-23T00:18:22+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3712

---

An issue has been found in openssl, a Secure Sockets Layer toolkit. Ingo Schwarze reported a buffer overrun flaw when processing ASN.1 strings, which can result in denial of service.
