---
title: "ELA-717-1 freerdp security update"
package: freerdp
version: 1.1.0~git20140921.1.440916e+dfsg1-13+deb9u5 (stretch)
version_map: {"9 stretch": "1.1.0~git20140921.1.440916e+dfsg1-13+deb9u5"}
description: "several out-of-bound memory access vulnerabilities"
date: 2022-10-29T15:34:37+02:00
draft: false
type: updates
tags:
- update
cvelist:
 - CVE-2020-4030
 - CVE-2020-4033
 - CVE-2020-11086
 - CVE-2020-11088 
 - CVE-2020-11089
 - CVE-2020-11095
 - CVE-2020-11096
 - CVE-2020-11098
 - CVE-2021-41160
---

Several memory access vulnerabilities have been discovered in FreeRDP, a
free implementation of Microsoft's Remote Desktop Protocol. These
vulnerabilities could lead to both Denial of Service and access to
privileged memory, like password hashes.
