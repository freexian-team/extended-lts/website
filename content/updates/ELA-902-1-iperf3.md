---
title: "ELA-902-1 iperf3 security update"
package: iperf3
version: 3.0.7-1+deb8u2 (jessie), 3.1.3-1+deb9u1 (stretch)
version_map: {"8 jessie": "3.0.7-1+deb8u2", "9 stretch": "3.1.3-1+deb9u1"}
description: "denial of service"
date: 2023-07-27T18:05:26+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-38403

---

A memory allocation issue was found in iperf3, the Internet Protocol bandwidth
measuring tool, that may cause a denial of service when encountering a certain
invalid length value in TCP packets.
