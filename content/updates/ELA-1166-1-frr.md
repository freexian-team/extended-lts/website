---
title: "ELA-1166-1 frr security update"
package: frr
version: 7.5.1-1.1+deb10u3 (buster)
version_map: {"10 buster": "7.5.1-1.1+deb10u3"}
description: "missing length check"
date: 2024-08-27T23:45:36+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-44070

---

An issue has been found in frr, a routing suite of internet protocols (BGP, OSPF, IS-IS, ...)
Before using the TLV value, due to a missing length check of the remaining stream, one could read behind the buffer.
