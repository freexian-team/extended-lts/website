---
title: "ELA-832-2 syslog-ng regression update"
package: syslog-ng
version: 3.5.6-2+deb8u2 (jessie)
version_map: {"8 jessie": "3.5.6-2+deb8u2"}
description: "regression update"
date: 2023-05-04T21:11:54+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

It has been reported that the previous security update, issued as ELA-832-1,
caused a regression leading to the syslog-ng daemon restarting.

This update fixes this regression.
