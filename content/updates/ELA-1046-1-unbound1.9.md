---
title: "ELA-1046-1 unbound1.9 security update"
package: unbound1.9
version: 1.9.0-2+deb10u2~deb9u4 (stretch)
version_map: {"9 stretch": "1.9.0-2+deb10u2~deb9u4"}
description: "denial of service"
date: 2024-02-24T11:49:09+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-50387
  - CVE-2023-50868

---

Two vulnerabilities were discovered in unbound, a validating, recursive,
caching DNS resolver. Specially crafted DNSSEC answers could lead unbound
down a very CPU intensive and time costly DNSSEC (CVE-2023-50387) or NSEC3
hash (CVE-2023-50868) validation path, resulting in denial of service.

