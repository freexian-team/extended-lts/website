---
title: "ELA-1324-1 openssh security update"
package: openssh
version: 1:7.4p1-10+deb9u10 (stretch), 1:7.9p1-10+deb10u5 (buster)
version_map: {"9 stretch": "1:7.4p1-10+deb9u10", "10 buster": "1:7.9p1-10+deb10u5"}
description: "machine-in-the-middle"
date: 2025-02-20T19:42:59+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2025-26465

---

The Qualys Threat Research Unit (TRU) discovered that the OpenSSH client
is vulnerable to a machine-in-the-middle attack if the VerifyHostKeyDNS
option is enabled (disabled by default).

The client side in OpenSSH 5.7 through 8.4 has an Observable Discrepancy
leading to an information leak in the algorithm negotiation. This allows
man-in-the-middle attackers to target initial connection attempts (where
no host key for the server has been cached by the client). This issue was
assigned CVE-2020-14145. Completely removing this information leak would
cause other problems, but this update includes a partial mitigation by
preferring the default ordering if the user has a key that matches the
best-preference default algorithm.

In addition, the stretch update contains a regression introduced with the
fix for CVE-2023-48795, which could cause segmentation faults under some
circumstances.
