---
title: "ELA-463-1 linux-4.9 security update"
package: linux-4.9
version: 4.9.272-2~deb8u1
distribution: "Debian 8 jessie"
description: "linux kernel update"
date: 2021-07-27T11:02:59+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3609
  - CVE-2021-21781
  - CVE-2021-33909
  - CVE-2021-34693

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2021-3609

    Norbert Slusarek reported a race condition vulnerability in the CAN
    BCM networking protocol, allowing a local attacker to escalate
    privileges.

CVE-2021-21781

    "Lilith >_>" of Cisco Talos discovered that the Arm initialisation
    code does not fully initialise the "sigpage" that is mapped into
    user-space processes to support signal handling.  This could
    result in leaking sensitive information, particularly when the
    system is rebooted.

CVE-2021-33909

    The Qualys Research Labs discovered a size_t-to-int conversion
    vulnerability in the Linux kernel's filesystem layer. An
    unprivileged local attacker able to create, mount, and then delete a
    deep directory structure whose total path length exceeds 1GB, can
    take advantage of this flaw for privilege escalation.

    Details can be found in the Qualys advisory at
    https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt

CVE-2021-34693

    Norbert Slusarek discovered an information leak in the CAN BCM
    networking protocol. A local attacker can take advantage of this
    flaw to obtain sensitive information from kernel stack memory.
