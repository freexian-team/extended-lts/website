---
title: "ELA-1000-1 ceph security update"
package: ceph
version: 0.80.7-2+deb8u6 (jessie), 10.2.11-2+deb9u2 (stretch)
version_map: {"8 jessie": "0.80.7-2+deb8u6", "9 stretch": "10.2.11-2+deb9u2"}
description: "unprivileged user access"
date: 2023-11-16T12:00:58Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-43040

---

A flaw was found in Ceph RGW component. An unprivileged user can write to any bucket(s) accessible by a given key if a POST's form-data contains a key called "bucket" with a value matching the name of the bucket used to sign the request. The result of this is that a user could actually upload to any bucket accessible by the specified access key as long as the bucket in the POST policy matches the bucket in said POST form part.
