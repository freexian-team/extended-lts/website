---
title: "ELA-705-1 qemu security update"
package: qemu
version: 1:2.1+dfsg-12+deb8u23 (jessie), 1:2.8+dfsg-6+deb9u18 (stretch)
version_map: {"8 jessie": "1:2.1+dfsg-12+deb8u23", "9 stretch": "1:2.8+dfsg-6+deb9u18"}
description: "multiple vulnerabilities"
date: 2022-10-17T16:11:01+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-35504
  - CVE-2020-35505
  - CVE-2021-3507
  - CVE-2021-4206
  - CVE-2021-4207
  - CVE-2022-0216

---

Multiple vulnerabilities were found in QEMU, a fast processor
emulator, which could result in denial of service or the execution of
arbitrary code.

In addition, the jessie package addresses CVE-2021-3930, a denial of service
vulnerability in the SCSI device emulation.
