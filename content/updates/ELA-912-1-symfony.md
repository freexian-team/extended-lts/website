---
title: "ELA-912-1 symfony security update"
package: symfony
version: 2.8.7+dfsg-1.3+deb9u4 (stretch)
version_map: {"9 stretch": "2.8.7+dfsg-1.3+deb9u4"}
description: "multiple issues"
date: 2023-08-02T18:25:20+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-14774
  - CVE-2021-21424
  - CVE-2022-24894
  - CVE-2022-24895

---
Multiple security vulnerabilities were found in symfony, a PHP framework for
web and console applications and a set of reusable PHP components, which could
lead to information disclosure or impersonation.


CVE-2018-14774

    When using HttpCache, the values of the X-Forwarded-Host headers are implicitly
    and wrongly set as trusted, leading to potential host header injection.

CVE-2021-21424

    James Isaac, Mathias Brodala and Laurent Minguet discovered that it was
    possible to enumerate users without relevant permissions due to different
    exception messages depending on whether the user existed or not. It was also
    possible to enumerate users by using a timing attack, by comparing time
    elapsed when authenticating an existing user and authenticating a
    non-existing user.

    403s are now returned whether the user exists or not if a user cannot
    switch to a user or if the user does not exist.

CVE-2022-24894

    Soner Sayakci discovered that when the Symfony HTTP cache system is
    enabled, the response header might be stored with a Set-Cookie header and
    returned to some other clients, thereby allowing an attacker to retrieve the
    victim's session.

    The HttpStore constructor now takes a parameter containing a list of
    private headers that are removed from the HTTP response headers. The default
    value for this parameter is Set-Cookie, but it can be overridden or extended
    by the application.

CVE-2022-24895

    Marco Squarcina discovered that CSRF tokens weren't cleared upon login,
    which could enable same-site attackers to bypass the CSRF protection
    mechanism by performing an attack similar to a session-fixation.

