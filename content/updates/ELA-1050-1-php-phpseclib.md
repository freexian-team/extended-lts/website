---
title: "ELA-1050-1 php-phpseclib security update"
package: php-phpseclib
version: 2.0.30-2~deb9u1 (stretch)
version_map: {"9 stretch": "2.0.30-2~deb9u1"}
description: "terrapin attack"
date: 2024-02-29T21:27:27Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-48795

---


The Terrapin attack is a cryptographic attack on the SSH prootocol reducing the security of SSH, by using a downgrade attack via man-in-the-middle interception.
By carefully adjusting the sequence numbers during the handshake, an attacker can remove an arbitrary amount of messages sent by the client or server at the beginning of the secure channel without the client or server noticing it.
