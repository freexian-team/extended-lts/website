---
title: "ELA-1017-1 opendkim security update"
package: opendkim
version: 2.11.0~alpha-10+deb9u2 (stretch)
version_map: {"9 stretch": "2.11.0~alpha-10+deb9u2"}
description: "information confusion"
date: 2023-12-01T19:48:43+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-48521

---

CVE-2022-48521:
   An issue was discovered in OpenDKIM through 2.10.3, and 2.11.x through 2.11.0-Beta2. It fails to keep track of ordinal numbers when removing fake Authentication-Results header fields, which allows a remote attacker to craft an e-mail message with a fake sender address such that programs that rely on Authentication-Results from OpenDKIM will treat the message as having a valid DKIM signature when in fact it has none.
