---
title: "ELA-842-1 tzdata new timezone database"
package: tzdata
version: 2021a-0+deb8u10 (jessie), 2021a-0+deb9u10 (stretch)
version_map: {"8 jessie": "2021a-0+deb8u10", "9 stretch": "2021a-0+deb9u10"}
description: "updated timezone database"
date: 2023-05-02T15:19:09+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update includes the changes in tzdata 2023c. Notable
changes are:

- Revert Lebanon DST changes.
- Updated leap second list.
