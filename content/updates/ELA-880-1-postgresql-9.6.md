---
title: "ELA-880-1 postgresql-9.6 security update"
package: postgresql-9.6
version: 9.6.24-0+deb9u4 (stretch)
version_map: {"9 stretch": "9.6.24-0+deb9u4"}
description: "multiple vulnerabilities"
date: 2023-06-28T21:14:33Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-2454
  - CVE-2023-2455

---

CVE-2023-2454:

	schema_element defeats protective search_path changes; It was found that certain database calls in PostgreSQL could permit an attacker with elevated database-level privileges to execute arbitrary code.
	
CVE-2023-2455:

	Row security policies disregard user ID changes after inlining; PostgreSQL could permit incorrect policies to be applied in certain cases where role-specific policies are used and a given query is planned under one role and then executed under other roles.

