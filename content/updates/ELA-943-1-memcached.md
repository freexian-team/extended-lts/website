---
title: "ELA-943-1 memcached security update"
package: memcached
version: 1.4.21-1.1+deb8u4 (jessie), 1.4.33-1+deb9u2 (stretch)
version_map: {"8 jessie": "1.4.21-1.1+deb8u4", "9 stretch": "1.4.33-1+deb9u2"}
description: "denial of service vulnerability"
date: 2023-09-07T11:46:12-07:00
draft: false
cvelist:
  - CVE-2022-48571
---

It was discovered that there was a potential Denial of Service (DoS)
vulnerability in `memcached`, a high-performance in-memory object caching
system.

A crash could have occurred when handling "multi-packet" uploads in UDP mode.
Deployments of memcached that only use TCP are likely unaffected by this issue.
