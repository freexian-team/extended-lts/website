---
title: "ELA-573-1 cyrus-sasl2 security update"
package: cyrus-sasl2
version: 2.1.26.dfsg1-13+deb8u3
distribution: "Debian 8 jessie"
description: "privilege escalation"
date: 2022-03-06T18:11:21+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-24407

---

It was discovered that the SQL plugin in cyrus-sasl2, a library implementing
the Simple Authentication and Security Layer, is prone to a SQL injection attack.
An authenticated remote attacker can take advantage of this flaw to execute
arbitrary SQL commands and for privilege escalation.
