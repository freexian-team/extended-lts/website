---
title: "ELA-1034-1 linux-4.19 security update"
package: linux-4.19
version: 4.19.304-1~deb8u1 (jessie), 4.19.304-1~deb9u1 (stretch)
version_map: {"8 jessie": "4.19.304-1~deb8u1", "9 stretch": "4.19.304-1~deb9u1"}
description: "linux kernel update"
date: 2024-01-25T11:45:33+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-44879
  - CVE-2023-0590
  - CVE-2023-1077
  - CVE-2023-1206
  - CVE-2023-1989
  - CVE-2023-3212
  - CVE-2023-3390
  - CVE-2023-3609
  - CVE-2023-3611
  - CVE-2023-3772
  - CVE-2023-3776
  - CVE-2023-4206
  - CVE-2023-4207
  - CVE-2023-4208
  - CVE-2023-4244
  - CVE-2023-4622
  - CVE-2023-4623
  - CVE-2023-4921
  - CVE-2023-5717
  - CVE-2023-6606
  - CVE-2023-6931
  - CVE-2023-6932
  - CVE-2023-25775
  - CVE-2023-34319
  - CVE-2023-34324
  - CVE-2023-35001
  - CVE-2023-39189
  - CVE-2023-39192
  - CVE-2023-39193
  - CVE-2023-39194
  - CVE-2023-40283
  - CVE-2023-42753
  - CVE-2023-42754
  - CVE-2023-42755
  - CVE-2023-45863
  - CVE-2023-45871
  - CVE-2023-51780
  - CVE-2023-51781
  - CVE-2023-51782

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2021-44879

    Wenqing Liu reported a NULL pointer dereference in the f2fs
    implementation. An attacker able to mount a specially crafted image
    can take advantage of this flaw for denial of service.

CVE-2023-0590

    Dmitry Vyukov discovered a race condition in the network scheduler
    core that that can lead to a use-after-free.  A local user with
    the CAP_NET_ADMIN capability in any user or network namespace
    could exploit this to cause a denial of service (crash or memory
    corruption) or possibly for privilege escalation.

CVE-2023-1077

    Pietro Borrello reported a type confusion flaw in the task
    scheduler.  A local user might be able to exploit this to cause a
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.

CVE-2023-1206

    It was discovered that the networking stack permits attackers to
    force hash collisions in the IPv6 connection lookup table, which
    may result in denial of service (significant increase in the cost
    of lookups, increased CPU utilization).

CVE-2023-1989

    Zheng Wang reported a race condition in the btsdio Bluetooth
    adapter driver that can lead to a use-after-free.  An attacker
    able to insert and remove SDIO devices can use this to cause a
    denial of service (crash or memory corruption) or possibly to run
    arbitrary code in the kernel.

CVE-2023-3212

    Yang Lan discovered that missing validation in the GFS2 filesystem
    could result in denial of service via a NULL pointer dereference
    when mounting a malformed GFS2 filesystem.

CVE-2023-3390

    A use-after-free flaw in the netfilter subsystem caused by
    incorrect error path handling may result in denial of service or
    privilege escalation.

CVE-2023-3609, CVE-2023-3776, CVE-2023-4206, CVE-2023-4207, CVE-2023-4208

    It was discovered that a use-after-free in the cls_fw, cls_u32,
    cls_route and network classifiers may result in denial of service
    or potential local privilege escalation.

CVE-2023-3611

    It was discovered that an out-of-bounds write in the traffic
    control subsystem for the Quick Fair Queueing scheduler (QFQ) may
    result in denial of service or privilege escalation.

CVE-2023-3772

    Lin Ma discovered a NULL pointer dereference flaw in the XFRM
    subsystem which may result in denial of service.

CVE-2023-4244

    A race condition was found in the nftables subsystem that could
    lead to a use-after-free.  A local user could exploit this to
    cause a denial of service (crash), information leak, or possibly
    for privilege escalation.

CVE-2023-4622

    Bing-Jhong Billy Jheng discovered a use-after-free within the Unix
    domain sockets component, which may result in local privilege
    escalation.

CVE-2023-4623

    Budimir Markovic reported a missing configuration check in the
    sch_hfsc network scheduler that could lead to a use-after-free or
    other problems.  A local user with the CAP_NET_ADMIN capability in
    any user or network namespace could exploit this to cause a denial
    of service (crash or memory corruption) or possibly for privilege
    escalation.

CVE-2023-4921

    "valis" reported flaws in the sch_qfq network scheduler that could
    lead to a use-after-free.  A local user with the CAP_NET_ADMIN
    capability in any user or network namespace could exploit this to
    cause a denial of service (crash or memory corruption) or possibly
    for privilege escalation.

CVE-2023-5717

    Budimir Markovic reported a heap out-of-bounds write vulnerability
    in the Linux kernel's Performance Events system caused by improper
    handling of event groups, which may result in denial of service or
    privilege escalation. The default settings in Debian prevent
    exploitation unless more permissive settings have been applied in
    the kernel.perf_event_paranoid sysctl.

CVE-2023-6606

    "j51569436" reported a potential out-of-bounds read in the CIFS
    filesystem implementation.  If a CIFS filesystem is mounted from a
    malicious server, the server could possibly exploit this to cause
    a denial of service (crash).

CVE-2023-6931

    Budimir Markovic reported a heap out-of-bounds write vulnerability
    in the Linux kernel's Performance Events system which may result in
    denial of service or privilege escalation. The default settings in
    Debian prevent exploitation unless more permissive settings have
    been applied in the kernel.perf_event_paranoid sysctl.

CVE-2023-6932

    A use-after-free vulnerability in the IPv4 IGMP implementation may
    result in denial of service or privilege escalation.

CVE-2023-25775

    Ivan D Barrera, Christopher Bednarz, Mustafa Ismail and Shiraz
    Saleem discovered that improper access control in the Intel Ethernet
    Controller RDMA driver may result in privilege escalation.

CVE-2023-34319

    Ross Lagerwall discovered a buffer overrun in Xen's netback driver
    which may allow a Xen guest to cause denial of service to the
    virtualisation host by sending malformed packets.

CVE-2023-34324

    Marek Marczykowski-Gorecki reported a possible deadlock in the Xen
    guests event channel code which may allow a malicious guest
    administrator to cause a denial of service.

CVE-2023-35001

    Tanguy DUBROCA discovered an out-of-bounds reads and write flaw in
    the Netfilter nf_tables implementation when processing an
    nft_byteorder expression, which may result in local privilege
    escalation for a user with the CAP_NET_ADMIN capability in any
    user or network namespace.

CVE-2023-39189, CVE-2023-39192, CVE-2023-39193

    Lucas Leong of Trend Micro Zero Day Initiative reported missing
    bounds checks in the nfnetlink_osf, xt_u32, and xt_sctp netfilter
    modules.  A local user with the CAP_NET_ADMIN capability in any
    user or network namespace could exploit these to leak sensitive
    information from the kernel or for denial of service (crash).

CVE-2023-39194

    Lucas Leong of Trend Micro Zero Day Initiative reported a missing
    bounds check in the xfrm (IPsec) subsystem.  A local user with the
    CAP_NET_ADMIN capability in any user or network namespace could
    exploit this to leak sensitive information from the kernel or for
    denial of service (crash).

CVE-2023-40283

    A use-after-free was discovered in Bluetooth L2CAP socket
    handling.

CVE-2023-42753

    Kyle Zeng discovered an off-by-one error in the netfilter ipset
    subsystem which could lead to out-of-bounds memory access.  A
    local user with the CAP_NET_ADMIN capability in any user or
    network namespace could exploit this to cause a denial of service
    (memory corruption or crash) and possibly for privilege
    escalation.

CVE-2023-42754

    Kyle Zeng discovered a flaw in the IPv4 implementation which could
    lead to a null pointer deference.  A local user could exploit this
    for denial of service (crash).

CVE-2023-42755

    Kyle Zeng discovered missing configuration validation in the
    cls_rsvp network classifier which could lead to out-of-bounds
    reads.  A local user with the CAP_NET_ADMIN capability in any user
    or network namespace could exploit this to cause a denial of
    service (crash) or to leak sensitive information.

    This flaw has been mitigated by removing the cls_rsvp classifier.

CVE-2023-45863

    A race condition in library routines for handling generic kernel
    objects may result in an out-of-bounds write in the
    fill_kobj_path() function.

CVE-2023-45871

    Manfred Rudigier reported a flaw in the igb network driver for
    Intel Gigabit Ethernet interfaces.  When the "rx-all" feature was
    enabled on such a network interface, an attacker on the same
    network segment could send packets that would overflow a receive
    buffer, leading to a denial of service (crash or memory
    corruption) or possibly remote code execution.

CVE-2023-51780

    It was discovered that a race condition in the ATM (Asynchronous
    Transfer Mode) subsystem may lead to a use-after-free.

CVE-2023-51781

    It was discovered that a race condition in the Appletalk subsystem
    may lead to a use-after-free.

CVE-2023-51782

    It was discovered that a race condition in the Amateur Radio X.25
    PLP (Rose) support may lead to a use-after-free. This module is not
    auto-loaded on Debian systems, so this issue only affects systems
    where it is explicitly loaded.

This update additionally includes many more bug fixes
from stable updates 4.19.290-4.19.304 inclusive.
