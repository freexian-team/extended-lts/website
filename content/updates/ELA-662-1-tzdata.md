---
title: "ELA-662-1 tzdata new timezone database"
package: tzdata
version: 2021a-0+deb8u5 (jessie), 2021a-0+deb9u5 (stretch)
version_map: {"8 jessie": "2021a-0+deb8u5", "9 stretch": "2021a-0+deb9u5"}
description: "new timezone database"
date: 2022-08-19T11:30:36+02:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2022b. Notable
changes are:

- Iran plans to stop observing DST permanently, after it falls back
  on 2022-09-21.
- Chile's 2022 DST start is delayed from September 4 to September 11.

Note that for jessie, the tzdata-java package which was built for Java 7
is no longer provided, as Java 7 is no longer supported.
