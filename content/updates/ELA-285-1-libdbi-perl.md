---
title: "ELA-285-1 libdbi-perl security update"
package: libdbi-perl
version: 1.631-3+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2020-09-28T14:54:27+02:00
draft: false
type: updates
cvelist:
  - CVE-2013-7490
  - CVE-2014-10401
  - CVE-2019-20919
  - CVE-2020-14392
  - CVE-2020-14393

---

Several vulnerabilities were discovered in the Perl5 Database
Interface (DBI). An attacker could trigger a denial-of-service (DoS),
information disclosure and possibly execute arbitrary code.

* CVE-2013-7490

    Using many arguments to methods for Callbacks may lead to memory
    corruption.

* CVE-2014-10401

    DBD::File drivers can open files from folders other than those
    specifically passed via the f_dir attribute.

* CVE-2019-20919

    The hv_fetch() documentation requires checking for NULL and the
    code does that. But, shortly thereafter, it calls SvOK(profile),
    causing a NULL pointer dereference.

* CVE-2020-14392

    An untrusted pointer dereference flaw was found in Perl-DBI. A
    local attacker who is able to manipulate calls to
    dbd_db_login6_sv() could cause memory corruption, affecting the
    service's availability.

* CVE-2020-14393

    A buffer overflow on via an overlong DBD class name in
    dbih_setup_handle function may lead to data be written past the
    intended limit.
