---
title: "ELA-188-1 djvulibre security update"
package: djvulibre
version: 3.5.25.3-1+deb7u2
distribution: "Debian 7 Wheezy"
description: "NULL pointer dereference"
date: 2019-11-10T19:26:04+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-18804

---

It was discovered that there was a NULL pointer dereference issue in the IW44
encoder/decoder within DjVu, a set of compression technologies, a file format,
and a software platform for the delivery over the Web of digital documents.
