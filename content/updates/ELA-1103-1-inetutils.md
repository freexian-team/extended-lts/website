---
title: "ELA-1103-1 inetutils security update"
package: inetutils
version: 2:1.9.4-2+deb9u3 (stretch)
version_map: {"9 stretch": "2:1.9.4-2+deb9u3"}
description: "multiple vulnerabilities"
date: 2024-05-31T17:45:03+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-0053
  - CVE-2023-40303

---

Two vulnerabilities were fixed in inetutils,
the GNU network utilities.

CVE-2019-0053

    Insufficient validation of environment variables in telnet

CVE-2023-40303

    Possible privilege escalation in ftpd, rcp, rlogin, rsh, rshd, and uucpd when a set*id() family function like setuid() fails
