---
title: "ELA-565-1 zsh security update"
package: zsh
version: 5.0.7-5+deb8u4
distribution: "Debian 8 jessie"
description: "arbitrary code execution"
date: 2022-02-18T09:35:03+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-45444

---

It was discovered that zsh, a powerful shell and scripting language,
did not prevent recursive prompt expansion. This would allow an
attacker to execute arbitrary commands into a user's shell, for
instance by tricking a vcs_info user into checking out a git branch
with a specially crafted name.
