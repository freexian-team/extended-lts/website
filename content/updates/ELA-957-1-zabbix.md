---
title: "ELA-957-1 zabbix security update"
package: zabbix
version: 1:2.2.23+dfsg-0+deb8u5 (jessie)
version_map: {"8 jessie": "1:2.2.23+dfsg-0+deb8u5"}
description: "multiple issues"
date: 2023-09-23T19:13:57+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2013-7484
  - CVE-2019-17382
  - CVE-2022-43515
  - CVE-2023-29450
  - CVE-2023-29451
  - CVE-2023-29454
  - CVE-2023-29455
  - CVE-2023-29456
  - CVE-2023-29457

---

Several security vulnerabilities have been discovered in zabbix, a network
monitoring solution, potentially allowing to crash the server, information
disclosure or Cross-Site-Scripting attacks.

Important Notices:
To mitigate CVE-2019-17382, on existing installations, the guest account
needs to be manually disabled, for example by disabling the the "Guest
group" in the UI:
   Administration -> User groups -> Guests -> Untick Enabled


CVE-2013-7484

    Zabbix before version 4.4.0alpha2 stores credentials in the "users"
    table with the password hash stored as a MD5 hash, which is a known
    insecure hashing method. Furthermore, no salt is used with the hash.

CVE-2019-17382 (Disputed, not considered by upstream to be a security issue)

    An issue was discovered in
    zabbix.php?action=dashboard.view&dashboardid=1 in Zabbix through
    4.4. An attacker can bypass the login page and access the dashboard
    page, and then create a Dashboard, Report, Screen, or Map without
    any Username/Password (i.e., anonymously). All created elements
    (Dashboard/Report/Screen/Map) are accessible by other users and by
    an admin.

CVE-2022-43515

    Zabbix Frontend provides a feature that allows admins to
    maintain the installation and ensure that only certain IP addresses
    can access it. In this way, any user will not be able to access the
    Zabbix Frontend while it is being maintained and possible sensitive
    data will be prevented from being disclosed. An attacker can bypass
    this protection and access the instance using IP address not listed
    in the defined range.

CVE-2023-29450

    JavaScript pre-processing can be used by the attacker to gain
    access to the file system (read-only access on behalf of user
    "zabbix") on the Zabbix Server or Zabbix Proxy, potentially leading
    to unauthorized access to sensitive data.

CVE-2023-29451

    Specially crafted string can cause a buffer overrun in the JSON
    parser library leading to a crash of the Zabbix Server or a Zabbix
    Proxy.

CVE-2023-29454

    A Stored or persistent cross-site scripting (XSS) vulnerability
    was found on “Users” section in “Media” tab in “Send to” form field.
    When new media is created with malicious code included into field
    “Send to” then it will execute when editing the same media.

Note: This issue was accidentially not mentioned in the debian changelog.

CVE-2023-29455

    A Reflected XSS attacks, also known as non-persistent attacks, was
    found where an attacker can pass malicious code as GET request to
    graph.php and system will save it and will execute when current
    graph page is opened.

CVE-2023-29456

    URL validation scheme receives input from a user and then parses
    it to identify its various components. The validation scheme can
    ensure that all URL components comply with internet standards.

CVE-2023-29457

    A Reflected XSS attacks, also known as non-persistent attacks, was
    found where XSS session cookies could be revealed, enabling a
    perpetrator to impersonate valid users and abuse their private
    accounts.
