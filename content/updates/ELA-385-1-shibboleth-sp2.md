---
title: "ELA-385-1 shibboleth-sp2 security update"
package: shibboleth-sp2
version: 2.5.3+dfsg-2+deb8u2
distribution: "Debian 8 jessie"
description: "phishing vulnerability"
date: 2021-03-20T08:57:20+05:30
draft: false
type: updates
cvelist:

---

Toni Huttunen discovered that the Shibboleth service provider's template
engine used to render error pages could be abused for phishing attacks.

For additional information please refer to the upstream advisory at
https://shibboleth.net/community/advisories/secadv_20210317.txt
