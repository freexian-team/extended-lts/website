---
title: "ELA-805-1 libgit2 security update"
package: libgit2
version: 0.25.1+really0.24.6-1+deb9u2 (stretch)
version_map: {"9 stretch": "0.25.1+really0.24.6-1+deb9u2"}
description: "improper verification of cryptographic signature"
date: 2023-02-21T21:40:14+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-22742

---

A vulnerability have been found in libgit2, a cross-platform, linkable
library implementation of Git.

Previous versions of libgit's SSH backend did by default not perform
certificate checking if the caller did not explicitly provide a
certificate check callback and so may be subjected to a
man-in-the-middle attack.



