---
title: "ELA-1179-1 firmware-nonfree security update"
package: firmware-nonfree
version: 20190114+really20220913-0+deb8u3 (jessie), 20190114+really20220913-0+deb9u3 (stretch), 20190114+really20220913-0+deb10u3 (buster)
version_map: {"8 jessie": "20190114+really20220913-0+deb8u3", "9 stretch": "20190114+really20220913-0+deb9u3", "10 buster": "20190114+really20220913-0+deb10u3"}
description: "multiple vulnerabilities"
date: 2024-09-14T09:34:31+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-35061
  - CVE-2023-38417
  - CVE-2023-47210

---

Intel® has released two advisories about potential security vulnerabilities in some Intel® PROSet/Wireless WiFi, Bluetooth® and Killer™ WiFi products may allow information disclosurre or denial of service. The full advisories are available at [1] and [2].

[1] https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00947.html
[2] https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-01039.html

This updated firmware-nonfree package includes the following firmware files:

      intel/ibt-0041-0041.sfi
      intel/ibt-17-16-1.sfi
      intel/ibt-17-2.sfi
      intel/ibt-18-16-1.sfi
      intel/ibt-18-2.sfi
      intel/ibt-19-0-0.sfi
      intel/ibt-19-0-1.sfi
      intel/ibt-19-0-4.sfi
      intel/ibt-19-16-4.sfi
      intel/ibt-19-240-1.sfi
      intel/ibt-19-240-4.sfi
      intel/ibt-19-32-0.sfi
      intel/ibt-19-32-1.sfi
      intel/ibt-19-32-4.sfi
      intel/ibt-20-0-3.sfi
      intel/ibt-20-1-3.sfi
      intel/ibt-20-1-4.sfi
      iwlwifi-Qu-b0-hr-b0-77.ucode
      iwlwifi-Qu-b0-jf-b0-77.ucode
      iwlwifi-Qu-c0-hr-b0-77.ucode
      iwlwifi-Qu-c0-jf-b0-77.ucode
      iwlwifi-QuZ-a0-hr-b0-77.ucode
      iwlwifi-QuZ-a0-jf-b0-77.ucode
      iwlwifi-cc-a0-77.ucode
      iwlwifi-so-a0-gf-a0-84.ucode
      iwlwifi-so-a0-gf-a0-86.ucode
      iwlwifi-so-a0-gf-a0.pnvm
      iwlwifi-so-a0-gf4-a0-84.ucode
      iwlwifi-so-a0-gf4-a0-86.ucode
      iwlwifi-so-a0-gf4-a0.pnvm
      iwlwifi-so-a0-hr-b0-83.ucode
      iwlwifi-so-a0-hr-b0-84.ucode
      iwlwifi-so-a0-hr-b0-86.ucode
      iwlwifi-ty-a0-gf-a0-84.ucode
      iwlwifi-ty-a0-gf-a0-86.ucode
      iwlwifi-ty-a0-gf-a0.pnvm

The updated firmware files might need updated kernel to work and as old firmware versions might loaded
on older kernels, it is encouraged to verify whether the kernel loaded the updated firmware file and take
additional measures if needed.

CVE-2023-35061

    Improper initialization for some Intel® PROSet/Wireless and Intel® Killer™ Wi-Fi software before version 22.240 may allow an unauthenticated user to potentially enable information disclosure via adjacent access.

CVE-2023-38417

    Improper input validation for some Intel® PROSet/Wireless WiFi software before version 23.20 may allow an unauthenticated user to potentially enable denial of service via adjacent access.

CVE-2023-47210

    Improper input validation for some Intel® PROSet/Wireless WiFi software for linux before version 23.20 may allow an unauthenticated user to potentially enable denial of service via adjacent access.
