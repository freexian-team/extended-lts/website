---
title: "ELA-1104-1 nghttp2 security update"
package: nghttp2
version: 1.18.1-1+deb9u4 (stretch)
version_map: {"9 stretch": "1.18.1-1+deb9u4"}
description: "denial of service"
date: 2024-06-01T01:02:24+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-28182

---

An issue has been found in nghttp2, a library, server, proxy and client implementing HTTP/2.
An implementation using the nghttp2 library will continue to receive CONTINUATION frames,
and will not callback to the application to allow visibility into this information before
it resets the stream, resulting in Denial of Service.

