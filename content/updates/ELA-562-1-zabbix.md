---
title: "ELA-562-1 zabbix security update"
package: zabbix
version: 1:2.2.23+dfsg-0+deb8u3
distribution: "Debian 8 jessie"
description: "configuration tampering"
date: 2022-02-07T22:49:25+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-23134

---

Thomas Chauchefoin from SonarSource discovered that in Zabbix, a
server/client network monitoring system, after the initial setup
process, some steps of setup.php file are reachable not only by
super-administrators, but by unauthenticated users as well. An
attacker could bypass checks and potentially change the configuration
of Zabbix Frontend.
