---
title: "ELA-1297-1 redis security update"
package: redis
version: 2:2.8.17-1+deb8u14 (jessie), 3:3.2.6-3+deb9u14 (stretch), 5:5.0.14-1+deb10u7 (buster)
version_map: {"8 jessie": "2:2.8.17-1+deb8u14", "9 stretch": "3:3.2.6-3+deb9u14", "10 buster": "5:5.0.14-1+deb10u7"}
description: "LUA garbage collector code execution"
date: 2025-01-20T17:02:28+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-46981

---

Possible code execution with Lua scripting due to a missing call to the
garbage collector has been fixed in the key–value database Redis.
