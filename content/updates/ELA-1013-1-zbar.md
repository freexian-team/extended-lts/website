---
title: "ELA-1013-1 zbar security update"
package: zbar
version: 0.10+doc-10.1+deb9u1 (stretch)
version_map: {"9 stretch": "0.10+doc-10.1+deb9u1"}
description: "heap overflow"
date: 2023-11-30T16:35:19Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-40889

---

Zbar, a barcode scanner application, was vulnerable. A heap-based buffer overflow existed
in the qr_reader_match_centers function.
Specially crafted QR codes may lead to information disclosure
and/or arbitrary code execution. To trigger this
vulnerability, an attacker can digitally input the
malicious QR code, or prepare it to be physically scanned
by the vulnerable scanner.
