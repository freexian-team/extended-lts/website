---
title: "ELA-802-1 nss security update"
package: nss
version: 2:3.26-1+debu8u17 (jessie), 2:3.26.2-1.1+deb9u6 (stretch)
version_map: {"8 jessie": "2:3.26-1+debu8u17", "9 stretch": "2:3.26.2-1.1+deb9u6"}
description: "execution of arbitrary code"
date: 2023-02-21T01:15:06+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-0767

---

Christian Holler discovered that incorrect handling of PKCS 12 Safe Bag
attributes in nss, the Mozilla Network Security Service library, may result in
execution of arbitrary code if a specially crafted PKCS 12 certificate bundle
is processed.
