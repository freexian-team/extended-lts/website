---
title: "ELA-721-1 libxml2 security update"
package: libxml2
version: 2.9.1+dfsg1-5+deb8u14 (jessie), 2.9.4+dfsg1-2.2+deb9u9 (stretch)
version_map: {"8 jessie": "2.9.1+dfsg1-5+deb8u14", "9 stretch": "2.9.4+dfsg1-2.2+deb9u9"}
description: "integer overflows and memory corruption"
date: 2022-10-30T16:59:12+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-40303
  - CVE-2022-40304

---

It was discovered that libxml2, the GNOME XML library, was vulnerable to
integer overflows and memory corruption.

CVE-2022-40303

     Parsing a XML document with the XML_PARSE_HUGE option enabled can result
     in an integer overflow because safety checks were missing in some
     functions. Also, the xmlParseEntityValue function did not have any length
     limitation.

CVE-2022-40304

     When a reference cycle is detected in the XML entity cleanup function the
     XML entity data can be stored in a dictionary. In this case, the
     dictionary becomes corrupted resulting in logic errors, including memory
     errors like double free.
