---
title: "ELA-830-1 tomcat8 security update"
package: tomcat8
version: 8.0.14-1+deb8u25 (jessie), 8.5.54-0+deb9u10 (stretch)
version_map: {"8 jessie": "8.0.14-1+deb8u25", "9 stretch": "8.5.54-0+deb9u10"}
description: "information disclosure"
date: 2023-04-10T17:35:18+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-28708

---

A flaw has been found in the Tomcat servlet and JSP engine. When using the
RemoteIpFilter with requests received from a reverse proxy via HTTP that
include the X-Forwarded-Proto header set to https, session cookies created by
Apache Tomcat did not include the secure attribute. This could result in the
user agent transmitting the session cookie over an insecure channel.
