---
title: "ELA-1290-1 rsync security update"
package: rsync
version: 3.1.1-3+deb8u3 (jessie), 3.1.2-1+deb9u4 (stretch), 3.1.3-6+deb10u1 (buster)
version_map: {"8 jessie": "3.1.1-3+deb8u3", "9 stretch": "3.1.2-1+deb9u4", "10 buster": "3.1.3-6+deb10u1"}
description: "information leaks (files or memory)"
date: 2025-01-14T19:51:14+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-12085
  - CVE-2024-12086
  - CVE-2024-12087
  - CVE-2024-12088
  - CVE-2024-12747

---

Several vulnerabilities were discovered in rsync, a fast, versatile,
remote (and local) file-copying tool.

CVE-2024-12085

    Simon Scannell, Pedro Gallegos and Jasiel Spelman reported a flaw in
    the way rsync compares file checksums, allowing a remote attacker to
    trigger an information leak.

CVE-2024-12086

    Simon Scannell, Pedro Gallegos and Jasiel Spelman discovered a flaw
    which would result in a server leaking contents of an arbitrary file
    from the client's machine.

CVE-2024-12087

    Simon Scannell, Pedro Gallegos and Jasiel Spelman reported a path
    traversal vulnerability in the rsync daemon affecting the
    --inc-recursive option, which could allow a server to write files
    outside of the client's intended destination directory.

CVE-2024-12088

    Simon Scannell, Pedro Gallegos and Jasiel Spelman reported that when
    using the --safe-links option, rsync fails to properly verify if a
    symbolic link destination contains another symbolic link with it,
    resulting in path traversal and arbitrary file write outside of the
    desired directory.

CVE-2024-12747

    Aleksei Gorban "loqpa" discovered a race condition when handling
    symbolic links resulting in an information leak which may enable
    escalation of privileges.

