---
title: "ELA-227-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u29
distribution: "Debian 7 Wheezy"
description: "fix for several out of bounds read/write"
date: 2020-04-30T16:23:37+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-18218
  - CVE-2020-7064
  - CVE-2020-7066
  - CVE-2020-7067

---

Four issues have been found in php5, a server-side, HTML-embedded scripting language.


CVE-2020-7064
     A one byte out-of-bounds read, which could potentially lead to
     information disclosure or crash.

CVE-2020-7066
     An URL containing zero (\0) character will be truncated at it, which
     may cause some software to make incorrect assumptions and possibly
     send some information to a wrong server.

CVE-2020-7067
     Using a malformed url-encoded string an Out-of-Bounds read can occur.

CVE-2019-18218
     Fix to restrict the number of CDF_VECTOR elements to prevent
     a heap-based buffer overflow (4-byte out-of-bounds write).
     (originally this CVE was filed against package "file" but
      php5 contains an embedded version of that package)
