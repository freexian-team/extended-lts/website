---
title: "ELA-1270-1 ntp security update"
package: ntp
version: 1:4.2.8p12+dfsg-4+deb10u1 (buster)
version_map: {"10 buster": "1:4.2.8p12+dfsg-4+deb10u1"}
description: "multiple vulnerabilities"
date: 2024-12-11T12:28:41+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-11868
  - CVE-2020-15025
  - CVE-2023-26555

---

Multiple vulnerabilities were discovered in ntp, a Network Time Protocol
daemon and set of utility programs.

### CVE-2020-11868

It was possible for an off-path attacker to block unauthenticated
synchronisation via a server mode packet with a spoofed source IP address.

### CVE-2020-15025

A remote attacker could cause a denial-of-service because of a memory leak in
situations where a CMAC key is used and associated with a CMAC algorithm in
the ntp.keys file.

### CVE-2023-26555

The clock driver for the Trimble Palisade GPS timing receiver contained an
out-of-bounds write, which could cause memory corruption or a crash.
