---
title: "ELA-438-1 ruby-nokogiri security update"
package: ruby-nokogiri
version: 1.6.3.1+ds-1+deb8u2
distribution: "Debian 8 jessie"
description: "XXE vulnerability"
date: 2021-06-01T00:18:53+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-26247

---

Nokogiri is a Rubygem providing HTML, XML, SAX, and Reader parsers with XPath
and CSS selector support. An XXE vulnerability was found in Nokogiri. XML
Schemas parsed by Nokogiri::XML::Schema were trusted by default, allowing
external resources to be accessed over the network, potentially enabling XXE or
SSRF attacks. The new default behavior is to treat all input as untrusted. See
also [upstream's security
advisory](https://github.com/sparklemotion/nokogiri/security/advisories/GHSA-vr8q-g5c7-m54m)
for more information how to mitigate the problem or to restore the old behavior
again.


