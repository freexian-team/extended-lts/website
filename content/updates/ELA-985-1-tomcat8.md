---
title: "ELA-985-1 tomcat8 security update"
package: tomcat8
version: 8.0.14-1+deb8u27 (jessie), 8.5.54-0+deb9u12 (stretch)
version_map: {"8 jessie": "8.0.14-1+deb8u27", "9 stretch": "8.5.54-0+deb9u12"}
description: "multiple issues"
date: 2023-10-16T00:14:14+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-42795
  - CVE-2023-45648
  - CVE-2023-44487

---

Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.

CVE-2023-42795

    Information Disclosure. When recycling various internal objects, including
    the request and the response, prior to re-use by the next request/response,
    an error could cause Tomcat to skip some parts of the recycling process
    leading to information leaking from the current request/response to the
    next.

CVE-2023-44487

    DoS caused by HTTP/2 frame overhead (Rapid Reset Attack).
    Only Tomcat 8 in Debian 9 "Stretch" was affected.

CVE-2023-45648

    Request smuggling. Tomcat did not correctly parse HTTP trailer headers. A
    specially crafted, invalid trailer header could cause Tomcat to treat a
    single request as multiple requests leading to the possibility of request
    smuggling when behind a reverse proxy.
