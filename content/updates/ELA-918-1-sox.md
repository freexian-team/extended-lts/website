---
title: "ELA-918-1 sox security update"
package: sox
version: 14.4.1-5+deb8u7 (jessie), 14.4.1-5+deb9u5 (stretch)
version_map: {"8 jessie": "14.4.1-5+deb8u7", "9 stretch": "14.4.1-5+deb9u5"}
description: "divide by zero"
date: 2023-08-13T15:47:40Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-32627

---

SoX is a command line utility that can convert various formats of computer audio files in to other formats. It can also apply various effects to these sound files during the conversion.

Sox was vulnerable to divide by zero vulnerability by reading an specialy crafted Creative Voice File (.voc) file, in the read_samples function. This flaw can lead to a denial of service.
