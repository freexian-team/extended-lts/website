---
title: "ELA-148-1 bind9 security update"
package: bind9
version: 1:9.8.4.dfsg.P1-6+nmu2+deb7u23
distribution: "Debian 7 Wheezy"
description: "issue with limits on simultaneous TCP connections"
date: 2019-07-27T12:12:16+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-5743

---

A vulnerability was found in the Bind DNS Server. Limits on simultaneous tcp connections have not been enforced correctly and could lead to exhaustion of file descriptors. In the worst case this could affect the file descriptors of the whole system. 
