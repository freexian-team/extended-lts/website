---
title: "ELA-567-2 apache2 regression update"
package: apache2
version: 2.4.10-10+deb8u21
distribution: "Debian 8 jessie"
description: "unknown symbol"
date: 2022-02-20T11:25:41+01:00
draft: false
type: updates
cvelist:

---

The patch for CVE-2021-44224 introduced an unknown symbol, which prevents apache2 from starting.
