---
title: "ELA-1302-1 postgresql-11 security update"
package: postgresql-11
version: 11.22-0+deb10u4 (buster)
version_map: {"10 buster": "11.22-0+deb10u4"}
description: "multiple vulnerabilities"
date: 2025-01-25T12:25:21-05:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-10976
  - CVE-2024-10977
  - CVE-2024-10978
  - CVE-2024-10979

---

Multiple security issues were discovered in PostgreSQL, which may result in
the execution of arbitrary code, privilege escalation, or log manipulation.

CVE-2024-10976

    Incomplete tracking in PostgreSQL of tables with row security allows a
    reused query to view or change different rows from those intended. It
    leads to potentially incorrect policies being applied in cases where
    role-specific policies are used and a given query is planned under one
    role and then executed under other roles.

CVE-2024-10977

    Client use of server error message in PostgreSQL allows a server not
    trusted under current SSL or GSS settings to furnish arbitrary non-NUL
    bytes to the libpq application. For example, a man-in-the-middle attacker
    could send a long error message that a human or screen-scraper user of
    psql mistakes for valid query results. 

CVE-2024-10978

    Incorrect privilege assignment in PostgreSQL allows a less-privileged
    application user to view or change different rows from those intended. An
    attack requires the application to use SET ROLE, SET SESSION
    AUTHORIZATION, or an equivalent feature.

CVE-2024-10979

    Incorrect control of environment variables in PostgreSQL PL/Perl allows
    an unprivileged database user to change sensitive process environment
    variables (e.g. PATH).
