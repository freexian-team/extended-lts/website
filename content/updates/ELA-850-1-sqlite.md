---
title: "ELA-850-1 sqlite security update"
package: sqlite
version: 2.8.17-12+deb8u1 (jessie), 2.8.17-14+deb9u1 (stretch)
version_map: {"8 jessie": "2.8.17-12+deb8u1", "9 stretch": "2.8.17-14+deb9u1"}
description: "multiple vulnerabilities"
date: 2023-05-13T13:08:51+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-6153
  - CVE-2018-8740

---

Two vulnerabilities have been fixed in sqlite (V2) which which might allow
local users to obtain sensitive information, cause a denial of service
(application crash), or have unspecified other impact.

CVE-2016-6153

    sqlite improperly implemented the temporary directory search algorithm, which
    might allow local users to obtain sensitive information, cause a denial of
    service (application crash), or have unspecified other impact by leveraging use
    of the current working directory for temporary files.

CVE-2018-8740

    Databases whose schema is corrupted using a CREATE TABLE AS statement could
    cause a NULL pointer dereference,

