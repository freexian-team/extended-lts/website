---
title: "ELA-833-1 ghostscript security update"
package: ghostscript
version: 9.26a~dfsg-0+deb8u11 (jessie), 9.26a~dfsg-0+deb9u11 (stretch)
version_map: {"8 jessie": "9.26a~dfsg-0+deb8u11", "9 stretch": "9.26a~dfsg-0+deb9u11"}
description: "buffer overflow"
date: 2023-04-18T01:14:56+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-28879

---

It was discovered that there was a potential buffer-overflow vulnerability in
ghostscript, a popular interpreter for the PostScript language used, for
example, to generate PDF files.

