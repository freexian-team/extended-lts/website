---
title: "ELA-183-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u25
distribution: "Debian 7 Wheezy"
description: "remote code execution"
date: 2019-10-26T16:53:50+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-11043

---

Emil Lerner, beched and d90pwn found a buffer underflow in php5-fpm, a
Fast Process Manager for the PHP language, which can lead to remote
code execution.

Instances are vulnerable depending on the web server configuration, in
particular PATH_INFO handling.  For a full list of preconditions,
check: https://github.com/neex/phuip-fpizdam
