---
title: "ELA-1037-1 squid3 security update"
package: squid3
version: 3.5.23-5+deb8u7 (jessie), 3.5.23-5+deb9u10 (stretch)
version_map: {"8 jessie": "3.5.23-5+deb8u7", "9 stretch": "3.5.23-5+deb9u10"}
description: "denial of service"
date: 2024-01-30T22:36:41+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-46847
  - CVE-2023-49285
  - CVE-2023-49286
  - CVE-2023-50269
  - CVE-2024-23638

---

Several security vulnerabilities have been discovered in Squid, a full featured
web proxy cache. Due to programming errors in Squid's HTTP request parsing,
remote attackers may be able to execute a denial of service attack by sending
large X-Forwarded-For header or trigger a stack buffer overflow while
performing HTTP Digest authentication. Other issues facilitate a denial of
service attack against Squid's Helper process management. In regard to
CVE-2023-46728: Please note that support for the Gopher protocol has simply
been removed in future Squid versions. There is no fix available. We recommend
to reject all gopher URL requests instead.
