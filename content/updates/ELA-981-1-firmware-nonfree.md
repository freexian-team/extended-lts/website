---
title: "ELA-981-1 firmware-nonfree security update"
package: firmware-nonfree
version: 20190114+really20220913-0+deb8u2 (jessie), 20190114+really20220913-0+deb9u2 (stretch)
version_map: {"8 jessie": "20190114+really20220913-0+deb8u2", "9 stretch": "20190114+really20220913-0+deb9u2"}
description: "multiple vulnerabilities"
date: 2023-10-08T13:04:23+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-27635
  - CVE-2022-36351
  - CVE-2022-38076
  - CVE-2022-40964
  - CVE-2022-46329

---

Intel® released the INTEL-SA-00766 advisory about potential security
vulnerabilities in some Intel® PROSet/Wireless WiFi and Killer™ WiFi products
may allow escalation of privilege or denial of service. The full advisory is
available at [1]

[1] https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00766.html

This updated firmware-nonfree package includes the following firmware files:
   - Intel Bluetooth AX2xx series:
     - ibt-0041-0041.sfi
     - ibt-19-0-0.sfi
     - ibt-19-0-1.sfi
     - ibt-19-0-4.sfi
     - ibt-19-16-4.sfi
     - ibt-19-240-1.sfi
     - ibt-19-240-4.sfi
     - ibt-19-32-0.sfi
     - ibt-19-32-1.sfi
     - ibt-19-32-4.sfi
     - ibt-20-0-3.sfi
     - ibt-20-1-3.sfi
     - ibt-20-1-4.sfi
   - Intel Wireless 22000 series
     - iwlwifi-Qu-b0-hr-b0-77.ucode
     - iwlwifi-Qu-b0-jf-b0-77.ucode
     - iwlwifi-Qu-c0-hr-b0-77.ucode
     - iwlwifi-Qu-c0-jf-b0-77.ucode
     - iwlwifi-QuZ-a0-hr-b0-77.ucode
     - iwlwifi-cc-a0-77.ucode

The updated firmware files might need updated kernel to work. It is encouraged
to verify whether the kernel loaded the updated firmware file and take
additional measures if needed.


CVE-2022-27635

    Improper access control for some Intel(R) PROSet/Wireless WiFi and Killer(TM)
    WiFi software may allow a privileged user to potentially enable escalation of
    privilege via local access.

CVE-2022-36351

    Improper input validation in some Intel(R) PROSet/Wireless WiFi and Killer(TM)
    WiFi software may allow an unauthenticated user to potentially enable denial of
    service via adjacent access.

CVE-2022-38076

    Improper input validation in some Intel(R) PROSet/Wireless WiFi and Killer(TM)
    WiFi software may allow an authenticated user to potentially enable escalation
    of privilege via local access.

CVE-2022-40964

    Improper access control for some Intel(R) PROSet/Wireless WiFi and Killer(TM)
    WiFi software may allow a privileged user to potentially enable escalation of
    privilege via local access.

CVE-2022-46329

    Protection mechanism failure for some Intel(R) PROSet/Wireless WiFi software
    may allow a privileged user to potentially enable escalation of privilege via
    local access.

