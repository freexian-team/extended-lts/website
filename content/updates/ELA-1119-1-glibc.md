---
title: "ELA-1119-1 glibc security update"
package: glibc
version: 2.19-18+deb8u14 (jessie), 2.24-11+deb9u7 (stretch)
version_map: {"8 jessie": "2.19-18+deb8u14", "9 stretch": "2.24-11+deb9u7"}
description: "nscd vulnerabilities"
date: 2024-06-30T23:53:41+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-33599
  - CVE-2024-33600
  - CVE-2024-33601
  - CVE-2024-33602

---

Multiple vulnerabilities have been fixed in the Name Service Cache Daemon
that is built by the GNU C library and shipped in the nscd binary package.

CVE-2024-33599

    nscd: Stack-based buffer overflow in netgroup cache

CVE-2024-33600

    nscd: Null pointer crashes after notfound response

CVE-2024-33601

    nscd: Daemon may terminate on memory allocation failure

CVE-2024-33602

    nscd: Possible memory corruption
