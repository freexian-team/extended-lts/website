---
title: "ELA-782-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u9 (jessie), 2:1.19.2-1+deb9u12 (stretch)
version_map: {"8 jessie": "2:1.16.4-1+deb8u9", "9 stretch": "2:1.19.2-1+deb9u12"}
description: "privilege escalation"
date: 2023-01-29T13:33:08+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-4283
  - CVE-2022-46340
  - CVE-2022-46341
  - CVE-2022-46342
  - CVE-2022-46343
  - CVE-2022-46344

---

Jan-Niklas Sohn discovered several vulnerabilities in X server extensions in the X.Org X server, which may result in privilege escalation if the X server is running privileged. 
