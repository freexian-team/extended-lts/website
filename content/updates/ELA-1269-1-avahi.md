---
title: "ELA-1269-1 avahi security update"
package: avahi
version: 0.6.31-5+deb8u3 (jessie), 0.6.32-2+deb9u3 (stretch), 0.7-4+deb10u4 (buster)
version_map: {"8 jessie": "0.6.31-5+deb8u3", "9 stretch": "0.6.32-2+deb9u3", "10 buster": "0.7-4+deb10u4"}
description: "multiple vulnerabilities"
date: 2024-12-09T14:37:19+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-38469
  - CVE-2023-38470
  - CVE-2023-38471
  - CVE-2023-38472
  - CVE-2023-38473

---

Multiple vulnerabilities have been fixed in the service discovery system Avahi.

Additionally, a GetAlternativeServiceName regression introduced by the CVE-2023-1981 fix in DLA-3414-1 (buster) and ELA-844-1 (jessie, stretch) has been fixed.

CVE-2023-38469

    Reachable assertion in avahi_dns_packet_append_record

CVE-2023-38470

    Reachable assertion in avahi_escape_label

CVE-2023-38471

    Reachable assertion in dbus_set_host_name

CVE-2023-38472

    Reachable assertion in avahi_rdata_parse

CVE-2023-38473

    Reachable assertion in avahi_alternative_host_name
