---
title: "ELA-1163-1 python-django security update"
package: python-django
version: 1.7.11-1+deb8u17 (jessie), 1:1.10.7-2+deb9u23 (stretch)
version_map: {"8 jessie": "1.7.11-1+deb8u17", "9 stretch": "1:1.10.7-2+deb9u23"}
description: "denial of service (DoS) vulnerability"
date: 2024-08-27T13:48:46+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-41989
---

*(Release for `jessie` and `stretch` only)*

A Denial of Service (DoS) vulnerability was discovered in Django, a popular
Python-based web development framework.

The `floatformat` template filter was subject to significant memory consumption
when given a string representation of a number in scientific notation with a
large exponent.
