---
title: "ELA-1240-1 glib2.0 security update"
package: glib2.0
version: 2.42.1-1+deb8u8 (jessie), 2.50.3-2+deb9u7 (stretch), 2.58.3-2+deb10u7 (buster)
version_map: {"8 jessie": "2.42.1-1+deb8u8", "9 stretch": "2.50.3-2+deb9u7", "10 buster": "2.58.3-2+deb10u7"}
description: "buffer overflow"
date: 2024-11-23T21:00:13+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-52533

---

A buffer overflow with long SOCKS4a proxy hostname and username has been fixed in the GNOME Input/Output library (GIO).
