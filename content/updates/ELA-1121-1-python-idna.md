---
title: "ELA-1121-1 python-idna security update"
package: python-idna
version: 2.2-1+deb9u1 (stretch)
version_map: {"9 stretch": "2.2-1+deb9u1"}
description: "denial of service"
date: 2024-07-03T12:09:46-04:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-3651

---

Guido Vranken discovered an issue in python3-idna, a library to support
the Internationalized Domain Names in Applications (IDNA) protocol.  A
specially crafted argument to the idna.encode() function could consume
significant resources, which may lead to Denial of Service.