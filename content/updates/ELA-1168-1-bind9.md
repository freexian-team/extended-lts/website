---
title: "ELA-1168-1 bind9 security update"
package: bind9
version: 1:9.11.5.P4+dfsg-5.1+deb10u13 (buster)
version_map: {"10 buster": "1:9.11.5.P4+dfsg-5.1+deb10u13"}
description: "denial of service"
date: 2024-08-28T17:28:37+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-4408
  - CVE-2024-1737
  - CVE-2024-1975

---

Several vulnerabilities were discovered in BIND, a DNS server
implementation, which may result in denial of service.
