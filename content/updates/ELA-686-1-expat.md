---
title: "ELA-686-1 expat security update"
package: expat
version: 2.1.0-6+deb8u9 (jessie), 2.2.0-2+deb9u6 (stretch)
version_map: {"8 jessie": "2.1.0-6+deb8u9", "9 stretch": "2.2.0-2+deb9u6"}
description: "heap use-after-free"
date: 2022-09-25T10:29:59+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-40674

---

Rhodri James discovered a heap use-after-free vulnerability in the doContent function in Expat, an XML parsing C library, which could result in denial of service or potentially the execution of arbitrary code, if a malformed XML file is processed.
