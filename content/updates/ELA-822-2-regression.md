---
title: "ELA-822-2 regression security update"
package: regression
version: 1:3.3.9-5+deb9u3 (stretch)
version_map: {"9 stretch": "1:3.3.9-5+deb9u3"}
description: "fix handling of RSH environment variables and check for dump/xfsdump"
date: 2025-03-01T00:02:49+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

A fix of CVE-2022-37704 for amanda, the Advanced Maryland Automatic Network Disk Archiver, has been found incomplete.
This update fixes handling of RSH environment variables and uses a correct check for dump/xfsdump.

