---
title: "ELA-1061-1 postgresql-9.4 security update"
package: postgresql-9.4
version: 9.4.26-0+deb8u9 (jessie)
version_map: {"8 jessie": "9.4.26-0+deb8u9"}
description: "privilege escalation"
date: 2024-03-20T23:00:29+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-0985

---

In the PostgreSQL database server, a late privilege drop in the
REFRESH MATERIALIZED VIEW CONCURRENTLY command could allow an
attacker to trick a user with higher privileges to run SQL commands.