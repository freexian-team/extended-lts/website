---
title: "ELA-718-1 batik security update"
package: batik
version: 1.7+dfsg-5+deb8u3 (jessie), 1.8-4+deb9u3 (stretch)
version_map: {"8 jessie": "1.7+dfsg-5+deb8u3", "9 stretch": "1.8-4+deb9u3"}
description: "arbitrary code execution"
date: 2022-10-29T01:56:35+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-41704
  - CVE-2022-42890

---

It was discovered that Apache Batik, an SVG library for Java, allowed attackers
to run arbitrary Java code when processing a malicious SVG file.
