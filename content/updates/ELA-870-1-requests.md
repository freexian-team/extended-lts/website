---
title: "ELA-870-1 requests security update"
package: requests
version: 2.4.3-6+deb8u1 (jessie), 2.12.4-1+deb9u1 (stretch)
version_map: {"8 jessie": "2.4.3-6+deb8u1", "9 stretch": "2.12.4-1+deb9u1"}
description: "information leakage"
date: 2023-06-18T19:56:05+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-32681

---

Requests, a Python HTTP library, has been leaking Proxy-Authorization headers
to destination servers when redirected to an HTTPS endpoint. For HTTP
connections sent through the tunnel, the proxy will identify the header in the
request itself and remove it prior to forwarding to the destination server.
However when sent over HTTPS, the `Proxy-Authorization` header must be sent in
the CONNECT request as the proxy has no visibility into the tunneled request.
This results in Requests forwarding proxy credentials to the destination
server unintentionally, allowing a malicious actor to potentially exfiltrate
sensitive information.
