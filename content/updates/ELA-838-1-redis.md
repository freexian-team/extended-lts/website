---
title: "ELA-838-1 redis security update"
package: redis
version: 2:2.8.17-1+deb8u11 (jessie), 2:2.8.17-1+deb8u11 (stretch)
version_map: {"8 jessie": "2:2.8.17-1+deb8u11", "9 stretch": "2:2.8.17-1+deb8u11"}
description: "denial-of-service vulnerability"
date: 2023-04-21T13:18:10+01:00
draft: false
cvelist:
  - CVE-2023-28856
---

It was discovered that there was a potential remote denial of service
vulnerability in Redis, a popular NoSQL key-value database.

Authenticated users could have used the `HINCRBYFLOAT` command to create an
invalid hash field that would have crashed the Redis server on access.
