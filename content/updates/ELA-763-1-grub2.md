---
title: "ELA-763-1 grub2 security update"
package: grub2
version: 2.02~beta2-22+deb8u2 (jessie)
version_map: {"8 jessie": "2.02~beta2-22+deb8u2"}
description: "integer overflows"
date: 2023-01-09T01:11:08+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

Several issues were found in GRUB2's font handling code, which could result in
crashes and potentially execution of arbitrary code. Further issues were found
in image loading that could potentially lead to memory overflows. Please note
that some integer overflow mitigations could not be applied because of builtin
GCC functions which are only available in newer Debian versions. Only system
administrators should be able to change grub2 fonts. If you use the default
fonts, your system is not affected.
