---
title: "ELA-815-1 net-snmp security update"
package: net-snmp
version: 5.7.2.1+dfsg-1+deb8u6 (jessie), 5.7.3+dfsg-1.7+deb9u5 (stretch)
version_map: {"8 jessie": "5.7.2.1+dfsg-1+deb8u6", "9 stretch": "5.7.3+dfsg-1.7+deb9u5"}
description: "denial of service"
date: 2023-03-13T03:54:45+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-44792
  - CVE-2022-44793

---

net-snmp, Simple Network Management Protocol agents, were reported to have
a couple of vulnerabilities, resulting in a denial of service.

CVE-2022-44792

    handle_ipDefaultTTL in agent/mibgroup/ip-mib/ip_scalars.c in Net-SNMP
    has a NULL Pointer Exception bug that can be used by a remote attacker
    (who has write access) to cause the instance to crash via a crafted UDP
    packet, resulting in Denial of Service.

CVE-2022-44793

    handle_ipv6IpForwarding in agent/mibgroup/ip-mib/ip_scalars.c in Net-SNMP
    has a NULL Pointer Exception bug that can be used by a remote attacker to
    cause the instance to crash via a crafted UDP packet, resulting in
    Denial of Service.
