---
title: "ELA-290-1 ruby2.1 security update"
package: ruby2.1
version: 2.1.5-2+deb8u11
distribution: "Debian 8 jessie"
description: "HTTP Request Smuggling Vulnerability"
date: 2020-10-01T19:44:12+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-25613

---

A potential HTTP request smuggling vulnerability in WEBrick was reported.

WEBrick (bundled along with ruby2.1) was too tolerant against an invalid
Transfer-Encoding header. This may lead to inconsistent interpretation
between WEBrick and some HTTP proxy servers, which may allow the attacker
to “smuggle” a request.
