---
title: "ELA-155-1 cups security update"
package: cups
version: 1.5.3-5+deb7u10
distribution: "Debian 7 Wheezy"
description: "fix for buffer overflow"
date: 2019-08-24T19:29:23+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-8675
  - CVE-2019-8696

---

Two issues have been found in cups, the Common UNIX Printing System(tm).

Basically both CVEs (CVE-2019-8675 and CVE-2019-8696) are about
stack-buffer-overflow in two functions of libcup. One happens in
asn1_get_type() the other one in asn1_get_packed().
