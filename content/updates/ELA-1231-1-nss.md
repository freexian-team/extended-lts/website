---
title: "ELA-1231-1 nss security update"
package: nss
version: 2:3.26-1+debu8u19 (jessie) 2:3.26.2-1.1+deb9u8 (stretch) 2:3.42.1-1+deb10u9 (buster)
version_map: {"8 jessie": "2:3.26-1+debu8u19", "9 stretch": "2:3.26.2-1.1+deb9u8", "10 buster": "2:3.42.1-1+deb10u9"}
description: "multiple vulnerabilities"
date: 2024-11-09T22:46:29+01:00
draft: false
type: updates
tags:
- update
cvelist:
 - CVE-2024-6602
 - CVE-2024-6609
---

Two vulnerabilities were discovered in the nss suite of packages, which include libnss3
and other tools for dealing with certificates and security standards.

CVE-2024-6602

    A mismatch between allocator and deallocator could have lead to memory corruption.

CVE-2024-6609

    When almost out-of-memory an elliptic curve key which was never allocated could have been freed again.
