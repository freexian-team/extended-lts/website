---
title: "ELA-1183-1 apache2 security update"
package: apache2
version: 2.4.10-10+deb8u29 (jessie), 2.4.25-3+deb9u19 (stretch)
version_map: {"8 jessie": "2.4.10-10+deb8u29", "9 stretch": "2.4.25-3+deb9u19"}
description: "multiple vulnerabilities"
date: 2024-09-27T19:58:15Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-38474
  - CVE-2024-38475

---

Apache2, a popular webserver, was vulnerable.

CVE-2024-38474

	Substitution encoding issue in mod_rewrite in Apache HTTP Server
	allowed and attacker to execute scripts in directories permitted
	by the configuration but not directly reachable by any URL or
	source disclosure of scripts meant to only to be executed as CGI.
	Some RewriteRules that capture and substitute unsafely will
	now fail unless rewrite flag "UnsafeAllow3F" is specified.


CVE-2024-38475

	Improper escaping of output in mod_rewrite allowed an attacker
	to map URLs to filesystem locations that are permitted
	to be served by the server but are not intentionally/directly
	reachable by any URL, resulting in code execution
	or source code disclosure.
	Substitutions in server context that use a backreferences
	or variables as the first segment of the substitution are affected.
	Some unsafe RewiteRules will be broken by this change
	and the rewrite flag "UnsafePrefixStat" can be used
	to opt back in once ensuring the substitution is
	appropriately constrained.

