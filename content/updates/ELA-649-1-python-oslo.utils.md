---
title: "ELA-649-1 python-oslo.utils security update"
package: python-oslo.utils
version: 3.16.0-2+deb9u1 (stretch)
version_map: {"9 stretch": "3.16.0-2+deb9u1"}
description: "credential exposure vulnerability"
date: 2022-07-20T09:17:15+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-0718
---

Prevent exposure of sensitive admin passwords due to poor handling of
credential masking.
