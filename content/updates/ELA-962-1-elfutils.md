---
title: "ELA-962-1 elfutils security update"
package: elfutils
version: 0.159-4.2+deb8u2 (jessie), 0.168-1+deb9u2 (stretch)
version_map: {"8 jessie": "0.159-4.2+deb8u2", "9 stretch": "0.168-1+deb9u2"}
description: "out-of-bounds write"
date: 2023-09-25T16:37:38+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-21047

---

An issue has been found in elfutils, a collection of utilities to handle ELF objects.
Due to missing bound checks and reachable asserts, an attacker can use crafted elf files
to trigger application crashes that result in denial-of-services.

As part of this update, CVE-2019-7149 has been fixed as well in Stretch.
Due to a heap-buffer-overflow problem in function read_srclines() a crafted ELF input can cause segmentation faults.
