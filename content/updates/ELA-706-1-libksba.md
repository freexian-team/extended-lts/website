---
title: "ELA-706-1 libksba security update"
package: libksba
version: 1.3.2-1+deb8u2 (jessie), 1.3.5-2+deb9u1 (stretch)
version_map: {"8 jessie": "1.3.2-1+deb8u2", "9 stretch": "1.3.5-2+deb9u1"}
description: "integer overflow"
date: 2022-10-18T12:39:05+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-3515

---

An integer overflow flaw was discovered in the CRL parser in libksba, an X.509
and CMS support library, which could result in denial of service or the
execution of arbitrary code.
