---
title: "ELA-493-1 transfig security update"
package: transfig
version: 1:3.2.6a-2~deb8u1
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2021-10-05T16:23:36+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-19797
  - CVE-2020-21529
  - CVE-2020-21530
  - CVE-2020-21531
  - CVE-2020-21532
  - CVE-2020-21533
  - CVE-2020-21534
  - CVE-2020-21535
  - CVE-2020-21675
  - CVE-2021-3561
  - CVE-2021-32280

---

Multiple security vulnerabilities have been discovered in transfig, utilities
for converting XFig figure files. Buffer overflows, out-of-bounds reads and
NULL pointer dereferences could lead to a denial-of-service or other
unspecified impact.
