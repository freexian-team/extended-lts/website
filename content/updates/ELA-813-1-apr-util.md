---
title: "ELA-813-1 apr-util security update"
package: apr-util
version: 1.5.4-1+deb8u1 (jessie), 1.5.4-3+deb9u1 (stretch)
version_map: {"8 jessie": "1.5.4-1+deb8u1", "9 stretch": "1.5.4-3+deb9u1"}
description: "multiple vulnerabilities"
date: 2023-03-13T02:24:43+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-12618
  - CVE-2022-25147

---

apr-util, Apache Portable Runtime Utility Library, had multiple
vulnerabilities.

CVE-2017-12618

    apr-util fails to validate the integrity of SDBM database files
    used by apr_sdbm*() functions, resulting in a possible out of
    bound read access. A local user with write access to the database
    can make a program or process using these functions crash, and
    cause a denial of service.

CVE-2022-25147

    Integer Overflow or Wraparound vulnerability in apr_base64
    functions of apr-util allows an attacker to write beyond bounds
    of a buffer.
