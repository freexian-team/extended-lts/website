---
title: "ELA-847-1 linux-4.19 security update"
package: linux-4.19
version: 4.19.282-1~deb8u1 (jessie), 4.19.282-1~deb9u1 (stretch)
version_map: {"8 jessie": "4.19.282-1~deb8u1", "9 stretch": "4.19.282-1~deb9u1"}
description: "linux kernel update"
date: 2023-05-05T19:04:57+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-2873
  - CVE-2022-3424
  - CVE-2022-3545
  - CVE-2022-3707
  - CVE-2022-4744
  - CVE-2022-36280
  - CVE-2022-41218
  - CVE-2022-45934
  - CVE-2022-47929
  - CVE-2023-0045
  - CVE-2023-0266
  - CVE-2023-0394
  - CVE-2023-0458
  - CVE-2023-0459
  - CVE-2023-0461
  - CVE-2023-1073
  - CVE-2023-1074
  - CVE-2023-1078
  - CVE-2023-1079
  - CVE-2023-1118
  - CVE-2023-1281
  - CVE-2023-1513
  - CVE-2023-1670
  - CVE-2023-1829
  - CVE-2023-1855
  - CVE-2023-1859
  - CVE-2023-1989
  - CVE-2023-1990
  - CVE-2023-1998
  - CVE-2023-2162
  - CVE-2023-2194
  - CVE-2023-23454
  - CVE-2023-23455
  - CVE-2023-23559
  - CVE-2023-26545
  - CVE-2023-28328
  - CVE-2023-30456
  - CVE-2023-30772

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service, or information
leak.

CVE-2022-2873

    Zheyu Ma discovered that an out-of-bounds memory access flaw in
    the Intel iSMT SMBus 2.0 host controller driver may result in
    denial of service (system crash).

CVE-2022-3424

    Zheng Wang and Zhuorao Yang reported a flaw in the SGI GRU driver
    which could lead to a use-after-free.  On systems where this driver
    is used, a local user can explit this for denial of service (crash
    or memory corruption) or possibly for privilege escalation.

    This driver is not enabled in Debian's official kernel
    configurations.

CVE-2022-3545

    It was discovered that the Netronome Flow Processor (NFP) driver
    contained a use-after-free flaw in area_cache_get(), which may
    result in denial of service or the execution of arbitrary code.

CVE-2022-3707

    Zheng Wang reported a flaw in the i915 graphics driver's
    virtualisation (GVT-g) support that could lead to a double-free.
    On systems where this feature is used, a guest can exploit this
    for denial of service (crash or memory corruption) or possibly for
    privilege escalation.

CVE-2022-4744

    The syzkaller tool found a flaw in the TUN/TAP network driver,
    which can lead to a double-free.  A local user can exploit this
    for denial of service (crash or memory corruption) or possibly for
    privilege escalation.

CVE-2022-36280

    An out-of-bounds memory write vulnerability was discovered in the
    vmwgfx driver, which may allow a local unprivileged user to cause
    a denial of service (system crash).

CVE-2022-41218

    Hyunwoo Kim reported a use-after-free flaw in the Media DVB core
    subsystem caused by refcount races, which may allow a local user
    to cause a denial of service or escalate privileges.

CVE-2022-45934

    An integer overflow in l2cap_config_req() in the Bluetooth
    subsystem was discovered, which may allow a physically proximate
    attacker to cause a denial of service (system crash).

CVE-2022-47929

    Frederick Lawler reported a NULL pointer dereference in the
    traffic control subsystem allowing an unprivileged user to cause a
    denial of service by setting up a specially crafted traffic
    control configuration.

CVE-2023-0045

    Rodrigo Branco and Rafael Correa De Ysasi reported that when a
    user-space task told the kernel to enable Spectre v2 mitigation
    for it, the mitigation was not enabled until the task was next
    rescheduled.  This might be exploitable by a local or remote
    attacker to leak sensitive information from such an application.

CVE-2023-0266

    A use-after-free flaw in the sound subsystem due to missing
    locking may result in denial of service or privilege escalation.

CVE-2023-0394

    Kyle Zeng discovered a NULL pointer dereference flaw in
    rawv6_push_pending_frames() in the network subsystem allowing a
    local user to cause a denial of service (system crash).

CVE-2023-0458

    Jordy Zimmer and Alexandra Sandulescu found that getrlimit() and
    related system calls were vulnerable to speculative execution
    attacks such as Spectre v1.  A local user could explot this to
    leak sensitive information from the kernel.

CVE-2023-0459

    Jordy Zimmer and Alexandra Sandulescu found a regression in
    Spectre v1 mitigation in the user-copy functions for the amd64
    (64-bit PC) architecture.  Where the CPUs do not implement SMAP or
    it is disabled, a local user could exploit this to leak sensitive
    information from the kernel.  Other architectures may also be
    affected.

CVE-2023-0461

    "slipper" reported a flaw in the kernel's support for ULPs (Upper
    Layer Protocols) on top of TCP that can lead to a double-free when
    using kernel TLS sockets.  A local user can exploit this for
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.

    Kernel TLS is not enabled in Debian's official kernel
    configurations.

CVE-2023-1073

    Pietro Borrello reported a type confusion flaw in the HID (Human
    Interface Device) subsystem.  An attacker able to insert and
    remove USB devices might be able to use this to cause a denial of
    service (crash or memory corruption) or possibly to run arbitrary
    code in the kernel.

CVE-2023-1074

    Pietro Borrello reported a type confusion flaw in the SCTP
    protocol implementation which can lead to a memory leak.  A local
    user could exploit this to cause a denial of service (resource
    exhaustion).

CVE-2023-1078

    Pietro Borrello reported a type confusion flaw in the RDS protocol
    implementation.  A local user could exploit this to cause a denial
    of service (crash or memory corruption) or possibly for privilege
    escalation.

CVE-2023-1079

    Pietro Borrello reported a race condition in the hid-asus HID
    driver which could lead to a use-after-free.  An attacker able to
    insert and remove USB devices can use this to cause a denial of
    service (crash or memory corruption) or possibly to run arbitrary
    code in the kernel.

CVE-2023-1118

    Duoming Zhou reported a race condition in the ene_ir remote
    control driver that can lead to a use-after-free if the driver
    is unbound.  It is not clear what the security impact of this is.

CVE-2023-1281, CVE-2023-1829

    "valis" reported two flaws in the cls_tcindex network traffic
    classifier which could lead to a use-after-free.  A local user can
    exploit these for privilege escalation.  This update removes
    cls_tcindex entirely.

CVE-2023-1513

    Xingyuan Mo reported an information leak in the KVM implementation
    for the i386 (32-bit PC) architecture.  A local user could exploit
    this to leak sensitive information from the kernel.

CVE-2023-1670

    Zheng Wang reported a race condition in the xirc2ps_cs network
    driver which can lead to a use-after-free.  An attacker able to
    insert and remove PCMCIA devices can use this to cause a denial of
    service (crash or memory corruption) or possibly to run arbitrary
    code in the kernel.

CVE-2023-1855

    Zheng Wang reported a race condition in the xgene-hwmon hardware
    monitoring driver that may lead to a use-after-free.  It is not
    clear what the security impact of this is.

CVE-2023-1859

    Zheng Wang reported a race condition in the 9pnet_xen transport
    for the 9P filesystem on Xen, which can lead to a use-after-free.
    On systems where this feature is used, a backend driver in another
    domain can use this to cause a denial of service (crash or memory
    corruption) or possibly to run arbitrary code in the vulnerable
    domain.

CVE-2023-1989

    Zheng Wang reported a race condition in the btsdio Bluetooth
    adapter driver that can lead to a use-after-free.  An attacker
    able to insert and remove SDIO devices can use this to cause a
    denial of service (crash or memory corruption) or possibly to run
    arbitrary code in the kernel.

CVE-2023-1990

    Zheng Wang reported a race condition in the st-nci NFC adapter
    driver that can lead to a use-after-free.  It is not clear what
    the security impact of this is.

    This driver is not enabled in Debian's official kernel
    configurations.

CVE-2023-1998

    José Oliveira and Rodrigo Branco reported a regression in Spectre
    v2 mitigation for user-space on x86 CPUs supporting IBRS but not
    eIBRS.  This might be exploitable by a local or remote attacker to
    leak sensitive information from a user-space application.

CVE-2023-2162

    Mike Christie reported a race condition in the iSCSI TCP transport
    that can lead to a use-after-free.  On systems where this feature
    is used, a local user might be able to use this to cause a denial
    of service (crash or memory corruption) or possibly for privilege
    escalation.

CVE-2023-2194

    Wei Chen reported a potential heap buffer overflow in the
    i2c-xgene-slimpro I²C adapter driver.  A local user with
    permission to access such a device can use this to cause a denial
    of service (crash or memory corruption) and probably for privilege
    escalation.

CVE-2023-23454

    Kyle Zeng reported that the Class Based Queueing (CBQ) network
    scheduler was prone to denial of service due to interpreting
    classification results before checking the classification return
    code.

CVE-2023-23455

    Kyle Zeng reported that the ATM Virtual Circuits (ATM) network
    scheduler was prone to a denial of service due to interpreting
    classification results before checking the classification return
    code.

CVE-2023-23559

    Szymon Heidrich reported incorrect bounds checks in the rndis_wlan
    Wi-Fi driver which may lead to a heap buffer overflow or overread.
    An attacker able to insert and remove USB devices can use this to
    cause a denial of service (crash or memory corruption) or
    information leak, or possibly to run arbitrary code in the kernel.

CVE-2023-26545

    Lianhui Tang reported a flaw in the MPLS protocol implementation
    that could lead to a double-free.  A local user might be able to
    exploit this to cause a denial of service (crash or memory
    corruption) or possibl for privilege escalation.

CVE-2023-28328

    Wei Chen reported a flaw in the az6927 DVB driver that can lead to
    a null pointer dereference.  A local user permitted to access an
    I²C adapter device that this driver creates can use this to cause
    a denial of service (crash).

CVE-2023-30456

    Reima ISHII reported a flaw in the KVM implementation for Intel
    CPUs affecting nested virtualisation.  When KVM was used as the L0
    hypervisor, and EPT and/or unrestricted guest mode was disabled,
    it did not prevent an L2 guest from being configured with an
    architecturally invalid protection/paging mode.  A malicious guest
    could exploit this to cause a denial of service (crash).

CVE-2023-30772

    Zheng Wang reported a race condition in the da9150 charger driver
    which could lead to a use-after-free.  It is not clear what the
    security impact of this is.

    This driver is not enabled in Debian's official kernel
    configurations.

This update additionally fixes Debian bug #825141, and
includes many more bug fixes from stable updates 4.19.270-4.19.282
inclusive.
