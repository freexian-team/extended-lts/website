---
title: "ELA-161-1 expat security update"
package: expat
version: 2.1.0-1+deb7u7
distribution: "Debian 7 Wheezy"
description: "heap-based vulnerability"
date: 2019-09-06T15:31:44+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-15903
---

A heap-based buffer overread vulnerability in expat, an XML parsing library.

A specially-crafted XML input could fool the parser into changing from DTD
parsing to document parsing too early; a consecutive call to
`XML_GetCurrentLineNumber` (or `XML_GetCurrentColumnNumber`) then resulted in a
heap-based buffer overread.
