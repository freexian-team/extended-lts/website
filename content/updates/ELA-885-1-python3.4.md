---
title: "ELA-885-1 python3.4 security update"
package: python3.4
version: 3.4.2-1+deb8u15 (jessie)
version_map: {"8 jessie": "3.4.2-1+deb8u15"}
description: "several vulnerabilities"
date: 2023-06-30T23:51:57+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2015-20107
  - CVE-2022-45061

---

Several vulnerabilities were fixed in the Python3 interpreter.

CVE-2015-20107

    The mailcap module did not add escape characters into commands discovered in the system mailcap file.

CVE-2022-45061

    Quadratic time in the IDNA decoder.

