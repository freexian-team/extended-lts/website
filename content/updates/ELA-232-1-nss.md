---
title: "ELA-232-1 nss security update"
package: nss
version: 2:3.26-1+debu7u12
distribution: "Debian 7 Wheezy"
description: "timing attack"
date: 2020-06-07T15:19:52+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-12399

---

A timing attack was found in the way NSS generated DSA signatures. A
man-in-the-middle attacker could use this flaw during DSA signature generation
to recover the private key.
