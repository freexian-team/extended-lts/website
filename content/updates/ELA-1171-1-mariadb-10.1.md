---
title: "ELA-1171-1 mariadb-10.1 security update"
package: mariadb-10.1
version: 10.1.48-0+deb9u3 (stretch)
version_map: {"9 stretch": "10.1.48-0+deb9u3"}
description: "multiple vulnerabilities"
date: 2024-09-01T14:33:28Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-2154
  - CVE-2021-2166
  - CVE-2021-2194
  - CVE-2021-2389
  - CVE-2021-46657
  - CVE-2021-46661
  - CVE-2021-46663
  - CVE-2021-46664
  - CVE-2021-46665
  - CVE-2021-46666
  - CVE-2021-46667
  - CVE-2021-46668
  - CVE-2021-46669

---

Multiple vulnerabilities were fixed in MariaDB, a popular database server.

CVE-2021-2154

    An easily exploitable vulnerability related to the UDF_INIT()
    function, used by MariaDB allows
    high privileged attacker with network access via multiple
    protocols to compromise MariaDB Server.
    Successful attacks of this vulnerability can result
    in unauthorized ability to cause the server to hang or frequently
    repeatable crashes.

CVE-2021-2166

	MySQL's SET plug-in variables wrongly locked making it possible for
    high privileged attackers with network access to compromise the MariaDB
    server, potentially causing Denial-of-Service (DoS).

CVE-2021-2194

    Incorrect handling of filters related to full-text search could be used by
    remote attackers to cause MariaDB Server to crash.

CVE-2021-2389

    Incorrect handling of SELECT and UPDATE queries on tables with full-text
    indices may cause out-of-memory errors.

CVE-2021-46657

	get_sort_by_table in MariaDB could be used to cause an
    application crash via certain subquery uses of ORDER BY.

CVE-2021-46661

    Incorrect handling of find functions in tables and lists makes it possible
    to cause a DoS.

CVE-2021-46663

    Incorrect handling of certain SELECT statements made it possible to crash
    the MariaDB server by the use of ha_maria::extra application.

CVE-2021-46664

    MariaDB crash in sub_select_postjoin_aggr for a NULL value of aggr.

CVE-2021-46665

    Incorrect handling of used_tables makes it possible to cause MariaDB to crash.

CVE-2021-46666

    Mishandling of HAVING and WHERE clauses allows attacker to produce a DoS.

CVE-2021-46667

    Integer overflow in sql_lex.cc may yield to an application crash.

CVE-2021-46668

	MariaDB crash via certain long
    SELECT DISTINCT statements that improperly interact with
    storage-engine resource limitations for temporary data structures.

CVE-2021-46669

    convert_const_to_int use-after-free when the BIGINT data type is used.
