---
title: "ELA-708-1 libxdmcp security update"
package: libxdmcp
version: 1:1.1.1-1+deb8u3 (jessie)
version_map: {"8 jessie": "1:1.1.1-1+deb8u3"}
description: "use of weak entropy"
date: 2022-10-19T09:26:54+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-2625

---

It was found that libxdmcp 1:1.1.1-1+deb8u1 released as DLA-2006-1 did not
properly apply the fix for CVE-2017-2625. That has been corrected now, the
description for that issue follows:

libxdmcp, the X11 Display Manager Control Protocol library, used weak entropy
to generate the session keys. A local attacker could brute force the keys to
connect to another user's session.
