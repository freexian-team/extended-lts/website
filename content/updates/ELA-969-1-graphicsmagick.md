---
title: "ELA-969-1 graphicsmagick security update"
package: graphicsmagick
version: 1.3.30+hg15796-1~deb9u7 (stretch)
version_map: {"9 stretch": "1.3.30+hg15796-1~deb9u7"}
description: "out-of-bounds write"
date: 2023-09-29T12:57:44+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-21679

---

It was discovered that a buffer overflow in GraphicsMagick, a collection
of image processing tools, could result in denial of service or potentially
in the execution of arbitrary code when converting crafted images to the PCX
format.
