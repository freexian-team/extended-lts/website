---
title: "ELA-1021-1 intel-microcode security update"
package: intel-microcode
version: 3.20231114.1~deb8u1 (jessie), 3.20231114.1~deb9u1 (stretch)
version_map: {"8 jessie": "3.20231114.1~deb8u1", "9 stretch": "3.20231114.1~deb9u1"}
description: "reptar"
date: 2023-12-17T18:38:36+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-23583

---

Benoit Morgan, Paul Grosen, Thais Moreira Hamasaki, Ke Sun, Alyssa
Milburn, Hisham Shafi, Nir Shlomovich, Tavis Ormandy, Daniel Moghimi,
Josh Eads, Salman Qazi, Alexandra Sandulescu, Andy Nguyen, Eduardo Vela,
Doug Kwan, and Kostik Shtoyk discovered that some Intel processors
mishandle repeated sequences of instructions leading to unexpected
behavior, which may result in privilege escalation, information
disclosure or denial of service. This vulnerability is also known as
reptar and has been assigend CVE-2023-23583.
