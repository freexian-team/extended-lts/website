---
title: "ELA-756-1 libksba security update"
package: libksba
version: 1.3.2-1+deb8u3 (jessie), 1.3.5-2+deb9u2 (stretch)
version_map: {"8 jessie": "1.3.2-1+deb8u3", "9 stretch": "1.3.5-2+deb9u2"}
description: "buffer overflow"
date: 2022-12-24T16:37:48+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-47629

---

An integer overflow flaw was discovered in the CRL signature parser in
libksba, an X.509 and CMS support library, which could result in denial
of service or the execution of arbitrary code.

