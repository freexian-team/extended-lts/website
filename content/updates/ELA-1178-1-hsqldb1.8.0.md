---
title: "ELA-1178-1 hsqldb1.8.0 security update"
package: hsqldb1.8.0
version: 1.8.0.10+dfsg-3+deb8u1 (jessie)
version_map: {"8 jessie": "1.8.0.10+dfsg-3+deb8u1"}
description: "arbitrary file write"
date: 2024-09-07T18:45:14+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-1183

---

Arbitrary file write with a SCRIPT command was fixed in the Java database engine hsqldb1.8.0.
