---
title: "ELA-960-1 libapache-mod-jk security update"
package: libapache-mod-jk
version: 1:1.2.46-0+deb8u2 (jessie), 1:1.2.46-0+deb9u2 (stretch)
version_map: {"8 jessie": "1:1.2.46-0+deb8u2", "9 stretch": "1:1.2.46-0+deb9u2"}
description: "bypass of security constraints"
date: 2023-09-24T21:18:28+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-41081

---

The mod_jk component of Apache Tomcat Connectors, an Apache 2 module to forward
requests from Apache to Tomcat, in some circumstances, such as when a
configuration included "JkOptions +ForwardDirectories" but the configuration
did not provide explicit mounts for all possible proxied requests, mod_jk would
use an implicit mapping and map the request to the first defined worker. Such
an implicit mapping could result in the unintended exposure of the status
worker and/or bypass security constraints configured in httpd. As of this
security update, the implicit mapping functionality has been removed and all
mappings must now be via explicit configuration. This issue affects Apache
Tomcat Connectors (mod_jk only).
