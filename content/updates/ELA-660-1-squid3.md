---
title: "ELA-660-1 squid3 security update"
package: squid3
version: 3.5.23-5+deb8u5 (jessie), 3.5.23-5+deb9u8 (stretch)
version_map: {"8 jessie": "3.5.23-5+deb8u5", "9 stretch": "3.5.23-5+deb9u8"}
description: "multiple vulnerabilities"
date: 2022-08-08T07:46:36-07:00
draft: false
type: updates
cvelist:
  - CVE-2021-28116
  - CVE-2021-46784
---

Two vulnerabilities were discovered in squid3, a popular HTTP caching proxy:

* CVE-2021-28116: Squid through 4.14 and 5.x through 5.0.5, in some
  configurations, allows information disclosure because of an out-of-bounds
  read in WCCP protocol data. This can be leveraged as part of a chain for
  remote code execution as nobody.

* CVE-2021-46784: In Squid 3.x through 3.5.28, 4.x through 4.17, and 5.x
  before 5.6, due to improper buffer management, a Denial of Service can occur
  when processing long Gopher server responses.
