---
title: "ELA-1260-1 activemq security update"
package: activemq
version: 5.14.3-3+deb9u3 (stretch) 5.15.16-0+deb10u2 (buster)
version_map: {"9 stretch": "5.14.3-3+deb9u3", "10 buster": "5.15.16-0+deb10u2"}
description: "multiple vulnerabilities"
date: 2024-11-30T14:49:59+01:00
draft: false
type: updates
tags:
- update
cvelist:
 - CVE-2023-46604
 - CVE-2022-41678
---

Two vulnerabilities were discovered in the activemq suite of packages. Activemq is the java-based
flexible & powerful open source multi-protocol message broker.

CVE-2022-41678

    Once an user is authenticated on Jolokia, he can potentially trigger arbitrary code execution.

    The fix for this problem has been added to both the Debian Stretch and the Debian Buster packages.

CVE-2023-46604

    Java OpenWire protocol marshaller is vulnerable to Remote Code Execution. This vulnerability may allow a remote attacker with network access to either a Java-based OpenWire broker or client to run arbitrary shell commands by manipulating serialized class types in the OpenWire protocol to cause either the client or the broker (respectively) to instantiate any class on the classpath.

    The fix for this problem has been added to the Debian Stretch package. The Debian Buster package was fixed already
    in a previous update, in version 5.15.16-0+deb10u1.
