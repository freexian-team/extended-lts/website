---
title: "ELA-248-1 librsvg security update"
package: librsvg
version: 2.40.5-1+deb8u3
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2020-07-22T12:50:33+02:00
draft: false
type: updates
cvelist:
  - CVE-2016-6163
  - CVE-2019-20446

---

Several issues have been fixed in librsvg, a library for rendering SVG
files. This update corrects some denial of service via infinite loop
or exponential element processing when parsing specially crafted files,
as well as some memory safety issues.
