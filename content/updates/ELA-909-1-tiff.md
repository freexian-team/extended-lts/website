---
title: "ELA-909-1 tiff security update"
package: tiff
version: 4.0.3-12.3+deb8u16 (jessie), 4.0.8-2+deb9u11 (stretch)
version_map: {"8 jessie": "4.0.3-12.3+deb8u16", "9 stretch": "4.0.8-2+deb9u11"}
description: "multiple vulnerabilities"
date: 2023-07-31T23:50:52Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-2908
  - CVE-2023-3316
  - CVE-2023-3618
  - CVE-2023-25433
  - CVE-2023-26965
  - CVE-2023-26966
  - CVE-2023-40745
  - CVE-2023-41175

---

CVE-2023-2908

    NULL pointer dereference in tif_dir.c

CVE-2023-3316

    NULL pointer dereference in TIFFClose()

CVE-2023-3618

    Buffer overflow in tiffcrop

CVE-2023-25433

    Buffer overflow in tiffcrop

CVE-2023-26965

    Use after free in tiffcrop

CVE-2023-26966

    Buffer overflow in uv_encode()

CVE-2023-40745

    Integer overflow in tiffcp

CVE-2023-41175

    Integer overflow in raw2tiff

