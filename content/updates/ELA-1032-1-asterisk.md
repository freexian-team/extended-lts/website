---
title: "ELA-1032-1 asterisk security update"
package: asterisk
version: 1:13.14.1~dfsg-2+deb9u9 (stretch)
version_map: {"9 stretch": "1:13.14.1~dfsg-2+deb9u9"}
description: "arbitrary file read"
date: 2024-01-24T20:02:22+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-37457
  - CVE-2023-49294

---

Two security vulnerabilities were discovered in Asterisk, a private branch
exchange.

CVE-2023-37457

    The 'update' functionality of the PJSIP_HEADER dialplan function can exceed
    the available buffer space for storing the new value of a header. By doing
    so this can overwrite memory or cause a crash. This is not externally
    exploitable, unless dialplan is explicitly written to update a header based
    on data from an outside source. If the 'update' functionality is not used
    the vulnerability does not occur.

CVE-2023-49294

    It is possible to read any arbitrary file even when the `live_dangerously`
    option is not enabled.

