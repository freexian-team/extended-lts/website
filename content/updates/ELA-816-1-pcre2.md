---
title: "ELA-816-1 pcre2 security update"
package: pcre2
version: 10.22-3+deb9u1 (stretch)
version_map: {"9 stretch": "10.22-3+deb9u1"}
description: "buffer overread"
date: 2023-03-18T17:04:51+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-1586

---

Multiple out-of-bounds read vulnerabilities were found in pcre2, a Perl
Compatible Regular Expression library, which could result in information
disclosure or denial or service.

CVE-2022-1586

    An out-of-bounds read vulnerability was discovered in the PCRE2 library
    in the compile_xclass_matchingpath() function of the pcre2_jit_compile.c
    file. This involves a unicode property matching issue in JIT-compiled
    regular expressions.  The issue occurs because the character was not
    fully read in case-less matching within JIT.

Additionally, this upload also fixes a subject buffer overread in JIT
when UTF is disabled and \X or \R has a greater than 1 fixed quantifier.
This issue was found by Yunho Kim.
