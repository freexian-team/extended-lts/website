---
title: "ELA-963-1 plexus-utils2 security update"
package: plexus-utils2
version: 3.0.15-1+deb8u2 (jessie), 3.0.22-1+deb9u1 (stretch)
version_map: {"8 jessie": "3.0.15-1+deb8u2", "9 stretch": "3.0.22-1+deb9u1"}
description: "directory traversal"
date: 2023-09-25T17:48:24+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-4244
  - CVE-2022-4245

---

Two security vulnerabilities have been found in plexus-utils2, a collection of
components used by Apache Maven.

CVE-2022-4244

    A Directory Traversal issue was discovered in plexus-utils2. This is an
    attack which aims to access files and directories that are stored outside
    the intended folder. By manipulating files with "dot-dot-slash (../)"
    sequences and its variations, or by using absolute file paths, it may be
    possible to access arbitrary files and directories stored on the file system,
    including application source code, configuration, and other critical system
    files.

CVE-2022-4245

    The org.codehaus.plexus.util.xml.XmlWriterUtil#writeComment fails to
    sanitize comments for a --> sequence. This issue means that text contained
    in the command string could be interpreted as XML and allow for XML
    injection.
