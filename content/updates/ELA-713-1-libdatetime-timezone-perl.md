---
title: "ELA-713-1 libdatetime-timezone-perl new timezone database"
package: libdatetime-timezone-perl
version: 1:1.75-2+2022e (jessie), 1:2.09-1+2022e (stretch)
version_map: {"8 jessie": "1:1.75-2+2022e", "9 stretch": "1:2.09-1+2022e"}
description: "update to tzdata 2022e"
date: 2022-10-26T19:50:05+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update includes the changes in tzdata 2022e for the Perl bindings.
