---
title: "ELA-154-2 openjdk-7 regression update"
package: openjdk-7
version: 7u231-2.6.19-1~deb7u2
distribution: "Debian 7 Wheezy"
description: "regression update"
date: 2019-08-23T00:14:31+02:00
draft: false
type: updates
cvelist:

---

The latest security update of openjdk-7 caused a regression when
applications relied on elliptic curve algorithms to establish SSL
connections. Several duplicate classes were removed from rt.jar by the
upstream developers of OpenJDK because they were also present in
sunec.jar. However Debian never shipped the SunEC security provider in
OpenJDK 7.

The issue was resolved by building sunec.jar and its corresponding
native library libsunec.so from source. In order to build these
libraries from source, an update of nss to version 2:3.26-1+debu7u8 is
required.

Updates for the amd64 architecture are already available, new packages
for i386 will be available within the next 24 hours.

