---
title: "ELA-1089-2 less regression update"
package: less
version: 458-3+deb8u2 (jessie), 481-2.1+deb9u1 (stretch)
version_map: {"8 jessie": "458-3+deb8u2", "9 stretch": "481-2.1+deb9u1"}
description: "no-change rebuild for less i386"
date: 2024-05-22T19:34:55+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

The i386 binaries for the less security update, announced as ELA-1089-1, were
not correctly published in the jessie-security archive. This issue has been
resolved by simply rebuilding the packages. No additional changes have been
made. No further action is required if you use either the amd64 binaries or
Debian 9 "stretch".
