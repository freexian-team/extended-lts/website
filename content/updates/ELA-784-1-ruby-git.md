---
title: "ELA-784-1 ruby-git security update"
package: ruby-git
version: 1.2.8-1+deb9u1 (stretch)
version_map: {"9 stretch": "1.2.8-1+deb9u1"}
description: "multiple vulnerabilities"
date: 2023-01-31T04:07:49+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-25648
  - CVE-2022-46648
  - CVE-2022-47318

---

A couple of vulnerabilities were reported against ruby-git, a Ruby
interface to the Git revision control system, that could lead to a
command injection and execution of an arbitrary ruby code by having
a user to load a repository containing a specially crafted filename
to the product.
