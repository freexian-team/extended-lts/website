---
title: "ELA-486-1 gst-plugins-bad0.10 security update"
package: gst-plugins-bad0.10
version: 0.10.23-7.4+deb8u5
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2021-09-27T06:01:13+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-3185

---

Andrew Wesie discovered a buffer overflow in
the H264 support of the GStreamer multimedia
framework, which could potentially result in
the execution of arbitrary code.
