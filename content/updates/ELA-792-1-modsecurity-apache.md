---
title: "ELA-792-1 modsecurity-apache security update"
package: modsecurity-apache
version: 2.8.0-3+deb8u2 (jessie)
version_map: {"8 jessie": "2.8.0-3+deb8u2"}
description: "web application firewall bypass"
date: 2023-02-03T20:42:59+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-48279

---

A issues was found in modsecurity-apache, open source, cross
platform web application firewall (WAF) engine for Apache which allows
remote attackers to bypass the applications firewall.

CVE-2022-48279

    In ModSecurity before 2.9.6 and 3.x before 3.0.8, HTTP multipart
    requests were incorrectly parsed and could bypass the Web Application
    Firewall.
    NOTE: this is related to CVE-2022-39956 but can be considered
    independent changes to the ModSecurity (C language) codebase.
