---
title: "ELA-555-1 shadow security update"
package: shadow
version: 1:4.2-3+deb8u5
distribution: "Debian 8 jessie"
description: "exposed sensitive information"
date: 2022-02-01T14:20:21+05:30
draft: false
type: updates
cvelist:
  - CVE-2017-12424
  - CVE-2018-7169

---

CVE-2017-12424

    It was discovered that shadow incorrectly handled certain inputs.
    An attacker could possibly use this issue to cause a crash or
    expose sensitive information.

CVE-2018-7169

    It was discovered that shadow incorrectly handled certain inputs.
    An attacker could possibly use this issue to expose sensitive
    information.
