---
title: "ELA-1310-1 libreoffice security update"
package: libreoffice
version: 1:4.3.3-2+deb8u16 (jessie)
version_map: {"8 jessie": "1:4.3.3-2+deb8u16"}
description: "multiple vulnerabilities"
date: 2025-01-30T22:18:24Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-12425
  - CVE-2024-12426

---

Libreoffice, an office productivity software suite, was affected by two vulnerabilities

CVE-2024-12425

    An Improper Limitation of a Pathname to a Restricted Directory ('Path Traversal') vulnerability was found
    in The Document Foundation LibreOffice and allows Absolute Path Traversal. An attacker can write to arbitrary
    locations, albeit suffixed with ".ttf", by supplying a file in a format that supports embedded font files.

CVE-2024-12426

    An Exposure of Environmental Variables and arbitrary INI file values to an Unauthorized Actor vulnerability
    was found in The Document Foundation LibreOffice. URLs could be constructed which expanded environmental
    variables or INI file values, so potentially sensitive information could be exfiltrated
    to a remote server on opening a document containing such links.

