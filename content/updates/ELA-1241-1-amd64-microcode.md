---
title: "ELA-1241-1 amd64-microcode security update"
package: amd64-microcode
version: 3.20240820.1~deb8u1 (jessie), 3.20240820.1~deb9u1 (stretch), 3.20240820.1~deb10u1 (buster)
version_map: {"8 jessie": "3.20240820.1~deb8u1", "9 stretch": "3.20240820.1~deb9u1", "10 buster": "3.20240820.1~deb10u1"}
description: "microcode update"
date: 2024-11-24T10:28:32+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-20569
  - CVE-2023-20584
  - CVE-2023-31315
  - CVE-2023-31356

---

AMD has released microcode updates to address multiple vulnerabilties.

This release requires *either* new-enough system firmware, *or* a
recent-enough Linux kernel to properly work on AMD Genoa and Bergamo
processors.

The firmware requirement is AGESA 1.0.0.8 or newer.

The Linux kernel requirement is a group of patches that are already
present in the Linux stable/LTS/ELTS trees since versions: v4.19.289,
v5.4.250, v5.10.187, v5.15.120, v6.1.37, v6.3.11 and v6.4.1.  These
patches are also present in Linux v6.5-rc1.


CVE-2023-20569

    A side channel vulnerability on some of the AMD CPUs may allow an
    attacker to influence the return address prediction. This may result
    in speculative execution at an attacker-controlled?address,
    potentially leading to information disclosure. 
    
    CVE-2023-20569 had been previously reported as fixed in an earlier
    update, this update expands the fixes to 4th Gen AMD EPYC
    processors, Genoa (Family=0x19 Model=0x11) and Bergamo (Family=0x19
    Model=0xa0). See Debian bug #1043381 for details.

CVE-2023-20584

    IOMMU improperly handles certain special address ranges with invalid
    device table entries (DTEs), which may allow an attacker with
    privileges and a compromised Hypervisor to induce DTE faults to
    bypass RMP checks in SEV-SNP, potentially leading to a loss of guest
    integrity.

CVE-2023-31315

    Improper validation in a model specific register (MSR) could allow a
    malicious program with ring0 access to modify SMM configuration
    while SMI lock is enabled, potentially leading to arbitrary code
    execution.

CVE-2023-31356

    Incomplete system memory cleanup in SEV firmware could allow a
    privileged attacker to corrupt guest private memory, potentially
    resulting in a loss of data integrity.
    
