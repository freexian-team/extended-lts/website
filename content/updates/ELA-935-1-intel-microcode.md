---
title: "ELA-935-1 intel-microcode security update"
package: intel-microcode
version: 3.20230808.1~deb8u1 (jessie), 3.20230808.1~deb9u1 (stretch)
version_map: {"8 jessie": "3.20230808.1~deb8u1", "9 stretch": "3.20230808.1~deb9u1"}
description: "new upstream version"
date: 2023-08-27T01:21:37+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-40982
  - CVE-2022-41804
  - CVE-2023-23908

---

This update ships updated CPU microcode for some types of Intel CPUs
and provides mitigations for security vulnerabilities.

CVE-2022-40982

    Daniel Moghimi discovered Gather Data Sampling (GDS), a hardware
    vulnerability which allows unprivileged speculative access to data
    which was previously stored in vector registers.

    For details please refer to https://downfall.page/ and
    https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html.

CVE-2022-41804

    Unauthorized error injection in Intel SGX or Intel TDX for some
    Intel Xeon Processors which may allow a local user to potentially
    escalate privileges.

CVE-2023-23908

    Improper access control in some 3rd Generation Intel Xeon Scalable
    processors may result in an information leak.
