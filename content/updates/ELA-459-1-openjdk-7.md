---
title: "ELA-459-1 openjdk-7 security update"
package: openjdk-7
version: 7u301-2.6.26-0+deb8u1
distribution: "Debian 8 jessie"
description: "sandbox bypass"
date: 2021-07-21T12:47:59+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-2161
  - CVE-2021-2163

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
resulting in bypass of sandbox restrictions.
