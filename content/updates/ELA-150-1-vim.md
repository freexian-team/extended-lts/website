---
title: "ELA-150-1 vim security update"
package: vim
version: 7.3.547-7+deb7u5
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2019-08-03T12:24:45+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-17087
  - CVE-2019-12735

---

Multiple vulnerabilities have been fixed in vim, a highly configurable
text editor.

* CVE-2017-17087

    Vim sets the group ownership of a .swp file to the editor's
    primary group (which may be different from the group ownership of
    the original file), which allows local users to obtain sensitive
    information by leveraging an applicable group membership.

* CVE-2019-12735

    Vim did not restrict the `:source!` command when executed in a
    sandbox.
