---
title: "ELA-965-1 tomcat7 security update"
package: tomcat7
version: 7.0.56-3+really7.0.109-1+deb8u4 (jessie)
version_map: {"8 jessie": "7.0.56-3+really7.0.109-1+deb8u4"}
description: "denial of service"
date: 2023-09-25T21:54:53+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-24998
  - CVE-2023-41080

---

Two security vulnerabilities were discovered in Apache Tomcat, a servlet and
JSP engine.

CVE-2023-24998

    Apache Tomcat uses a packaged renamed copy of Apache Commons FileUpload to
    provide the file upload functionality defined in the Jakarta Servlet
    specification. Apache Tomcat was, therefore, also vulnerable to the Apache
    Commons FileUpload vulnerability CVE-2023-24998 as there was no limit to
    the number of request parts processed. This resulted in the possibility of
    an attacker triggering a DoS with a malicious upload or series of uploads.

CVE-2023-41080

    If the ROOT (default) web application is configured to use FORM
    authentication then it is possible that a specially crafted URL could be
    used to trigger a redirect to an URL of the attacker's choice.

