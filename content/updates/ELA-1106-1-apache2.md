---
title: "ELA-1106-1 apache2 security update"
package: apache2
version: 2.4.10-10+deb8u27 (jessie)
version_map: {"8 jessie": "2.4.10-10+deb8u27"}
description: "faulty input validation"
date: 2024-06-14T19:38:36Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-38709

---

Faulty input validation in the core of Apache allowed malicious or exploitable backend/content generators to split HTTP responses
