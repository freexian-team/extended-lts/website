---
title: "ELA-68-1 libapache-mod-jk security update"
package: libapache-mod-jk
version: 1.2.46-0+deb7u1
distribution: "Debian 7 Wheezy"
description: "information disclosure and privilege escalation"
date: 2018-12-17T21:42:02Z
draft: false
type: updates
cvelist:
  - CVE-2018-11759

---

A vulnerability has been discovered in libapache-mod-jk, the Apache 2
connector for the Tomcat Java servlet engine.

The libapache-mod-jk connector is susceptible to information disclosure
and privilege escalation because of a mishandling of URL normalization.

The nature of the fix required that libapache-mod-jk in Debian 7
"Wheezy" be updated to the latest upstream release.  For reference, the
upstream changes associated with each release version are documented
here:

http://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html
