---
title: "ELA-881-1 libx11 security update"
package: libx11
version: 2:1.6.2-3+deb8u6 (jessie), 2:1.6.4-3+deb9u5 (stretch)
version_map: {"8 jessie": "2:1.6.2-3+deb8u6", "9 stretch": "2:1.6.4-3+deb9u5"}
description: "missing input validation"
date: 2023-06-29T14:57:16+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-3138

---

Missing input validation in various functions may have resulted in
denial of service in various functions provided by libx11, the X11
client-side library.

