---
title: "ELA-44-1 suricata security update"
package: suricata
version: 1.2.1-2+deb7u2
distribution: "Debian 7 Wheezy"
description: "wrong handling of package flow"
date: 2018-09-24T20:10:18+02:00
draft: false
type: updates
cvelist:
  - CVE-2016-10728
---

If an ICMPv4 error packet is received as the first packet on a flow in the to_client direction, it can lead to missed TCP/UDP detection in packets arriving afterwards.

