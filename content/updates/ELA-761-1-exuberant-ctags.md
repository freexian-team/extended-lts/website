---
title: "ELA-761-1 exuberant-ctags security update"
package: exuberant-ctags
version: 1:5.9~svn20110310-11+deb9u1 (stretch)
version_map: {"9 stretch": "1:5.9~svn20110310-11+deb9u1"}
description: "arbitrary code execution vulnerability"
date: 2022-12-31T13:03:14+00:00
draft: false
cvelist:
  - CVE-2022-4515
---

A flaw was found in the way the exubertant-ctags source code parser handled the
"-o" command-line option which specifies the tag filename. A crafted tag
filename specified in the command line or in the configuration file could have
resulted in arbitrary command execution because the externalSortTags() in
sort.c calls the system(3) function in an unsafe way.
