---
title: "ELA-312-1 tzdata new upstream version"
package: tzdata
version: 2020d-0+deb8u1
distribution: "Debian 8 jessie"
description: "timezone database update"
date: 2020-11-09T19:14:06+05:30
draft: false
type: updates
cvelist:

---

This update brings the timezone changes from the upstream 2020d release.
  - Revised predictions for Morocco's changes starting in 2023.
  - Macquarie Island has stayed in sync with Tasmania since 2011.
  - Casey, Antarctica is at +08 in winter and +11 in summer since 2018.
  - Palestine ends DST earlier than predicted, on 2020-10-24.
  - Fiji starts DST later than usual, on 2020-12-20.
