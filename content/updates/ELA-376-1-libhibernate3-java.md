---
title: "ELA-376-1 libhibernate3-java security update"
package: libhibernate3-java
version: 3.6.10.Final-3+deb8u1
distribution: "Debian 8 jessie"
description: "SQL injection"
date: 2021-03-08T04:29:15+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-25638

---

A flaw was found in hibernate-core. A SQL injection in the implementation
of the JPA Criteria API can permit unsanitized literals when a literal is
used in the SQL comments of the query. This flaw could allow an attacker to
access unauthorized information or possibly conduct further attacks.
