---
title: "ELA-1139-1 phppgadmin security update"
package: phppgadmin
version: 5.1-1.1+deb8u1 (jessie)
version_map: {"8 jessie": "5.1-1.1+deb8u1"}
description: "remote code execution vulnerability"
date: 2024-07-25T12:11:05+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-40619

---

A potential Remote Code Execution (RCE) vulnerability was discovered in
phppgadmin, a web-based administration tool for the PostgreSQL database.

This was an issue related to the deserialisation of untrusted data, which may
have led to remote code execution because user-controlled data was passed
directly to the PHP `unserialize()` function.
