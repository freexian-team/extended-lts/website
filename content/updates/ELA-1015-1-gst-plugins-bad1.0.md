---
title: "ELA-1015-1 gst-plugins-bad1.0 security update"
package: gst-plugins-bad1.0
version: 1.4.4-2.1+deb8u7 (jessie), 1.10.4-1+deb9u5 (stretch)
version_map: {"8 jessie": "1.4.4-2.1+deb8u7", "9 stretch": "1.10.4-1+deb9u5"}
description: "use after free"
date: 2023-11-30T23:35:57+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-44446

---

An issue has been found in gst-plugins-bad1.0, which contains several GStreamer plugins from the "bad" set.
The issue is related to use-after-free of some pointers within the MXF demuxer.
