---
title: "ELA-1220-1 shadow security update"
package: shadow
version: 1:4.4-4.1+deb9u2 (stretch), 1:4.5-1.1+deb10u1 (buster)
version_map: {"9 stretch": "1:4.4-4.1+deb9u2", "10 buster": "1:4.5-1.1+deb10u1"}
description: "multiple vulnerabilities"
date: 2024-10-28T23:29:35+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-7169
  - CVE-2023-4641
  - CVE-2023-29383

---

Multiple vulnerabilities have been fixed in shadow, commonly used utilities to change and administer password and group data.

CVE-2018-7169

    unprivileged user can drop supplementary groups

CVE-2023-4641

    gpasswd password leak

CVE-2023-29383

    chfn missing control character check
