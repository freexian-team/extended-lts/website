---
title: "ELA-896-1 twisted security update"
package: twisted
version: 14.0.2-3+deb8u6 (jessie), 16.6.0-2+deb9u4 (stretch)
version_map: {"8 jessie": "14.0.2-3+deb8u6", "9 stretch": "16.6.0-2+deb9u4"}
description: "multiple vulnerabilities"
date: 2023-07-22T18:08:15+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-12387
  - CVE-2019-12855
  - CVE-2022-39348

---

Multiple vulnerabilities were discovered in Twisted, an event-based
framework for internet applications written in Python. An attacker may
initiate request smuggling, Man-In-The-Middle (MITM) communication
interception and cross-site-scripting (XSS).

* CVE-2019-12387

    twisted.web did not validate or sanitize URIs or HTTP methods,
    allowing an attacker to inject invalid characters such as CRLF.

* CVE-2019-12855

    In words.protocols.jabber.xmlstream in Twisted through 19.2.1,
    XMPP support did not verify certificates when used with TLS,
    allowing an attacker to MITM connections.

* CVE-2022-39348

    When the host header does not match a configured host
    `twisted.web.vhost.NameVirtualHost` will return a `NoResource`
    resource which renders the Host header unescaped into the 404
    response allowing HTML and script injection. In practice this
    should be very difficult to exploit as being able to modify the
    Host header of a normal HTTP request implies that one is already
    in a privileged position.
