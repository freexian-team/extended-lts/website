---
title: "ELA-1063-1 qemu security update"
package: qemu
version: 1:2.8+dfsg-6+deb9u19 (stretch)
version_map: {"9 stretch": "1:2.8+dfsg-6+deb9u19"}
description: "multiple vulnerabilities"
date: 2024-03-24T19:52:48+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-14394
  - CVE-2023-0330
  - CVE-2023-2861
  - CVE-2023-3180
  - CVE-2023-3354
  - CVE-2023-5088

---

Multiple vulnerabilities have been fixed in the machine emulator
and virtualizer QEMU.

CVE-2020-14394

    infinite loop in the USB xHCI controller emulation

CVE-2023-0330

    reentrancy issues in the LSI controller

CVE-2023-2861

    9pfs did not prohibit opening special files on the host side

CVE-2023-3180

    heap buffer overflow in the virtual crypto device

CVE-2023-3354

    remote unauthenticated clients could cause denial of service in VNC server

CVE-2023-5088

    IDE guest I/O operation addressed to an arbitrary disk offset might get targeted to offset 0 instead

