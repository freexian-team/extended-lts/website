---
title: "ELA-980-1 libxpm security update"
package: libxpm
version: 1:3.5.12-0+deb8u3 (jessie), 1:3.5.12-1+deb9u2 (stretch)
version_map: {"8 jessie": "1:3.5.12-0+deb8u3", "9 stretch": "1:3.5.12-1+deb9u2"}
description: "multiple vulnerabilities"
date: 2023-10-05T13:11:12+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-43786
  - CVE-2023-43787
  - CVE-2023-43788
  - CVE-2023-43789

---

Several vulnerabilities were found in libXpm, the X Pixmap (XPM) image
library.

CVE-2023-43786

    Yair Mizrahi discovered an infinite recursion issue when parsing
    crafted XPM files, which would result in denial of service.

CVE-2023-43787

    Yair Mizrahi discovered a buffer overflow vulnerability in libX11
    when parsing crafted XPM files, which could result in denial of
    service or potentially the execution of arbitrary code.

CVE-2023-43788

    Alan Coopersmith found an out of bounds read in
    XpmCreateXpmImageFromBuffer, which could result in denial of
    service when parsing crafted XPM files.

CVE-2023-43789

    Alan Coopersmith discovered an out of bounds read issue when
    parsing corrupted colormaps, which could lead to denial of
    service when parsing crafted XPM files.
