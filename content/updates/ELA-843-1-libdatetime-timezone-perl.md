---
title: "ELA-843-1 libdatetime-timezone-perl new timezone database"
package: libdatetime-timezone-perl
version: 1:1.75-2+2023c (jessie), 1:2.09-1+2023c (stretch)
version_map: {"8 jessie": "1:1.75-2+2023c", "9 stretch": "1:2.09-1+2023c"}
description: "update to tzdata 2023c"
date: 2023-05-02T15:23:12+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update includes the changes in tzdata 2023c for the Perl bindings.
