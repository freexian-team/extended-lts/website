---
title: "ELA-1292-1 tomcat8 security update"
package: tomcat8
version: 8.5.54-0+deb9u17 (stretch)
version_map: {"9 stretch": "8.5.54-0+deb9u17"}
description: "denial of service"
date: 2025-01-15T16:20:19+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-21733
  - CVE-2024-38286
  - CVE-2024-52316

---

Several problems have been addressed in Tomcat 8, a Java based web server,
servlet and JSP engine, which may have led to an OutOfMemoryError or the
revelation of sensitive information.

CVE-2024-21733

    Generation of Error Message Containing Sensitive Information vulnerability
    in Apache Tomcat.

CVE-2024-38286

    Apache Tomcat, under certain configurations, allows an attacker to cause an
    OutOfMemoryError by abusing the TLS handshake process.

CVE-2024-52316

    Unchecked Error Condition vulnerability in Apache Tomcat. If Tomcat is
    configured to use a custom Jakarta Authentication (formerly JASPIC)
    ServerAuthContext component which may throw an exception during the
    authentication process without explicitly setting an HTTP status to
    indicate failure, the authentication may not fail, allowing the user to
    bypass the authentication process. There are no known Jakarta
    Authentication components that behave in this way.
