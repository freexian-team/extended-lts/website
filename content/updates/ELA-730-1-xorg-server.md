---
title: "ELA-730-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u8 (jessie), 2:1.19.2-1+deb9u11 (stretch)
version_map: {"8 jessie": "2:1.16.4-1+deb8u8", "9 stretch": "2:1.19.2-1+deb9u11"}
description: "multiple vulnerabilities"
date: 2022-11-11T13:44:15+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-3550
  - CVE-2022-3551

---

Two vulnerabilities were found in the Xkb extension of the X.org X server,
which could result in denial of service or possibly privilege escalation
if the X server is running privileged.
