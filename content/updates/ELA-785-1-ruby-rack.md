---
title: "ELA-785-1 ruby-rack security update"
package: ruby-rack
version: 1.6.4-4+deb9u4 (stretch)
version_map: {"9 stretch": "1.6.4-4+deb9u4"}
description: "regular expression denial of service"
date: 2023-01-31T04:10:10+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-44570
  - CVE-2022-44571

---

A couple of ReDoS vulnerabilities were found in multipart parser and
Rack::Utils.byte_ranges in ruby-rack, a modular Ruby webserver interface.
