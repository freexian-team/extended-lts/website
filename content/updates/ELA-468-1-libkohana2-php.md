---
title: "ELA-468-1 libkohana2-php security update"
package: libkohana2-php
version: 2.3.4-2+deb8u1
distribution: "Debian 8 jessie"
description: "cross-site scripting (XSS) attack"
date: 2021-08-04T21:10:56+05:30
draft: false
type: updates
cvelist:
  - CVE-2016-10510

---

David Sopas discovered that Kohana, a PHP framework, was vulnerable to
a Cross-site scripting (XSS) attack that allowed remote attackers to
inject arbitrary web script or HTML by bypassing the strip_image_tags
protection mechanism in system/classes/Kohana/Security.php. This issue
was resolved by permanently removing the strip_image_tags function.
Users are advised to sanitize user input by using external libraries
instead.
