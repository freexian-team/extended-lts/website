---
title: "ELA-1290-2 rsync regression update"
package: rsync
version: 3.1.1-3+deb8u4 (jessie), 3.1.2-1+deb9u5 (stretch), 3.1.3-6+deb10u2 (buster)
version_map: {"8 jessie": "3.1.1-3+deb8u4", "9 stretch": "3.1.2-1+deb9u5", "10 buster": "3.1.3-6+deb10u2"}
description: "bad flag definition"
date: 2025-01-19T19:32:48+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

The update for rsync announced in ELA 1290-1 introduced a regression
when using the -H option to preserve hard links. Updated packages are
now available to correct this issue.

