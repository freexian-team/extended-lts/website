---
title: "ELA-1200-1 libgsf security update"
package: libgsf
version: 1.14.41-1+deb9u1 (stretch), 1.14.45-1+deb10u1 (buster)
version_map: {"9 stretch": "1.14.41-1+deb9u1", "10 buster": "1.14.45-1+deb10u1"}
description: "integer overflows"
date: 2024-10-07T01:04:31+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-36474
  - CVE-2024-42415

---

Integer overflows have been fixed in libgsf, the GNOME Project G Structured File Library.

CVE-2024-36474

    directory integer overflow

CVE-2024-42415

    sector allocation table integer overflow

