---
title: "ELA-863-1 cpio security update"
package: cpio
version: 2.11+dfsg-4.1+deb8u4 (jessie), 2.11+dfsg-6+deb9u1 (stretch)
version_map: {"8 jessie": "2.11+dfsg-4.1+deb8u4", "9 stretch": "2.11+dfsg-6+deb9u1"}
description: "improper validation of input"
date: 2023-06-05T01:38:06+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-14866
  - CVE-2021-38185

---

Improper validation of input was fixed in GNU cpio, a program to manage
archives of files.

