---
title: "ELA-1301-1 rails security update"
package: rails
version: 2:4.2.7.1-1+deb9u6 (stretch)
version_map: {"9 stretch": "2:4.2.7.1-1+deb9u6"}
description: "multiple vulnerabilities"
date: 2025-01-24T13:32:31-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-27777
  - CVE-2023-22792
  - CVE-2023-22795
  - CVE-2023-22796
  - CVE-2023-28120

---

Multiple vunerabilities were discovered in rails, the Ruby based server-side
MVC web application framework, which could result in XSS, data disclosure
and open redirect.

CVE-2022-27777

  A XSS Vulnerability in Action View tag helpers which would allow an attacker
  to inject content if able to control input into specific attributes.

CVE-2023-22792

  A regular expression based DoS vulnerability in Action Dispatch. Specially
  crafted cookies, in combination with a specially crafted `X_FORWARDED_HOST`
  header can cause the regular expression engine to enter a state of
  catastrophic backtracking. This can cause the process to use large amounts of
  CPU and memory, leading to a possible DoS vulnerability.

CVE-2023-22795

  A regular expression based DoS vulnerability in Action Dispatch related to the
  If-None-Match header. A specially crafted HTTP If-None-Match header can cause
  the regular expression engine to enter a state of catastrophic backtracking,
  when on a version of Ruby below 3.2.0. This can cause the process to use large
  amounts of CPU and memory, leading to a possible DoS vulnerability.

CVE-2023-22796

  A regular expression based DoS vulnerability in Active Support. A specially
  crafted string passed to the underscore method can cause the regular
  expression engine to enter a state of catastrophic backtracking. This can
  cause the process to use large amounts of CPU and memory, leading to a
  possible DoS vulnerability.

CVE-2023-28120

  A vulnerability in ActiveSupport if the new bytesplice method is called on a
  SafeBuffer with untrusted user input.
