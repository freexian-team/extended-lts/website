---
title: "ELA-211-1 wget security update"
package: wget
version: 
distribution: "Debian 7 Wheezy"
description: "race condition"
date: 2020-01-30T13:58:28+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-7098

---

An issue has been found in wget, a tool to retrieve files from the web.
A race condition might occur as files rejected by an access list are kept
on the disk for the duration of a HTTP connection.
