---
title: "ELA-166-1 libpng security update"
package: libpng
version: 1.2.49-1+deb7u3
distribution: "Debian 7 Wheezy"
description: "fix for null pointer dereference"
date: 2019-09-21T18:45:51+02:00
draft: false
type: updates
cvelist:
  - CVE-2016-10087

---

Patrick Keshishian found a null pointer dereference in a function of libpng, a library to handle PNG files.
