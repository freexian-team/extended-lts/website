---
title: "ELA-1070-1 libcaca security update"
package: libcaca
version: 0.99.beta19-2+deb8u3 (jessie), 0.99.beta19-2.2+deb9u3 (stretch)
version_map: {"8 jessie": "0.99.beta19-2+deb8u3", "9 stretch": "0.99.beta19-2.2+deb9u3"}
description: "heap buffer overflow"
date: 2024-04-07T10:48:47+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-30498
  - CVE-2021-30499

---

Two issues have been found in libcaca, a colour ASCII art library.
Both are related to heap buffer overflow, which might lead to memory
corruption.
