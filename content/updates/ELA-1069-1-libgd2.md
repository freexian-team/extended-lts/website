---
title: "ELA-1069-1 libgd2 security update"
package: libgd2
version: 2.1.0-5+deb8u15 (jessie), 2.2.4-2+deb9u6 (stretch)
version_map: {"8 jessie": "2.1.0-5+deb8u15", "9 stretch": "2.2.4-2+deb9u6"}
description: "out-fo-bounds read and NULL pointer dereference"
date: 2024-04-07T01:44:26+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-14553
  - CVE-2021-38115
  - CVE-2021-40812

---

Several issues have been found in libgd2, a GD Graphics Library.
They are related to out-of-bounds reads or NULL pointer derefence allowing
denial of service attacks.
