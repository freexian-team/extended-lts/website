---
title: "ELA-791-1 python-django security update"
package: python-django
version: 1.7.11-1+deb8u16 (jessie), 1:1.10.7-2+deb9u19 (stretch)
version_map: {"8 jessie": "1.7.11-1+deb8u16", "9 stretch": "1:1.10.7-2+deb9u19"}
description: "denial of service vulnerability"
date: 2023-02-01T13:26:35-08:00
draft: false
cvelist:
  - CVE-2023-23969
---

It was discovered that there was a potential Denial of Service (DoS)
vulnerability in Django, a popular Python-based web development framework.

Parsed values of the Accept-Language HTTP headers are cached by Django order to
avoid repetitive parsing. This could have led to a potential denial-of-service
attack via excessive memory usage if the raw value of Accept-Language headers
was very large.

Accept-Language headers are now limited to a maximum length specifically in
order to avoid this issue.
