---
title: "ELA-589-1 libgc security update"
package: libgc
version: 1:7.2d-6.4+deb8u1
distribution: "Debian 8 jessie"
description: "integer overflow"
date: 2022-03-30T23:24:03+02:00
draft: false
type: updates
cvelist:
  - CVE-2016-9427

---

libgc, a conservative garbage collector, is vulnerable to integer
overflows in multiple places. In some cases, when asked to allocate a huge
quantity of memory, instead of failing the request, it will return a
pointer to a small amount of memory possibly tricking the application into
a buffer overwrite.

