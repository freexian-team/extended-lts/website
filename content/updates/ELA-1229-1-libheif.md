---
title: "ELA-1229-1 libheif security update"
package: libheif
version: 1.3.2-2+deb10u3 (buster)
version_map: {"10 buster": "1.3.2-2+deb10u3"}
description: "denial of service"
date: 2024-11-05T13:21:10-08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-0996
---

There was a vulnerability in the strided image parsing code in
[`libheif`](https://github.com/strukturag/libheif), a decoder/encoder for the
HEIF and AVIF image formats.

An attacker could have exploited this through a crafted image file to cause a
buffer overflow in linear memory during a `memcpy` call.
