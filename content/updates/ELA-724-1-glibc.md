---
title: "ELA-724-1 glibc security update"
package: glibc
version: 2.24-11+deb9u5 (stretch)
version_map: {"9 stretch": "2.24-11+deb9u5"}
description: "multiple vulnerabilities"
date: 2022-11-07T12:32:37+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-10228
  - CVE-2017-12132
  - CVE-2018-6485
  - CVE-2018-6551
  - CVE-2018-1000001
  - CVE-2019-9169
  - CVE-2019-19126
  - CVE-2019-25013
  - CVE-2020-1752
  - CVE-2020-10029
  - CVE-2020-27618
  - CVE-2021-3326
  - CVE-2021-3999
  - CVE-2021-33574
  - CVE-2021-35942
  - CVE-2022-23218
  - CVE-2022-23219

---

This update fixes a significant number of minor to important vulnerabilities in
glibc.

CVE-2016-10228

    The iconv program in the GNU C Library, when invoked with multiple suffixes
    in the destination encoding (TRANSLATE or IGNORE) along with the -c option,
    enters an infinite loop when processing invalid multi-byte input sequences,
    leading to a denial of service.

CVE-2017-12132

    The DNS stub resolver in the GNU C Library, when EDNS support is enabled,
    will solicit large UDP responses from name servers, potentially simplifying
    off-path DNS spoofing attacks due to IP fragmentation.

CVE-2018-6485

    An integer overflow in the implementation of the posix_memalign in memalign
    functions in the GNU C Library could cause these functions to return a
    pointer to a heap area that is too small, potentially leading to heap
    corruption.

CVE-2018-6551

    The malloc implementation in the GNU C Library on powerpc and i386
    did not properly handle malloc calls with arguments close to SIZE_MAX
    and could return a pointer to a heap region that is smaller than
    requested, eventually leading to heap corruption.

CVE-2018-1000001

    In glibc is confusion in the usage of getcwd() by realpath() which can be
    used to write before the destination buffer leading to a buffer underflow
    and potential code execution.

CVE-2019-19126

    On the x86-64 architecture, the GNU C Library fails to ignore the
    LD_PREFER_MAP_32BIT_EXEC environment variable during program execution
    after a security transition, allowing local attackers to restrict the
    possible mapping addresses for loaded libraries and thus bypass ASLR for
    a setuid program.

CVE-2019-25013

    The iconv feature in the GNU C Library, when processing invalid multi-byte
    input sequences in the EUC-KR encoding, may have a buffer over-read.

CVE-2019-9169

    In the GNU C Library, proceed_next_node in posix/regexec.c has a heap-based
    buffer over-read via an attempted case-insensitive regular-expression match.

CVE-2020-1752

    A use-after-free vulnerability in glibc was found in the way the tilde
    expansion was carried out.  Directory paths containing an initial tilde
    followed by a valid username were affected by this issue. A local attacker
    could exploit this flaw by creating a specially crafted path that, when
    processed by the glob function, would potentially lead to arbitrary code
    execution.

CVE-2020-10029

    The GNU C Library could overflow an on-stack buffer during range
    reduction if an input to an 80-bit long double function contains a
    non-canonical bit pattern, a seen when passing a
    0x5d414141414141410000 value to sinl on x86 targets. This is related
    to sysdeps/ieee754/ldbl-96/e_rem_pio2l.c.

CVE-2020-27618

    The iconv function in the GNU C Library, when processing invalid
    multi-byte input sequences in IBM1364, IBM1371, IBM1388, IBM1390,
    and IBM1399 encodings, fails to advance the input state, which could
    lead to an infinite loop in applications, resulting in a denial of
    service, a different vulnerability from CVE-2016-10228.

CVE-2021-3326

    The iconv function in the GNU C Library, when processing invalid
    input sequences in the ISO-2022-JP-3 encoding, fails an assertion in
    the code path and aborts the program, potentially resulting in a
    denial of service.

CVE-2021-3999

    A flaw was found in glibc. An off-by-one buffer overflow and underflow
    in getcwd() may lead to memory corruption when the size of the buffer
    is exactly 1. A local attacker who can control the input buffer and
    size passed to getcwd() in a setuid program could use this flaw to
    potentially execute arbitrary code and escalate their privileges on
    the system.

CVE-2021-33574

    The mq_notify function in the GNU C Library has a use-after-free. It
    may use the notification thread attributes object (passed through
    its struct sigevent parameter) after it has been freed by the
    caller, leading to a denial of service (application crash) or
    possibly unspecified other impact.

CVE-2021-35942

    The wordexp function in the GNU C Library may crash or read
    arbitrary memory in parse_param (in posix/wordexp.c) when called
    with an untrusted, crafted pattern, potentially resulting in a
    denial of service or disclosure of information. This occurs because
    atoi was used but strtoul should have been used to ensure correct
    calculations.

CVE-2022-23218

    The deprecated compatibility function svcunix_create in the sunrpc
    module of the GNU C Library copies its path argument on the stack
    without validating its length, which may result in a buffer
    overflow, potentially resulting in a denial of service or (if an
    application is not built with a stack protector enabled) arbitrary
    code execution.

CVE-2022-23219

    The deprecated compatibility function clnt_create in the sunrpc
    module of the GNU C Library copies its hostname argument on the
    stack without validating its length, which may result in a buffer
    overflow, potentially resulting in a denial of service or (if an
    application is not built with a stack protector enabled) arbitrary
    code execution.
