---
title: "ELA-387-1 imagemagick security update"
package: imagemagick
version: 8:6.8.9.9-5+deb8u23
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-03-21T22:46:02+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-25666
  - CVE-2020-25675
  - CVE-2020-25676
  - CVE-2020-27754
  - CVE-2020-27757
  - CVE-2020-27758
  - CVE-2020-27759
  - CVE-2020-27761
  - CVE-2020-27762
  - CVE-2020-27764
  - CVE-2020-27766
  - CVE-2020-27767
  - CVE-2020-27768
  - CVE-2020-27769
  - CVE-2020-27770
  - CVE-2020-27771
  - CVE-2020-27772
  - CVE-2020-27774
  - CVE-2020-27775
  - CVE-2021-20176
  - CVE-2021-20241
  - CVE-2021-20244
  - CVE-2021-20246

---

Multiple security vulnerabilities were fixed in Imagemagick. Missing or
incomplete input sanitising may lead to undefined behavior which can result in
denial of service (application crash) or other unspecified impact.
