---
title: "ELA-525-2 nss regression update"
package: nss
version: 2:3.26-1+debu8u15
distribution: "Debian 8 jessie"
description: "regression update"
date: 2021-12-08T04:46:08+05:30
draft: false
type: updates
cvelist:

---

ELA-525-1 was rolled out, fixing CVE-2021-43527 in nss, but that
lead to a regression, preventing SSL connections in Chromium. The
complete bug report could be found here:
https://bugs.debian.org/1001219.
