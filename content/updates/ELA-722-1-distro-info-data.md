---
title: "ELA-722-1 distro-info-data database update"
package: distro-info-data
version: 0.36~bpo8+2 (jessie), 0.41+deb10u2~bpo9+2 (stretch)
version_map: {"8 jessie": "0.36~bpo8+2", "9 stretch": "0.41+deb10u2~bpo9+2"}
description: "non-security-related database update"
date: 2022-10-31T11:52:04+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

This is a routine update of the distro-info-data database for Debian ELTS users.

It includes a correction to some historical data, and adds newer Debian
and Ubuntu releases up to the current date.
