---
title: "ELA-492-1 tiff security update"
package: tiff
version: 4.0.3-12.3+deb8u12
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2021-10-03T05:04:29+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-19131
  - CVE-2020-19144

---

Two security issues were found in TIFF, a widely used
format for storing image data, as follows:

CVE-2020-19131

    Buffer Overflow in LibTiff allows attackers to cause
    a denial of service via the "invertImage()" function
    in the component "tiffcrop".

CVE-2020-19144

    Buffer Overflow in LibTiff allows attackers to cause
    a denial of service via the 'in _TIFFmemcpy' funtion
    in the component 'tif_unix.c'.
