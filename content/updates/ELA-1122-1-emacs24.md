---
title: "ELA-1122-1 emacs24 security update"
package: emacs24
version: 24.4+1-5+deb8u5 (jessie), 24.5+1-11+deb9u5 (stretch)
version_map: {"8 jessie": "24.4+1-5+deb8u5", "9 stretch": "24.5+1-11+deb9u5"}
description: "arbitrary code execution"
date: 2024-07-05T10:00:21+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-39331

---

A vulnerability was discovered in GNU Emacs, the extensible, customisable,
self-documenting display editor.

The org-link-expand-abbrev function expanded a %(...) link abbrev even when
the abbrev specified an unsafe function, such as shell-command-to-string.
This could lead to arbitrary code execution as soon as an Org-mode format file
was opened.

