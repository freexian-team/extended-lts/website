---
title: "ELA-74-1 sqlite3 security update"
package: sqlite3
version: 3.7.13-1+deb7u5
distribution: "Debian 7 Wheezy"
description: "arbitrary code execution"
date: 2019-01-09T14:33:43+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-2518
  - CVE-2018-8740
  - CVE-2018-20346

---

CVE-2018-8740

    Sqlite3 is vulnerable to a NULL pointer dereference when using databases
    that have been corrupted with 'CREATE TABLE AS' statements. An attacker
    could exploit this with a crafted database file to trigger a crash and
    resulting denial of service.

CVE-2018-20346

    An attacker who is able to run arbitrary SQL statements could use this flaw
    to corrupt the internal databases when the FTS3 extension is enabled, which
    can lead to arbitrary code execution as the user running sqlite.

CVE-2017-2518

    A use-after-free vulnerability may allow remote attackers to execute
    arbitrary code or cause a denial of service (buffer overflow and
    application crash) via a crafted SQL statement.

