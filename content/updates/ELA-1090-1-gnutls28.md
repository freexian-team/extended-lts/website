---
title: "ELA-1090-1 gnutls28 security update"
package: gnutls28
version: 3.5.8-5+deb9u7 (stretch)
version_map: {"9 stretch": "3.5.8-5+deb9u7"}
description: "denial of service"
date: 2024-05-10T14:44:26+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-4209

---

A NULL pointer dereference flaw was found in GnuTLS, a library implementing the
TLS and SSL protocols.  As Nettle's hash update functions internally call
memcpy, providing zero-length input may cause undefined behavior. This flaw
possibly leads to a denial of service after authentication.
