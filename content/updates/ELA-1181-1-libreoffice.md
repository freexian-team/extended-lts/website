---
title: "ELA-1181-1 libreoffice security update"
package: libreoffice
version: 1:6.1.5-3+deb9u4 (stretch), 1:6.1.5-3+deb10u13 (buster)
version_map: {"9 stretch": "1:6.1.5-3+deb9u4", "10 buster": "1:6.1.5-3+deb10u13"}
description: "potential malicious code execution"
date: 2024-09-17T10:21:15Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-6472

---

libreoffice a popular office productivity software suite, was vulnerable.

Certificate Validation user interface in LibreOffice allowed a potential vulnerability.
Signed macros are scripts that have been digitally signed by the developer
using a cryptographic signature.
When a document with a signed macro is opened a warning is displayed by LibreOffice
before the macro is executed.

Previously, if verification failed the user could fail to understand the failure
and may choose to enable the macros anyway.
