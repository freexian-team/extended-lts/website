---
title: "ELA-1132-1 php-horde-mime-viewer security update"
package: php-horde-mime-viewer
version: 2.0.7-2+deb8u1 (jessie)
version_map: {"8 jessie": "2.0.7-2+deb8u1"}
description: "XSS vulnerability"
date: 2024-07-17T12:02:03+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-26874
---

A Cross-Site Scripting (XSS) vulnerability was discovered in
php-horde-mime-viewer, a PHP library for parsing and displaying email messages
encoded in the MIME (or "Multipurpose Internet Mail Extensions") format.
