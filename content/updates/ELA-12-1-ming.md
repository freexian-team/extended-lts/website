---
title: "ELA-12-1 ming security update"
package: ming
version: 1:0.4.4-1.1+deb7u10
distribution: "Debian 7 Wheezy"
description: "several heap buffer overflow vulnerabilities"
date: 2018-07-01T14:25:47-04:00
draft: false
type: updates
cvelist:
  - CVE-2018-11226
  - CVE-2018-11225
  - CVE-2018-11100
  - CVE-2018-11095
---

Multiple vulnerabilities have been discovered in ming:

* CVE-2018-11226

    The getString function in decompile.c in libming through 0.4.8 is vulnerable
    to a heap buffer overflow. This vulnerability might be triggered by remote
    attackers to cause a denial of service (buffer over-read and application
    crash) via a crafted SWF file.

* CVE-2018-11225

    The dcputs function in decompile.c in libming through 0.4.8 is vulnerable
    to a NULL pointer dereference. This vulnerability might be triggered by
    remote attackers to cause a denial of service (NULL pointer dereference and
    application crash) via a crafted SWF file.

* CVE-2018-11100

    The decompileSETTARGET function in decompile.c in libming through 0.4.8 is
    vulnerable to a heap buffer overflow. This vulnerability might be triggered
    by remote attackers to cause a denial of service (buffer over-read and
    application crash) via a crafted SWF file.

* CVE-2018-11095

    The decompileJUMP function in decompile.c in libming through 0.4.8 is
    vulnerable to a heap buffer overflow. This vulnerability might be triggered
    by remote attackers to cause a denial of service (buffer over-read and
    application crash) via a crafted SWF file.
