---
title: "ELA-727-1 ffmpeg security update"
package: ffmpeg
version: 7:3.2.19-0+deb9u1 (stretch)
version_map: {"9 stretch": "7:3.2.19-0+deb9u1"}
description: "several vulnerabilities"
date: 2022-11-09T09:23:27+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-21697

---

Several vulnerabilities have been discovered in the FFmpeg multimedia
framework, which could result in denial of service or potentially the
execution of arbitrary code if malformed files/streams are processed.
