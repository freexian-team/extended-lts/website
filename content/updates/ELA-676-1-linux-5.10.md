---
title: "ELA-676-1 linux-5.10 security update"
package: linux-5.10
version: 5.10.136-1~deb9u1 (stretch)
version_map: {"9 stretch": "5.10.136-1~deb9u1"}
description: "linux kernel update"
date: 2022-09-09T10:38:16+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-2585
  - CVE-2022-2586
  - CVE-2022-2588
  - CVE-2022-23816
  - CVE-2022-26373
  - CVE-2022-29900
  - CVE-2022-29901
  - CVE-2022-36879
  - CVE-2022-36946

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2022-2585

    A use-after-free flaw in the implementation of POSIX CPU timers may
    result in denial of service or in local privilege escalation.

CVE-2022-2586

    A use-after-free in the Netfilter subsystem may result in local
    privilege escalation for a user with the CAP_NET_ADMIN capability in
    any user or network namespace.

CVE-2022-2588

    Zhenpeng Lin discovered a use-after-free flaw in the cls_route
    filter implementation which may result in local privilege escalation
    for a user with the CAP_NET_ADMIN capability in any user or network
    namespace.

CVE-2022-26373

    It was discovered that on certain processors with Intel's Enhanced
    Indirect Branch Restricted Speculation (eIBRS) capabilities there
    are exceptions to the documented properties in some situations,
    which may result in information disclosure.

> Intel's explanation of the issue can be found at [the Intel advisory](https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/post-barrier-return-stack-buffer-predictions.html)

CVE-2022-29900

    Johannes Wikner and Kaveh Razavi reported that for AMD/Hygon
    processors, mis-trained branch predictions for return instructions
    may allow arbitrary speculative code execution under certain
    microarchitecture-dependent conditions.

> A list of affected AMD CPU types can be found at [the AMD bulletin](https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-1037)

CVE-2022-29901

    Johannes Wikner and Kaveh Razavi reported that for Intel processors
    (Intel Core generation 6, 7 and 8), protections against speculative
    branch target injection attacks were insufficient in some
    circumstances, which may allow arbitrary speculative code execution
    under certain microarchitecture-dependent conditions.

> More information can be found at [the Intel advisory](https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html)

CVE-2022-36879

    A flaw was discovered in xfrm_expand_policies in the xfrm subsystem
    which can cause a reference count to be dropped twice.

CVE-2022-36946

    Domingo Dirutigliano and Nicola Guerrera reported a memory
    corruption flaw in the Netfilter subsystem which may result in
    denial of service.
