---
title: "ELA-1274-1 astropy bugfix update"
package: astropy
version: 3.1.2-2+deb10u2 (buster)
version_map: {"10 buster": "3.1.2-2+deb10u2"}
description: "bugfix update"
date: 2024-12-20T08:12:54+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

Due to an issue unrelated to the DLA changes, the DLA-3803-1 update of astropy (an Astronomy package for Python) containing the CVE-2023-41334 fix had bever been successfully built.
