---
title: "ELA-17-1 linux security update"
package: linux
version: 3.16.57-2~deb7u2
distribution: "Debian 7 Wheezy"
description: "linux kernel 3.16 security update"
date: 2018-07-19T17:49:07+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-5715
  - CVE-2017-5753
  - CVE-2018-1066
  - CVE-2018-1093
  - CVE-2018-1130
  - CVE-2018-3665
  - CVE-2018-5814
  - CVE-2018-9422
  - CVE-2018-10853
  - CVE-2018-10940
  - CVE-2018-11506
  - CVE-2018-12233
  - CVE-2018-1000204
---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2017-5715

    Multiple researchers have discovered a vulnerability in various
    processors supporting speculative execution, enabling an attacker
    controlling an unprivileged process to read memory from arbitrary
    addresses, including from the kernel and all other processes
    running on the system.

    This specific attack has been named Spectre variant 2 (branch
    target injection) and is mitigated for the x86 architecture (amd64
    and i386) by using new microcoded features.

    This mitigation requires an update to the processor's microcode,
    which is non-free. For recent Intel processors, this is included
    in the intel-microcode package from version 3.20180703.2~bpo8+1~deb7u1.
    For other processors, it may be included in an update to the
    system BIOS or UEFI firmware, or in a later update to the
    amd64-microcode package.

    This vulnerability was already mitigated for the x86 architecture
    by the "retpoline" feature.

CVE-2017-5753

    Further instances of code that was vulnerable to Spectre variant 1
    (bounds-check bypass) have been mitigated.

CVE-2018-1066

    Dan Aloni reported to Red Hat that the CIFS client implementation
    would dereference a null pointer if the server sent an invalid
    response during NTLMSSP setup negotiation. This could be used by a
    malicious server for denial of service.

    The previously applied mitigation for this issue was not
    appropriate for Linux 3.16 and has been replaced by an alternate
    fix.

CVE-2018-1093

    Wen Xu reported that a crafted ext4 filesystem image could trigger
    an out-of-bounds read in the ext4_valid_block_bitmap() function. A
    local user able to mount arbitrary filesystems could use this for
    denial of service.

CVE-2018-1130

    The syzbot software found that the DCCP implementation of
    sendmsg() does not check the socket state, potentially leading
    to a null pointer dereference.  A local user could use this to
    cause a denial of service (crash).

CVE-2018-3665

    Multiple researchers have discovered that some Intel x86
    processors can speculatively read floating-point and vector
    registers even when access to those registers is disabled.  The
    Linux kernel's "lazy FPU" feature relies on that access control to
    avoid saving and restoring those registers for tasks that do not
    use them, and was enabled by default on x86 processors that do
    not support the XSAVEOPT instruction.

    If "lazy FPU" is enabled on one of the affected processors, an
    attacker controlling an unprivileged process may be able to read
    sensitive information from other users' processes or the kernel.
    This specifically affects processors based on the "Nehalem" and
    "Westemere" core designs.

    This issue has been mitigated by disabling "lazy FPU" by default
    on all x86 processors that support the FXSAVE and FXRSTOR
    instructions, which includes all processors known to be affected
    and most processors that perform speculative execution.  It can
    also be mitigated by adding the kernel parameter: eagerfpu=on

CVE-2018-5814

    Jakub Jirasek reported race conditions in the USB/IP host driver.
    A malicious client could use this to cause a denial of service
    (crash or memory corruption), and possibly to execute code, on a
    USB/IP server.

CVE-2018-9422

    It was reported that the futex() system call could be used by an
    unprivileged user for privilege escalation.

CVE-2018-10853

    Andy Lutomirski and Mika Penttilä reported that KVM for x86
    processors did not perform a necessary privilege check when
    emulating certain instructions.  This could be used by an
    unprivileged user in a guest VM to escalate their privileges
    within the guest.

CVE-2018-10940

    Dan Carpenter reported that the optical disc driver (cdrom) does
    not correctly validate the parameter to the CDROM_MEDIA_CHANGED
    ioctl.  A user with access to a cdrom device could use this to
    cause a denial of service (crash).

CVE-2018-11506

    Piotr Gabriel Kosinski and Daniel Shapira reported that the
    SCSI optical disc driver (sr) did not allocate a sufficiently
    large buffer for sense data.  A user with access to a SCSI
    optical disc device that can produce more than 64 bytes of
    sense data could use this to cause a denial of service (crash
    or memory corruption), and possibly for privilege escalation.

CVE-2018-12233

    Shankara Pailoor reported that a crafted JFS filesystem image
    could trigger a denial of service (memory corruption).  This
    could possibly also be used for privilege escalation.

CVE-2018-1000204

    The syzbot software found that the SCSI generic driver (sg) would
    in some circumstances allow reading data from uninitialised
    buffers, which could include sensitive information from the kernel
    or other tasks.  However, only privileged users with the
    CAP_SYS_ADMIN or CAP_SYS_RAWIO capability were allowed to do this,
    so this has little or no security impact.
