---
title: "ELA-729-1 libjettison-java security update"
package: libjettison-java
version: 1.4.0-1+deb9u1 (stretch)
version_map: {"9 stretch": "1.4.0-1+deb9u1"}
description: "denial of service"
date: 2022-11-11T13:42:09+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-40149

---

It was discovered that libjettison-java, a collection of StAX parsers and
writers for JSON, was vulnerable to a denial-of-service attack, if the attacker
provided untrusted XML or JSON data.
