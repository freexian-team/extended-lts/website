---
title: "ELA-867-1 vim security update"
package: vim
version: 2:7.4.488-7+deb8u10 (jessie), 2:8.0.0197-4+deb9u10 (stretch)
version_map: {"8 jessie": "2:7.4.488-7+deb8u10", "9 stretch": "2:8.0.0197-4+deb9u10"}
description: "multiple vulnerabilities"
date: 2023-06-12T05:58:04+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-4141
  - CVE-2023-0054
  - CVE-2023-1175
  - CVE-2023-2610

---

Multiple security vulnerabilities have been discovered in vim, an enhanced vi
editor. Buffer overflows and out-of-bounds reads may lead to a
denial-of-service (application crash) or other unspecified impact.
