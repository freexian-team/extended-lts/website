---
title: "ELA-233-1 openjdk-7 security update"
package: openjdk-7
version: 7u261-2.6.22-1~deb7u2
distribution: "Debian 7 Wheezy"
description: "timing attack"
date: 2020-06-08T00:26:18+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-12399

---

The embedded static NSS library, which is required to build the SunEC security
provider, was affected by a vulnerability in the way NSS generated DSA
signatures. A man-in-the-middle attacker could use this flaw during DSA
signature generation to recover the private key. Erring on the side of caution,
OpenJDK 7 was rebuilt against the latest version of NSS in Wheezy which
provides a solution for this problem.
