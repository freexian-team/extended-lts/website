---
title: "ELA-1191-1 sqlite3 security update"
package: sqlite3
version: 3.27.2-3+deb10u3 (buster)
version_map: {"10 buster": "3.27.2-3+deb10u3"}
description: "multiple vulnerabilities"
date: 2024-09-30T23:57:48+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-19244
  - CVE-2021-36690
  - CVE-2023-7104

---

Multiple vulnerabilities have been fixed in the SQLite database.

CVE-2019-19244

    Mishandling of sub-select that uses both DISTINCT and window functions, and also has certain ORDER BY usage

CVE-2021-36690

    Expert extension segfault

CVE-2023-7104

    Session extension buffer overread

