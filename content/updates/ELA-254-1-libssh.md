---
title: "ELA-254-1 libssh security update"
package: libssh
version: 0.6.3-4+deb8u5
distribution: "Debian 8 jessie"
description: "NULL pointer dereference"
date: 2020-07-31T00:33:20+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-16135

---

The code in src/sftpserver.c did not verify the validity of certain pointers
and expected them to be valid. A NULL pointer dereference could have been
occured that typically causes a crash and thus a denial-of-service.
