---
title: "ELA-72-1 jasper security update"
package: jasper
version: 1.900.1-13+deb7u8
distribution: "Debian 7 Wheezy"
description: "buffer overflows"
date: 2019-01-03T18:55:30+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-19540
  - CVE-2018-19541
  - CVE-2018-20570
  - CVE-2018-20584
  - CVE-2018-20622
---

Several flaws were corrected in Jasper, a JPEG 2000 image library. Heap-based
buffer overflows may lead to memory corruption, the exposure of sensitive
information or the execution of arbitrary code.
