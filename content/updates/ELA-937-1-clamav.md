---
title: "ELA-937-1 clamav security update"
package: clamav
version: 0.103.9+dfsg-0+deb8u1 (jessie), 0.103.9+dfsg-0+deb9u1 (stretch)
version_map: {"8 jessie": "0.103.9+dfsg-0+deb8u1", "9 stretch": "0.103.9+dfsg-0+deb9u1"}
description: "dos; new upstream version"
date: 2023-08-28T16:02:20+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-20197

---

A vulnerability in the filesystem image parser for Hierarchical File
System Plus (HFS+) of ClamAV, an anti-virus utility for Unix, could
allow an unauthenticated, remote attacker to cause a denial of service
(DoS) condition on an affected device.
