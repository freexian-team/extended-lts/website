---
title: "ELA-1230-1 context bugfix update"
package: context
version: 2018.04.04.20181118-1+deb10u1 (buster)
version_map: {"10 buster": "2018.04.04.20181118-1+deb10u1"}
description: "regression fix"
date: 2024-11-05T21:43:40Z
draft: false
type: updates
tags:
- update
cvelist:

---

The CVE-2023-32700 fix for the texlive-bin package, released for Debian 10
"buster" as DLA-3427-1, introduced a regression in context, a general-purpose
document processor. The DLA-3427-1 update broke the context binary package
installation process.

This regression update corrects the issue, fixing the context package's mtxrun script
