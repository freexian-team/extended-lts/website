---
title: "ELA-999-1 open-vm-tools security update"
package: open-vm-tools
version: 10.1.5-5055683-4+deb9u6 (jessie), 2:10.1.5-5055683-4+deb9u6 (stretch)
version_map: {"8 jessie": "10.1.5-5055683-4+deb9u6", "9 stretch": "2:10.1.5-5055683-4+deb9u6"}
description: "multiple vulnerabilities"
date: 2023-11-14T19:49:26Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-34058
  - CVE-2023-34059

---

The Open Virtual Machine Tools (open-vm-tools) project is an open source
implementation of VMware Tools. It is a suite of virtualization utilities and
drivers to improve the functionality, user experience and administration of
VMware virtual machines.

CVE-2023-34058:

	A file descriptor hijack vulnerability was found in the `vmware-user-suid-wrapper`
	command. A malicious actor with non-root privileges might have been able
	to hijack the `block` file descriptor. Compared to the most recent upstream version,
	the `uinput` file descriptor hijack vulnerability was not present (this file descriptor
	was added latter for supporting Wayland).
 
CVE-2023-34059:

	A SAML Token Signature Bypass vulnerability was found in VGAUTH component.
    A malicious actor that has been granted Guest Operation Privileges
    in a target virtual machine might have been able to
    elevate their privileges if that target
    virtual machine has been assigned a more privileged Guest Alias.

This update fixes CVE-2023-34058 and CVE-2023-34059 for Stretch, but fixes only
CVE-2023-34058 for Jessie. Indeed, the vulnerable code (VGAUTH component) was introduced
later in upstream version 9.10.0, and thus Jessie was not vulnerable to the attack exposed
in CVE-2023-34059.
