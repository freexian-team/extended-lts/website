---
title: "ELA-1321-1 dcmtk security update"
package: dcmtk
version: 3.6.1~20160216-4.1+deb9u1 (stretch), 3.6.4-2.1+deb10u2 (buster)
version_map: {"9 stretch": "3.6.1~20160216-4.1+deb9u1", "10 buster": "3.6.4-2.1+deb10u2"}
description: "improper array index validation"
date: 2025-02-12T23:50:44+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-47796
  - CVE-2024-52333

---

Two cases of improper array index validation have been fixed in DCMTK, a collection of libraries and applications implementing large parts the DICOM standard for medical images.

Additionally, a regression introduced in the previous update has been fixed.
