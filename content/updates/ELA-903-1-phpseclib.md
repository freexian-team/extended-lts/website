---
title: "ELA-903-1 phpseclib security update"
package: phpseclib
version: 1.0.19-1~deb9u1 (stretch)
version_map: {"9 stretch": "1.0.19-1~deb9u1"}
description: "improper verification of cryptographic signature"
date: 2023-07-27T21:09:45Z
draft: false
type: updates
tags:
- update
cvelist: 
- CVE-2021-30130

---

The PHP Secure Communications Library is a fully PKCS#1 (v2.1) compliant RSA, DES, 3DES, RC4, Rijndael, AES, Blowfish, Twofish, SSH-1, SSH-2, SFTP, and X.509 implementation. This library mishandled RSA PKCS#1 v1.5 signature verification.
