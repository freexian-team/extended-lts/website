---
title: "ELA-64-1 suricata security update"
package: suricata
version: 1.2.1-2+deb7u3
distribution: "Debian 7 Wheezy"
description: "denial-of-service"
date: 2018-12-03T15:19:31+01:00
draft: false
type: updates
cvelist:
  - CVE-2015-0928
  - CVE-2015-8954
  - CVE-2018-6794
  - TEMP-0000000-C04FE8

---

CVE-2015-0928

    A NULL pointer dereference allows remote attackers to cause a
    denial-of-service by specially crafted network traffic.

CVE-2015-8954

    The MemcmpLowercase function in Suricata improperly excludes the first
    byte from comparisons, which might allow remote attackers to bypass
    intrusion-prevention functionality via a crafted HTTP request.

CVE-2018-6794

    Suricata is prone to an HTTP detection bypass vulnerability
    in detect.c and stream-tcp.c. If a malicious server breaks a normal TCP
    flow and sends data before the 3-way handshake is complete, then the data
    sent by the malicious server will be accepted by web clients such as a
    web browser or Linux CLI utilities, but ignored by Suricata IDS
    signatures. This mostly affects IDS signatures for the HTTP protocol and
    TCP stream content; signatures for TCP packets will inspect such network
    traffic as usual.

TEMP-0000000-C04FE8 (no CVE assigned yet)

    If memory allocation fails and Suricata runs out of memory, a flaw in the
    DCERP parser may lead to a denial-of-service (application crash).
