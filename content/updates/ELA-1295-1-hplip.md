---
title: "ELA-1295-1 hplip security update"
package: hplip
version: 3.16.11+repack0-3+deb9u1 (stretch), 3.18.12+dfsg0-2+deb10u1 (buster)
version_map: {"9 stretch": "3.16.11+repack0-3+deb9u1", "10 buster": "3.18.12+dfsg0-2+deb10u1"}
description: "MDNS buffer issues"
date: 2025-01-20T15:56:11+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-6923

---

MDNS buffer issues have been fixed in HPLIP, the HP Linux Imaging and Printing system.
