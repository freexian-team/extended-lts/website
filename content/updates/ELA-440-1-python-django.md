---
title: "ELA-440-1 python-django security update"
package: python-django
version: 1.7.11-1+deb8u14
distribution: "Debian 8 Jessie"
description: "multiple vulnerabilities"
date: 2021-06-06T10:50:03+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-33203
  - CVE-2021-33571
---

Two issues were discovered in Django, the Python-based web development
framework:

```
* CVE-2021-33203: Potential directory traversal via admindocs

  Staff members could use the admindocs TemplateDetailView view to
  check the existence of arbitrary files. Additionally, if (and only if) the
  default admindocs templates have been customized by the developers to also
  expose the file contents, then not only the existence but also the file
  contents would have been exposed.

  As a mitigation, path sanitation is now applied and only files within the
  template root directories can be loaded.

  This issue has low severity, according to the Django security policy.

  Thanks to Rasmus Lerchedahl Petersen and Rasmus Wriedt Larsen from the CodeQL
  Python team for the report.

* CVE-2021-33571: Possible indeterminate SSRF, RFI, and LFI attacks
  since validators accepted leading zeros in IPv4 addresses

  URLValidator, validate_ipv4_address(), and validate_ipv46_address() didn't
  prohibit leading zeros in octal literals. If you used such values you could
  suffer from indeterminate SSRF, RFI, and LFI attacks.

  validate_ipv4_address() and validate_ipv46_address() validators were not
  affected on Python 3.9.5+.

  This issue has medium severity, according to the Django security policy.
```
