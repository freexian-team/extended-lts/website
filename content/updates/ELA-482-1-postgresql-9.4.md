---
title: "ELA-482-1 postgresql-9.4 security update"
package: postgresql-9.4
version: 9.4.26-0+deb8u4
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-09-10T13:36:36+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-25694
  - CVE-2020-25695
  - CVE-2020-25696
  - CVE-2021-32027

---

Several vulnerabilities were discovered in PostgreSQL, an
object-relational SQL database.  An attacker could have an opportunity
to complete a MITM attack, execute arbitrary SQL functions under the
identity of a superuser, execute arbitrary code as the operating
system account running psql when connecting to a rogue server, and
corrupt server memory, in some conditions.

* CVE-2020-25694

    If a client application that creates additional database
    connections only reuses the basic connection parameters while
    dropping security-relevant parameters, an opportunity for a
    man-in-the-middle attack, or the ability to observe clear-text
    transmissions, could exist.

* CVE-2020-25695

    An attacker having permission to create non-temporary objects in
    at least one schema can execute arbitrary SQL functions under the
    identity of a superuser.

* CVE-2020-25696

    If an interactive psql session uses \gset when querying a
    compromised server, the attacker can execute arbitrary code as the
    operating system account running psql.

* CVE-2021-32027

    While modifying certain SQL array values, missing bounds checks
    let authenticated database users write arbitrary bytes to a wide
    area of server memory.
