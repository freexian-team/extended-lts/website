---
title: "ELA-497-1 squashfs-tools security update"
package: squashfs-tools
version: 1:4.2+20130409-2+deb8u2
distribution: "Debian 8 jessie"
description: "writing to arbitrary files"
date: 2021-10-21T08:55:16+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-41072

---

Richard Weinberger reported that unsquashfs in squashfs-tools, the tools to create and extract Squashfs filesystems, does not check for duplicate filenames within a directory. An attacker can take advantage of this flaw for writing to arbitrary files to the filesystem if a malformed Squashfs image is processed.
