---
title: "ELA-16-1 tiff security update"
package: tiff
version: 4.0.2-6+deb7u22
distribution: "Debian 7 Wheezy"
description: "denial of service"
date: 2018-07-18T18:03:56+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-10963
---

The TIFFWriteDirectorySec() function in tif_dirwrite.c in LibTIFF allows remote
attackers to cause a denial of service (assertion failure and application
crash) via a crafted file, a different vulnerability than CVE-2017-13726.
