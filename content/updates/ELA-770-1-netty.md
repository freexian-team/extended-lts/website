---
title: "ELA-770-1 netty security update"
package: netty
version: 1:4.1.7-2+deb9u4 (stretch)
version_map: {"9 stretch": "1:4.1.7-2+deb9u4"}
description: "denial of service"
date: 2023-01-16T00:10:34+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-37136
  - CVE-2021-37137
  - CVE-2021-43797
  - CVE-2022-41915

---

Several out-of-memory, stack overflow or HTTP request smuggling vulnerabilities
have been discovered in Netty, a Java NIO client/server socket framework, which
may allow attackers to cause a denial of service or bypass restrictions when
used as a proxy.

