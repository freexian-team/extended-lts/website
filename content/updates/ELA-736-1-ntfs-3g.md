---
title: "ELA-736-1 ntfs-3g security update"
package: ntfs-3g
version: 1:2014.2.15AR.2-1+deb8u7 (jessie), 1:2016.2.22AR.1+dfsg-1+deb9u4 (stretch)
version_map: {"8 jessie": "1:2014.2.15AR.2-1+deb8u7", "9 stretch": "1:2016.2.22AR.1+dfsg-1+deb9u4"}
description: "local privilege escalation"
date: 2022-11-22T00:16:02+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-40284

---

Yuchen Zeng and Eduardo Vela discovered a buffer overflow in NTFS-3G,
a read-write NTFS driver for FUSE, due to incorrect validation of some
of the NTFS metadata. A local user can take advantage of this flaw for
local root privilege escalation.
