---
title: "ELA-750-1 clamav security update"
package: clamav
version: 0.103.7+dfsg-0+deb8u1 (jessie), 0.103.7+dfsg-0+deb9u1 (stretch)
version_map: {"8 jessie": "0.103.7+dfsg-0+deb8u1", "9 stretch": "0.103.7+dfsg-0+deb9u1"}
description: "new upstream version"
date: 2022-12-05T17:08:16+05:30
draft: false
type: updates
tags:
- update
cvelist:

---

ClamAV, an anti-virus utility for Unix, v0.103.7 is a critical patch
release with the following fixes:

* Fix logical signature "Intermediates" feature.
* Relax constraints on slightly malformed zip archives that contain
  overlapping file entries.
