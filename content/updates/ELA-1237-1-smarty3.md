---
title: "ELA-1237-1 smarty3 security update"
package: smarty3
version: 3.1.33+20180830.1.3a78a21f+selfpack1-1+deb10u3 (buster)
version_map: {"10 buster": "3.1.33+20180830.1.3a78a21f+selfpack1-1+deb10u3"}
description: "multiple vulnerabilties"
date: 2024-11-17T12:54:19+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-25047
  - CVE-2023-28447
  - CVE-2024-35226

---

Multiple vulnerabilties were discovered for smarty3, a widely-used PHP
templating engine, which potentially allows an attacker to perform an
XSS (e.g JavaScript or PHP code injection).

CVE-2018-25047

    In Smarty before 3.1.47 and 4.x before 4.2.1,
    libs/plugins/function.mailto.php allows XSS. A web page that uses
    smarty_function_mailto, and that could be parameterized using GET or
    POST input parameters, could allow injection of JavaScript code by a
    user.

CVE-2018-25047 had already been reported as fixed previously via
DLA-3262-1, however it was found the fix was incomplete.

CVE-2023-28447

    In affected versions smarty did not properly escape javascript code.
    An attacker could exploit this vulnerability to execute arbitrary
    JavaScript code in the context of the user's browser session. This
    may lead to unauthorized access to sensitive user data, manipulation
    of the web application's behavior, or unauthorized actions performed
    on behalf of the user. Users are advised to upgrade to either
    version 3.1.48 or to 4.3.1 to resolve this issue. There are no known
    workarounds for this vulnerability.

CVE-2024-35226

    In affected versions template authors could inject php code by
    choosing a malicious file name for an extends-tag. Sites that cannot
    fully trust template authors should update asap. All users are
    advised to update.  There is no patch for users on the v3 branch.
    There are no known workarounds for this vulnerability.
