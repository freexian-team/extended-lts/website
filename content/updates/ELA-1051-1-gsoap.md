---
title: "ELA-1051-1 gsoap security update"
package: gsoap
version: 2.8.35-4+deb9u3 (stretch)
version_map: {"9 stretch": "2.8.35-4+deb9u3"}
description: "multiple vulnerabilities"
date: 2024-02-29T23:51:06Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-13574
  - CVE-2020-13575
  - CVE-2020-13576
  - CVE-2020-13577
  - CVE-2020-13578

---

Multiple vulnerabilities have been fixed in the gSOAP toolkit for
developing Web services.

CVE-2020-13574

    WS-Security plugin denial-of-service

CVE-2020-13575

    WS-Addressing plugin denial-of-service

CVE-2020-13576

    WS-Addressing plugin code execution

CVE-2020-13577

    WS-Security plugin denial-of-service

CVE-2020-13578

    WS-Security plugin denial-of-service

