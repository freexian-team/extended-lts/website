---
title: "ELA-1086-2 emacs25 regression update"
package: emacs25
version: 25.1+1-4+deb9u4 (stretch)
version_map: {"9 stretch": "25.1+1-4+deb9u4"}
description: "regression update"
date: 2024-05-25T09:31:18+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

The previous update to Emacs did not include builds for all supported
architectures.  The same update has been reissued to include all builds.
