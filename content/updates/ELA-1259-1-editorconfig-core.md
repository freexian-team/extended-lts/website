---
title: "ELA-1259-1 editorconfig-core security update"
package: editorconfig-core
version: 0.12.1-1.1+deb10u1 (buster)
version_map: {"10 buster": "0.12.1-1.1+deb10u1"}
description: "buffer overflows"
date: 2024-11-30T13:07:19+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-0341
  - CVE-2024-53849

---

Two issues have been found in editorconfig-core, a coding style indenter
for all editors. Both issues are related to buffer overflows in different
locations.
