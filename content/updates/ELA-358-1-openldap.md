---
title: "ELA-358-1 openldap security update"
package: openldap
version: 2.4.40+dfsg-1+deb8u9
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-02-05T20:56:36+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-36221
  - CVE-2020-36222
  - CVE-2020-36223
  - CVE-2020-36224
  - CVE-2020-36225
  - CVE-2020-36226
  - CVE-2020-36227
  - CVE-2020-36228
  - CVE-2020-36229
  - CVE-2020-36230

---

Several vulnerabilities were discovered in OpenLDAP, a free
implementation of the Lightweight Directory Access Protocol. An
unauthenticated remote attacker can take advantage of these flaws to
cause a denial of service (slapd daemon crash, infinite loops) via
specially crafted packets.
