---
title: "ELA-758-1 exempi security update"
package: exempi
version: 2.4.1-1+deb9u1 (stretch)
version_map: {"9 stretch": "2.4.1-1+deb9u1"}
description: "multiple vulnerabilities"
date: 2022-12-29T08:25:09+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-18233
  - CVE-2017-18234
  - CVE-2017-18235
  - CVE-2017-18236
  - CVE-2017-18237
  - CVE-2017-18238
  - CVE-2018-7728
  - CVE-2018-7729
  - CVE-2018-7730
  - CVE-2018-7731
  - CVE-2018-12648
  - CVE-2021-36045
  - CVE-2021-36046
  - CVE-2021-36047
  - CVE-2021-36048
  - CVE-2021-36050
  - CVE-2021-36051
  - CVE-2021-36052
  - CVE-2021-36053
  - CVE-2021-36054
  - CVE-2021-36055
  - CVE-2021-36056
  - CVE-2021-36057
  - CVE-2021-36058
  - CVE-2021-36064
  - CVE-2021-39847
  - CVE-2021-40716
  - CVE-2021-40732
  - CVE-2021-42528
  - CVE-2021-42529
  - CVE-2021-42530
  - CVE-2021-42531
  - CVE-2021-42532

---

Multiple issues were found in exempi, a library to parse XMP (Extensible
Metadata Platform) metadata, which may result in denial of service (infinite
loops and crashes), memory disclosures, potentially arbitrary code executions in
the context of the current user or other unspecified impacts.


CVE-2017-18233

	An issue was discovered in Exempi before 2.4.4. Integer overflow in the Chunk
	class in XMPFiles/source/FormatSupport/RIFF.cpp allows remote attackers to
	cause a denial of service (infinite loop) via crafted XMP data in a .avi file.


CVE-2017-18234

	An issue was discovered in Exempi before 2.4.3. It allows remote attackers to
	cause a denial of service (invalid memcpy with resultant use-after-free) or
	possibly have unspecified other impact via a .pdf file containing JPEG data,
	related to XMPFiles/source/FormatSupport/ReconcileTIFF.cpp,
	XMPFiles/source/FormatSupport/TIFF_MemoryReader.cpp, and
	XMPFiles/source/FormatSupport/TIFF_Support.hpp

CVE-2017-18235

	An issue was discovered in Exempi before 2.4.3. The VPXChunk class in
	XMPFiles/source/FormatSupport/WEBP_Support.cpp does not ensure nonzero widths
	and heights, which allows remote attackers to cause a denial of service
	(assertion failure and application exit) via a crafted .webp file.

CVE-2017-18236

	An issue was discovered in Exempi before 2.4.4. The
	ASF_Support::ReadHeaderObject function in
	XMPFiles/source/FormatSupport/ASF_Support.cpp allows remote attackers to cause
	a denial of service (infinite loop) via a crafted .asf file.

CVE-2017-18237

	An issue was discovered in Exempi before 2.4.3. The
	PostScript_Support::ConvertToDate function in
	XMPFiles/source/FormatSupport/PostScript_Support.cpp allows remote attackers to
	cause a denial of service (invalid pointer dereference and application crash)
	via a crafted .ps file.

CVE-2017-18238

	An issue was discovered in Exempi before 2.4.4. The
	TradQT_Manager::ParseCachedBoxes function in
	XMPFiles/source/FormatSupport/QuickTime_Support.cpp allows remote attackers to
	cause a denial of service (infinite loop) via crafted XMP data in a .qt file.


CVE-2018-7728

	An issue was discovered in Exempi through 2.4.4.
	XMPFiles/source/FileHandlers/TIFF_Handler.cpp mishandles a case of a zero
	length, leading to a heap-based buffer over-read in the MD5Update() function in
	third-party/zuid/interfaces/MD5.cpp.

CVE-2018-7729

	An issue was discovered in Exempi through 2.4.4. There is a stack-based buffer
	over-read in the PostScript_MetaHandler::ParsePSFile() function in
	XMPFiles/source/FileHandlers/PostScript_Handler.cpp.

CVE-2018-7730

	An issue was discovered in Exempi through 2.4.4. A certain case of a 0xffffffff
	length is mishandled in XMPFiles/source/FormatSupport/PSIR_FileWriter.cpp,
	leading to a heap-based buffer over-read in the
	PSD_MetaHandler::CacheFileData() function.

CVE-2018-7731

	An issue was discovered in Exempi through 2.4.4.
	XMPFiles/source/FormatSupport/WEBP_Support.cpp does not check whether a
	bitstream has a NULL value, leading to a NULL pointer dereference in the
	WEBP::VP8XChunk class.

CVE-2018-12648

	The WEBP::GetLE32 function in XMPFiles/source/FormatSupport/WEBP_Support.hpp in
	Exempi 2.4.5 has a NULL pointer dereference.

CVE-2021-36045

	XMP Toolkit SDK versions 2020.1 (and earlier) are affected by an out-of-bounds
	read vulnerability that could lead to disclosure of arbitrary memory. An
	attacker could leverage this vulnerability to bypass mitigations such as ASLR.
	Exploitation of this issue requires user interaction in that a victim must open
	a malicious file.

CVE-2021-36046 / CVE-2021-36052

	XMP Toolkit version 2020.1 (and earlier) is affected by a memory corruption
	vulnerability, potentially resulting in arbitrary code execution in the context
	of the current user. User interaction is required to exploit this
	vulnerability.

CVE-2021-36047 / CVE-2021-36048

	XMP Toolkit SDK version 2020.1 (and earlier) is affected by an Improper Input
	Validation vulnerability potentially resulting in arbitrary code execution in
	the context of the current user. Exploitation requires user interaction in that
	a victim must open a crafted file.

CVE-2021-36050 / CVE-2021-36051

	XMP Toolkit SDK version 2020.1 (and earlier) is affected by a buffer overflow
	vulnerability potentially resulting in arbitrary code execution in the context
	of the current user. Exploitation requires user interaction in that a victim
	must open a crafted file.

CVE-2021-36053

	XMP Toolkit SDK versions 2020.1 (and earlier) are affected by an out-of-bounds
	read vulnerability that could lead to disclosure of arbitrary memory. An
	attacker could leverage this vulnerability to bypass mitigations such as ASLR.
	Exploitation of this issue requires user interaction in that a victim must open
	a malicious file.

CVE-2021-36054

	XMP Toolkit SDK version 2020.1 (and earlier) is affected by a buffer overflow
	vulnerability potentially resulting in local application denial of service in
	the context of the current user. Exploitation requires user interaction in that
	a victim must open a crafted file.

CVE-2021-36055 / CVE-2021-36056

	XMP Toolkit SDK versions 2020.1 (and earlier) are affected by a use-after-free
	vulnerability that could result in arbitrary code execution in the context of
	the current user. Exploitation of this issue requires user interaction in that
	a victim must open a malicious file.

CVE-2021-36057

	XMP Toolkit SDK version 2020.1 (and earlier) is affected by a write-what-where
	condition vulnerability caused during the application's memory allocation
	process. This may cause the memory management functions to become mismatched
	resulting in local application denial of service in the context of the current
	user.

CVE-2021-36058

	XMP Toolkit SDK version 2020.1 (and earlier) is affected by an Integer Overflow
	vulnerability potentially resulting in application-level denial of service in
	the context of the current user. Exploitation requires user interaction in that
	a victim must open a crafted file.

CVE-2021-36064

	XMP Toolkit version 2020.1 (and earlier) is affected by a Buffer Underflow
	vulnerability which could result in arbitrary code execution in the context of
	the current user. Exploitation of this issue requires user interaction in that
	a victim must open a malicious file.

CVE-2021-39847

	XMP Toolkit SDK version 2020.1 (and earlier) is affected by a stack-based
	buffer overflow vulnerability potentially resulting in arbitrary code execution
	in the context of the current user. Exploitation requires user interaction in
	that a victim must open a crafted file.

CVE-2021-40716

	XMP Toolkit SDK versions 2021.07 (and earlier) are affected by an out-of-bounds
	read vulnerability that could lead to disclosure of sensitive memory. An
	attacker could leverage this vulnerability to bypass mitigations such as ASLR.
	Exploitation of this issue requires user interaction in that a victim must open
	a malicious file.

CVE-2021-40732

	XMP Toolkit version 2020.1 (and earlier) is affected by a null pointer
	dereference vulnerability that could result in leaking data from certain memory
	locations and causing a local denial of service in the context of the current
	user. User interaction is required to exploit this vulnerability in that the
	victim will need to open a specially crafted MXF file.

CVE-2021-42528

	XMP Toolkit 2021.07 (and earlier) is affected by a Null pointer dereference
	vulnerability when parsing a specially crafted file. An unauthenticated
	attacker could leverage this vulnerability to achieve an application
	denial-of-service in the context of the current user. Exploitation of this
	issue requires user interaction in that a victim must open a malicious file.

CVE-2021-42529

	XMP Toolkit SDK version 2021.07 (and earlier) is affected by a stack-based
	buffer overflow vulnerability potentially resulting in arbitrary code execution
	in the context of the current user. Exploitation requires user interaction in
	that a victim must open a crafted file.

CVE-2021-42530

	XMP Toolkit SDK version 2021.07 (and earlier) is affected by a stack-based
	buffer overflow vulnerability potentially resulting in arbitrary code execution
	in the context of the current user. Exploitation requires user interaction in
	that a victim must open a crafted file.

CVE-2021-42531

	XMP Toolkit SDK version 2021.07 (and earlier) is affected by a stack-based
	buffer overflow vulnerability potentially resulting in arbitrary code execution
	in the context of the current user. Exploitation requires user interaction in
	that a victim must open a crafted file.

CVE-2021-42532

	XMP Toolkit SDK version 2021.07 (and earlier) is affected by a stack-based
	buffer overflow vulnerability potentially resulting in arbitrary code execution
	in the context of the current user. Exploitation requires user interaction in
	that a victim must open a crafted file.
