---
title: "ELA-345-1 imagemagick security update"
package: imagemagick
version: 8:6.8.9.9-5+deb8u22
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-01-12T18:23:04+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-19667
  - CVE-2020-25665
  - CVE-2020-25674
  - CVE-2020-27560
  - CVE-2020-27750
  - CVE-2020-27760
  - CVE-2020-27763
  - CVE-2020-27765
  - CVE-2020-27773
  - CVE-2020-29599

---

Several security vulnerabilities were found in ImageMagick, a suite of
image manipulation programs. An attacker could cause denial of service
and execution of arbitrary code when a crafted image file is
processed.

* CVE-2020-19667

    Stack-based buffer overflow and unconditional jump in ReadXPMImage
    in coders/xpm.c

* CVE-2020-25665

    The PALM image coder at coders/palm.c makes an improper call to
    AcquireQuantumMemory() in routine WritePALMImage() because it
    needs to be offset by 256. This can cause a out-of-bounds read
    later on in the routine. This could cause impact to reliability.

* CVE-2020-25674

    WriteOnePNGImage() from coders/png.c (the PNG coder) has a for
    loop with an improper exit condition that can allow an
    out-of-bounds READ via heap-buffer-overflow. This occurs because
    it is possible for the colormap to have less than 256 valid values
    but the loop condition will loop 256 times, attempting to pass
    invalid colormap data to the event logger.

* CVE-2020-27560

    ImageMagick allows Division by Zero in OptimizeLayerFrames in
    MagickCore/layer.c, which may cause a denial of service.

* CVE-2020-27750

    A flaw was found in MagickCore/colorspace-private.h and
    MagickCore/quantum.h. An attacker who submits a crafted file that
    is processedcould trigger undefined behavior in the form of values
    outside the range of type `unsigned char` and math division by
    zero. This would most likely lead to an impact to application
    availability, but could potentially cause other problems related
    to undefined behavior.

* CVE-2020-27760

    In `GammaImage()` of /MagickCore/enhance.c, depending on the
    `gamma` value, it's possible to trigger a divide-by-zero condition
    when a crafted input file is processed by ImageMagick. This could
    lead to an impact to application availability.

* CVE-2020-27763

    A flaw was found in MagickCore/resize.c. An attacker who submits a
    crafted file that is processed by ImageMagick could trigger
    undefined behavior in the form of math division by zero. This
    would most likely lead to an impact to application availability,
    but could potentially cause other problems related to undefined
    behavior.

* CVE-2020-27765

    A flaw was found in MagickCore/segment.c. An attacker who submits
    a crafted file that is processed by ImageMagick could trigger
    undefined behavior in the form of math division by zero. This
    would most likely lead to an impact to application availability,
    but could potentially cause other problems related to undefined
    behavior.

* CVE-2020-27773

    A flaw was found in MagickCore/gem-private.h. An attacker who
    submits a crafted file that is processed by ImageMagick could
    trigger undefined behavior in the form of values outside the range
    of type `unsigned char` or division by zero. This would most
    likely lead to an impact to application availability, but could
    potentially cause other problems related to undefined behavior.

* CVE-2020-29599

    ImageMagick mishandles the -authenticate option, which allows
    setting a password for password-protected PDF files. The
    user-controlled password was not properly escaped/sanitized and it
    was therefore possible to inject additional shell commands via
    coders/pdf.c.
