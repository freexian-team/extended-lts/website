---
title: "ELA-1165-1 systemd security update"
package: systemd
version: 232-25+deb9u17 (stretch), 241-7~deb10u11 (buster)
version_map: {"9 stretch": "232-25+deb9u17", "10 buster": "241-7~deb10u11"}
description: "DNSSEC vulnerabilities"
date: 2024-08-27T19:11:32+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-7008
  - CVE-2023-50387
  - CVE-2023-50868

---

Multiple vulnerabilities have been fixed in systemd, the default init system in Debian, when using systemd-resolved with DNSSEC.
