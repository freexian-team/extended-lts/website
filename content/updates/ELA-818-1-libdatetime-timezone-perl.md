---
title: "ELA-818-1 libdatetime-timezone-perl new timezone database"
package: libdatetime-timezone-perl
version: 1:1.75-2+2023b (jessie), 1:2.09-1+2023b (stretch)
version_map: {"8 jessie": "1:1.75-2+2023b", "9 stretch": "1:2.09-1+2023b"}
description: "update to tzdata 2023b"
date: 2023-03-24T13:30:26+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update includes the changes in tzdata 2023b for the Perl bindings.
