---
title: "ELA-1059-1 pillow security update"
package: pillow
version: 2.6.1-2+deb8u9 (jessie), 4.0.0-4+deb9u5 (stretch)
version_map: {"8 jessie": "2.6.1-2+deb8u9", "9 stretch": "4.0.0-4+deb9u5"}
description: "multiple vulnerabilities"
date: 2024-03-19T13:42:17+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-23437
  - CVE-2022-22817
  - CVE-2023-44271
  - CVE-2023-50447

---

Multiple vulnerabilities were discovered in the Python Imaging Library (PIL),
an image processing library for Python.

### CVE-2021-23437

It was discovered that the getrgb function was vulnerable to a regular
expression denial-of-service attack.

### CVE-2022-22817

A fix for this CVE was announced in advisories DLA-2893-1 and ELA-546-1.  It
was discovered that this fix was incomplete.  This update completes the fix.

### CVE-2023-44271

It was discovered that an overlong text length argument passed to an ImageDraw
instance could cause uncontrollable memory allocation and denial-of-service.

### CVE-2023-50447

It was discovered that PIL.ImageMath.eval could permit arbitrary code
execution via the environment parameter (see also CVE-2022-22817, which
concerned the expression parameter).

