---
title: "ELA-732-1 jackson-databind security update"
package: jackson-databind
version: 2.8.6-1+deb9u11 (stretch)
version_map: {"9 stretch": "2.8.6-1+deb9u11"}
description: "denial of service"
date: 2022-11-13T23:27:14+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-42003
  - CVE-2022-42004

---

Several flaws were discovered in jackson-databind, a fast and powerful JSON
library for Java. A denial of service (resource exhaustion) could occur because
of a missing check in primitive value deserializers to avoid deep wrapper array
nesting, when the UNWRAP_SINGLE_VALUE_ARRAYS feature is enabled.
