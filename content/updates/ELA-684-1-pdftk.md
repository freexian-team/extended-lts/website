---
title: "ELA-684-1 pdftk security update"
package: pdftk
version: 2.02-2+deb8u1 (jessie), 2.02-4+deb9u1 (stretch)
version_map: {"8 jessie": "2.02-2+deb8u1", "9 stretch": "2.02-4+deb9u1"}
description: "infinite loop"
date: 2022-09-23T11:42:25+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-37819

---

It was found that PDFtk, a tool for manipulating PDF documents, was
vulnerable to an infinite loop if a crafted file was processed, which
could result in denial of service.
