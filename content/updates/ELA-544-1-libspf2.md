---
title: "ELA-544-1 libspf2 security update"
package: libspf2
version: 1.2.10-5+deb8u2
distribution: "Debian 8 jessie"
description: "heap-based buffer overflow"
date: 2022-01-21T00:16:01+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-33912
  - CVE-2021-33913

---

Two issues have been found in libspf2, a library for validating mail senders with SPF.
Both issues are related to heap-based buffer overflows.
