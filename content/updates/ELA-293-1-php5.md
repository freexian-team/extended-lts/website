---
title: "ELA-293-1 php5 security update"
package: php5
version: 5.6.40+dfsg-0+deb8u13
distribution: "Debian 8 jessie"
description: "insecure decoding of cookie names"
date: 2020-10-07T15:09:24-04:00
draft: false
type: updates
cvelist:
  - CVE-2020-7070

---

A vulnerability was discovered in PHP, a server-side, HTML-embedded
scripting language.  When PHP is processing incoming HTTP cookie values,
the cookie names are url-decoded. This may lead to cookies with prefixes
like \_\_Host confused with cookies that decode to such prefix, thus
leading to an attacker being able to forge a cookie which is supposed to
be secure.
