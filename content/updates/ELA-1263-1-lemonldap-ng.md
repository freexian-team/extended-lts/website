---
title: "ELA-1263-1 lemonldap-ng security update"
package: lemonldap-ng
version: 2.0.2+ds-7+deb10u11 (buster)
version_map: {"10 buster": "2.0.2+ds-7+deb10u11"}
description: "multiple XSS vulnerabilities"
date: 2024-11-30T22:25:46+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-48933
  - CVE-2024-52947

---

Two Cross-site scripting (XSS) vulnerabilities were discovered in
Lemonldap::NG, an OpenID-Connect, CAS and SAML compatible Web-SSO
system, which could lead to injection of arbitrary scripts or HTML
content.

  * CVE-2024-48933: XSS vulnerability which allows remote attackers to
    inject arbitrary web script or HTML into the login page via a
    username if `userControl` has been set to a non-default value that
    allows special HTML characters.

  * CVE-2024-52947: XSS vulnerability which allows remote attackers to
    inject arbitrary web script or HTML via the `url` parameter of the
    upgrade session confirmation page (`upgradeSession`) if the "Upgrade
    session" plugin has been enabled by an admin.
