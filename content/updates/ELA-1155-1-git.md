---
title: "ELA-1155-1 git security update"
package: git
version: 1:2.1.4-2.1+deb8u14 (jessie), 1:2.11.0-3+deb9u11 (stretch)
version_map: {"8 jessie": "1:2.1.4-2.1+deb8u14", "9 stretch": "1:2.11.0-3+deb9u11"}
description: "multiple vulnerabilities"
date: 2024-08-17T08:15:19+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-1387
  - CVE-2023-25652
  - CVE-2023-25815
  - CVE-2023-29007
  - CVE-2024-32002
  - CVE-2024-32021

---

Multiple vulnerabilities were discovered in git, a fast, scalable and
distributed revision control system.

### CVE-2019-1387

It was possible to bypass the previous check for this vulnerability using
parallel cloning, or the --recurse-submodules option to git-checkout(1).
(applicable to Debian "stretch" only)

### CVE-2023-25652

Feeding specially-crafted input to 'git apply --reject' could overwrite a path
outside the working tree with partially controlled contents, corresponding to
the rejected hunk or hunks from the given patch.

### CVE-2023-25815

Low-privileged users could inject malicious messages into Git's output under
MINGW.

### CVE-2023-29007

A specially-crafted .gitmodules file with submodule URLs longer than 1024
characters could be used to inject arbitrary configuration into
$GIT_DIR/config.

### CVE-2024-32002

Repositories with submodules could be specially-crafted to write hooks into
.git/ which would then be executed during an ongoing clone operation.

### CVE-2024-32004

A specially-crafted local repository could cause the execution of arbitrary
code when cloned by another user.

### CVE-2024-32021

When cloning a local repository that contains symlinks via the filesystem, Git
could have created hardlinks to arbitrary user-readable files on the same
filesystem as the target repository in the objects/ directory.

### CVE-2024-32465

When cloning a local repository obtained from a downloaded archive, hooks in
that repository could be used for arbitrary code execution.

(Updates for Debian "buster" were announced in DLA-3844-1, during the Debian
LTS support period.)
