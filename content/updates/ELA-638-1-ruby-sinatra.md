---
title: "ELA-638-1 ruby-sinatra security update"
package: ruby-sinatra
version: 1.4.7-5+deb9u1 (stretch)
version_map: {"9 stretch": "1.4.7-5+deb9u1"}
description: "file traversal vulnerability"
date: 2022-07-12T10:43:33+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-29970
---

A file traversal vulnerability was discovered in `ruby-sinatra`, a popular web
server often used with Ruby on Rails. We now validate that any expanded paths
match the allowed `public_dir` when serving static files.
