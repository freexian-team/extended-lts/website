---
title: "ELA-920-1 datatables.js security update"
package: datatables.js
version: 1.10.13+dfsg-2+deb9u1 (stretch)
version_map: {"9 stretch": "1.10.13+dfsg-2+deb9u1"}
description: "cross-site scripting (XSS) vulnerability"
date: 2023-08-15T19:46:27+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-23445

---

datatables.js is a jQuery plug-in that makes nice tables from different
data sources.

It was discovered that if an array is passed to the HTML escape entities
function, it would not have its contents escaped.
