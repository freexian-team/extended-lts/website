---
title: "ELA-994-1 distro-info-data database update"
package: distro-info-data
version: 0.36~bpo8+4 (jessie), 0.41+deb10u2~bpo9+4 (stretch)
version_map: {"8 jessie": "0.36~bpo8+4", "9 stretch": "0.41+deb10u2~bpo9+4"}
description: "non-security-related database update"
date: 2023-10-30T15:25:28+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

This is a routine update of the distro-info-data database for Debian LTS
users.

It includes Ubuntu 24.10, and makes some minor updates to older EoL
dates.
