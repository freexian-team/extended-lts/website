---
title: "ELA-1186-1 cups-filters security update"
package: cups-filters
version: 1.11.6-3+deb9u3 (stretch), 1.21.6-5+deb10u2 (buster)
version_map: {"9 stretch": "1.11.6-3+deb9u3", "10 buster": "1.21.6-5+deb10u2"}
description: "missing validation of IPP attributes"
date: 2024-09-29T23:44:59+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-47076
  - CVE-2024-47176

---

Simone Margaritelli reported several vulnerabilities in cups-filters.
Missing validation of IPP attributes returned from an IPP server and
multiple bugs in the cups-browsed component can result in the execution
of arbitrary commands without authentication when a print job is
started.
