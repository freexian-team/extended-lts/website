---
title: "ELA-319-1 libass security update"
package: libass
version: 0.10.2-3+deb8u1
distribution: "Debian 8 jessie"
description: "fixes for wrong memory allocation and illegal read"
date: 2020-11-29T00:02:11+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-7969
  - CVE-2016-7972

---

Two issues have been found in libass, a library for SSA/ASS subtitles rendering.

CVE-2016-7972:
     Fix memory reallocation in the shaper.

CVE-2016-7969:
     Fix mode 0/3 line wrapping equalization in specific cases which could
     result in illegal reads while laying out and shaping text.

