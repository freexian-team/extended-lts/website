---
title: "ELA-1101-1 python-django security update"
package: python-django
version: 1:1.10.7-2+deb9u22 (stretch)
version_map: {"9 stretch": "1:1.10.7-2+deb9u22"}
description: "multiple DoS vulnerabilities"
date: 2024-05-29T09:58:00+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-36053
  - CVE-2023-43665
  - CVE-2024-24680
---

Three vulnerabilities were fixed in `python-django`, a popular Python-based web
development framework:

* CVE-2023-36053: Prevent a potential regular expression denial of service
  (DoS) vulnerability in `EmailValidator` and `URLValidator`. `EmailValidator`
  and `URLValidator` were subject to potential regular expression denial of
  service attack via a very large number of domain name labels of emails and
  URLs.

* CVE-2023-43665: Fix a DoS vulnerability in `django.utils.text.Truncator`.
  Following the fix for CVE-2019-14232, the regular expressions used in the
  implementation of `django.utils.text.Truncator`’s `chars`() and `words`()
  methods were revised and improved. However, these regular expressions still
  exhibited linear backtracking complexity, so when given a very long,
  potentially malformed HTML input, the evaluation would still be slow, leading
  to a potential denial of service vulnerability.

* CVE-2024-24680: Prevent a potential DoS in the `intcomma` template filter.
  The `intcomma` template filter was subject to a potential denial-of-service
  attack when used with very long strings.
