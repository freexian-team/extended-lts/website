---
title: "ELA-899-1 gst-plugins-base1.0 security update"
package: gst-plugins-base1.0
version: 1.4.4-2+deb8u4 (jessie), 1.10.4-1+deb9u3 (stretch)
version_map: {"8 jessie": "1.4.4-2+deb8u4", "9 stretch": "1.10.4-1+deb9u3"}
description: "probably DoS or RCE"
date: 2023-07-25T23:48:57+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-37328

---

Multiple multiple vulnerabilities were discovered in plugins for the
GStreamer media framework and its codecs and demuxers, which may result
in denial of service or potentially the execution of arbitrary code if
a malformed media file is opened.
