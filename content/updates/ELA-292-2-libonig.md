---
title: "ELA-292-2 libonig regression update"
package: libonig
version: 5.9.5-3.2+deb8u6
distribution: "Debian 8 jessie"
description: "revert rejected CVE fix"
date: 2021-08-28T09:58:48-04:00
draft: false
type: updates
cvelist:
  - CVE-2020-26159

---

CVE-2020-26159 was determined to not be a vulnerability and was
rejected.  As a result, the previously applied patch to address
CVE-2020-26159 has been reverted.  The additional fixes which were
included in ELA-292-1 remain in place.
