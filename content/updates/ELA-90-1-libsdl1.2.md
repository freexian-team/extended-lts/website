---
title: "ELA-90-1 libsdl1.2 security update"
package: libsdl1.2
version: 1.2.15-5+deb7u1
distribution: "Debian 7 Wheezy"
description: "heap-based buffer overflows"
date: 2019-03-06T20:43:08+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-7572
  - CVE-2019-7573
  - CVE-2019-7574
  - CVE-2019-7575
  - CVE-2019-7576
  - CVE-2019-7577
  - CVE-2019-7578
  - CVE-2019-7635
  - CVE-2019-7636
  - CVE-2019-7637
  - CVE-2019-7638

---

 Several heap-based buffer overflow vulnerabilities were discovered in the
 Simple DirectMedia Layer library which may lead to information disclosure,
 memory corruption, denial-of-service or other unspecified impact when input
 is processed.

