---
title: "ELA-466-1 libjdom1-java security update"
package: libjdom1-java
version: 1.1.3-1+deb8u1
distribution: "Debian 8 jessie"
description: "XML External Entity (XXE) vulnerability"
date: 2021-08-04T19:22:10+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-33813

---

It was discovered that there was an XML External Entity (XXE) issue in
`libjdom1-java`, a library for reading and manipulating XML documents.
Attackers could have caused a denial of service attack via a specially-crafted
HTTP request.
