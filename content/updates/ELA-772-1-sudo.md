---
title: "ELA-772-1 sudo security update"
package: sudo
version: 1.8.10p3-1+deb8u9 (jessie), 1.8.19p1-2.1+deb9u5 (stretch)
version_map: {"8 jessie": "1.8.10p3-1+deb8u9", "9 stretch": "1.8.19p1-2.1+deb9u5"}
description: "privilege escalation"
date: 2023-01-18T16:33:58+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-22809

---

Matthieu Barjole and Victor Cutillas discovered that sudoedit in sudo, a
program designed to provide limited super user privileges to specific
users, does not properly handle '--' to separate the editor and
arguments from files to edit. A local user permitted to edit certain
files can take advantage of this flaw to edit a file not permitted by
the security policy, resulting in privilege escalation.

