---
title: "ELA-1057-1 inetutils security update"
package: inetutils
version: 2:1.9.2.39.3a460-3+deb8u2 (jessie)
version_map: {"8 jessie": "2:1.9.2.39.3a460-3+deb8u2"}
description: "multiple issues"
date: 2024-03-19T02:08:12+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-0053
  - CVE-2021-40491
  - CVE-2022-39028
  - CVE-2023-40303

---

Multiple vulnerabilities were found in the inetutils package, a collection
of common network programs.

* CVE-2019-0053

    A stack-based overflow is present in the handling of environment variables
    when connecting via the telnet client to remote telnet servers. This issue
    only affects the telnet client — accessible from the CLI or shell — in
    Junos OS. Inbound telnet services are not affected by this issue. 

* CVE-2021-40491

    The ftp client in inetutils does not validate addresses returned by
    PASV/LSPV responses to make sure they match the server address.

* CVE-2022-39028

    telnetd in inetutils has a NULL pointer dereference via 0xff 0xf7 or 0xff
    0xf8. In a typical installation, the telnetd application would crash but
    the telnet service would remain available through inetd. However, if the
    telnetd application has many crashes within a short time interval, the
    telnet service would become unavailable after inetd logs a "telnet/tcp
    server failing (looping), service terminated" error. 

* CVE-2023-40303

    inetutils may allow privilege escalation because of unchecked return values
    of set*id() family functions in ftpd, rcp, rlogin, rsh, rshd, and uucpd.
    This is, for example, relevant if the setuid system call fails when a
    process is trying to drop privileges before letting an ordinary user
    control the activities of the process.
