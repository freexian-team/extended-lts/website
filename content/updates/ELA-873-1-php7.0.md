---
title: "ELA-873-1 php7.0 security update"
package: php7.0
version: 7.0.33-0+deb9u15 (stretch)
version_map: {"9 stretch": "7.0.33-0+deb9u15"}
description: "insufficient randomness"
date: 2023-06-20T19:50:37+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-3247

---

Niels Dossche and Tim Düsterhus discovered that PHP's implementation of the
SOAP HTTP Digest authentication used an insufficient number of random bytes.
This would affect PHP applications that use SOAP with HTTP Digest
authentication against a possibly malicious server over HTTP.
