---
title: "ELA-1109-1 nano security update"
package: nano
version: 2.2.6-3+deb8u1 (jessie), 2.7.4-1+deb9u1 (stretch)
version_map: {"8 jessie": "2.2.6-3+deb8u1", "9 stretch": "2.7.4-1+deb9u1"}
description: "symlink attack"
date: 2024-06-17T14:58:58+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-5742

---

A symlink attack with emergency file saving has been fixed in the text
editor nano.
