---
title: "ELA-1040-1 xorg-server security update"
package: xorg-server
version: 2:1.19.2-1+deb9u18 (stretch)
version_map: {"9 stretch": "2:1.19.2-1+deb9u18"}
description: "privilege escalation"
date: 2024-01-31T22:09:37+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-6816
  - CVE-2024-0229
  - CVE-2024-0408
  - CVE-2024-0409
  - CVE-2024-21885
  - CVE-2024-21886

---

Several vulnerabilities were discovered in the Xorg X server, which may
result in privilege escalation if the X server is running privileged
or denial of service.
