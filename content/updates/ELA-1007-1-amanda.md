---
title: "ELA-1007-1 amanda security update"
package: amanda
version: 1:3.3.9-5+deb9u2 (stretch)
version_map: {"9 stretch": "1:3.3.9-5+deb9u2"}
description: "multiple vulnerabilities"
date: 2023-11-27T19:06:34+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-37703
  - CVE-2022-37705
  - CVE-2023-30577

---

Multiple vulnerabilties have been found in Amanda, a backup system designed to archive many computers on a
network to a single large-capacity tape drive. The vulnerabilties potentially allow local privilege escalation
from the backup user to root or allow leaking information whether a directory exists in the filesystem.

CVE-2022-37703

    In Amanda 3.5.1, an information leak vulnerability was found in the calcsize SUID binary. An attacker can abuse this vulnerability to know if a directory exists or not anywhere in the fs. The binary will use `opendir()` as root directly without checking the path, letting the attacker provide an arbitrary path.


CVE-2022-37705

    A privilege escalation flaw was found in Amanda 3.5.1 in which the backup user can acquire root privileges. The vulnerable component is the runtar SUID program, which is a wrapper to run /usr/bin/tar with specific arguments that are controllable by the attacker. This program mishandles the arguments passed to tar binary.

CVE-2023-30577

    The SUID binary "runtar" can accept the possibly malicious GNU tar options if fed with some non-argument option starting with "--exclude" (say --exclude-vcs). The following option will be accepted as "good" and it could be an option passing some script/binary that would be executed with root permissions.
