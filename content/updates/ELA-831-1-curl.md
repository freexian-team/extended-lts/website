---
title: "ELA-831-1 curl security update"
package: curl
version: 7.38.0-4+deb8u26 (jessie), 7.52.1-5+deb9u19 (stretch)
version_map: {"8 jessie": "7.38.0-4+deb8u26", "9 stretch": "7.52.1-5+deb9u19"}
description: "authentication bypass"
date: 2023-04-10T17:45:15+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-27533
  - CVE-2023-27535
  - CVE-2023-27536
  - CVE-2023-27538

---

Several security vulnerabilities have been found in cURL, an easy-to-use client-side
URL transfer library.

CVE-2023-27533

    A vulnerability in input validation exists in curl during
    communication using the TELNET protocol. It may allow an attacker to pass on
    maliciously crafted user name and "telnet options" during server
    negotiation. The lack of proper input scrubbing allows an attacker to send
    content or perform option negotiation without the application's intent.
    This vulnerability could be exploited if an application allows user input,
    thereby enabling attackers to execute arbitrary code on the system.

CVE-2023-27535

    An authentication bypass vulnerability exists in libcurl in the FTP
    connection reuse feature that can result in wrong credentials being used
    during subsequent transfers. Previously created connections are kept in a
    connection pool for reuse if they match the current setup. However, certain
    FTP settings such as CURLOPT_FTP_ACCOUNT, CURLOPT_FTP_ALTERNATIVE_TO_USER,
    CURLOPT_FTP_SSL_CCC, and CURLOPT_USE_SSL were not included in the
    configuration match checks, causing them to match too easily. This could
    lead to libcurl using the wrong credentials when performing a transfer,
    potentially allowing unauthorized access to sensitive information.

CVE-2023-27536

    An authentication bypass vulnerability exists in libcurl in the
    connection reuse feature which can reuse previously established connections
    with incorrect user permissions due to a failure to check for changes in
    the CURLOPT_GSSAPI_DELEGATION option. This vulnerability affects
    krb5/kerberos/negotiate/GSSAPI transfers and could potentially result in
    unauthorized access to sensitive information. The safest option is to not
    reuse connections if the CURLOPT_GSSAPI_DELEGATION option has been changed.

CVE-2023-27538

    An authentication bypass vulnerability exists in libcurl where it
    reuses a previously established SSH connection despite the fact that an SSH
    option was modified, which should have prevented reuse. libcurl maintains a
    pool of previously used connections to reuse them for subsequent transfers
    if the configurations match. However, two SSH settings were omitted from
    the configuration check, allowing them to match easily, potentially leading
    to the reuse of an inappropriate connection.


