---
title: "ELA-1338-1 nodejs security update"
package: nodejs
version: 10.24.0~dfsg-1~deb10u5 (buster)
version_map: {"10 buster": "10.24.0~dfsg-1~deb10u5"}
description: "memory exhaustion denial of service"
date: 2025-03-02T11:06:01Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2025-23085

---

A vulnerability was fixed in Node.js, a popular JavaScript runtime
implementation.

A memory leak could occur when a remote peer (client) abruptly closes an HTTP/2
socket without sending a GOAWAY notification.
Additionally, the same leak could be triggered if an invalid header is detected
by nghttp2, causing the connection to be terminated by the peer.

This flaw could lead to increased memory consumption and potential denial of service
under certain conditions. This vulnerability affects Node.js HTTP/2 Server users.
