---
title: "ELA-704-1 isc-dhcp security update"
package: isc-dhcp
version: 4.3.1-6+deb8u6 (jessie), 4.3.5-3+deb9u3 (stretch)
version_map: {"8 jessie": "4.3.1-6+deb8u6", "9 stretch": "4.3.5-3+deb9u3"}
description: "denial of service"
date: 2022-10-13T20:19:23+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-2928
  - CVE-2022-2929

---

Several vulnerabilities have been discovered in the ISC DHCP client,
relay and server.

CVE-2022-2928

    It was discovered that the DHCP server does not correctly perform
    option reference counting when configured with "allow leasequery;".
    A remote attacker can take advantage of this flaw to cause a denial
    of service (daemon crash).

CVE-2022-2929

    It was discovered that the DHCP server is prone to a memory leak
    flaw when handling contents of option 81 (fqdn) data received in
    a DHCP packet. A remote attacker can take advantage of this flaw
    to cause DHCP servers to consume resources, resulting in denial
    of service.
