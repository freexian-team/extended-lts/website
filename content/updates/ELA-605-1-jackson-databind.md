---
title: "ELA-605-1 jackson-databind security update"
package: jackson-databind
version: 2.4.2-2+deb8u17
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2022-05-03T15:32:10+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-36518

---

It was discovered that the implementation of UntypedObjectDeserializer in
jackson-databind, a fast and powerful JSON library for Java, was prone to a
denial of service attack when deeply nested object and array values were
processed.
