---
title: "ELA-409-1 opendmarc security update"
package: opendmarc
version: 1.3.0+dfsg-1+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-04-25T13:00:36+05:30
draft: false
type: updates
cvelist:
  - CVE-2019-16378
  - CVE-2020-12460

---

CVE-2019-16378
    It was discovered that OpenDMARC, a milter implementation of DMARC, is
    prone to a signature-bypass vulnerability with multiple From: addresses.

CVE-2020-12460
    It was discovered that OpenDMARC, a milter implementation of DMARC, has
    improper null termination in the function opendmarc_xml_parse that can
    result in a one-byte heap overflow in opendmarc_xml when parsing a
    specially crafted DMARC aggregate report. This can cause remote memory
    corruption when a '\0' byte overwrites the heap metadata of the next
    chunk and its PREV_INUSE flag.
