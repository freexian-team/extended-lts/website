---
title: "ELA-1174-1 postgresql-9.4 security update"
package: postgresql-9.4
version: 9.4.26-0+deb8u10 (jessie)
version_map: {"8 jessie": "9.4.26-0+deb8u10"}
description: "privilege escalation"
date: 2024-09-04T15:42:21-04:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-7348

---

Noah Misch discovered a race condition in the pg\_dump tool included in
PostgreSQL, which may result in privilege escalation.
