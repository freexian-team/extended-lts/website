---
title: "ELA-1151-1 gdk-pixbuf security update"
package: gdk-pixbuf
version: 2.31.1-2+deb8u10 (jessie), 2.36.5-2+deb9u3 (stretch), 2.38.1+dfsg-1+deb10u1 (buster)
version_map: {"8 jessie": "2.31.1-2+deb8u10", "9 stretch": "2.36.5-2+deb9u3", "10 buster": "2.38.1+dfsg-1+deb10u1"}
description: "ANI memory corruption"
date: 2024-08-13T16:53:04+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-48622

---

Memory corruption has been fixed in the loader for ANI (animated cursors) files in GDK Pixbuf, a library used by the GTK widget toolkit.
