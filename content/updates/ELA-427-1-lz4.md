---
title: "ELA-427-1 lz4 security update"
package: lz4
version: 0.0~r122-2+deb8u1
distribution: "Debian 8 Jessie"
description: "memory corruption vulnerability"
date: 2021-05-12T11:01:11+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-3520
---

It was discovered that there was a potential memory corruption vulnerability in
the `lz4` compression algorithm library.
