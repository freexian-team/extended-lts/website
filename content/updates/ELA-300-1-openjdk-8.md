---
title: "ELA-300-1 openjdk-8 security update"
package: openjdk-8
version: 8u272-b10-0+deb8u1
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2020-10-23T11:40:20+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-14779
  - CVE-2020-14781
  - CVE-2020-14782
  - CVE-2020-14792
  - CVE-2020-14796
  - CVE-2020-14797
  - CVE-2020-14798
  - CVE-2020-14803

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
resulting in denial of service, bypass of sandbox restrictions or
information disclosure.
