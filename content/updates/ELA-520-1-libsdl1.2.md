---
title: "ELA-520-1 libsdl1.2 security update"
package: libsdl1.2
version: 1.2.15-10+deb8u3
distribution: "Debian 8 jessie"
description: "heap-based buffer over-read"
date: 2021-11-21T17:04:46+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-13616

---

An issue has been found in libsdl1.2, a library for portable low
level access to a video framebuffer, audio output, mouse, and keyboard.
It is related to an heap-based buffer over-read, resulting in a DoS by
using a crafted BMP file.
