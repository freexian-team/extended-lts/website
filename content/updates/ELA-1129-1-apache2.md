---
title: "ELA-1129-1 apache2 security update"
package: apache2
version: 2.4.25-3+deb9u17 (stretch)
version_map: {"9 stretch": "2.4.25-3+deb9u17"}
description: "HTTP/2 multiple vulnerabilities"
date: 2024-07-11T20:47:15Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-9490
  - CVE-2020-11993
  - CVE-2021-33193
  - CVE-2023-45802
  - CVE-2024-27316

---

Multiple vulnerabilities were fixed in the HTTP2 module of apache2.

CVE-2020-9490

	A specially crafted value for the 'Cache-Digest' header in a HTTP/2 request would resulted in a crash when the server actually tries to HTTP/2 PUSH a resource afterwards.

CVE-2020-11993

	When trace/debug was enabled for the HTTP/2 module and on certain traffic edge patterns, logging statements were made on the wrong connection, causing concurrent use of memory pools.

CVE-2021-33193

	A crafted method sent through HTTP/2 bypassed validation and were forwarded by mod_proxy, which could lead to request splitting or cache poisoning.

CVE-2023-45802

	When a HTTP/2 stream was reset (RST frame) by a client, there was a time window were the request's memory resources were not reclaimed immediately. Instead, de-allocation was deferred to connection close. A client could send new requests and resets, keeping the connection busy and open and causing the memory footprint to keep on growing. On connection close, all resources were reclaimed, but the process might run out of memory before that.

CVE-2024-27316

	HTTP/2 incoming headers exceeding the limit were temporarily buffered in nghttp2 in order to generate an informative HTTP 413 response. If a client did not stop sending headers, this led to memory exhaustion.
