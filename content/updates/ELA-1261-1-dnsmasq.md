---
title: "ELA-1261-1 dnsmasq security update"
package: dnsmasq
version: 2.80-1+deb10u3 (buster)
version_map: {"10 buster": "2.80-1+deb10u3"}
description: "multiple vulnerabilities"
date: 2024-11-30T16:28:29+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-50387
  - CVE-2023-50868
---
Two vulnerabilities were found in dnsmasq, a small caching DNS proxy and
DHCP/TFTP server, which could lead to denial of service by querying specially
crafted DNS resource records in control of an attacker.

CVE-2023-50387

    Certain DNSSEC aspects of the DNS protocol (in RFC 4033, 4034, 4035, 6840,
    and related RFCs) allow remote attackers to cause a denial of service (CPU
    consumption) via one or more DNSSEC responses, aka the "KeyTrap" issue. One
    of the concerns is that, when there is a zone with many DNSKEY and RRSIG
    records, the protocol specification implies that an algorithm must evaluate
    all combinations of DNSKEY and RRSIG records.

CVE-2023-50868

    The Closest Encloser Proof aspect of the DNS protocol (in RFC 5155 when RFC
    9276 guidance is skipped) allows remote attackers to cause a denial of
    service (CPU consumption for SHA-1 computations) via DNSSEC responses in a
    random subdomain attack, aka the "NSEC3" issue. The RFC 5155 specification
    implies that an algorithm must perform thousands of iterations of a hash
    function in certain situations.

