---
title: "ELA-1334-1 emacs security update"
package: emacs
version: 1:26.1+1-3.2+deb10u7 (buster)
version_map: {"10 buster": "1:26.1+1-3.2+deb10u7"}
description: "multiple vulnerabilities"
date: 2025-02-28T17:02:42+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-53920
  - CVE-2025-1244

---

Multiple problems were discovered in GNU Emacs, the extensible, customisable,
self-documenting real-time display editor.

### CVE-2024-53920

Several ways to trigger arbitrary code execution were discovered in Emacs's
support for editing files in its own dialect of Lisp. These include arbitrary
code execution upon opening an otherwise innocent-looking file, with any (or
no) file extension, for editing.

### CVE-2025-1244

Improper handling of custom 'man' URI schemes could allow an attacker to
execute arbitrary shell commands by tricking users into visiting a specially
crafted website, or an HTTP URL with a redirect.
