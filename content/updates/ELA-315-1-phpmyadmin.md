---
title: "ELA-315-1 phpmyadmin security update"
package: phpmyadmin
version: 4:4.2.12-2+deb8u10
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2020-11-19T13:17:13+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-6606
  - CVE-2020-26934
  - CVE-2020-26935

---

Several vulnerabilities have been fixed in phpMyAdmin, the web-based MySQL administration interface.

CVE-2016-6606

    Two issues were found affecting the way cookies are stored.

    The decryption of the username/password is vulnerable to a padding oracle attack.
    This can allow an attacker who has access to a user's browser cookie file to
    decrypt the username and password.

    A vulnerability was found where the same initialization vector is used to hash
    the username and password stored in the phpMyAdmin cookie. If a user has the
    same password as their username, an attacker who examines the browser cookie
    can see that they are the same, but the attacker can not directly decode these
    values from the cookie as it is still hashed.

CVE-2020-26934

    A vulnerability was discovered where an attacker can cause an XSS
    attack through the transformation feature.

    If an attacker sends a crafted link to the victim with the malicious
    JavaScript, when the victim clicks on the link, the JavaScript will run
    and complete the instructions made by the attacker.

CVE-2020-26935

    An SQL injection vulnerability was discovered in how phpMyAdmin
    processes SQL statements in the search feature. An attacker could use
    this flaw to inject malicious SQL in to a query.
