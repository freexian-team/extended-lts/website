---
title: "ELA-799-1 asterisk security update"
package: asterisk
version: 1:13.14.1~dfsg-2+deb9u8 (stretch)
version_map: {"9 stretch": "1:13.14.1~dfsg-2+deb9u8"}
description: "denial of service and directory traversal"
date: 2023-02-17T00:51:34+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-37325
  - CVE-2022-42706

---

Two security vulnerabilities were discovered in Asterisk, an Open Source
Private Branch Exchange.

CVE-2022-37325

    An incoming Setup message to addons/ooh323c/src/ooq931.c with a malformed
    Calling or Called Party IE can cause a denial of service.

CVE-2022-42706

    GetConfig, via Asterisk Manager Interface, allows a connected application
    to access files outside of the asterisk configuration directory, aka
    Directory Traversal.
