---
title: "ELA-987-1 krb5 security update"
package: krb5
version: 1.12.1+dfsg-19+deb8u8 (jessie), 1.15-1+deb9u5 (stretch)
version_map: {"8 jessie": "1.12.1+dfsg-19+deb8u8", "9 stretch": "1.15-1+deb9u5"}
description: "freeing of uninitialized pointer"
date: 2023-10-23T14:27:02+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-36054

---

Potential freeing of an uninitialized pointer in kadm_rpc_xdr.c
was fixed in krb5, the MIT implementation of the Kerberos network
authentication protocol.

