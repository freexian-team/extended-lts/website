---
title: "ELA-297-1 packagekit security update"
package: packagekit
version: 1.0.1-2+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2020-10-11T12:13:49+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-16121
  - CVE-2020-16122

---

Two vulnerabilities have been discovered in packagekit, a package
management service.

CVE-2020-16121

    Vaisha Bernard discovered that PackageKit incorrectly handled
    certain methods. A local attacker could use this issue to learn the
    MIME type of any file on the system.

CVE-2020-16122

    Sami Niemimäki discovered that PackageKit incorrectly handled local
    deb packages. A local user could possibly use this issue to install
    untrusted packages, contrary to expectations.
