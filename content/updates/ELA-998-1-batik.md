---
title: "ELA-998-1 batik security update"
package: batik
version: 1.7+dfsg-5+deb8u4 (jessie), 1.8-4+deb9u4 (stretch)
version_map: {"8 jessie": "1.7+dfsg-5+deb8u4", "9 stretch": "1.8-4+deb9u4"}
description: "multiple Server-Side Request Forgery (SSRF) vulnerabilities"
date: 2023-11-05T17:14:28Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-11987
  - CVE-2022-38398
  - CVE-2022-38648
  - CVE-2022-40146
  - CVE-2022-44729
  - CVE-2022-44730

---

Batik is a toolkit for applications or applets that want to use images in the Scalable Vector Graphics (SVG) format for various purposes, such as viewing, generation or manipulation. Various Server-Side Request Forgery (SSRF) vulnerabilities were fixed.

CVE-2020-11987

	A server-side request forgery was found, caused by improper input validation by the NodePickerPanel. By using a specially-crafted argument, an attacker could exploit this vulnerability to cause the underlying server to make arbitrary GET requests.

CVE-2022-38398

	A Server-Side Request Forgery (SSRF) vulnerability was found that allows an attacker to load a URL thru the JAR protocol.

CVE-2022-38648

	A Server-Side Request Forgery (SSRF) vulnerability was found that allows an attacker to fetch external resources.

CVE-2022-40146

	A Server-Side Request Forgery (SSRF) vulnerability was found that allows an attacker to access files using a JAR type URL.

CVE-2022-44729

	A Server-Side Request Forgery (SSRF) vulnerability was found. A malicious SVG could trigger loading external resources by default, causing resource consumption or in some cases even information disclosure.

CVE-2022-44730

	A Server-Side Request Forgery (SSRF) vulnerability was found. A malicious SVG can probe user profile / data and send it directly as parameter to a URL.
