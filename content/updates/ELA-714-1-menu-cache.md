---
title: "ELA-714-1 menu-cache security update"
package: menu-cache
version: 1.0.0-1+deb8u1 (jessie)
version_map: {"8 jessie": "1.0.0-1+deb8u1"}
description: "denial of service"
date: 2022-10-28T00:24:51+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-8933

---

It was discovered that menu-cache, the LXDE implementation of freedesktop's
menu cache, insecurely uses /tmp for a socket file, allowing a local user to
cause a denial of service (menu unavailability).
