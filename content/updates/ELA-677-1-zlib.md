---
title: "ELA-677-1 zlib security update"
package: zlib
version: 1:1.2.8.dfsg-2+deb8u3 (jessie), 1:1.2.8.dfsg-5+deb9u2 (stretch)
version_map: {"8 jessie": "1:1.2.8.dfsg-2+deb8u3", "9 stretch": "1:1.2.8.dfsg-5+deb9u2"}
description: "heap buffer overflow"
date: 2022-09-12T11:06:34+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-37434

---

Evgeny Legerov reported a heap-based buffer overflow vulnerability in
the inflate operation in zlib, which could result in denial of service
or potentially the execution of arbitrary code if specially crafted
input is processed.
