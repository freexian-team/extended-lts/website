---
title: "ELA-510-1 python3.4 security update"
package: python3.4
version: 3.4.2-1+deb8u11
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-11-05T14:43:58+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-3426
  - CVE-2021-3733
  - CVE-2021-3737

---

There were a couple of vulnerabilities found in src:python3.4, the
Python interpreter v3.4, and are as follows:

CVE-2021-3426

    Running `pydoc -p` allows other local users to extract
    arbitrary files. The `/getfile?key=path` URL allows to read
    arbitrary file on the filesystem.

    The fix removes the "getfile" feature of the pydoc module which
    could be abused to read arbitrary files on the disk (directory
    traversal vulnerability).

CVE-2021-3733

    The ReDoS-vulnerable regex has quadratic worst-case complexity
    and it allows cause a denial of service when identifying
    crafted invalid RFCs. This ReDoS issue is on the client side
    and needs remote attackers to control the HTTP server.

CVE-2021-3737

    HTTP client can get stuck infinitely reading len(line) < 64k
    lines after receiving a '100 Continue' HTTP response. This
    could lead to the client being a bandwidth sink for anyone
    in control of a server.
