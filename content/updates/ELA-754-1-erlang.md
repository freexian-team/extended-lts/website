---
title: "ELA-754-1 erlang security update"
package: erlang
version: 1:19.2.1+dfsg-2+really23.3.4.18-0+deb9u2 (stretch)
version_map: {"9 stretch": "1:19.2.1+dfsg-2+really23.3.4.18-0+deb9u2"}
description: "client authentication bypass"
date: 2022-12-12T15:17:19+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-37026

---

A Client Authentication Bypass vulnerability has been discovered in the
concurrent, real-time, distributed functional language Erlang. Impacted are
those who are running an ssl/tls/dtls server using the ssl application either
directly or indirectly via other applications. Note that the vulnerability
only affects servers that request client certification, that is sets the option
{verify, verify_peer}.

The rabbitmq-server binary package is most affected by this vulnerability. In
order to remedy the problem rabbitmq-server was upgraded to version
3.6.6+really3.8.9-0+deb9u1.

Please note that the versioning scheme +really{$upstream_version} indicates the
real upstream version. This was done to allow seamless upgrades from Stretch to
Buster.
