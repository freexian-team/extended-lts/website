---
title: "ELA-835-1 pjproject security update"
package: pjproject
version: 2.5.5~dfsg-6+deb9u9 (stretch)
version_map: {"9 stretch": "2.5.5~dfsg-6+deb9u9"}
description: "buffer overflow"
date: 2023-04-18T22:50:07+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-27585

---

PJSIP is a free and open source multimedia communication library written in C.
A buffer overflow vulnerability affects applications that use PJSIP DNS
resolver. It doesn't affect PJSIP users who do not utilise PJSIP DNS resolver.
This vulnerability is related to CVE-2022-24793. The difference is that this
issue is in parsing the query record `parse_query()`, while the issue in
CVE-2022-24793 is in `parse_rr()`. A workaround is to disable DNS resolution in
PJSIP config (by setting `nameserver_count` to zero) or use an external
resolver implementation instead.
