---
title: "ELA-1204-1 libapache-mod-jk security update"
package: libapache-mod-jk
version: 1:1.2.46-0+deb8u3 (jessie),  1:1.2.46-1+deb10u3 (buster)
version_map: {"10 buster": "1:1.2.46-1+deb10u3"}
description: "insecure configuration vulnerability"
date: 2024-10-14T12:09:28-07:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-46544
---

It was discovered that there was an insecure configuration issue in
`libapache-mod-jk`, an Apache web server module used to forward requests from
Apache to Tomcat using the AJP protocol.

An issue with incorrect default permissions could have allowed local users to
view and modify shared memory containing `mod_jk`'s configuration, which may
have potentially led to information disclosure and/or a denial of service
attack.
