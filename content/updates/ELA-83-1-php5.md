---
title: "ELA-83-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u18
distribution: "Debian 7 Wheezy"
description: "several heap-based buffer overflows"
date: 2019-02-11T10:53:14+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-9020
  - CVE-2019-9021
  - CVE-2019-9023
  - CVE-2019-9024

---

Several heap-based buffer overflows were found in PHP, the widely-used
general-purpose scripting language, which may lead to information disclosure,
memory corruption or other unspecified impact if a malformed file or other
input is processed.

At the moment no CVE numbers have been assigned yet but PHP upstream intends to
announce them later.
