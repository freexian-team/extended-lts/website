---
title: "ELA-752-1 jqueryui security update"
package: jqueryui
version: 1.12.1+dfsg-4+deb9u1 (stretch)
version_map: {"9 stretch": "1.12.1+dfsg-4+deb9u1"}
description: "multiple vulnerabilities"
date: 2022-12-07T16:02:47+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-41182
  - CVE-2021-41183
  - CVE-2021-41184
  - CVE-2022-31160

---

jQuery-UI, the official jQuery user interface library, is a curated set
of user interface interactions, effects, widgets, and themes built on top
of jQuery were reported to have the following vulnerabilities.

CVE-2021-41182

    jQuery-UI was accepting the value of the `altField` option of the
    Datepicker widget from untrusted sources may execute untrusted code.
    This has been fixed and now any string value passed to the `altField`
    option is now treated as a CSS selector.

CVE-2021-41183

    jQuery-UI was accepting the value of various `*Text` options of the
    Datepicker widget from untrusted sources may execute untrusted code.
    This has been fixed and now the values passed to various `*Text`
    options are now always treated as pure text, not HTML.

CVE-2021-41184

    jQuery-UI was accepting the value of the `of` option of the
    `.position()` util from untrusted sources may execute untrusted code.
    This has been fixed and now any string value passed to the `of`
    option is now treated as a CSS selector.

CVE-2022-31160

    jQuery-UI was potentially vulnerable to cross-site scripting.
    Initializing a checkboxradio widget on an input enclosed within a
    label makes that parent label contents considered as the input label.
    Calling `.checkboxradio( "refresh" )` on such a widget and the initial
    HTML contained encoded HTML entities will make them erroneously get
