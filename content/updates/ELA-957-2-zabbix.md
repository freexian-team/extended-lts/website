---
title: "ELA-957-2 zabbix regression update"
package: zabbix
version: 1:2.2.23+dfsg-0+deb8u6 (jessie), 1:3.0.32+dfsg-0+deb9u5 (stretch)
version_map: {"8 jessie": "1:2.2.23+dfsg-0+deb8u6", "9 stretch": "1:3.0.32+dfsg-0+deb9u5"}
description: "regression update"
date: 2023-10-21T12:29:26+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

The last update required an update to the database scheme, but as
zabbix does not support upgrading the database scheme if SQlite3 is used,
using zabbix-proxy-sqlite3 requires the user to drop the database and recreate
it with a supplied sql template file. 

However, this template file has not been updated in the previous upload,
making this recreation difficult when not knowing the details.

Please read /usr/share/doc/zabbix-proxy-sqlite3/README.Debian for instructions
how to create the database file.

Note: All other database backends will automatically update the schema.

