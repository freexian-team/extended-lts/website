---
title: "ELA-1039-1 postfix security update"
package: postfix
version: 2.11.3-1+deb8u3 (jessie), 3.1.15-0+deb9u2 (stretch)
version_map: {"8 jessie": "2.11.3-1+deb8u3", "9 stretch": "3.1.15-0+deb9u2"}
description: "SMTP smuggling"
date: 2024-01-31T09:28:33Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-51764

---

Postfix, a popular mail server, was vulnerable.


Postfix allowed SMTP smuggling unless configured with 
`smtpd_data_restrictions=reject_unauth_pipelining` and
`smtpd_discard_ehlo_keywords=chunking`.

Remote attackers can use a published exploitation technique
to inject e-mail messages with a spoofed `MAIL FROM` address, allowing bypass
of an SPF protection mechanism. 

This occurs because Postfix supports `<LF>.<CR><LF>` but some other popular e-mail servers do not.
To prevent attack variants (by always disallowing `<LF>` without `<CR>`), 
a different solution is required, such as using the backported `smtpd_forbid_bare_newline=yes` option.
