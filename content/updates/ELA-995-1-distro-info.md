---
title: "ELA-995-1 distro-info test-suite update"
package: distro-info
version: 0.14+deb8u1 (jessie), 0.14+deb9u1 (stretch)
version_map: {"8 jessie": "0.14+deb8u1", "9 stretch": "0.14+deb9u1"}
description: "non-security-related test-suite update"
date: 2023-10-30T15:26:11+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

This is a non-security update, enabling distro-info to continue to build
with the distro-info-data update in ELA-994-1, which broke some
test-suite assumptions.

This update also allows distro-info to support additional columns in
distro-info-data, easing future updates.
