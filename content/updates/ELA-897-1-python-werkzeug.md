---
title: "ELA-897-1 python-werkzeug security update"
package: python-werkzeug
version: 0.9.6+dfsg-1+deb8u3 (jessie), 0.11.15+dfsg1-1+deb9u2 (stretch)
version_map: {"8 jessie": "0.9.6+dfsg-1+deb8u3", "9 stretch": "0.11.15+dfsg1-1+deb9u2"}
description: "multiple vulnerabilities"
date: 2023-07-25T08:36:06Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-23934
  - CVE-2023-25577

---
Multiple vulnerabilities were found in Werkzeug, a comprehensive WSGI web application library written in python.

CVE-2023-23934:

	Werkzeug will parse the cookie `=__Host-test=bad` as
	`__Host-test=bad`. If a Werkzeug application is running next to a
	vulnerable or malicious subdomain which sets such a cookie using a
	vulnerable browser, the Werkzeug application will see the bad cookie
    value but the valid cookie key. Browsers may allow "nameless" cookies
    that look like `=value` instead of `key=value`. A vulnerable browser
    may allow a compromised application on an adjacent subdomain to
    exploit this to set a cookie like `=__Host-test=bad` for another
    subdomain.

CVE-2023-25577: 

	Werkzeug's multipart form data parser will parse an
    unlimited number of parts, including file parts. Parts can be a small
    amount of bytes, but each requires CPU time to parse and may use more
    memory as Python data. If a request can be made to an endpoint that
    accesses `request.data`, `request.form`, `request.files`, or
    `request.get_data(parse_form_data=False)`, it can cause unexpectedly
    high resource usage. This allows an attacker to cause a denial of
    service by sending crafted multipart data to an endpoint that will
    parse it.
