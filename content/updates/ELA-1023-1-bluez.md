---
title: "ELA-1023-1 bluez security update"
package: bluez
version: 5.43-2+deb9u2~deb8u6 (jessie), 5.43-2+deb9u7 (stretch)
version_map: {"8 jessie": "5.43-2+deb9u2~deb8u6", "9 stretch": "5.43-2+deb9u7"}
description: "input injection vulnerability"
date: 2023-12-20T12:01:28+00:00
draft: false
cvelist:
  - CVE-2023-45866
---

It was discovered that there was a keyboard injection attack in Bluez, a set of
services and tools for interacting with wireless Bluetooth devices.

Prior to this change, BlueZ may have permitted unauthenticated peripherals
to establish encrypted connections and thereby accept keyboard reports,
potentially permitting injection of HID (~keyboard) commands, despite no user
authorising such access.
