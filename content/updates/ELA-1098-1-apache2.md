---
title: "ELA-1098-1 apache2 security update"
package: apache2
version: 2.4.25-3+deb9u16 (stretch)
version_map: {"9 stretch": "2.4.25-3+deb9u16"}
description: "multiple vulnerabilities"
date: 2024-05-26T19:13:52Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-31122
  - CVE-2023-38709
  - CVE-2024-24795

---

CVE-2023-31122

	An Out-of-bounds Read vulnerability was found in mod_macro of Apache HTTP Server.

CVE-2023-38709

    A faulty input validation in the core of Apache allows malicious or exploitable backend/content generators to split HTTP responses.
	
CVE-2024-24795

	HTTP Response splitting in multiple modules in Apache HTTP Server allows an attacker that can inject malicious response headers into backend applications to cause an HTTP desynchronization attack.

Please note that the fix of CVE-2024-24795, may break unrelated CGI-BIN scripts. As part of the security fix, the Apache webserver
mod_cgi module has stopped relaying the Content-Length field of the HTTP reply header from the CGI programs back to the client in cases where the connection is to be closed and the client
is able to read until end-of-file. You may restore legacy behavior for trusted scripts by adding the following configuration environment variable to the
Apache configuration, scoped to the ```<Directory>``` entry or entries in which script is being served via CGI,
```SetEnv ap_trust_cgilike_cl "yes"```.
The definitive fix is to read the whole input, re-allocating the input buffer to fit as more input is received,
and to not trust that CONTENT_LENGTH variable is always present.
