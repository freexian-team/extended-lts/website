---
title: "ELA-783-1 modsecurity-crs security update"
package: modsecurity-crs
version: 3.2.3-0+deb9u1 (stretch)
version_map: {"9 stretch": "3.2.3-0+deb9u1"}
description: "web applications firewall bypass"
date: 2023-01-30T19:44:32+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-16384
  - CVE-2019-13464
  - CVE-2020-22669
  - CVE-2021-35368
  - CVE-2022-39955
  - CVE-2022-39956
  - CVE-2022-39957
  - CVE-2022-39958

---

Multiple issues were found in modsecurity-crs, a set of generic attack
detection rules for use with ModSecurity or compatible web application
firewalls, which allows remote attackers to bypass the web applications
firewall.

If you are using modsecurity-crs with apache2 / libapache2-modsecurity, please
make sure to review your modsecurity configuration, usually
/etc/modsecurity/modsecurity.conf, against the updated recommended
configration, available in /etc/modsecurity/modsecurity.conf-recommended:
Some of the changes to the recommended rules are required to avoid WAF bypasses
in certain circumstances.

Please note that CVE-2022-39956 requires an updated modsecurity-apache packge,
which has been previously uploaded to buster-security, see Debian ELTS Advisory
ELA-779-1 for details.

Kudos to @airween for the support and help while perparing the update.

CVE-2018-16384

    A SQL injection bypass (aka PL1 bypass) exists in OWASP ModSecurity Core Rule
    Set (owasp-modsecurity-crs) through v3.1.0-rc3 via {`a`b} where a is a special
    function name (such as "if") and b is the SQL statement to be executed.

CVE-2019-13464

    An issue was discovered in OWASP ModSecurity Core Rule Set (CRS) 3.0.2.
    Use of X.Filename instead of X_Filename can bypass some PHP Script Uploads
    rules, because PHP automatically transforms dots into underscores in
    certain contexts where dots are invalid.

CVE-2020-22669

    Modsecurity owasp-modsecurity-crs 3.2.0 (Paranoia level at PL1) has a SQL
    injection bypass vulnerability. Attackers can use the comment characters
    and variable assignments in the SQL syntax to bypass Modsecurity WAF 
    protection and implement SQL injection attacks on Web applications.

CVE-2021-35368

    OWASP ModSecurity Core Rule Set 3.1.x before 3.1.2, 3.2.x before 3.2.1,
    and 3.3.x before 3.3.2 is affected by a Request Body Bypass via a 
    trailing pathname.

CVE-2022-39955

    The OWASP ModSecurity Core Rule Set (CRS) is affected by a partial rule set
    bypass by submitting a specially crafted HTTP Content-Type header field that
    indicates multiple character encoding schemes. A vulnerable back-end can
    potentially be exploited by declaring multiple Content-Type "charset" names and
    therefore bypassing the configurable CRS Content-Type header "charset" allow
    list. An encoded payload can bypass CRS detection this way and may then be
    decoded by the backend. The legacy CRS versions 3.0.x and 3.1.x are affected,
    as well as the currently supported versions 3.2.1 and 3.3.2. Integrators and
    users are advised to upgrade to 3.2.2 and 3.3.3 respectively.

CVE-2022-39956

    The OWASP ModSecurity Core Rule Set (CRS) is affected by a partial rule set
    bypass for HTTP multipart requests by submitting a payload that uses a
    character encoding scheme via the Content-Type or the deprecated
    Content-Transfer-Encoding multipart MIME header fields that will not be decoded
    and inspected by the web application firewall engine and the rule set. The
    multipart payload will therefore bypass detection. A vulnerable backend that
    supports these encoding schemes can potentially be exploited. The legacy CRS
    versions 3.0.x and 3.1.x are affected, as well as the currently supported
    versions 3.2.1 and 3.3.2. Integrators and users are advised upgrade to 3.2.2
    and 3.3.3 respectively. The mitigation against these vulnerabilities depends on
    the installation of the latest ModSecurity version (v2.9.6 / v3.0.8).

CVE-2022-39957

    The OWASP ModSecurity Core Rule Set (CRS) is affected by a response body
    bypass. A client can issue an HTTP Accept header field containing an optional
    "charset" parameter in order to receive the response in an encoded form.
    Depending on the "charset", this response can not be decoded by the web
    application firewall. A restricted resource, access to which would ordinarily
    be detected, may therefore bypass detection. The legacy CRS versions 3.0.x and
    3.1.x are affected, as well as the currently supported versions 3.2.1 and
    3.3.2. Integrators and users are advised to upgrade to 3.2.2 and 3.3.3
    respectively.

CVE-2022-39958

    The OWASP ModSecurity Core Rule Set (CRS) is affected by a response body bypass
    to sequentially exfiltrate small and undetectable sections of data by
    repeatedly submitting an HTTP Range header field with a small byte range. A
    restricted resource, access to which would ordinarily be detected, may be
    exfiltrated from the backend, despite being protected by a web application
    firewall that uses CRS. Short subsections of a restricted resource may bypass
    pattern matching techniques and allow undetected access. The legacy CRS
    versions 3.0.x and 3.1.x are affected, as well as the currently supported
    versions 3.2.1 and 3.3.2. Integrators and users are advised to upgrade to 3.2.2
    and 3.3.3 respectively and to configure a CRS paranoia level of 3 or higher.

