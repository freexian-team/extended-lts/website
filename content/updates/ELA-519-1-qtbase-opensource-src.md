---
title: "ELA-519-1 qtbase-opensource-src security update"
package: qtbase-opensource-src
version: 5.3.2+dfsg-4+deb8u6
distribution: "Debian 8 jessie"
description: "division by zero"
date: 2021-11-20T18:55:45+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-19872

---

A malformed PPM file could crash the application by generating a division by zero.
