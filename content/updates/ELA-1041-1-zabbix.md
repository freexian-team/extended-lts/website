---
title: "ELA-1041-1 zabbix security update"
package: zabbix
version: 2.2.23+dfsg-0+deb8u7 (jessie), 1:3.0.32+dfsg-0+deb9u6 (stretch)
version_map: {"8 jessie": "2.2.23+dfsg-0+deb8u7", "9 stretch": "1:3.0.32+dfsg-0+deb9u6"}
description: "multiple vulnerabilities"
date: 2024-02-03T15:49:27+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-32721
  - CVE-2023-32726

---

Several security vulnerabilities have been discovered in zabbix, a
network monitoring solution, potentially allowing an attacker to perform
a stored XSS, Server-Side Request Forgery (SSRF), exposure of sensitive
information, a system crash, or arbitrary code execution.

CVE-2023-32721

    A stored XSS has been found in the Zabbix web application in the
    Maps element if a URL field is set with spaces before URL.

CVE-2023-32726

    Possible buffer overread from reading DNS responses.

