---
title: "ELA-746-1 inetutils security update"
package: inetutils
version: 2:1.9.4-2+deb9u2 (stretch)
version_map: {"9 stretch": "2:1.9.4-2+deb9u2"}
description: "missing validation and NULL pointer dereference"
date: 2022-11-30T23:33:34+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-40491
  - CVE-2022-39028

---

Several security vulnerabilities were discovered in inetutils, a
collection of common network programs.

CVE-2021-40491

    inetutils' ftp client before 2.2 does not validate addresses
    returned by PSV/LSPV responses to make sure they match the server
    address.  A malicious server can exploit this flaw to reach services
    in the client's private network.  (This is similar to curl's
    CVE-2020-8284.)

CVE-2022-39028

    inetutils's telnet server through 2.3 has a NULL pointer dereference
    which a client can trigger by sending 0xff 0xf7 or 0xff 0xf8.  In a
    typical installation, the telnetd application would crash but the
    telnet service would remain available through inetd.  However, if the
    telnetd application has many crashes within a short time interval,
    the telnet service would become unavailable after inetd logs a
    "telnet/tcp server failing (looping), service terminated" error.
