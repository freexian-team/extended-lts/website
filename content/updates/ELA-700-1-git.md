---
title: "ELA-700-1 git security update"
package: git
version: 1:2.1.4-2.1+deb8u11 (jessie), 1:2.11.0-3+deb9u8 (stretch)
version_map: {"8 jessie": "1:2.1.4-2.1+deb8u11", "9 stretch": "1:2.11.0-3+deb9u8"}
description: "symlink attack"
date: 2022-10-10T01:02:06+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-21300
  - CVE-2021-40330

---

Several security vulnerabilities have been discovered in Git, a fast, scalable,
distributed revision control system, which may affect multi-user systems.

CVE-2021-21300

    A specially crafted repository that contains symbolic links as well as
    files using a clean/smudge filter such as Git LFS, may cause just-checked
    out script to be executed while cloning onto a case-insensitive file system
    such as NTFS, HFS+ or APFS (i.e. the default file systems on Windows and
    macOS).

CVE-2021-40330

    git_connect_git in connect.c allows a repository path to contain a newline
    character, which may result in unexpected cross-protocol requests, as
    demonstrated by the git://localhost:1234/%0d%0a%0d%0aGET%20/%20HTTP/1.1
    substring.
