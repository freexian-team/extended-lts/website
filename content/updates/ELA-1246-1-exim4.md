---
title: "ELA-1246-1 exim4 security update"
package: exim4
version: 4.84.2-2+deb8u13 (jessie), 4.89-2+deb9u14 (stretch), 4.92-8+deb10u11 (buster)
version_map: {"8 jessie": "4.84.2-2+deb8u13", "9 stretch": "4.89-2+deb9u14", "10 buster": "4.92-8+deb10u11"}
description: "multiple vulnerabilities"
date: 2024-11-27T16:22:28+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-42117
  - CVE-2023-42119

---

Multiple potential security vulnerabilities have been addressed in exim4, a
mail transport agent. These issues may allow remote attackers to disclose
sensitive information or execute arbitrary code but only if Exim4 is run behind
or with untrusted proxy servers or DNS resolvers. If your proxy-protocol proxy
or DNS resolver are trustworthy, you are not affected.

In addition
[CVE-2021-38371](https://deb.freexian.com/extended-lts/tracker/CVE-2021-38371) and
[CVE-2022-3559](https://deb.freexian.com/extended-lts/tracker/CVE-2022-3559) have been addressed for Debian 10
"Buster" and
[CVE-2022-3559](https://deb.freexian.com/extended-lts/tracker/CVE-2022-3559) for Debian 9 "Stretch".


