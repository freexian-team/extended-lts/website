---
title: "ELA-1238-2 needrestart regression update"
package: needrestart
version: 1.2-8+deb8u4 (jessie), 2.11-3+deb9u4 (stretch), 3.4-5+deb10u3 (buster)
version_map: {"8 jessie": "1.2-8+deb8u4", "9 stretch": "2.11-3+deb9u4", "10 buster": "3.4-5+deb10u3"}
description: "regression update"
date: 2024-12-02T21:55:26-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-48991

---

The update for needrestart announced as ELA 1228-1 introduced a
regression, reporting false positives for processes running in chroot or
mountns. Updated packages are now available to correct this issue.
