---
title: "ELA-804-1 libarchive security update"
package: libarchive
version: 3.1.2-11+deb8u11 (jessie)
version_map: {"8 jessie": "3.1.2-11+deb8u11"}
description: "armhf build fix"
date: 2023-02-21T16:14:31+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update fixes the build on armhf, which was preventing security
updates from reaching that architecture.
