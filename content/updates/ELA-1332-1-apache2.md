---
title: "ELA-1332-1 apache2 security update"
package: apache2
version: 2.4.10-10+deb8u30 (jessie)
version_map: {"8 jessie": "2.4.10-10+deb8u30"}
description: "proxy authentication bypass"
date: 2025-02-27T19:13:28Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-38473

---

apache2 a popular webserver was affected by a vulnerability.

Encoding problem allows request URLs with incorrect encoding to be sent
to backend services, potentially bypassing authentication via crafted
requests.
