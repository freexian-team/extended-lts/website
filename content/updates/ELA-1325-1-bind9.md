---
title: "ELA-1325-1 bind9 security update"
package: bind9
version: 1:9.10.3.dfsg.P4-12.3+deb9u18 (stretch), 1:9.11.5.P4+dfsg-5.1+deb10u14 (buster)
version_map: {"9 stretch": "1:9.10.3.dfsg.P4-12.3+deb9u18", "10 buster": "1:9.11.5.P4+dfsg-5.1+deb10u14"}
description: "denial of service"
date: 2025-02-21T09:55:59+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-11187

---

One vulnerability was discovered in BIND, a DNS server implementation, which
may result in denial of service.

It is possible to construct a zone such that some queries to it will generate
responses containing numerous records in the Additional section. An attacker
sending many such queries can cause either the authoritative server itself or
an independent resolver to use disproportionate resources processing the
queries. Zones will usually need to have been deliberately crafted to exploit
this flaw.
