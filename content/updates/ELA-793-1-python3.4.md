---
title: "ELA-793-1 python3.4 bugfix update"
package: python3.4
version: 3.4.2-1+deb8u13 (jessie)
version_map: {"8 jessie": "3.4.2-1+deb8u13"}
description: "armhf build fix"
date: 2023-02-06T09:43:37+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update fixes the build on armhf, which was preventing security
updates from reaching that architecture.
