---
title: "ELA-1110-1 netty security update"
package: netty
version: 1:4.1.7-2+deb9u5 (stretch)
version_map: {"9 stretch": "1:4.1.7-2+deb9u5"}
description: "denial of service"
date: 2024-06-18T22:43:21+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-29025

---

Julien Viet discovered that Netty, a Java NIO client/server socket framework,
was vulnerable to allocation of resources without limits or throttling due to
the accumulation of data in the HttpPostRequestDecoder. This would allow an
attacker to cause a denial of service.
