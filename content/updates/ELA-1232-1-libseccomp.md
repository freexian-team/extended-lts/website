---
title: "ELA-1232-1 libseccomp security update"
package: libseccomp
version: 2.4.1-1~deb8u1 (jessie), 2.4.1-1~deb9u1 (stretch), 2.4.1-1~deb10u1 (buster)
version_map: {"8 jessie": "2.4.1-1~deb8u1", "9 stretch": "2.4.1-1~deb9u1", "10 buster": "2.4.1-1~deb10u1"}
description: "fix 64-bit argument comparisons"
date: 2024-11-11T13:02:02+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-9893

---

The kernel syscall filtering library libseccomp has been upgraded to version 2.4.1 to fix 64-bit argument comparisons.
