---
title: "ELA-1006-1 libde265 security update"
package: libde265
version: 1.0.11-0+deb9u5 (stretch)
version_map: {"9 stretch": "1.0.11-0+deb9u5"}
description: "buffer over read"
date: 2023-11-26T13:49:50+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-43887

---

An issue has been found in libde265, an open H.265 video codec implementation.
It is related to a buffer over read in pic_parameter_set::dump, which might result in an information leak or denial of service with crafted H.265 files.
