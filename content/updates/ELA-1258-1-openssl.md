---
title: "ELA-1258-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb8u22 (jessie)
version_map: {"8 jessie": "1.0.1t-1+deb8u22"}
description: "multiple vulnerabilities"
date: 2024-11-30T19:15:19+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-5678
  - CVE-2024-0727

---

Multiple vulnerabilities were discovered in OpenSSL, the Secure Sockets Layer
toolkit.

### CVE-2023-5678

A denial of service could occur with excessively long X9.42 DH keys.

### CVE-2024-0727

A denial of service could occur with a null field in a PKCS12 file.
