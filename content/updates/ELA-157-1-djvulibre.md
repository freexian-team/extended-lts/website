---
title: "ELA-157-1 djvulibre security update"
package: djvulibre
version: 3.5.25.3-1+deb7u1
distribution: "Debian 7 Wheezy"
description: "several issues (overflows)"
date: 2019-08-29T20:11:15+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-15142
  - CVE-2019-15143
  - CVE-2019-15144
  - CVE-2019-15145

---

Hongxu Chen found several issues in djvulibre, a library and set of tools
to handle images in the DjVu format.
The issues are a heap-buffer-overflow, a stack-overflow, an infinite loop
and an invalid read when working with crafted files as input.
