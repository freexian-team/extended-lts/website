---
title: "ELA-1094-1 bind9 security update"
package: bind9
version: 9.9.5.dfsg-9+deb8u31 (jessie), 1:9.10.3.dfsg.P4-12.3+deb9u16 (stretch)
version_map: {"8 jessie": "9.9.5.dfsg-9+deb8u31", "9 stretch": "1:9.10.3.dfsg.P4-12.3+deb9u16"}
description: "denial of service vulnerabilities"
date: 2024-05-17T19:33:12-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-50387
  - CVE-2023-50868

---

Two vulnerabilities were discovered in BIND, a DNS server implementation, which
may result in denial of service.

CVE-2023-50387

    Certain DNSSEC aspects of the DNS protocol allow remote attackers to cause
    a denial of service via DNSSEC queries. This is known as the "KeyTrap"
    issue.

CVE-2023-50868

    The Closest Encloser Proof aspect of the DNS protocol allows remote
    attackers to cause a denial of service via DNSSEC queries in a random
    subdomain attack. This is known as the "NSEC3" issue.
