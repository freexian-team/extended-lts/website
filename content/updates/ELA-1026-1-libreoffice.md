---
title: "ELA-1026-1 libreoffice security update"
package: libreoffice
version: 1:4.3.3-2+deb8u15 (jessie)
version_map: {"8 jessie": "1:4.3.3-2+deb8u15"}
description: "improper input validation"
date: 2023-12-31T10:21:47Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-6185

---

An Improper Input Validation vulnerability
was found in GStreamer integration of The Document
Foundation LibreOffice allows an attacker to execute arbitrary
GStreamer plugins. In affected versions the filename of the
embedded video is not sufficiently escaped when passed to
GStreamer enabling an attacker to run arbitrary
gstreamer plugins depending on what plugins are installed
on the target system.
