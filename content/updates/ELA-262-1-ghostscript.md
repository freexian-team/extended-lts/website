---
title: "ELA-262-1 ghostscript security update"
package: ghostscript
version: 9.26a~dfsg-0+deb8u7
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2020-08-20T17:59:55+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-16287
  - CVE-2020-16288
  - CVE-2020-16289
  - CVE-2020-16290
  - CVE-2020-16291
  - CVE-2020-16292
  - CVE-2020-16293
  - CVE-2020-16294
  - CVE-2020-16295
  - CVE-2020-16296
  - CVE-2020-16297
  - CVE-2020-16298
  - CVE-2020-16299
  - CVE-2020-16300
  - CVE-2020-16301
  - CVE-2020-16302
  - CVE-2020-16303
  - CVE-2020-16304
  - CVE-2020-16305
  - CVE-2020-16306
  - CVE-2020-16307
  - CVE-2020-16308
  - CVE-2020-16309
  - CVE-2020-16310
  - CVE-2020-17538

---

Multiple vulnerabilities were found in ghostscript, an interpreter for
the PostScript language and for PDF, allowing an attacker to escalate
privileges and cause denial of service via crafted PS/EPS/PDF files.
