---
title: "ELA-979-1 libx11 security update"
package: libx11
version: 2:1.6.2-3+deb8u7 (jessie), 2:1.6.4-3+deb9u6 (stretch)
version_map: {"8 jessie": "2:1.6.2-3+deb8u7", "9 stretch": "2:1.6.4-3+deb9u6"}
description: "multiple vulnerabilities"
date: 2023-10-05T12:50:34+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-43785
  - CVE-2023-43786
  - CVE-2023-43787

---

Several vulnerabilities were found in libx11, the X11 client-side
library.

CVE-2023-43785

    Gregory James Duck discovered an out of bounds memory access in
    _XkbReadKeySyms, which could result in denial of service.

CVE-2023-43786

    Yair Mizrahi found an infinite recursion in PutSubImage when
    parsing a crafted file, which would result in stack exhaustion
    and denial of service.

CVE-2023-43787

    Yair Mizrahi discovered an integer overflow in XCreateImage
    when parsing crafted input, which would result in a small buffer
    allocation leading into a buffer overflow. This could result
    in denial of service or potentially in arbitrary code execution.
