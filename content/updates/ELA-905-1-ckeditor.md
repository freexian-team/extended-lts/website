---
title: "ELA-905-1 ckeditor security update"
package: ckeditor
version: 4.4.4+dfsg1-2+deb8u2 (jessie), 4.5.7+dfsg-2+deb9u2 (stretch)
version_map: {"8 jessie": "4.4.4+dfsg1-2+deb8u2", "9 stretch": "4.5.7+dfsg-2+deb9u2"}
description: "regression fix of CVE-2021-37695"
date: 2023-07-29T09:32:16Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-37695

---

A regression was introduced after fixing CVE-2021-37695 in ckeditor a rich text editor for the web written in javascript.
This regression was due to lack of polyfill (a snippet of code that patches a piece of functionality that is missing in
some browsers) in stretch and jessie for javascript array class. This was fixed by manually emulating the polyfill. This regression
was introduced in DLA-2813-1 for stretch and ELA-513-1 for jessie.
