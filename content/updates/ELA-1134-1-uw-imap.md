---
title: "ELA-1134-1 uw-imap regression update"
package: uw-imap
version: 8:2007f~dfsg-6+deb10u1 (buster)
version_map: {"10 buster": "8:2007f~dfsg-6+deb10u1"}
description: "regression update"
date: 2024-07-20T20:15:04+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

The uw-imap toolkit package had a problem when used with openssl 1.1.1.

It could not work with Google IMAP servers because Google wants SNI requests if the client supports TLS 1.3.
