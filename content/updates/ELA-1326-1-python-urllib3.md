---
title: "ELA-1326-1 python-urllib3 security update"
package: python-urllib3
version: 1.9.1-3+deb8u3 (jessie), 1.19.1-1+deb9u3 (stretch), 1.24.1-1+deb10u3 (buster)
version_map: {"8 jessie": "1.9.1-3+deb8u3", "9 stretch": "1.19.1-1+deb9u3", "10 buster": "1.24.1-1+deb10u3"}
description: "authorization bypass vulnerability"
date: 2025-02-21T00:59:00+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-37891

---

It was discovered that when sending HTTP requests *without* using
urllib3's proxy support, it's possible to accidentally set the
`Proxy-Authorization` header even though it won't have any effect as the
request is not using a forwarding proxy or a tunneling proxy.

In those cases, urllib3 doesn't treat the `Proxy-Authorization` HTTP
header as one carrying authentication material and thus doesn't strip
the header on cross-origin redirects, which might lead to authorization
bypass.
