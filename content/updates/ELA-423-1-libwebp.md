---
title: "ELA-423-1 libwebp security update"
package: libwebp
version: 0.4.1-1.2+deb8u1
distribution: "Debian 8 jessie"
description: "buffer overflows"
date: 2021-05-09T20:49:47+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-25009
  - CVE-2018-25010
  - CVE-2018-25011
  - CVE-2018-25012
  - CVE-2018-25013
  - CVE-2018-25014
  - CVE-2020-36328
  - CVE-2020-36329
  - CVE-2020-36330
  - CVE-2020-36331

---

Several security vulnerabilities were discovered in libwebp, a lossy
compression library for digital photographic images. Heap-based buffer overflows may
lead to a denial-of-service or potentially the execution of arbitrary code.
