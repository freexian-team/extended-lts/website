---
title: "ELA-390-1 lxml security update"
package: lxml
version: 3.4.0-1+deb8u4
distribution: "Debian 8 jessie"
description: "missing input sanitization"
date: 2021-03-24T19:07:04+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-28957

---

An issue has been found in lxml, a pythonic binding for the libxml2 and libxslt libraries.
Due to missing input sanitization, XSS is possible for the HTML5 formaction attribute.

