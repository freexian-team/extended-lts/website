---
title: "ELA-429-1 jetty security update"
package: jetty
version: 6.1.26-4+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-05-14T14:36:41+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-9735
  - CVE-2019-10247

---

It was discovered that jetty, a Java servlet engine and webserver, is
vulnerable to a timing attack and an information leak. An attacker
might reveal cryptographic credentials such as passwords to a local
user, or disclose webapps installation path.
