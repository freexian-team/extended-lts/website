---
title: "ELA-702-1 strongswan security update"
package: strongswan
version: 5.5.1-4+deb9u7 (stretch), 5.2.1-6+deb8u10 (jessie)
version_map: {"9 stretch": "5.5.1-4+deb9u7", "8 jessie": "5.2.1-6+deb8u10"}
description: "denial of service (DoS) vulnerability"
date: 2022-10-10T11:40:12-07:00
draft: false
cvelist:
  - CVE-2022-40617
---

It was discovered that there was a potential denial of service vulnerability in
strongswan, an IPsec VPN solution.

Strongswan could have queried URLs with untrusted certificates, and this could
potentially lead to a DoS attack by blocking the fetcher thread.
