---
title: "ELA-333-1 openexr security update"
package: openexr
version: 1.6.1-8+deb8u1
distribution: "Debian 8 Jessie"
description: "multiple vulnerabilities"
date: 2020-12-15T12:48:11+00:00
draft: false
type: updates
cvelist:
  - CVE-2020-11764
  - CVE-2020-15304
  - CVE-2020-16588
---


Three issues were discovered in `openexr`, a set of tools to manipulate
[OpenEXR image files](https://en.wikipedia.org/wiki/OpenEXR), often in the
computer-graphics industry for visual effects and animation.
