---
title: "ELA-192-1 isc-dhcp security update"
package: isc-dhcp
version: 4.2.2.dfsg.1-5+deb70u10
distribution: "Debian 7 Wheezy"
description: "issues with OMAPI network connections"
date: 2019-11-23T22:59:21+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-2774
  - CVE-2017-3144

---

Two issues have been found in isc-dhcp, a server for automatic IP address assignment.

CVE-2016-2774
     The number of simultaneous open TCP connections to OMAPI port of
     the server has to be limited in order to avoid a denial of service.

CVE-2017-3144
     A failure to properly clean up OMAPI connections might result in an
     exhaustion of socket descriptors and thus lead to a denial of service.
