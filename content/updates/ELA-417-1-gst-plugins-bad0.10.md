---
title: "ELA-417-1 gst-plugins-bad0.10 security update"
package: gst-plugins-bad0.10
version: 0.10.23-7.4+deb8u4
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-04-27T13:07:32+02:00
draft: false
type: updates
cvelist:

---

Multiple vulnerabilities were discovered in plugins for the GStreamer
media framework, which may result in denial of service or potentially
the execution of arbitrary code if a malformed media file is opened.
