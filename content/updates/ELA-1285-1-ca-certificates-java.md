---
title: "ELA-1285-1 ca-certificates-java bugfix update"
package: ca-certificates-java
version: 20190405+deb10u1 (buster)
version_map: {"10 buster": "20190405+deb10u1"}
description: "bug fix update"
date: 2025-01-03T12:27:48Z
draft: false
type: updates
tags:
- update
cvelist:

---

ca-certificate-java, a package that update the cacerts keystore
(a collection of trusted certificate authority certificates) used for many java runtimes,
failed to install.
