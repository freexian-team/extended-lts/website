---
title: "ELA-491-1 apache2 security update"
package: apache2
version: 2.4.10-10+deb8u19
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-10-02T17:22:23+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-34798
  - CVE-2021-39275
  - CVE-2021-40438

---

Several vulnerabilities were discovered in the Apache HTTP server.
An attacker could send proxied requests to arbitrary servers, corrupt
memory in some setups involving third-party modules, and cause the
server to crash.

* CVE-2021-34798

    Malformed requests may cause the server to dereference
    a NULL pointer.

* CVE-2021-39275

    ap_escape_quotes() may write beyond the end of a buffer when given
    malicious input. No included modules pass untrusted data to these
    functions, but third-party / external modules may.

* CVE-2021-40438

    A crafted request uri-path can cause mod_proxy to forward the
    request to an origin server choosen by the remote user.
