---
title: "ELA-1147-1 python-aiosmtpd security update"
package: python-aiosmtpd
version: 1.2-3+deb10u1 (buster)
version_map: {"10 buster": "1.2-3+deb10u1"}
description: "multiple vulnerabilities"
date: 2024-08-12T16:39:23+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-27305
  - CVE-2024-34083

---

Two vulnerabilities have been fixed in python-aiosmtpd, an asyncio based SMTP server.

CVE-2024-27305

    SMTP smuggling with non-standard line endings

CVE-2024-34083

    STARTTLS unencrypted command injection

