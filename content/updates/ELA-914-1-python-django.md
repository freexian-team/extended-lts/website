---
title: "ELA-914-1 python-django security update"
package: python-django
version: 1:1.10.7-2+deb9u20 (stretch)
version_map: {"9 stretch": "1:1.10.7-2+deb9u20"}
description: "denial of service"
date: 2023-08-04T13:07:39+01:00
draft: false
cvelist:
  - CVE-2021-45115
  - CVE-2021-45116
  - CVE-2021-45452
  - CVE-2023-24580
  - CVE-2023-31047
---

A number of vulnerabilities were discovered in
[Django](https://www.djangoproject.com/), a popular Python-based web
development framework.


* CVE-2021-45115: Denial-of-service possibility in the
  `UserAttributeSimilarityValidator` class. `UserAttributeSimilarityValidator`
  incurred significant overhead evaluating submitted password that were
  artificially large in relative to the comparison values. On the assumption
  that access to user registration was unrestricted this provided a potential
  vector for a denial-of-service attack. In order to mitigate this issue,
  relatively long values are now ignored by this class.

* CVE-2021-45116: Potential information disclosure in `dictsort` template
  filter. Due to leveraging the Django Template Language's variable resolution
  logic, the `dictsort` template filter was potentially vulnerable to
  information disclosure or unintended method calls, if passed a suitably
  crafted key. In order to avoid this possibility, `dictsort` now works with a
  restricted resolution logic, that will not call methods, nor allow indexing
  on dictionaries.

* CVE-2021-45452: Potential directory-traversal via `Storage.save()`.
  `Storage.save()` allowed directory-traversal if directly passed suitably
  crafted file names.

* CVE-2023-24580: Potential denial-of-service vulnerability in file uploads.
  Passing certain inputs to multipart forms could result in too many open files
  or memory exhaustion, and provided a potential vector for a denial-of-service
  attack. The number of files parts parsed is now limited via the new
  `DATA_UPLOAD_MAX_NUMBER_FILES` setting.

* CVE-2023-31047: Prevent a potential bypass of validation when uploading
  multiple files using one form field. Uploading multiple files using one form
  field has never been supported by `forms.FileField` or `forms.ImageField` as
  only the last uploaded file was validated. Unfortunately, Uploading multiple
  files topic suggested otherwise. In order to avoid the vulnerability, the
  `ClearableFileInput` and `FileInput` form widgets now raise `ValueError` when
  the multiple HTML attribute is set on them. To prevent the exception and keep
  the old behavior, set the `allow_multiple_selected` attribute to `True`.
