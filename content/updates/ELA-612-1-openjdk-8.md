---
title: "ELA-612-1 openjdk-8 security update"
package: openjdk-8
version: 8u332-ga-1~deb8u1
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2022-05-14T11:25:52+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-21426
  - CVE-2022-21434
  - CVE-2022-21443
  - CVE-2022-21476
  - CVE-2022-21496

---

Several vulnerabilities have been discovered in the OpenJDK Java
runtime, which may result in information disclosure or denial of service.
