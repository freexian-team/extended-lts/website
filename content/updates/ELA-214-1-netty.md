---
title: "ELA-214-1 netty security update"
package: netty
version: 3.2.6.Final-2+deb7u2
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2020-02-19T18:24:48+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-20444
  - CVE-2019-20445
  - CVE-2020-7238

---

Several vulnerabilities were discovered in the HTTP server provided by
Netty, a Java NIO client/server socket framework:

* CVE-2019-20444

    HttpObjectDecoder.java allows an HTTP header that lacks a colon,
    which might be interpreted as a separate header with an incorrect
    syntax, or might be interpreted as an "invalid fold."

* CVE-2019-20445

    HttpObjectDecoder.java allows a Content-Length header to be
    accompanied by a second Content-Length header, or by a
    Transfer-Encoding header.

* CVE-2020-7238

    Netty allows HTTP Request Smuggling because it mishandles
    Transfer-Encoding whitespace (such as a
    [space]Transfer-Encoding:chunked line) and a later Content-Length
    header.
