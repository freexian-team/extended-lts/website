---
title: "ELA-906-1 monit security update"
package: monit
version: 1:5.9-1+deb8u3 (jessie), 1:5.20.0-6+deb9u3 (stretch)
version_map: {"8 jessie": "1:5.9-1+deb8u3", "9 stretch": "1:5.20.0-6+deb9u3"}
description: "authentication bypass"
date: 2023-07-30T22:33:32+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-26563

---

Youssef Rebahi-Gilbert discovered that users with disabled accounts but with a
valid password can login to Monit, a utility for monitoring and managing
daemons or similar programs, due to a flaw in the PAM authentication check.
