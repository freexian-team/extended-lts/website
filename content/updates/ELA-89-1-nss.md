---
title: "ELA-89-1 nss security update"
package: nss
version: 2:3.26-1+debu7u6
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2019-03-05T03:17:03Z
draft: false
type: updates
cvelist:
  - CVE-2018-12404
  - CVE-2018-18508

---

Vulnerabilities have been discovered in nss, the Mozilla Network
Security Service library.

CVE-2018-12404

    Cache side-channel variant of the Bleichenbacher attack

CVE-2018-18508

    NULL pointer dereference in several CMS functions resulting in a
    denial of service

