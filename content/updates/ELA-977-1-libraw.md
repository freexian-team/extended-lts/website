---
title: "ELA-977-1 libraw security update"
package: libraw
version: 0.17.2-6+deb9u5 (stretch)
version_map: {"9 stretch": "0.17.2-6+deb9u5"}
description: "buffer overflow"
date: 2023-10-03T08:57:46+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-22628
  - CVE-2021-32142

---

Two buffer overflow vulnerabilities were found in libraw, a raw image
decoder library, which could lead to denial of service via application
crash or potentially other unspecified impact.
