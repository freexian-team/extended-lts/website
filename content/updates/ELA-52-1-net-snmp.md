---
title: "ELA-52-1 net-snmp security update"
package: net-snmp
version: 5.4.3~dfsg-2.8+deb7u3
distribution: "Debian 7 Wheezy"
description: "fix denial of service"
date: 2018-10-15T19:56:03+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-18065
---

Magnus K. Stubman found that an authenticated remote attacker could crash an instance of Net-SNMP by sending a specially crafted UDP packet resulting in a denial-of-service.

