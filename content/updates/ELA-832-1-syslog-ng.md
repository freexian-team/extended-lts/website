---
title: "ELA-832-1 syslog-ng security update"
package: syslog-ng
version: 3.5.6-2+deb8u1 (jessie), 3.8.1-10+deb9u1 (stretch)
version_map: {"8 jessie": "3.5.6-2+deb8u1", "9 stretch": "3.8.1-10+deb9u1"}
description: "denial of service"
date: 2023-04-16T14:41:10+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-38725

---

It was discovered that an integer overflow in the RFC3164 parser of
syslog-ng, a system logging daemon, may result in denial of service
via malformed syslog messages.
