---
title: "ELA-107-1 libxslt security update"
package: libxslt
version: 1.1.26-14.1+deb7u4
distribution: "Debian 7 Wheezy"
description: "fix authentication bypass vulnerability"
date: 2019-04-16T18:08:08+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-11068

---

It was discovered that there was a authentication bypass vulnerability in
libxslt, a widely-used library for transforming files from XML to other
arbitrary format.

This vulnerability was caused by invalid handling of xsltCheckRead and
xsltCheckWrite -1 error return value, handled as a success code. Remote
attackers could leverage this vulnerability to bypass protection mechanisms
and possibly cause unauthorized disclosure of information or modification.
