---
title: "ELA-854-1 openjdk-8 security update"
package: openjdk-8
version: 8u372-ga-1~deb8u1 (jessie), 8u372-ga-1~deb9u1 (stretch)
version_map: {"8 jessie": "8u372-ga-1~deb8u1", "9 stretch": "8u372-ga-1~deb9u1"}
description: "multiple vulnerabilities"
date: 2023-05-26T10:34:51+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-21930
  - CVE-2023-21937
  - CVE-2023-21938
  - CVE-2023-21939
  - CVE-2023-21954
  - CVE-2023-21967
  - CVE-2023-21968

---

Several vulnerabilities have been discovered in the OpenJDK Java
runtime, which may result in information disclosure or denial of service.
