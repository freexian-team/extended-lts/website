---
title: "ELA-96-1 bash security update"
package: bash
version: 4.2+dfsg-0.1+deb7u5
distribution: "Debian 7 Wheezy"
description: "denial of service and restricted shell bypass"
date: 2019-03-25T12:56:01+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-9401
  - CVE-2019-9924

---

Two issues have been fixed in bash, the GNU Bourne-Again Shell:

CVE-2016-9401

    The popd builtin segfaulted when called with negative out of range
    offsets.

CVE-2019-9924

    Sylvain Beucler discovered that it was possible to call commands
    that contained a slash when in restricted mode (rbash) by adding
    them to the BASH_CMDS array.
