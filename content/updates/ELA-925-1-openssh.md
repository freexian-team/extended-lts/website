---
title: "ELA-925-1 openssh security update"
package: openssh
version: 1:6.7p1-5+deb8u9 (jessie), 1:7.4p1-10+deb9u8 (stretch)
version_map: {"8 jessie": "1:6.7p1-5+deb8u9", "9 stretch": "1:7.4p1-10+deb9u8"}
description: "remote code execution"
date: 2023-08-18T05:00:01+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-38408

---

	
A vulnerability was found in OpenSSH. The PKCS#11 feature in the ssh-agent in
OpenSSH has an insufficiently trustworthy search path, leading to remote code
execution if an agent is forwarded to an attacker-controlled system (the code
in /usr/lib is not necessarily safe for loading into ssh-agent).

This flaw allows an attacker with control of the forwarded agent-socket on the
server and the ability to write to the filesystem of the client host to execute
arbitrary code with the privileges of the user running the ssh-agent.
