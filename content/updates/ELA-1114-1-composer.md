---
title: "ELA-1114-1 composer security update"
package: composer
version: 1.2.2-1+deb9u3 (stretch)
version_map: {"9 stretch": "1.2.2-1+deb9u3"}
description: "command-line injection vulnerability"
date: 2024-06-19T12:25:28-07:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-35241
  - CVE-2024-35242
---

It was discovered that there were a number of command-line injection
vulnerabilities in Composer, a popular dependency manager for PHP.

The `install`, `status`, `reinstall` and `remove` functionality had issues when
used with Git or Hg repositories which used maliciously- crafted branch names,
which could have been abused to execute arbitrary shell commands.
