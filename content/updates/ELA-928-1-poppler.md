---
title: "ELA-928-1 poppler security update"
package: poppler
version: 0.26.5-2+deb8u16 (jessie), 0.48.0-2+deb9u6 (stretch)
version_map: {"8 jessie": "0.26.5-2+deb8u16", "9 stretch": "0.48.0-2+deb9u6"}
description: "two vulnerabilities"
date: 2023-08-21T17:04:48+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-36023
  - CVE-2020-36024

---

Two vulnerabilities have been fixed in poppler,
a PDF rendering library.

CVE-2020-36023

    Infinite loop in FoFiType1C::cvtGlyph()

CVE-2020-36024

    NULL dereference in FoFiType1C::convertToType1()

