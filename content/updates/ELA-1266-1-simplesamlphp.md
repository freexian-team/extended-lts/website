---
title: "ELA-1266-1 simplesamlphp security update"
package: simplesamlphp
version: 1.16.3-1+deb10u3 (buster)
version_map: {"10 buster": "1.16.3-1+deb10u3"}
description: "multiple XXE vulnerabilities"
date: 2024-12-03T12:01:32-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-52596
  - CVE-2024-52806

---

It was discovered that in SimpleSAMLphp, an implementation of the SAML 2.0
protocol, is prone to XML external entity (XXE) vulnerabilities when loading
(untrusted) XML documents or parsing SAML messages.
