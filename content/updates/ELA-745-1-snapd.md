---
title: "ELA-745-1 snapd security update"
package: snapd
version: 2.21-2+deb9u2 (stretch)
version_map: {"9 stretch": "2.21-2+deb9u2"}
description: "privileg escalation"
date: 2022-11-30T23:29:26+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-44730
  - CVE-2021-44731

---

Multiple vulnerabilties were discovered in snapd, a daemon and tooling that enable Snap packages,
which could result in bypass of access restrictions or privilege escalation.
