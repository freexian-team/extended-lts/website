---
title: "ELA-694-1 tzdata new timezone database"
package: tzdata
version: 2021a-0+deb8u6 (jessie), 2021a-0+deb9u6 (stretch)
version_map: {"8 jessie": "2021a-0+deb8u6", "9 stretch": "2021a-0+deb9u6"}
description: "updated timezone database"
date: 2022-10-03T14:15:09+02:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2022d. Notable
changes are:

- Palestine now switches back to standard time on October 29.
- Updated leap second list, which was set to expire by the end of
  December.
