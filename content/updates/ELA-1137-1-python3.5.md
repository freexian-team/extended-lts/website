---
title: "ELA-1137-1 python3.5 security update"
package: python3.5
version: 3.5.3-1+deb9u10 (stretch)
version_map: {"9 stretch": "3.5.3-1+deb9u10"}
description: "multiple vulnerabilities"
date: 2024-07-23T16:05:19+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-0397
  - CVE-2024-4032
  - CVE-2024-5642

---

Multiple vulnerabilities have been fixed in the Python3 interpreter.

CVE-2024-0397

    Race condition in ssl.SSLContext

CVE-2024-4032

    Incorrect information about private addresses in the ipaddress module

CVE-2024-5642

    NPN buffer overread when using empty list in SSLContext.set_npn_protocols()

Note that the CVE-2024-5642 fix disables NPN (Next Protocol Negotiation) in the ssl module, NPN is a TLS extension for the obsolete SPDY protocol (HTTP/2 is the successor to SPDY). Support for the NPN-successor ALPN for HTTP/2 continues to be available.
