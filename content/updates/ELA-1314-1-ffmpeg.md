---
title: "ELA-1314-1 ffmpeg security update"
package: ffmpeg
version: 7:3.2.19-0+deb9u6 (stretch)
version_map: {"9 stretch": "7:3.2.19-0+deb9u6"}
description: "integer overflow, double-free, out-of-bounds access, incomplete checks"
date: 2025-02-01T00:33:13+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-35366
  - CVE-2024-35367
  - CVE-2024-36616
  - CVE-2024-36617
  - CVE-2024-36618

---

Several issues have been found in ffmpeg, a package that contains tools
for transcoding, streaming and playing of multimedia files
Those issues are related to possible integer overflows, double-free on
errors, out-of-bounds access and an incomplete check of negative durations.

