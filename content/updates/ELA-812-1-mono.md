---
title: "ELA-812-1 mono security update"
package: mono
version: 4.6.2.7+dfsg-1+deb9u1 (stretch)
version_map: {"9 stretch": "4.6.2.7+dfsg-1+deb9u1"}
description: "execution of arbitrary code"
date: 2023-03-05T23:11:42+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-26314

---

Triggering arbitrary code execution was possible due to .desktop files
registered as application/x-ms-dos-executable MIME handlers in the open
source .NET framework Mono.
