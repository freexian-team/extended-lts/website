---
title: "ELA-975-1 libvpx security update"
package: libvpx
version: 1.6.1-3+deb9u5 (stretch)
version_map: {"9 stretch": "1.6.1-3+deb9u5"}
description: "buffer overflow"
date: 2023-10-02T13:17:41+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-44488

---

A buffer overflow vulnerability was found in libvpx, a multimedia
library for the VP8 and VP9 video codecs, which could result in the
execution of arbitrary code if a specially crafted VP9 media stream
is processed.
