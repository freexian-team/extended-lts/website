---
title: "ELA-972-1 exempi security update"
package: exempi
version: 2.4.1-1+deb9u2 (stretch)
version_map: {"9 stretch": "2.4.1-1+deb9u2"}
description: "buffer overflows"
date: 2023-09-30T21:35:20+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-18651
  - CVE-2020-18652

---

Buffer overflows were fixed in the functions ID3_Support::ID3v2Frame::getFrameValue()
and WEBP_Support::VP8XChunk::VP8XChunk() of Exempi, an implementation of XMP (Extensible Metadata Platform).
