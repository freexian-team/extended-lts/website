---
title: "ELA-773-1 pjproject security update"
package: pjproject
version: 2.5.5~dfsg-6+deb9u8 (stretch)
version_map: {"9 stretch": "2.5.5~dfsg-6+deb9u8"}
description: "multiple vulnerabilities"
date: 2023-01-18T17:11:47+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-23537
  - CVE-2022-23547

---

Multiple security issues were discovered in pjproject, a free and open
source multimedia communication library written in C implementing
standard based protocols such as SIP, SDP, RTP, STUN, TURN, and ICE

CVE-2022-23537

    Buffer overread when parsing a specially crafted STUN message with
    unknown attribute. The vulnerability affects applications that
    uses STUN including PJNATH and PJSUA-LIB.


CVE-2022-23547

    Possible buffer overread when parsing a certain STUN message.
    The vulnerability affects applications that uses STUN including
    PJNATH and PJSUA-LIB.
