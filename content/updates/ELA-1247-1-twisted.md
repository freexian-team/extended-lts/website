---
title: "ELA-1247-1 twisted security update"
package: twisted
version: 18.9.0-3+deb10u3 (buster)
version_map: {"10 buster": "18.9.0-3+deb10u3"}
description: "multiple vulnerabilities"
date: 2024-11-28T16:17:48+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-46137
  - CVE-2024-41671
  - CVE-2024-41810

---

Multiple security issues were found in Twisted, an event-based framework
for internet applications, which could result in incorrect ordering of
HTTP requests or cross-site scripting.

* CVE-2023-46137

    When sending multiple HTTP requests in one TCP packet, twisted.web
    will process the requests asynchronously without guaranteeing the
    response order. If one of the endpoints is controlled by an
    attacker, the attacker can delay the response on purpose to
    manipulate the response of the second request when a victim
    launched two requests using HTTP pipeline.

* CVE-2024-41671

    The HTTP 1.0 and 1.1 server provided by twisted.web could process
    pipelined HTTP requests out-of-order, possibly resulting in
    information disclosure.

* CVE-2024-41810

    The `twisted.web.util.redirectTo` function contains an HTML
    injection vulnerability. If application code allows an attacker to
    control the redirect URL this vulnerability may result in
    Reflected Cross-Site Scripting (XSS) in the redirect response HTML
    body.
