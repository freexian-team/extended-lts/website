---
title: "ELA-1150-1 ruby2.5 security update"
package: ruby2.5
version: 2.5.5-3+deb10u7 (buster)
version_map: {"10 buster": "2.5.5-3+deb10u7"}
description: "multiple vulnerabilities"
date: 2024-08-13T13:06:52+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-36617
  - CVE-2024-27280
  - CVE-2024-27281
  - CVE-2024-27282

---

Several vulnerabilities have been discovered in the interpreter for
the Ruby language, which may result in denial-of-service (DoS),
information leak, and remote code execution.


* CVE-2023-36617

    Follow-up fix for CVE-2023-28755.

    A ReDoS issue was discovered in the URI component. The URI parser
    mishandles invalid URLs that have specific characters. It causes
    an increase in execution time for parsing strings to URI objects.

* CVE-2024-27280

    A buffer-overread issue was discovered in StringIO. The ungetbyte
    and ungetc methods on a StringIO can read past the end of a
    string, and a subsequent call to StringIO.gets may return the
    memory value.

* CVE-2024-27281

    When parsing .rdoc_options (used for configuration in RDoc) as a
    YAML file, object injection and resultant remote code execution
    are possible because there are no restrictions on the classes that
    can be restored. (When loading the documentation cache, object
    injection and resultant remote code execution are also possible if
    there were a crafted cache.)

* CVE-2024-27282

    If attacker-supplied data is provided to the Ruby regex compiler,
    it is possible to extract arbitrary heap data relative to the
    start of the text, including pointers and sensitive strings.
