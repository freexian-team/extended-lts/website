---
title: "ELA-600-1 golang security update"
package: golang
version: 2:1.3.3-1+deb8u5
distribution: "Debian 8 jessie"
description: "invalid cryptographic computation"
date: 2022-04-28T11:36:26+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-23806

---

In the Go programming language, Curve.IsOnCurve in crypto/elliptic can
incorrectly return true in situations with a big.Int value that is not
a valid field element. Operating on those values may cause a panic or
an invalid curve operation.
