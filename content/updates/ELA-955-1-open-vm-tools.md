---
title: "ELA-955-1 open-vm-tools security update"
package: open-vm-tools
version: 2:10.1.5-5055683-4+deb9u5 (stretch)
version_map: {"9 stretch": "2:10.1.5-5055683-4+deb9u5"}
description: "privilege escalation"
date: 2023-09-22T19:49:26+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-20900

---

A security vulnerability was found in the Open VMware Tools. A malicious actor
that has been granted Guest Operation Privileges in a target virtual machine
may be able to elevate their privileges if that target virtual machine has
been assigned a more privileged Guest Alias.

