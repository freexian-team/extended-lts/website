---
title: "ELA-919-1 hdf5 security update"
package: hdf5
version: 1.10.0-patch1+docs-3+deb9u2 (stretch)
version_map: {"9 stretch": "1.10.0-patch1+docs-3+deb9u2"}
description: "denial of service"
date: 2023-08-14T00:35:20+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-17233
  - CVE-2018-17234
  - CVE-2018-17237
  - CVE-2018-17434
  - CVE-2018-17437

---

Multiple security vulnerabilities were discovered in HDF5, a Hierarchical Data
Format and a library for scientific data. Memory leaks, out-of-bound reads and
division by zero errors may lead to a denial of service when processing a
malformed HDF file.
