---
title: "ELA-1187-1 cups-filters security update"
package: cups-filters
version: 1.0.61-5+deb8u5 (jessie)
version_map: {"8 jessie": "1.0.61-5+deb8u5"}
description: "arbitrary code execution"
date: 2024-09-29T23:47:39+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-47176

---

Simone Margaritelli an issue in cups-filters.
Multiple bugs in the cups-browsed component can result in the execution
of arbitrary commands without authentication when a print job is
started.

(Jessie is only affected by CVE-2024-47176; the code for CVE-2024-47076 is not available)
