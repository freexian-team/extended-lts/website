---
title: "ELA-37-2 openssh regression update"
package: openssh
version: 1:6.0p1-4+deb7u9
distribution: "Debian 7 Wheezy"
description: "regression update"
date: 2018-09-17T13:26:26+02:00
draft: false
type: updates
cvelist:

---

It was discovered that the recent openssh update issued as ELA-37-1 caused a
regression. Authentication failed during public key exchange and a NULL pointer was
passed as argument instead. This could prevent a user from logging into a
system. This update reverts to the previous state until more information are
available.
