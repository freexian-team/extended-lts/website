---
title: "ELA-927-1 ffmpeg security update"
package: ffmpeg
version: 7:3.2.19-0+deb9u3 (stretch)
version_map: {"9 stretch": "7:3.2.19-0+deb9u3"}
description: "integer overflow"
date: 2023-08-21T10:28:42+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-28429

---

An issue has been found in ffmpeg, a tool/library for transcoding, streaming and playing of multimedia files.
Due to an integer overflow in av_timecode_make_string() in libavutil/timecode.c, local attackers might cause a Dos with crafted .mov files.

