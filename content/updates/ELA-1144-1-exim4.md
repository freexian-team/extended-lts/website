---
title: "ELA-1144-1 exim4 security update"
package: exim4
version: 4.89-2+deb9u13 (stretch), 4.92-8+deb10u10 (buster)
version_map: {"9 stretch": "4.89-2+deb9u13", "10 buster": "4.92-8+deb10u10"}
description: "wrong parsing of multiline RFC 2231 header filenames"
date: 2024-07-31T23:08:08+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-39929

---

An issue has been found in exim4, the Mail Transport Agent.
Due to bad parsing of multiline RFC 2231 header filenames in mime ACL,
a remote attacker could bypass this protection mechanism and potentially
deliver executable attachements to mailboxes.
