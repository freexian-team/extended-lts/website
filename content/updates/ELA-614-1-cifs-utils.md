---
title: "ELA-614-1 cifs-utils security update"
package: cifs-utils
version: 2:6.4-1+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2022-05-16T19:16:35+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-27239
  - CVE-2022-29869

---

A couple of vulnerabilities were found in src:cifs-utils, a Common
Internet File System utilities, and are as follows:

CVE-2022-27239

    In cifs-utils, a stack-based buffer overflow when parsing the
    mount.cifs ip= command-line argument could lead to local attackers
    gaining root privileges.

CVE-2022-29869

    cifs-utils, with verbose logging, can cause an information leak
    when a file contains = (equal sign) characters but is not a valid
    credentials file.
