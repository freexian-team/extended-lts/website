---
title: "ELA-882-1 postgresql-9.4 security update"
package: postgresql-9.4
version: 9.4.26-0+deb8u7 (jessie)
version_map: {"8 jessie": "9.4.26-0+deb8u7"}
description: "arbitrary code execution"
date: 2023-06-29T20:43:30Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-2454

---

schema_element defeats protective search_path changes; It was found that certain database calls in PostgreSQL could permit an attacker with elevated database-level privileges to execute arbitrary code.
