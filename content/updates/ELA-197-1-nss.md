---
title: "ELA-197-1 nss security update"
package: nss
version: 2:3.26-1+debu7u10
distribution: "Debian 7 Wheezy"
description: "fix NULL deref leading to DoS"

date: 2019-11-29T21:36:19+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-17007

---

Handling of Netscape Certificate Sequences in CERT_DecodeCertPackage()
may have crashed with a NULL deref leading to a Denial-of-Service.

