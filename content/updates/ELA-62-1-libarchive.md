---
title: "ELA-62-1 libarchive security update"
package: libarchive
version: 3.0.4-3+wheezy6+deb7u2
distribution: "Debian 7 Wheezy"
description: "denial-of-service"
date: 2018-11-30T21:01:41+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-14501
  - CVE-2017-14502
  - CVE-2017-14503
---

Several security vulnerabilities were found in libarchive, a multi-format
archive and compression library. Heap-based buffer over-reads, NULL pointer
dereferences and out-of-bounds reads allow remote attackers to cause a
denial-of-service (application crash) via specially crafted archive files.
