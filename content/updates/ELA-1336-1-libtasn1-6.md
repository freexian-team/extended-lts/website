---
title: "ELA-1336-1 libtasn1-6 security update"
package: libtasn1-6
version: 4.2-3+deb8u6 (jessie), 4.10-1.1+deb9u3 (stretch), 4.13-3+deb10u2 (buster)
version_map: {"8 jessie": "4.2-3+deb8u6", "9 stretch": "4.10-1.1+deb9u3", "10 buster": "4.13-3+deb10u2"}
description: "denial of service vulnerability"
date: 2025-02-28T18:25:03+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-12133

---

Bing Shi discovered that certificate data with a large number of names
or name constraints were handled inefficiently, which may lead to Denial
of Service upon specially crafted certificates.
