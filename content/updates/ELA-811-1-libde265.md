---
title: "ELA-811-1 libde265 security update"
package: libde265
version: 1.0.11-0+deb9u3 (stretch)
version_map: {"9 stretch": "1.0.11-0+deb9u3"}
description: "multiple vulnerabilities"
date: 2023-03-05T09:24:34+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-47664
  - CVE-2022-47665
  - CVE-2023-24751
  - CVE-2023-24752
  - CVE-2023-24754
  - CVE-2023-24755
  - CVE-2023-24756
  - CVE-2023-24757
  - CVE-2023-24758
  - CVE-2023-25221

---

Multiple issues were found in libde265, an open source implementation of the
h.265 video codec, which may result in denial of service, have unspecified
other impact, possibly code execution due to a heap-based buffer overflow.

CVE-2022-47664

    Libde265 1.0.9 is vulnerable to Buffer Overflow in
    ff_hevc_put_hevc_qpel_pixels_8_sse

CVE-2022-47665

    Libde265 1.0.9 has a heap buffer overflow vulnerability in
    de265_image::set_SliceAddrRS(int, int, int)

CVE-2023-24751

    libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the mc_chroma function at motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS)
    via a crafted input file.

CVE-2023-24752

    libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the ff_hevc_put_hevc_epel_pixels_8_sse function at
    sse-motion.cc. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted input file.

CVE-2023-24754

    libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the ff_hevc_put_weighted_pred_avg_8_sse function at
    sse-motion.cc. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted input file.

CVE-2023-24755

    libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the put_weighted_pred_8_fallback function at
    fallback-motion.cc. This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted input file.

CVE-2023-24756

    libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the ff_hevc_put_unweighted_pred_8_sse function at
    sse-motion.cc. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted input file.

CVE-2023-24757

    libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the put_unweighted_pred_16_fallback function at
    fallback-motion.cc. This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted input file.

CVE-2023-24758

    libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the ff_hevc_put_weighted_pred_avg_8_sse function at
    sse-motion.cc. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted input file.

CVE-2023-25221

    Libde265 v1.0.10 was discovered to contain a heap-buffer-overflow
    vulnerability in the derive_spatial_luma_vector_prediction function
    in motion.cc.

