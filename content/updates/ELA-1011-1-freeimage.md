---
title: "ELA-1011-1 freeimage security update"
package: freeimage
version: 3.17.0+ds1-5+deb9u2
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2023-11-28T07:14:06+01:00
draft: false
cvelist:
  - CVE-2020-21427
  - CVE-2020-22524

---

Multiple vulnerabilities were discovered in freeimage, library for graphics
image formats.

CVE-2020-21427

    Buffer overflow vulnerability in function LoadPixelDataRLE8
    in PluginBMP.cpp allows remote attackers to run arbitrary code and cause
    other impacts via crafted image file.

CVE-2020-22524

    Buffer overflow vulnerability in FreeImage_Load function
    allows remote attackers to run arbitrary code and cause other
    impacts via crafted PFM file.
