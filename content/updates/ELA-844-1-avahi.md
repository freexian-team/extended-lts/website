---
title: "ELA-844-1 avahi security update"
package: avahi
version: 0.6.31-5+deb8u2 (jessie), 0.6.32-2+deb9u2 (stretch)
version_map: {"8 jessie": "0.6.31-5+deb8u2", "9 stretch": "0.6.32-2+deb9u2"}
description: "local Denial of Service (DoS) vulnerability"
date: 2023-05-02T11:29:35-07:00
draft: false
cvelist:
  - CVE-2023-1981
---

It was discovered that there was a local Denial of Service (DoS) vulnerability
in [Avahi](https://www.avahi.org/), a system that facilitates service discovery
on a local network.

The `avahi-daemon` process could have been crashed over the DBus message bus.
