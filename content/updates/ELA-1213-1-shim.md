---
title: "ELA-1213-1 shim new certificates"
package: shim
version: 15.8-1~deb10u2 (buster)
version_map: {"10 buster": "15.8-1~deb10u2"}
description: "new signing certificate from Freexian"
date: 2024-10-24T14:10:36+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

In order to support Secure Boot in buster ELTS, the shim needs to have
the Freexian public certificate used to sign Linux kernels and other
packages. This update adds that certificate to the shim alongside
the Debian public CA, which allows to boot both old (signed by Debian)
and new (signed by Freexian) packages.

The respective shim-signed package has also been updated to reflect
this change.

In order to be able to boot future kernel security updates on setups
where Secure Boot is enabled, these shim packages need to be upgraded,
otherwise the old versions will not be able to verify the new signatures
and the bootloader will refuse to load those kernel versions.
