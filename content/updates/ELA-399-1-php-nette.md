---
title: "ELA-399-1 php-nette security update"
package: php-nette
version: 2.1.5-1+deb8u1
distribution: "Debian 8 jessie"
description: "remote code execution"
date: 2021-04-04T11:14:04+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-15227

---

php-nette, a PHP MVC framework, is vulnerable to a code injection attack by
passing specially formed parameters to URL that may possibly leading to remote
code execution.
