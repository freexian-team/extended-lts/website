---
title: "ELA-743-1 squid3 security update"
package: squid3
version: 3.5.23-5+deb8u6 (jessie), 3.5.23-5+deb9u9 (stretch)
version_map: {"8 jessie": "3.5.23-5+deb8u6", "9 stretch": "3.5.23-5+deb9u9"}
description: "multiple vulnerabilities"
date: 2022-11-29T15:06:12+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-41317
  - CVE-2022-41318

---

This update fixes two vulnerabilities in squid3

CVE-2022-41317

    Due to inconsistent handling of internal URIs Squid is
    vulnerable to Exposure of Sensitive Information about clients
    using the proxy.

CVE-2022-41318

    Due to an incorrect integer overflow protection Squid SSPI and
    SMB authentication helpers are vulnerable to a Buffer Overflow
    attack.
