---
title: "ELA-907-1 linux-4.19 security update"
package: linux-4.19
version: 4.19.289-1~deb8u1 (jessie), 4.19.289-1~deb9u1 (stretch)
version_map: {"8 jessie": "4.19.289-1~deb8u1", "9 stretch": "4.19.289-1~deb9u1"}
description: "linux kernel update"
date: 2023-07-31T12:02:43+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-1380
  - CVE-2023-2002
  - CVE-2023-2007
  - CVE-2023-2269
  - CVE-2023-3090
  - CVE-2023-3111
  - CVE-2023-3141
  - CVE-2023-3268
  - CVE-2023-3338
  - CVE-2023-20593
  - CVE-2023-31084
  - CVE-2023-32233
  - CVE-2023-34256
  - CVE-2023-35788
  - CVE-2023-35823
  - CVE-2023-35824
  - CVE-2023-35828

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2023-1380

    Jisoo Jang reported a heap out-of-bounds read in the brcmfmac
    Wi-Fi driver.  On systems using this driver, a local user could
    exploit this to read sensitive information or to cause a denial of
    service (crash).

CVE-2023-2002

    Ruiahn Li reported an incorrect permissions check in the Bluetooth
    subsystem.  A local user could exploit this to reconfigure local
    Bluetooth interfaces, resulting in information leaks, spoofing, or
    denial of service (loss of connection).

CVE-2023-2007

    Lucas Leong (@_wmliang_) and Reno Robert of Trend Micro Zero Day
    Initiative discovered a time-of-check-to-time-of-use flaw in the
    dpt_i2o SCSI controller driver.  A local user with access to a
    SCSI device using this driver could exploit this for privilege
    escalation.

    This flaw has been mitigated by removing support for the I2OUSRCMD
    operation.

CVE-2023-2269

    Zheng Zhang reported that improper handling of locking in the
    device mapper implementation may result in denial of service.

CVE-2023-3090

    It was discovered that missing initialization in ipvlan networking
    may lead to an out-of-bounds write vulnerability, resulting in
    denial of service or potentially the execution of arbitrary code.

CVE-2023-3111

    The TOTE Robot tool found a flaw in the Btrfs filesystem driver
    that can lead to a use-after-free.  It's unclear whether an
    unprivileged user can exploit this.

CVE-2023-3141

    A flaw was discovered in the r592 memstick driver that could lead
    to a use-after-free after the driver is removed or unbound from a
    device.  The security impact of this is unclear.

CVE-2023-3268

    It was discovered that an out-of-bounds memory access in relayfs
    could result in denial of service or an information leak.

CVE-2023-3338

    Ornaghi Davide discovered a flaw in the DECnet protocol
    implementation which could lead to a null pointer dereference or
    use-after-free.  A local user can exploit this to cause a denial
    of service (crash or memory corruption) and probably for privilege
    escalation.

    This flaw has been mitigated by removing the DECnet protocol
    implementation.

CVE-2023-20593

    Tavis Ormandy discovered that under specific microarchitectural
    circumstances, a vector register in AMD "Zen 2" CPUs may not be
    written to 0 correctly.  This flaw allows an attacker to leak
    sensitive information across concurrent processes, hyper threads
    and virtualized guests.

    For details please refer to
    <https://lock.cmpxchg8b.com/zenbleed.html> and
    <https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8>.

    This issue can also be mitigated by a microcode update through the
    amd64-microcode package or a system firmware (BIOS/UEFI) update.
    However, the initial microcode release by AMD only provides
    updates for second generation EPYC CPUs.  Various Ryzen CPUs are
    also affected, but no updates are available yet.

CVE-2023-31084

    It was discovered that the DVB Core driver does not properly
    handle locking of certain events, allowing a local user to cause a
    denial of service.

CVE-2023-32233

    Patryk Sondej and Piotr Krysiuk discovered a use-after-free flaw
    in the Netfilter nf_tables implementation when processing batch
    requests, which may result in local privilege escalation for a
    user with the CAP_NET_ADMIN capability in any user or network
    namespace.

CVE-2023-34256

    The syzbot tool found a time-of-check-to-time-of-use flaw in the
    ext4 filesystem driver.  An attacker able to mount a disk image or
    device that they can also write to directly could exploit this to
    cause an out-of-bounds read, possibly resulting in a leak of
    sensitive information or denial of service (crash).

CVE-2023-35788

    Hangyu Hua discovered an out-of-bounds write vulnerability in the
    Flower classifier which may result in denial of service or the
    execution of arbitrary code.

CVE-2023-35823

    A flaw was discovered in the saa7134 media driver that could lead
    to a use-after-free after the driver is removed or unbound from a
    device.  The security impact of this is unclear.

CVE-2023-35824

    A flaw was discovered in the dm1105 media driver that could lead
    to a use-after-free after the driver is removed or unbound from a
    device.  The security impact of this is unclear.

CVE-2023-35828

    A flaw was discovered in the renesas_udc USB device-mode driver
    that could lead to a use-after-free after the driver is removed or
    unbound from a device.  The security impact of this is unclear.

    This driver is not enabled in Debian's official kernel
    configurations.
