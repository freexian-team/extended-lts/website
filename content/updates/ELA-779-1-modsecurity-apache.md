---
title: "ELA-779-1 modsecurity-apache security update"
package: modsecurity-apache
version: 2.9.1-2+deb9u2 (stretch)
version_map: {"9 stretch": "2.9.1-2+deb9u2"}
description: "web application firewall bypass"
date: 2023-01-26T19:48:35+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-48279
  - CVE-2023-24021

---

Multiple issues were found in modsecurity-apache, open source, cross
platform web application firewall (WAF) engine for Apache which allows
remote attackers to bypass the applications firewall and other
unspecified impact.

CVE-2022-48279

    In ModSecurity before 2.9.6 and 3.x before 3.0.8, HTTP multipart
    requests were incorrectly parsed and could bypass the Web Application
    Firewall.
    NOTE: this is related to CVE-2022-39956 but can be considered
    independent changes to the ModSecurity (C language) codebase.

CVE-2023-24021

    Incorrect handling of null-bytes in file uploads in ModSecurity
    before 2.9.7 may allow for Web Application Firewall bypasses and
    buffer overflows on the Web Application Firewall when executing
    rules reading the FILES_TMP_CONTENT collection.

