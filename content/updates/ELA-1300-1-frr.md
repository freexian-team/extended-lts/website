---
title: "ELA-1300-1 frr security update"
package: frr
version: 7.5.1-1.1+deb10u4 (buster)
version_map:
  "10 buster": "7.5.1-1.1+deb10u4"
description: "route revalidation issue"
date: 2025-01-23T12:29:54+01:00
draft: false
type: updates
tags:
  - update
cvelist:
  - CVE-2024-55553

---

In FRR, all routes are re-validated if the total size of an update received via RTR exceeds the internal socket's buffer
size, default 4K on most OSes. An attacker can use this to trigger re-parsing of the RIB for FRR routers using RTR by
causing more than this number of updates during an update interval (usually 30 minutes). Additionally, this effect
regularly occurs organically. Furthermore, an attacker can use this to trigger route validation continuously. Given that
routers with large full tables may need more than 30 minutes to fully re-validate the table, continuous
issuance/withdrawal of large numbers of ROA may be used to impact the route handling performance of all FRR instances
using RPKI globally. Additionally, the re-validation will cause heightened BMP traffic to ingestors.
