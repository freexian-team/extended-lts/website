---
title: "ELA-757-1 proftpd-dfsg security update"
package: proftpd-dfsg
version: 1.3.5e+r1.3.5-2+deb8u8 (jessie), 1.3.5e+r1.3.5b-4+deb9u3 (stretch)
version_map: {"8 jessie": "1.3.5e+r1.3.5-2+deb8u8", "9 stretch": "1.3.5e+r1.3.5b-4+deb9u3"}
description: "memory disclosure"
date: 2022-12-25T23:50:38+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-46854

---

It was discovered that mod_radius in ProFTPD, a versatile, virtual-hosting FTP
daemon, allows memory disclosure to RADIUS servers.
