---
title: "ELA-1199-1 cups security update"
package: cups
version: 2.2.1-8+deb9u12 (stretch)
version_map: {"9 stretch": "2.2.1-8+deb9u12"}
description: "stronger validation of input data / fix domain socket handling"
date: 2024-10-06T19:37:30+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-35235
  - CVE-2024-47175

---

Two issues have been found in cups, the Common UNIX Printing System(tm).
This update introduces stronger validations of input data from external printers.

Please be aware that now bugs in the firmware of the printer might be detected. In case of problems, that should appear in the error.log, please update this firmware first.

The other issue is related to domain socket handling, where files might be overwritten.
