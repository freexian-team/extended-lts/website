---
title: "ELA-1248-1 twisted security update"
package: twisted
version: 16.6.0-2+deb9u5 (stretch)
version_map: {"9 stretch": "16.6.0-2+deb9u5"}
description: "multiple vulnerabilities"
date: 2024-11-28T16:18:16+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-41671
  - CVE-2024-41810

---

Multiple security issues were found in Twisted, an event-based framework
for internet applications, which could result in incorrect ordering of
HTTP requests or cross-site scripting.

* CVE-2024-41671

    The HTTP 1.0 and 1.1 server provided by twisted.web could process
    pipelined HTTP requests out-of-order, possibly resulting in
    information disclosure.

* CVE-2024-41810

    The `twisted.web.util.redirectTo` function contains an HTML
    injection vulnerability. If application code allows an attacker to
    control the redirect URL this vulnerability may result in
    Reflected Cross-Site Scripting (XSS) in the redirect response HTML
    body.
