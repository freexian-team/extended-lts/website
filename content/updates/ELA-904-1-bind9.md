---
title: "ELA-904-1 bind9 security update"
package: bind9
version: 1:9.9.5.dfsg-9+deb8u29 (jessie), 1:9.10.3.dfsg.P4-12.3+deb9u14 (stretch)
version_map: {"9 stretch": "1:9.10.3.dfsg.P4-12.3+deb9u14", "8 jessie": "1:9.9.5.dfsg-9+deb8u29"}
description: "denial-of-service vulnerability"
date: 2023-07-25T12:15:22+01:00
draft: false
cvelist:
  - CVE-2023-2828
---

A potential denial of service (DoS) vulnerability was discovered in bind9, the
popular DNS server.

Every `named` instance configured to run as a recursive resolver maintains a
cache database holding the responses to the queries it has recently sent to
authoritative servers. The size limit for that cache database can be configured
using the `max-cache-size` statement in the configuration file which defaults
to 90% of the total amount of memory available on the host. When the size of
the cache reaches 7/8 of the configured limit, a cache-cleaning algorithm
starts to remove expired and/or least-recently used RRsets from the cache in
order to keep memory use below the configured limit.

However, it was discovered that the effectiveness of the cache-cleaning
algorithm used in `named` can be severely diminished by querying the resolver
for specific RRsets in a certain order, effectively allowing the configured
max-cache-size limit to be significantly exceeded.

By exploiting this flaw, an attacker could have caused the amount of memory
used by a `named` resolver to go well beyond the configured `max-cache-size`
limit. The effectiveness of the attack depends on a number of factors (e.g.
query load, query patterns), but since the default value of the
`max-cache-size` statement is 90%, in the worst case, the attacker can exhaust
all available memory on the host running `named`, leading to a
denial-of-service condition.
