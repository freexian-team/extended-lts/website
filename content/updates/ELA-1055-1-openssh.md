---
title: "ELA-1055-1 openssh security update"
package: openssh
version: 1:6.7p1-5+deb8u10 (jessie)
version_map: {"8 jessie": "1:6.7p1-5+deb8u10"}
description: "multiple vulnerabilities"
date: 2024-03-11T12:41:11-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-41617
  - CVE-2023-51385

---

Several vulnerabilities have been discovered in OpenSSH, an implementation of
the SSH protocol suite.
 
CVE-2021-41617

    It was discovered that sshd failed to correctly initialise supplemental
    groups when executing an AuthorizedKeysCommand or
    AuthorizedPrincipalsCommand, where a AuthorizedKeysCommandUser or
    AuthorizedPrincipalsCommandUser directive has been set to run the command
    as a different user. Instead these commands would inherit the groups that
    sshd was started with.


CVE-2023-51385

    It was discovered that if an invalid user or hostname that contained shell
    metacharacters was passed to ssh, and a ProxyCommand, LocalCommand
    directive or "match exec" predicate referenced the user or hostname via
    expansion tokens, then an attacker who could supply arbitrary
    user/hostnames to ssh could potentially perform command injection. The
    situation could arise in case of git repositories with submodules, where the
    repository could contain a submodule with shell characters in its user or
    hostname.


Unfortunately, the changes required to fix the Terrapin Attack (CVE-2023-48795)
in jessie are too intrusive to be backported and represent a high risk of
introducing regressions. We also concluded that the Terrapin Attack is hardly
exploitable on the server side of the OpenSSH packaged in jessie, since it does
not support `EXT_INFO` messages, which are required to take advantage of the
attack. To mitigate this attack, we recommend to OpenSSH users to disable the
ChaCha20-Poly1305 algorithm from the allowed cipher suites used by both OpenSSH
client and server. For convenience, we include here examples of the `Ciphers`
configuration option that can be used removing ChaCha20-Poly1305 from the
default list. This is the example for OpenSSH server's `/etc/ssh/sshd_config`:

```
Ciphers aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com
```

And this is for system-wise OpenSSH client's `/etc/ssh/ssh_config`:

```
Ciphers aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com,arcfour256,arcfour128,aes128-cbc,3des-cbc,blowfish-cbc,cast128-cbc,aes192-cbc,aes256-cbc,arcfour
```

Users should adapt those examples to their local configuration.
