---
title: "ELA-1253-1 redis security update"
package: redis
version: 2:2.8.17-1+deb8u13 (jessie), 3:3.2.6-3+deb9u13 (stretch), 5:5.0.14-1+deb10u6 (buster)
version_map: {"8 jessie": "2:2.8.17-1+deb8u13", "9 stretch": "3:3.2.6-3+deb9u13", "10 buster": "5:5.0.14-1+deb10u6"}
description: "multiple vulnerabilities"
date: 2024-11-28T23:45:11+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-35977
  - CVE-2022-36021
  - CVE-2023-25155
  - CVE-2024-31228
  - CVE-2024-31449

---

Multiple vulnerabilities have been fixed in the key–value database Redis.

CVE-2022-35977

    integer overflows in SETRANGE and SORT

CVE-2022-36021 (jessie, stretch)

    string pattern matching DoS

CVE-2023-25155

    SRANDMEMBER integer overflow

CVE-2024-31228

    unbounded pattern matching DoS

CVE-2024-31449 (stretch)

    Lua bit library stack overflow
