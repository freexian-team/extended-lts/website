---
title: "ELA-1161-1 libvirt security update"
package: libvirt
version: 1.2.9-9+deb8u8 (jessie), 3.0.0-4+deb9u6 (stretch)
version_map: {"8 jessie": "1.2.9-9+deb8u8", "9 stretch": "3.0.0-4+deb9u6"}
description: "multiple vulnerabilities"
date: 2024-08-25T14:20:42+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-3631
  - CVE-2021-3975
  - CVE-2022-0897
  - CVE-2024-1441
  - CVE-2024-2494
  - CVE-2024-2496

---


Several issue have been found in libvirt, a library for interfacing with different virtualization systems.
The issues are related to use-after-free, an off-by-one, a null pointer dereference and badly handled mutex, which could be used for a denial of service.
The other issues are related to privilege escalation and breaking out of the sVirt confinement.

(strictly speaking CVE-2021-3975 only affects Stretch)
