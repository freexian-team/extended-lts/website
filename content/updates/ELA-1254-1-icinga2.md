---
title: "ELA-1254-1 icinga2 security update"
package: icinga2
version: 2.10.3-2+deb10u2 (buster)
version_map: {"10 buster": "2.10.3-2+deb10u2"}
description: "multiple vulnerabilities"
date: 2024-11-28T22:50:44+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-29663
  - CVE-2021-32739
  - CVE-2021-32743
  - CVE-2021-37698
  - CVE-2024-49369

---

Multiple vulnerabilities were discovered in icinga2, a general-purpose
monitoring application.

CVE-2020-29663

    An issue was discovered where revoked certificates due for renewal were
    automatically being renewed, ignoring the CRL.

CVE-2021-32739

    A vulnerability was discovered that may allow privilege escalation for
    authenticated API users. With a read-only user's credentials, an attacker can
    view most attributes of all config objects including `ticket_salt` of
    `ApiListener`. This salt is enough to compute a ticket for every possible
    common name (CN). A ticket, the master node's certificate, and a self-signed
    certificate are enough to successfully request the desired certificate from
    Icinga. That certificate may in turn be used to steal an endpoint or API user's
    identity. 

CVE-2021-32743

    Some of the Icinga 2 features that require credentials for external
    services expose those credentials through the API to authenticated API users
    with read permissions for the corresponding object types.  IdoMysqlConnection
    and IdoPgsqlConnection (every released version) exposes the password of the
    user used to connect to the database.  ElasticsearchWriter (added in 2.8.0)
    exposes the password used to connect to the Elasticsearch server. An attacker
    who obtains these credentials can impersonate Icinga to these services and add,
    modify and delete information there.

CVE-2021-37698

    ElasticsearchWriter, GelfWriter, InfluxdbWriter and Influxdb2Writer do
    not verify the server's certificate despite a certificate authority being
    specified. Instances which connect to any of the mentioned time series
    databases (TSDBs) using TLS over a spoofable infrastructure should change the
    credentials (if any) used by the TSDB writer feature to authenticate against
    the TSDB.

CVE-2024-49369

    The TLS certificate validation in all Icinga 2 versions starting from
    2.4.0 was flawed, allowing an attacker to impersonate both trusted cluster
    nodes as well as any API users that use TLS client certificates for
    authentication (ApiUser objects with the `client_cn` attribute set).
