---
title: "ELA-828-1 dnsmasq security update"
package: dnsmasq
version: 2.72-3+deb8u7 (jessie), 2.76-5+deb9u4 (stretch)
version_map: {"8 jessie": "2.72-3+deb8u7", "9 stretch": "2.76-5+deb9u4"}
description: "multiple security vulnerabilities"
date: 2023-04-10T13:01:40Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-15107
  - CVE-2019-14834
  - CVE-2022-0934
  - CVE-2023-28450

---
Multiple security vulnerabilities were found on dnsmasq, a lightweight, easy to configure 
DNS forwarder, designed to provide DNS (and optionally DHCP and TFTP)
services to a small-scale network.

CVE-2017-15107

    A vulnerability was found in the implementation of
    DNSSEC in Dnsmasq. Wildcard synthesized NSEC records could be
    improperly interpreted to prove the non-existence of hostnames that
    actually exist.
    This particular CVE was only fixed for 2.76-5+deb9u4. DNSSEC validation
    for jessie (until 2.73) does a bottom/top validation instead of a top/bottom
	validation.

CVE-2019-14834

    A vulnerability was found in dnsmasq before version
    2.81, where the memory leak allows remote attackers to cause a denial
    of service (memory consumption) via vectors involving DHCP response
    creation

CVE-2022-0934

    A single-byte, non-arbitrary write/use-after-free flaw
    was found in dnsmasq. This flaw allows an attacker who sends a crafted
    packet processed by dnsmasq, potentially causing a denial of
    service.

CVE-2023-28450

    The default maximum EDNS.0 UDP packet size was set
    to 4096 but should be 1232 because of DNS Flag Day 2020.
