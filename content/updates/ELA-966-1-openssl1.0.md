---
title: "ELA-966-1 openssl1.0 security update"
package: openssl1.0
version: 1.0.2u-1~deb9u9 (stretch)
version_map: {"9 stretch": "1.0.2u-1~deb9u9"}
description: "excessively long key or parameter checks"
date: 2023-09-26T23:36:02+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-3446
  - CVE-2023-3817

---

Two issues have been discovered in openssl, a Secure Sockets Layer toolkit.
Excessively long DH key or parameter checks can cause significant delays
in applications using DH_check(), DH_check_ex(), or EVP_PKEY_param_check()
functions, potentially leading to Denial of Service attacks when keys or
parameters are obtained from untrusted sources.
