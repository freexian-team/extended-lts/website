---
title: "ELA-1042-1 sudo security update"
package: sudo
version: 1.8.19p1-2.1+deb9u6 (stretch)
version_map: {"9 stretch": "1.8.19p1-2.1+deb9u6"}
description: "multiple vulnerabilities"
date: 2024-02-03T18:15:38Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-28486
  - CVE-2023-28487
---
Sudo, a program designed to allow a sysadmin to give limited
root privileges to users and log root activity, was vulnerable.

CVE-2023-28486

    Sudo did not escape control characters in log messages.

CVE-2023-28487

    Sudo did not escape control characters in sudoreplay output.

