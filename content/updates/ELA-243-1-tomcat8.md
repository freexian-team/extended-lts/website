---
title: "ELA-243-1 tomcat8 security update"
package: tomcat8
version: 8.0.14-1+deb8u19
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2020-07-15T23:42:38+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-13935

---

The payload length in a WebSocket frame was not correctly validated. Invalid
payload lengths could trigger an infinite loop. Multiple requests with invalid
payload lengths could lead to a denial of service.
