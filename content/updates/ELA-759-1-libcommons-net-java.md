---
title: "ELA-759-1 libcommons-net-java security update"
package: libcommons-net-java
version: 3.6-1+deb9u1 (stretch)
version_map: {"9 stretch": "3.6-1+deb9u1"}
description: "information leakage"
date: 2022-12-29T21:50:49+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-37533

---

ZeddYu Lu discovered that the FTP client of Apache Commons Net, a Java
client API for basic Internet protocols, trusts the host from PASV response
by default. A malicious server can redirect the Commons Net code to use a
different host, but the user has to connect to the malicious server in the
first place. This may lead to leakage of information about services running
on the private network of the client.
