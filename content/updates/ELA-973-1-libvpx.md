---
title: "ELA-973-1 libvpx security update"
package: libvpx
version: 1.3.0-3+deb8u4 (jessie), 1.6.1-3+deb9u4 (stretch)
version_map: {"8 jessie": "1.3.0-3+deb8u4", "9 stretch": "1.6.1-3+deb9u4"}
description: "buffer overflow"
date: 2023-09-30T20:56:51+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-5217

---

Clement Lecigne discovered a heap-based buffer overflow in libvpx, a
multimedia library for the VP8 and VP9 video codecs, which may result in
the execution of arbitrary code if a specially crafted VP8 media stream
is processed.
