---
title: "ELA-1124-1 sendmail security update"
package: sendmail
version: 8.15.2-8+deb9u2 (stretch)
version_map: {"9 stretch": "8.15.2-8+deb9u2"}
description: "SMTP smuggling"
date: 2024-07-05T20:34:20Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-51765

---

sendmail allowed SMTP smuggling in certain configurations.
Remote attackers can use a published exploitation technique to inject e-mail
messages with a spoofed MAIL FROM address, allowing bypass
of an SPF protection mechanism. This occurs because sendmail supports
<LF>.<CR><LF> but some other popular e-mail servers do not.

This particular injection vulnerability has been closed,
unfortunatly full closure need to reject mail that
contain NUL (0x00 byte).

This is slighly non conformant with RFC and could
be opt-out by setting confREJECT_NUL to 'false'
in sendmail.mc file.
