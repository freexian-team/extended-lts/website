---
title: "ELA-1074-1 tzdata new timezone database"
package: tzdata
version: 2024a-0+deb8u1 (jessie), 2024a-0+deb9u1 (stretch)
version_map: {"8 jessie": "2024a-0+deb8u1", "9 stretch": "2024a-0+deb9u1"}
description: "updated timezone database"
date: 2024-04-24T15:57:56+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update includes the changes in tzdata 2024a. Notable
changes are:

- - Kazakhstan unifies on UTC+5 beginning 2024-03-01.
- - Palestine springs forward a week later after Ramadan.
