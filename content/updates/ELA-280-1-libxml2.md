---
title: "ELA-280-1 libxml2 security update"
package: libxml2
version: 2.9.1+dfsg1-5+deb8u9
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2020-09-08T23:52:43+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-8872
  - CVE-2019-20388
  - CVE-2020-7595
  - CVE-2020-24977

---

Several security vulnerabilities were corrected in libxml2, the GNOME
XML library.

CVE-2017-8872

    Global buffer-overflow in the htmlParseTryOrFinish function.

CVE-2019-20388

    A memory leak was found in the xmlSchemaValidateStream function of libxml2.
    Applications that use this library may be vulnerable to memory not being
    freed leading to a denial of service.

CVE-2020-24977

    Out-of-bounds read restricted to xmllint --htmlout.

CVE-2020-7595

    Infinite loop in xmlStringLenDecodeEntities can cause a denial of service.

