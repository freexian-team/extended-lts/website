---
title: "ELA-845-1 linux-5.10 security update"
package: linux-5.10
version: 5.10.178-3~deb9u1 (stretch)
version_map: {"9 stretch": "5.10.178-3~deb9u1"}
description: "linux kernel update"
date: 2023-05-03T09:48:03+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-2196
  - CVE-2022-3424
  - CVE-2022-3707
  - CVE-2022-4129
  - CVE-2022-4379
  - CVE-2023-0045
  - CVE-2023-0458
  - CVE-2023-0459
  - CVE-2023-0461
  - CVE-2023-1073
  - CVE-2023-1074
  - CVE-2023-1076
  - CVE-2023-1077
  - CVE-2023-1078
  - CVE-2023-1079
  - CVE-2023-1118
  - CVE-2023-1281
  - CVE-2023-1513
  - CVE-2023-1611
  - CVE-2023-1670
  - CVE-2023-1829
  - CVE-2023-1855
  - CVE-2023-1859
  - CVE-2023-1872
  - CVE-2023-1989
  - CVE-2023-1990
  - CVE-2023-1998
  - CVE-2023-2162
  - CVE-2023-2194
  - CVE-2023-22998
  - CVE-2023-23004
  - CVE-2023-23559
  - CVE-2023-25012
  - CVE-2023-26545
  - CVE-2023-28328
  - CVE-2023-28466
  - CVE-2023-30456

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service, or information
leak.

CVE-2022-2196

    A regression was discovered in the KVM implementation for Intel CPUs,
    affecting Spectre v2 mitigation for nested virtualisation.  When
    KVM was used as the L0 hypervisor, an L2 guest could exploit this
    to leak sensitive information from its L1 hypervisor.

CVE-2022-3424

    Zheng Wang and Zhuorao Yang reported a flaw in the SGI GRU driver
    which could lead to a use-after-free.  On systems where this driver
    is used, a local user can explit this for denial of service (crash
    or memory corruption) or possibly for privilege escalation.

    This driver is not enabled in Debian's official kernel
    configurations.

CVE-2022-3707

    Zheng Wang reported a flaw in the i915 graphics driver's
    virtualisation (GVT-g) support that could lead to a double-free.
    On systems where this feature is used, a guest can exploit this
    for denial of service (crash or memory corruption) or possibly for
    privilege escalation.

CVE-2022-4129

    Haowei Yan reported a race condition in the L2TP protocol
    implementation which could lead to a null pointer dereference.  A
    local user could exploit this for denial of service (crash).

CVE-2022-4379

    Xingyuan Mo reported a flaw in the NFSv4.2 inter server to
    server copy implementation which could lead to a use-after-free.

    This feature is not enabled in Debian's official kernel
    configurations.

CVE-2023-0045

    Rodrigo Branco and Rafael Correa De Ysasi reported that when a
    user-space task told the kernel to enable Spectre v2 mitigation
    for it, the mitigation was not enabled until the task was next
    rescheduled.  This might be exploitable by a local or remote
    attacker to leak sensitive information from such an application.

CVE-2023-0458

    Jordy Zimmer and Alexandra Sandulescu found that getrlimit() and
    related system calls were vulnerable to speculative execution
    attacks such as Spectre v1.  A local user could explot this to
    leak sensitive information from the kernel.

CVE-2023-0459

    Jordy Zimmer and Alexandra Sandulescu found a regression in
    Spectre v1 mitigation in the user-copy functions for the amd64
    (64-bit PC) architecture.  Where the CPUs do not implement SMAP or
    it is disabled, a local user could exploit this to leak sensitive
    information from the kernel.  Other architectures may also be
    affected.

CVE-2023-0461

    "slipper" reported a flaw in the kernel's support for ULPs (Upper
    Layer Protocols) on top of TCP that can lead to a double-free when
    using kernel TLS sockets.  A local user can exploit this for
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.

    Kernel TLS is not enabled in Debian's official kernel
    configurations.

CVE-2023-1073

    Pietro Borrello reported a type confusion flaw in the HID (Human
    Interface Device) subsystem.  An attacker able to insert and
    remove USB devices might be able to use this to cause a denial of
    service (crash or memory corruption) or possibly to run arbitrary
    code in the kernel.

CVE-2023-1074

    Pietro Borrello reported a type confusion flaw in the SCTP
    protocol implementation which can lead to a memory leak.  A local
    user could exploit this to cause a denial of service (resource
    exhaustion).

CVE-2023-1076

    Pietro Borrello reported a type confusion flaw in the TUN/TAP
    network driver, which results in all TUN/TAP sockets being marked
    as belonging to user ID 0 (root).  This may allow local users to
    evade local firewall rules based on user ID.

CVE-2023-1077

    Pietro Borrello reported a type confusion flaw in the task
    scheduler.  A local user might be able to exploit this to cause a
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.

CVE-2023-1078

    Pietro Borrello reported a type confusion flaw in the RDS protocol
    implementation.  A local user could exploit this to cause a denial
    of service (crash or memory corruption) or possibly for privilege
    escalation.

CVE-2023-1079

    Pietro Borrello reported a race condition in the hid-asus HID
    driver which could lead to a use-after-free.  An attacker able to
    insert and remove USB devices can use this to cause a denial of
    service (crash or memory corruption) or possibly to run arbitrary
    code in the kernel.

CVE-2023-1118

    Duoming Zhou reported a race condition in the ene_ir remote
    control driver that can lead to a use-after-free if the driver
    is unbound.  It is not clear what the security impact of this is.

CVE-2023-1281, CVE-2023-1829

    "valis" reported two flaws in the cls_tcindex network traffic
    classifier which could lead to a use-after-free.  A local user can
    exploit these for privilege escalation.  This update removes
    cls_tcindex entirely.

CVE-2023-1513

    Xingyuan Mo reported an information leak in the KVM implementation
    for the i386 (32-bit PC) architecture.  A local user could exploit
    this to leak sensitive information from the kernel.

CVE-2023-1611

    "butt3rflyh4ck" reported a race condition in the btrfs filesystem
    driver which can lead to a use-after-free.  A local user could
    exploit this to cause a denial of service (crash or memory
    corruption) or possibly for privilege escalation.

CVE-2023-1670

    Zheng Wang reported a race condition in the xirc2ps_cs network
    driver which can lead to a use-after-free.  An attacker able to
    insert and remove PCMCIA devices can use this to cause a denial of
    service (crash or memory corruption) or possibly to run arbitrary
    code in the kernel.

CVE-2023-1855

    Zheng Wang reported a race condition in the xgene-hwmon hardware
    monitoring driver that may lead to a use-after-free.  It is not
    clear what the security impact of this is.

CVE-2023-1859

    Zheng Wang reported a race condition in the 9pnet_xen transport
    for the 9P filesystem on Xen, which can lead to a use-after-free.
    On systems where this feature is used, a backend driver in another
    domain can use this to cause a denial of service (crash or memory
    corruption) or possibly to run arbitrary code in the vulnerable
    domain.

CVE-2023-1872

    Bing-Jhong Billy Jheng reported a race condition in the io_uring
    subsystem that can lead to a use-after-free.  A local user could
    exploit this to cause a denial of service (crash or memory
    corruption) or possibly for privilege escalation.

CVE-2023-1989

    Zheng Wang reported a race condition in the btsdio Bluetooth
    adapter driver that can lead to a use-after-free.  An attacker
    able to insert and remove SDIO devices can use this to cause a
    denial of service (crash or memory corruption) or possibly to run
    arbitrary code in the kernel.

CVE-2023-1990

    Zheng Wang reported a race condition in the st-nci NFC adapter
    driver that can lead to a use-after-free.  It is not clear what
    the security impact of this is.

    This driver is not enabled in Debian's official kernel
    configurations.    

CVE-2023-1998

    José Oliveira and Rodrigo Branco reported a regression in Spectre
    v2 mitigation for user-space on x86 CPUs supporting IBRS but not
    eIBRS.  This might be exploitable by a local or remote attacker to
    leak sensitive information from a user-space application.

CVE-2023-2162

    Mike Christie reported a race condition in the iSCSI TCP transport
    that can lead to a use-after-free.  On systems where this feature
    is used, a local user might be able to use this to cause a denial
    of service (crash or memory corruption) or possibly for privilege
    escalation.

CVE-2023-2194

    Wei Chen reported a potential heap buffer overflow in the
    i2c-xgene-slimpro I²C adapter driver.  A local user with
    permission to access such a device can use this to cause a denial
    of service (crash or memory corruption) and probably for privilege
    escalation.

CVE-2023-22998

    Miaoqian Lin reported an incorrect error check in the virtio-gpu
    GPU driver.  A local user with access to such a device might be
    able to use this to cause a denial of service (crash).

CVE-2023-23004

    Miaoqian Lin reported an incorrect error check in the mali-dp GPU
    driver.  A local user with access to such a device might be able
    to use this to cause a denial of service (crash).

CVE-2023-23559

    Szymon Heidrich reported incorrect bounds checks in the rndis_wlan
    Wi-Fi driver which may lead to a heap buffer overflow or overread.
    An attacker able to insert and remove USB devices can use this to
    cause a denial of service (crash or memory corruption) or
    information leak, or possibly to run arbitrary code in the kernel.

CVE-2023-25012

    Pietro Borrello reported a race condition in the hid-bigbenff HID
    driver which could lead to a use-after-free.  An attacker able to
    insert and remove USB devices can use this to cause a denial of
    service (crash or memory corruption) or possibly to run arbitrary
    code in the kernel.

CVE-2023-26545

    Lianhui Tang reported a flaw in the MPLS protocol implementation
    that could lead to a double-free.  A local user might be able to
    exploit this to cause a denial of service (crash or memory
    corruption) or possibl for privilege escalation.

CVE-2023-28328

    Wei Chen reported a flaw in the az6927 DVB driver that can lead to
    a null pointer dereference.  A local user permitted to access an
    I²C adapter device that this driver creates can use this to cause
    a denial of service (crash).

CVE-2023-28466

    Hangyu Hua reported a race condition in the kernel TLS socket
    implementation which can lead to a use-after-free or null
    pointer dereference.  A local user can exploit this for
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.

    This feature is not enabled in Debian's official kernel
    configurations.

CVE-2023-30456

    Reima ISHII reported a flaw in the KVM implementation for Intel
    CPUs affecting nested virtualisation.  When KVM was used as the L0
    hypervisor, and EPT and/or unrestricted guest mode was disabled,
    it did not prevent an L2 guest from being configured with an
    architecturally invalid protection/paging mode.  A malicious guest
    could exploit this to cause a denial of service (crash).

This update additionally fixes Debian bugs
#989705, #993612, #1022126, and #1031753; and includes many more bug
fixes from stable updates 5.10.163-5.10.178 inclusive.
