---
title: "ELA-751-1 giflib security update"
package: giflib
version: 4.1.6-11+deb8u2 (jessie), 5.1.4-0.4+deb9u1 (stretch)
version_map: {"8 jessie": "4.1.6-11+deb8u2", "9 stretch": "5.1.4-0.4+deb9u1"}
description: "multiple file format vulnerabilities"
date: 2022-12-05T13:36:18+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-3977
  - CVE-2018-11490
  - CVE-2019-15133

---

This update fixes two file format vulnerabilities in giflib and one in the
gif2rgb utility.

CVE-2016-3977

    Heap-based buffer overflow in util/gif2rgb.c in gif2rgb allows
    remote attackers to cause a denial of service (application crash)
    via the background color index in a GIF file

CVE-2018-11490

    The DGifDecompressLine function in dgif_lib.c, as later shipped in
    cgif.c in sam2p 0.49.4, has a heap-based buffer overflow because a
    certain "Private->RunningCode - 2" array index is not checked.  This
    will lead to a denial of service or possibly unspecified other
    impact.

CVE-2019-15133

    A malformed GIF file triggers a divide-by-zero exception in the
    decoder function DGifSlurp in dgif_lib.c if the height field of the
    ImageSize data structure is equal to zero.
