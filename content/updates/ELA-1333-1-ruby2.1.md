---
title: "ELA-1333-1 ruby2.1 security update"
package: ruby2.1
version: 2.1.5-2+deb8u15 (jessie)
version_map: {"8 jessie": "2.1.5-2+deb8u15"}
description: "multiple vulnerabilities"
date: 2025-02-27T19:27:49Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-35176
  - CVE-2024-39908
  - CVE-2024-41946
  - CVE-2024-43398
  - CVE-2024-49761

---

Multiple vulnerabilities were found in ruby a popular programming
language.

CVE-2024-35176

    The REXML gem has a Denial of Service (DoS) vulnerability
    when it parses an XML that has many <s in
    an attribute value. Those who need to parse
    untrusted XMLs may be impacted to this vulnerability.

CVE-2024-39908

    The REXML gem has some Denial of Service (DoS) vulnerabilities
    when it parses an XML that has many specific characters such
    as <, 0 and %>. If you need to parse untrusted XMLs,
    you many be impacted to these vulnerabilities.

CVE-2024-41123

    The REXML gem has some Denial of Service (DoS) vulnerabilities
    when it parses an XML that has many specific characters
    such as whitespace character, >] and ]>.
    If you need to parse untrusted XMLs, you may be impacted
    to these vulnerabilities.

CVE-2024-41946

    The REXML gem had a Denial of Service (DoS) vulnerability
    when it parses an XML that has many entity expansions
    with SAX2 or pull parser API.

CVE-2024-43398

    REXML is an XML toolkit for Ruby.
    The REXML gem before 3.3.6 has a Denial of Service (DoS)
    vulnerability when it parses an XML that has many deep
    elements that have same local name attributes.
    If you need to parse untrusted XMLs with tree parser
    API like REXML::Document.new, you may be impacted
    to this vulnerability. If you use other parser APIs
    such as stream parser API and SAX2 parser API,
    you are not impacted.

CVE-2024-49761

    REXML is an XML toolkit for Ruby.
    The REXML gem before 3.3.9 has a ReDoS vulnerability
    when it parses an XML that has many digits between
    &# and x...; in a hex numeric character reference (&#x...;)


