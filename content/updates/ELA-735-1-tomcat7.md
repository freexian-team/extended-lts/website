---
title: "ELA-735-1 tomcat7 security update"
package: tomcat7
version: 7.0.56-3+really7.0.109-1+deb8u1 (jessie)
version_map: {"8 jessie": "7.0.56-3+really7.0.109-1+deb8u1"}
description: "request smuggling"
date: 2022-11-20T23:59:57+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-30640
  - CVE-2022-42252

---

Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.

CVE-2022-42252

    If Apache Tomcat was configured to ignore invalid HTTP headers via setting
    rejectIllegalHeader to false (the default for 8.5.x only), Tomcat did not
    reject a request containing an invalid Content-Length header making a
    request smuggling attack possible if Tomcat was located behind a reverse
    proxy that also failed to reject the request with the invalid header.


CVE-2021-30640

    A vulnerability in the JNDI Realm of Apache Tomcat allows an attacker to
    authenticate using variations of a valid user name and/or to bypass some of
    the protection provided by the LockOut Realm. This update fixes a
    regression due to the fix for CVE-2021-30640.
