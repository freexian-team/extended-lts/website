---
title: "ELA-664-1 curl security update"
package: curl
version: 7.38.0-4+deb8u23 (jessie), 7.52.1-5+deb9u17 (stretch)
version_map: {"8 jessie": "7.38.0-4+deb8u23", "9 stretch": "7.52.1-5+deb9u17"}
description: "information leakage"
date: 2022-08-21T01:17:00+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-22576
  - CVE-2022-27776
  - CVE-2022-27781
  - CVE-2022-32208

---

Multiple security vulnerabilities have been discovered in cURL, an URL transfer
library. These flaws may allow remote attackers to obtain sensitive
information, leak authentication or cookie header data or facilitate a denial
of service attack.

The following CVE has been additionally addressed in Debian 9 "Stretch".

CVE-2022-27782

    libcurl would reuse a previously created connection even when a TLS or SSH
    related option had been changed that should have prohibited reuse. libcurl
    keeps previously used connections in a connection pool for subsequent
    transfers to reuse if one of them matches the setup. However, several TLS and
    SSH settings were left out from the configuration match checks, making them
    match too easily.
