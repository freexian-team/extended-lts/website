---
title: "ELA-1318-1 iperf3 security update"
package: iperf3
version: 3.9-1+deb8u1 (jessie), 3.9-1+deb9u1 (stretch), 3.9-1+deb10u1 (buster)
version_map: {"8 jessie": "3.9-1+deb8u1", "9 stretch": "3.9-1+deb9u1", "10 buster": "3.9-1+deb10u1"}
description: "denial of service"
date: 2025-02-11T00:36:47+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-7250
  - CVE-2024-26306
  - CVE-2024-53580

---

Several security vulnerabilities have been discovered in iperf3, an internet
protocol bandwidth measuring tool, which may lead to a denial-of-service. When
iperf3 was used as a server with RSA authentication CVE-2024-26306 allowed a
timing side channel attack in RSA decryption operations sufficient for an
attacker to recover credential plaintext.

