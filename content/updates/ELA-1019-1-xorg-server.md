---
title: "ELA-1019-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u13 (jessie), 2:1.19.2-1+deb9u16 (stretch)
version_map: {"8 jessie": "2:1.16.4-1+deb8u13", "9 stretch": "2:1.19.2-1+deb9u16"}
description: "integer overflow and OOB reads/writes"
date: 2023-12-13T12:05:42+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-6377
  - CVE-2023-6478

---

Jan-Niklas Sohn discovered several vulnerabilities in the Xorg X server,
which may result in privilege escalation if the X server is running
privileged.
