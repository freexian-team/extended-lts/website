---
title: "ELA-1322-1 pypy security update"
package: pypy
version: 5.6.0+dfsg-4+deb9u1 (stretch)
version_map: {"9 stretch": "5.6.0+dfsg-4+deb9u1"}
description: "multiple vulnerabilities"
date: 2025-02-14T10:27:12+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2014-7185
  - CVE-2015-20107
  - CVE-2018-1060
  - CVE-2018-1061
  - CVE-2018-20852
  - CVE-2018-1000802
  - CVE-2019-9636
  - CVE-2019-9948
  - CVE-2019-16056
  - CVE-2019-16935
  - CVE-2019-20907
  - CVE-2020-8492
  - CVE-2020-26116
  - CVE-2020-29651
  - CVE-2021-3733
  - CVE-2021-3737
  - CVE-2021-4189
  - CVE-2022-45061
  - CVE-2022-48565
  - CVE-2022-48566
  - CVE-2023-40217
  - CVE-2024-0450

---

Multiple vulnerabilities were discovered in PyPy, a fast, compliant
alternative implementation of the Python language.

All fixed vulnerabilities come from embedded code copies.

For vulnerabilities from the python2.7 standard library, please refer
to:

* [DSA-4306-1](https://www.debian.org/security/dsa-4306-1)
* [DLA-2337-1](https://www.debian.org/lts/security/dla-2337-1)
* [DLA-2628-1](https://www.debian.org/lts/security/dla-2628-1)
* [DLA-2919-1](https://www.debian.org/lts/security/dla-2919-1)
* [ELA-853-1](https://www.freexian.com/fr/lts/extended/updates/ela-853-1-python2.7/)
* [ELA-950-1](https://www.freexian.com/fr/lts/extended/updates/ela-950-1-python2.7/)
* [ELA-1065-1](https://www.freexian.com/fr/lts/extended/updates/ela-1065-1-python2.7/)

One vulnerability comes from internal python2.7 C code copy, Pypy is
only affected when making use of the compatibility layer for Python C
extension (cpyext):

* CVE-2014-7185

  Integer overflow in bufferobject.c in Python before 2.7.8 allows
  context-dependent attackers to obtain sensitive information from
  process memory via a large size and offset in a "buffer" function.

The remaining minor vulnerability comes from a python-pi embedded
copy. We believe it is not exploitable, as the bundled py module is
only used during package build, but it is included for consistency
with pypy3 DLA-3966-1:

* CVE-2020-29651

  A denial of service via regular expression in the py.path.svnwc
  component of py (aka python-py) could be used by attackers to cause
  a compute-time denial of service attack by supplying malicious input
  to the blame functionality.
