---
title: "ELA-879-1 lua5.3 security update"
package: lua5.3
version: 5.3.3-1+deb9u2 (stretch)
version_map: {"9 stretch": "5.3.3-1+deb9u2"}
description: "use after free"
date: 2023-06-26T10:10:31Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-6706

---

A use after free was found in lua 5.3. A crash might be triggered by a debug.upvaluejoin call with specially crafted parameters.
