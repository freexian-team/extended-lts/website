---
title: "ELA-837-1 libxml2 security update"
package: libxml2
version: 2.9.1+dfsg1-5+deb8u15 (jessie), 2.9.4+dfsg1-2.2+deb9u10 (stretch)
version_map: {"8 jessie": "2.9.1+dfsg1-5+deb8u15", "9 stretch": "2.9.4+dfsg1-2.2+deb9u10"}
description: "multiple Vulnerabilities"
date: 2023-04-20T19:40:36+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-5130
  - CVE-2017-5969
  - CVE-2023-28484
  - CVE-2023-29469

---

Multiple issues were found in libxml2, the GNOME XML library, which possibly
allows an remote attacker to trigger a potential heap memory corruption or
trigger a denial of service or other unspecified impact.

The Jessie update 2.9.1+dfsg1-5+deb8u15 fixes all mentioned CVEs.
The Stretch update 2.9.4+dfsg1-2.2+deb9u10 fixes CVE-2023-28484 and CVE-2023-29469,
as the other have been fixed by an previous upload -- see DLA-2972-1 for details.


CVE-2017-5130

    An integer overflow in xmlmemory.c in libxml2 before 2.9.5, as used in
    Google Chrome prior to 62.0.3202.62 and other products, allowed a remote
    attacker to potentially exploit heap corruption via a crafted XML file.

CVE-2017-5969

    libxml2 2.9.4, when used in recover mode, allows one to cause a denial
    of service (NULL pointer dereference) via a crafted XML document.

CVE-2023-28484

    NULL dereference in xmlSchemaFixupComplexType.

CVE-2023-29469

    Hashing of empty dict strings isn't deterministic.

