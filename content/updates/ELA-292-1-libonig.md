---
title: "ELA-292-1 libonig security update"
package: libonig
version: 5.9.5-3.2+deb8u5
distribution: "Debian 8 jessie"
description: "buffer overwrite"
date: 2020-10-02T17:24:20+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-26159

---

In Oniguruma, an attacker able to supply a regular expression
for compilation may be able to overflow a buffer by one byte
in `concat_opt_exact_info_str` & `concat_opt_exact_info` in
`regcomp.c`.

Besides, there were other other issues like resource leaks in
`bbuf_clone()`, `not_code_range_buf()`, etc in `regparse.c` and
some other issues that needed fixing as well.
