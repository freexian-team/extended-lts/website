---
title: "ELA-398-1 libxstream-java security update"
package: libxstream-java
version: 1.4.11.1-1+deb8u2
distribution: "Debian 8 jessie"
description: "remote code execution"
date: 2021-04-03T19:28:15+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-21341
  - CVE-2021-21342
  - CVE-2021-21343
  - CVE-2021-21344
  - CVE-2021-21345
  - CVE-2021-21346
  - CVE-2021-21347
  - CVE-2021-21348
  - CVE-2021-21349
  - CVE-2021-21350
  - CVE-2021-21351

---

In XStream there is a vulnerability which may allow a remote attacker to
load and execute arbitrary code from a remote host only by manipulating the
processed input stream.

The type hierarchies for java.io.InputStream, java.nio.channels.Channel,
javax.activation.DataSource and javax.sql.rowsel.BaseRowSet are now
blacklisted as well as the individual types
com.sun.corba.se.impl.activation.ServerTableEntry,
com.sun.tools.javac.processing.JavacProcessingEnvironment$NameProcessIterator,
sun.awt.datatransfer.DataTransferer$IndexOrderComparator, and
sun.swing.SwingLazyValue. Additionally the internal type
Accessor$GetterSetterReflection of JAXB, the internal types
MethodGetter$PrivilegedGetter and ServiceFinder$ServiceNameIterator of
JAX-WS, all inner classes of javafx.collections.ObservableList and an
internal ClassLoader used in a private BCEL copy are now part of the
default blacklist and the deserialization of XML containing one of the two
types will fail. You will have to enable these types by explicit
configuration, if you need them.


