---
title: "ELA-726-1 pixman security update"
package: pixman
version: 0.32.6-3+deb8u2 (jessie), 0.34.0-1+deb9u1 (stretch)
version_map: {"8 jessie": "0.32.6-3+deb8u2", "9 stretch": "0.34.0-1+deb9u1"}
description: "buffer overflow"
date: 2022-11-08T10:44:51+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-44638

---

Maddie Stone found that pixman, a pixel manipulation and processing library,
was vulnerable to a heap buffer overwrite, which could lead to a denial of
service or potentially other unspecified impact.
