---
title: "ELA-495-1 ruby2.1 security update"
package: ruby2.1
version: 2.1.5-2+deb8u12
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-10-11T11:24:06+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-31799
  - CVE-2021-31810
  - CVE-2021-32066

---

Multiple vulnerabilities in ruby2.1, interpreter of object-oriented
scripting language Ruby, were discovered.

CVE-2021-31799

    In RDoc 3.11 through 6.x before 6.3.1, as distributed with
    Ruby through 2.1.5, it is possible to execute arbitrary
    code via | and tags in a filename.

CVE-2021-31810

    An issue was discovered in Ruby through 2.1.5. A malicious
    FTP server can use the PASV response to trick Net::FTP into
    connecting back to a given IP address and port. This
    potentially makes curl extract information about services
    that are otherwise private and not disclosed (e.g., the
    attacker can conduct port scans and service banner extractions).

CVE-2021-32066

    An issue was discovered in Ruby through 2.1.5. Net::IMAP does
    not raise an exception when StartTLS fails with an an unknown
    response, which might allow man-in-the-middle attackers to
    bypass the TLS protections by leveraging a network position
    between the client and the registry to block the StartTLS
    command, aka a "StartTLS stripping attack."
