---
title: "ELA-134-1 python2.7 security update"
package: python2.7
version: 2.7.3-6+deb7u6
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2019-06-25T03:25:21Z
draft: false
type: updates
cvelist:
  - CVE-2019-9636
  - CVE-2019-9740
  - CVE-2019-9947
  - CVE-2019-9948
  - CVE-2019-10160

---

Multiple vulnerabilities were discovered in Python, an interactive
high-level object-oriented language, including 

CVE-2019-9636

    Improper Handling of Unicode Encoding (with an incorrect netloc)
    during NFKC normalization resulting in information disclosure
    (credentials, cookies, etc. that are cached against a given
    hostname).  A specially crafted URL could be incorrectly parsed to
    locate cookies or authentication data and send that information to
    a different host than when parsed correctly.

CVE-2019-9740

    An issue was discovered in urllib2 where CRLF injection is possible
    if the attacker controls a url parameter, as demonstrated by the
    first argument to urllib.request.urlopen with \r\n (specifically in
    the query string after a ? character) followed by an HTTP header or
    a Redis command.

CVE-2019-9947

    An issue was discovered in urllib2 where CRLF injection is possible
    if the attacker controls a url parameter, as demonstrated by the
    first argument to urllib.request.urlopen with \r\n (specifically in
    the path component of a URL that lacks a ? character) followed by an
    HTTP header or a Redis command. This is similar to the CVE-2019-9740
    query string issue.

CVE-2019-9948

    urllib supports the local_file: scheme, which makes it easier for
    remote attackers to bypass protection mechanisms that blacklist
    file: URIs, as demonstrated by triggering a
    urllib.urlopen('local_file:///etc/passwd') call.

CVE-2019-10160

    A security regression of CVE-2019-9636 was discovered which still
    allows an attacker to exploit CVE-2019-9636 by abusing the user and
    password parts of a URL. When an application parses user-supplied
    URLs to store cookies, authentication credentials, or other kind of
    information, it is possible for an attacker to provide specially
    crafted URLs to make the application locate host-related information
    (e.g. cookies, authentication data) and send them to a different
    host than where it should, unlike if the URLs had been correctly
    parsed. The result of an attack may vary based on the application.

