---
title: "ELA-460-1 redis security update"
package: redis
version: 2:2.8.17-1+deb8u8
distribution: "Debian 8 Jessie"
description: "overflow vulnerabilities"
date: 2021-07-22T11:31:58+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-32761
---

It was discovered that there were several integer overflow issues in Redis, a
popular key-value database system. Some BITFIELD-related commands were affected
on 32-bit systems.
