---
title: "ELA-1249-1 tzdata new timezone database"
package: tzdata
version: 2024b-0+deb8u1 (jessie), 2024b-0+deb9u1 (stretch), 2024b-0+deb10u1 (buster)
version_map: {"8 jessie": "2024b-0+deb8u1", "9 stretch": "2024b-0+deb9u1", "10 buster": "2024b-0+deb10u1"}
description: "updated timezone database"
date: 2024-11-28T20:44:04+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update includes the changes in tzdata 2024b. Notable
changes are:

- Updated leap second list, which was set to expire by the end of
  December.
- Correction of historical data for Mexico, Mongolia and Portugal.
