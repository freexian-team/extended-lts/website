---
title: "ELA-1048-1 jinja2 security update"
package: jinja2
version: 2.7.3-1+deb8u1 (jessie), 2.8-1+deb9u1 (stretch)
version_map: {"8 jessie": "2.7.3-1+deb8u1", "9 stretch": "2.8-1+deb9u1"}
description: "cross-site scripting"
date: 2024-02-25T22:59:30+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-22195

---

It was discovered that there was an injection attack in jinja2, a
popular templating engine used in various Python applications.

It was possible to inject arbitrary HTML attributes into rendered
HTML via the "xmlattr" filter, potentially leading to a Cross-Site
Scripting (XSS) attack. It may also have been possible to bypass
attribute validation checks if they were blacklist-based.

