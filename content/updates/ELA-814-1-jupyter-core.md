---
title: "ELA-814-1 jupyter-core security update"
package: jupyter-core
version: 4.2.1-1+deb9u1 (stretch)
version_map: {"9 stretch": "4.2.1-1+deb9u1"}
description: "arbitrary code execution"
date: 2023-03-13T03:32:20+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-39286

---

It was discovered that jupyter-core, the base framework for Jupyter projects
like Jupyter Notebooks, could execute arbitrary code when loading
configuration files.
