---
title: "ELA-119-1 zookeeper security update"
package: zookeeper
version: 3.4.5+dfsg-2+deb7u2
distribution: "Debian 7 Wheezy"
description: "information disclosure vulnerability"
date: 2019-05-24T09:24:08+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-0201
---

It was discovered that there was an information disclosure vulnerability in zookeeper, a distributed co-ordination server. Users who were not authorised to read data were able to view the access control list.
