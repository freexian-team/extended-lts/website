---
title: "ELA-1096-1 composer security update"
package: composer
version: 1.2.2-1+deb9u2 (stretch)
version_map: {"9 stretch": "1.2.2-1+deb9u2"}
description: "multiple Vulnerabilities"
date: 2024-05-24T20:24:17Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-24828
  - CVE-2023-43655

---

Composer, an application-level dependency manager for the PHP programming language, was vulnerable.

CVE-2022-24828

	 Integrators using Composer code to call `VcsDriver::getFileContent` can have a code injection vulnerability if the user can control the `$file` or `$identifier` argument. This leads to a vulnerability on packagist.org for example where the composer.json's `readme` field can be used as a vector for injecting parameters into hg/Mercurial via the `$file` argument, or git via the `$identifier` argument if you allow arbitrary data there.

CVE-2023-43655

	Users publishing a composer.phar to a public web-accessible server where the composer.phar can be executed as a php file may be subject to a remote code execution vulnerability if PHP also has `register_argc_argv` enabled in php.ini.
