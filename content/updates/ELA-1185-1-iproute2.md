---
title: "ELA-1185-1 iproute2 security update"
package: iproute2
version: 4.20.0-2+deb10u2 (buster)
version_map: {"10 buster": "4.20.0-2+deb10u2"}
description: "use-after-free"
date: 2024-09-28T04:30:43+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-20795

---

Use-after-free in get_netnsid_from_name() has been fixed in iproute2, a collection of utilities for controlling TCP/IP networking and traffic control.
