---
title: "ELA-1008-1 audiofile security update"
package: audiofile
version: 0.3.6-4+deb9u2 (stretch)
version_map: {"9 stretch": "0.3.6-4+deb9u2"}
description: "denial of service"
date: 2023-11-27T19:52:54+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-13147
  - CVE-2022-24599

---

The audiofile library allows the processing of audio data to and from audio
files of many common formats (currently AIFF, AIFF-C, WAVE, NeXT/Sun, BICS, and
raw data).

CVE-2019-13147

    Audiofile was vulnerable due to an integer overflow. The program quits
    early if NeXT audio files include too many channels now.

CVE-2022-24599

    A memory leak was found due to reading a not null terminated copyright field.
    Preallocate zeroed memory and always NUL terminate C strings from now on.
