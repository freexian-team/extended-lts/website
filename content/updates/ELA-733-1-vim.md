---
title: "ELA-733-1 vim security update"
package: vim
version: 2:7.4.488-7+deb8u8 (jessie), 2:8.0.0197-4+deb9u8 (stretch)
version_map: {"8 jessie": "2:7.4.488-7+deb8u8", "9 stretch": "2:8.0.0197-4+deb9u8"}
description: "denial of service"
date: 2022-11-14T13:44:05+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-2285
  - CVE-2022-2304
  - CVE-2022-2946
  - CVE-2022-3099
  - CVE-2022-3134
  - CVE-2022-3234
  - CVE-2022-3324

---

Multiple security vulnerabilities have been discovered in vim, an enhanced vi
editor. Buffer overflows, out-of-bounds reads and use-after-free may lead to a
denial-of-service (application crash) or other unspecified impact.

