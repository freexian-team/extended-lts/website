---
title: "ELA-383-1 pillow security update"
package: pillow
version: 2.6.1-2+deb8u6
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2021-03-19T11:57:05+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-35653
  - CVE-2021-25290

---

Multiple vulnerabilities were discovered in Pillow, a Python Imaging
Library. An attacker could cause a denial-of-service (DoS) with
crafted image files.

* CVE-2020-35653

    PcxDecode has a buffer over-read when decoding a crafted PCX file
    because the user-supplied stride value is trusted for buffer
    calculations.

* CVE-2021-25290

    In TiffDecode.c, there is a negative-offset memcpy with an invalid
    size.
