---
title: "ELA-1283-1 gst-plugins-base0.10 security update"
package: gst-plugins-base0.10
version: 0.10.36-2+deb8u5 (jessie)
version_map: {"8 jessie": "0.10.36-2+deb8u5"}
description: "multiple vulnerabilities"
date: 2024-12-29T11:08:47Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-47541
  - CVE-2024-47542
  - CVE-2024-47615

---

gstreamer a multimedia framework was affected by multiple vulnerabilities.

CVE-2024-47541

    An Out of Bound write vulnerability has been
    identified in the gst_ssa_parse_remove_override_codes
    function of the gstssaparse.c file.

CVE-2024-47542

    A null pointer dereference has been
    discovered in the id3v2_read_synch_uint function, located
    in id3v2.c

CVE-2024-47615

    An Out Of Bound Write has been detected
    in the function gst_parse_vorbis_setup_packet within
    vorbis_parse.c.

