---
title: "ELA-723-1 linux-5.10 security update"
package: linux-5.10
version: 5.10.149-2~deb9u1 (stretch)
version_map: {"9 stretch": "5.10.149-2~deb9u1"}
description: "linux kernel update"
date: 2022-11-04T12:27:36+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-4037
  - CVE-2022-0171
  - CVE-2022-1184
  - CVE-2022-1679
  - CVE-2022-2153
  - CVE-2022-2602
  - CVE-2022-2663
  - CVE-2022-2905
  - CVE-2022-3028
  - CVE-2022-3061
  - CVE-2022-3176
  - CVE-2022-3303
  - CVE-2022-3586
  - CVE-2022-3621
  - CVE-2022-3625
  - CVE-2022-3629
  - CVE-2022-3633
  - CVE-2022-3635
  - CVE-2022-3646
  - CVE-2022-3649
  - CVE-2022-20421
  - CVE-2022-20422
  - CVE-2022-39188
  - CVE-2022-39190
  - CVE-2022-39842
  - CVE-2022-40307
  - CVE-2022-41222
  - CVE-2022-41674
  - CVE-2022-42719
  - CVE-2022-42720
  - CVE-2022-42721
  - CVE-2022-42722
  - CVE-2022-43750

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2021-4037

    Christian Brauner reported that the inode_init_owner function for
    the XFS filesystem in the Linux kernel allows local users to
    create files with an unintended group ownership allowing attackers
    to escalate privileges by making a plain file executable and SGID.

CVE-2022-0171

    Mingwei Zhang reported that a cache incoherence issue in the SEV
    API in the KVM subsystem may result in denial of service.

CVE-2022-1184

    A flaw was discovered in the ext4 filesystem driver which can lead
    to a use-after-free. A local user permitted to mount arbitrary
    filesystems could exploit this to cause a denial of service (crash
    or memory corruption) or possibly for privilege escalation.

CVE-2022-1679

    The syzbot tool found a race condition in the ath9k_htc driver
    which can lead to a use-after-free.  This might be exploitable to
    cause a denial service (crash or memory corruption) or possibly
    for privilege escalation.

CVE-2022-2153

    "kangel" reported a flaw in the KVM implementation for x86
    processors which could lead to a null pointer dereference. A local
    user permitted to access /dev/kvm could exploit this to cause a
    denial of service (crash).

CVE-2022-2602

    A race between handling an io_uring request and the Unix socket
    garbage collector was discovered. An attacker can take advantage
    of this flaw for local privilege escalation.

CVE-2022-2663

    David Leadbeater reported flaws in the nf_conntrack_irc
    connection-tracking protocol module. When this module is enabled
    on a firewall, an external user on the same IRC network as an
    internal user could exploit its lax parsing to open arbitrary TCP
    ports in the firewall, to reveal their public IP address, or to
    block their IRC connection at the firewall.

CVE-2022-2905

    Hsin-Wei Hung reported a flaw in the eBPF verifier which can lead
    to an out-of-bounds read.  If unprivileged use of eBPF is enabled,
    this could leak sensitive information.  This was already disabled
    by default, which would fully mitigate the vulnerability.

CVE-2022-3028

    Abhishek Shah reported a race condition in the AF_KEY subsystem,
    which could lead to an out-of-bounds write or read.  A local user
    could exploit this to cause a denial of service (crash or memory
    corruption), to obtain sensitive information, or possibly for
    privilege escalation.

CVE-2022-3061

    A flaw was discovered in the i740 driver which may result in
    denial of service.

    This driver is not enabled in Debian's official kernel
    configurations.

CVE-2022-3176

    A use-after-free flaw was discovered in the io_uring subsystem
    which may result in local privilege escalation to root.

CVE-2022-3303

    A race condition in the snd_pcm_oss_sync function in the sound
    subsystem in the Linux kernel due to improper locking may result
    in denial of service.

CVE-2022-3586 (ZDI-22-1452)

    The Zero Day Initiative reported a flaw in the sch_sfb network
    scheduler, which may lead to a use-after-free and leak of
    sensitive information from the kernel.

CVE-2022-3621, CVE-2022-3646

    The syzbot tool found flaws in the nilfs2 filesystem driver which
    can lead to a null pointer dereference or memory leak.  A user
    permitted to mount arbitrary filesystem images could use these to
    cause a denial of service (crash or resource exhaustion).

CVE-2022-3625

    A flaw was discovered in the devlink subsystem which can lead to
    a use-after-free.  The security impact of this is unclear.

CVE-2022-3629

    The syzbot tool found a memory leak in the Virtual Socket Protocol
    implementation.  A local user could exploit this to cause a denial
    of service (resource exhaustion).

CVE-2022-3633

    The Linux Verification Center found a memory leak in the SAE J1939
    protocol implementation.  A local user could exploit this to cause
    a denial of service (resource exhaustion).

CVE-2022-3635

    Several race conditions were discovered in the idt77252 ATM
    driver, which can lead to a use-after-free if the module is
    removed.  The security impact of this is unclear.

CVE-2022-3649

    The syzbot tool found flaws in the nilfs2 filesystem driver which
    can lead to a use-after-free.  A user permitted to mount arbitrary
    filesystem images could use these to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.

CVE-2022-20421

    A use-after-free vulnerability was discovered in the
    binder_inc_ref_for_node function in the Android binder driver. On
    systems where the binder driver is loaded, a local user could
    exploit this for privilege escalation.

CVE-2022-20422

    A race condition was discovered in the instruction emulator for
    64-bit Arm systems.  Concurrent changes to the sysctls that
    control the emulator could result in a null pointer dereference.
    The security impact of this is unclear.

CVE-2022-39188

    Jann Horn reported a race condition in the kernel's handling of
    unmapping of certain memory ranges. When a driver created a memory
    mapping with the VM_PFNMAP flag, which many GPU drivers do, the
    memory mapping could be removed and freed before it was flushed
    from the CPU TLBs. This could result in a page use-after-free. A
    local user with access to such a device could exploit this to
    cause a denial of service (crash or memory corruption) or possibly
    for privilege escalation.

CVE-2022-39190

    Gwangun Jung reported a flaw in the nf_tables subsystem.  A local
    user could exploit this to cause a denial of service (crash).

CVE-2022-39842

    An integer overflow was discovered in the pxa3xx-gcu video driver
    which could lead to a heap out-of-bounds write.

    This driver is not enabled in Debian's official kernel
    configurations.

CVE-2022-40307

    A race condition was discovered in the EFI capsule-loader driver,
    which could lead to use-after-free. A local user permitted to
    access this device (/dev/efi_capsule_loader) could exploit this to
    cause a denial of service (crash or memory corruption) or possibly
    for privilege escalation. However, this device is normally only
    accessible by the root user.

CVE-2022-41222

    A race condition was discovered in the memory management subsystem
    that can lead to stale TLB entries.  A local user could exploit
    this to cause a denial of service (memory corruption or crash),
    information leak, or privilege escalation.

CVE-2022-41674, CVE-2022-42719, CVE-2022-42720, CVE-2022-42721,
CVE-2022-42722

    Soenke Huster discovered several vulnerabilities in the mac80211
    subsystem triggered by WLAN frames which may result in denial of
    service or the execution of arbitrary code.

CVE-2022-43750

    The syzbot tool found that the USB monitor (usbmon) driver allowed
    user-space programs to overwrite the driver's data structures.  A
    local user permitted to access a USB monitor device could exploit
    this to cause a denial of service (memory corruption or crash) or
    possibly for privilege escalation.  However, by default only the
    root user can access such devices.

This update also fixes a regression for some older 32-bit PCs (bug #1017425),
and enables the i10nm_edac driver (bug #1019248). It additionally includes
many more bug fixes from stable updates 5.10.137-5.10.149 inclusive.
