---
title: "ELA-970-1 libreoffice security update"
package: libreoffice
version: 1:4.3.3-2+deb8u14 (jessie)
version_map: {"8 jessie": "1:4.3.3-2+deb8u14"}
description: "abitrary code execution"
date: 2023-09-29T19:10:07Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-0950

---

An abitrary code execution vulnerability was found in LibreOffice, an office
productivity software suite.

CVE-2023-0950

     An improper Validation of Array Index
     vulnerability was present in the spreadsheet component of
     LibreOffice. This allows an attacker to craft a spreadsheet
     document that will cause an array index underflow when loaded.
     In the affected versions of LibreOffice certain malformed
     spreadsheet formulas, such as AGGREGATE, could be created
     with less parameters passed to the formula interpreter than
     it expected, leading to an array index underflow,
     in which case there is a risk that arbitrary code could be executed

Unfortunately the changes required to fix the remaining issues affecting
LibreOffice in Debian jessie are too invasive to be backported. Those
issues affect only the use of LibreOffice via its Graphical User Interface
(GUI). Users of LibreOffice needing the GUI are encouraged to migrate
to Debian stretch or newer. From this point onwards the GUI components
of LibreOffice are no longer supported in Debian jessie. Headless
LibreOffice will continue to be supported.
