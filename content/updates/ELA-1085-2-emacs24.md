---
title: "ELA-1085-2 emacs24 regression update"
package: emacs24
version: 24.4+1-5+deb8u4 (jessie), 24.5+1-11+deb9u4 (stretch)
version_map: {"8 jessie": "24.4+1-5+deb8u4", "9 stretch": "24.5+1-11+deb9u4"}
description: "regression update"
date: 2024-05-25T09:31:12+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

The previous update to Emacs did not include builds for all supported
architectures.  The same update has been reissued to include all builds.
