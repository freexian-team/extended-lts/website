---
title: "ELA-748-1 libraw security update"
package: libraw
version: 0.16.0-9+deb8u6 (jessie)
version_map: {"8 jessie": "0.16.0-9+deb8u6"}
description: "thumbnail size range check"
date: 2022-12-01T18:03:52+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-15503

---

This update adds thumbnail size checks to avoid out of bounds memory accesses.

CVE-2020-15503

    LibRaw lacks a thumbnail size range check. This affects
    decoders/unpack_thumb.cpp, postprocessing/mem_image.cpp, and
    utils/thumb_utils.cpp. For example,
    malloc(sizeof(libraw_processed_image_t)+T.tlength) occurs without
    validating T.tlength.
