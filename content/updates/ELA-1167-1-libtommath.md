---
title: "ELA-1167-1 libtommath security update"
package: libtommath
version: 0.42.0-1.1+deb8u1 (jessie), 1.0-4+deb9u1 (stretch), buster (1.1.0-3+deb10u1)
version_map: {"8 jessie": "0.42.0-1.1+deb8u1", "9 stretch": "1.0-4+deb9u1", "10 buster": "1.1.0-3+deb10u1"}
description: "integer overflow vulnerabilities"
date: 2024-08-28T10:43:51+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-36328
---

It was discovered that there was a series of integer overflow vulnerabilities
in LibTomMath, a multiple-precision mathematics library.

This could have led attackers to execute arbitrary code and/or cause a denial
of service (DoS).
