---
title: "ELA-1113-1 libndp security update"
package: libndp
version: 1.4-2+deb8u2 (jessie), 1.6-1+deb9u1 (stretch)
version_map: {"8 jessie": "1.4-2+deb8u2", "9 stretch": "1.6-1+deb9u1"}
description: "buffer overflow vulnerability"
date: 2024-06-19T11:44:30-07:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-5564
---

It was discovered that there was a buffer overflow vulnerability in `libndp`, a
library for implementing IPv6's "Neighbor Discovery Protocol" (NDP) and is used
by Network Manager and other networking tools.

A local, malicious user could have caused a buffer overflow in Network Manager
by sending a malformed IPv6 router advertisement packet. This issue existed
because `libndp` was not correctly validating route length information.
