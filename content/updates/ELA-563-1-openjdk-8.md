---
title: "ELA-563-1 openjdk-8 security update"
package: openjdk-8
version: 8u322-b06-1~deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2022-02-10T11:50:20+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-21248
  - CVE-2022-21282
  - CVE-2022-21283
  - CVE-2022-21293
  - CVE-2022-21294
  - CVE-2022-21296
  - CVE-2022-21299
  - CVE-2022-21305
  - CVE-2022-21340
  - CVE-2022-21341
  - CVE-2022-21349
  - CVE-2022-21360
  - CVE-2022-21365

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
which may result in denial of service, bypass of deserialization
restrictions or information disclosure.
