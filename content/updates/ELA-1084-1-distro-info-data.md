---
title: "ELA-1084-1 distro-info-data database update"
package: distro-info-data
version: 0.36~bpo8+5 (jessie), 0.41+deb10u2~bpo9+5 (stretch)
version_map: {"8 jessie": "0.36~bpo8+5", "9 stretch": "0.41+deb10u2~bpo9+5"}
description: "non-security-related database update"
date: 2024-05-01T10:27:40-04:00
draft: false
type: updates
tags:
- update
cvelist:

---

This is a routine update of the distro-info-data database for Debian
ELTS users.

It adds Ubuntu 24.10.

Also included are LTS and ELTS columns for Debian, and ESM columns for
Ubuntu. The versions of distro-info in jessie and stretch are not able
to display the data from these columns, but they are present in the CSV.
