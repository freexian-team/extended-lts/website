---
title: "ELA-527-1 libsamplerate security update"
package: libsamplerate
version: 0.1.8-8+deb8u1
distribution: "Debian 8 jessie"
description: "buffer over-read"
date: 2021-12-14T00:22:18+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-7697

---

An issue has been found in libsamplerate, an audio sample rate conversion
library. Using a crafted audio file a buffer over-read might happen in
calc_output_single() in src_sinc.c.

