---
title: "ELA-1024-1 haproxy security update"
package: haproxy
version: 1.5.8-3+deb8u4 (jessie), 1.7.5-2+deb9u2 (stretch)
version_map: {"8 jessie": "1.5.8-3+deb8u4", "9 stretch": "1.7.5-2+deb9u2"}
description: "information disclosure"
date: 2023-12-24T11:47:09+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-45539

---

It was discovered that there was a potential information disclosure vulnerability in HAProxy, a reverse proxy server used to load balance HTTP requests across multiple servers.

HAProxy formerly accepted the # (ie. the "pound" or "hash") symbol as part of a URI component. This might have allowed remote attackers to obtain sensitive information upon HAProxy's misinterpretation of a path_end rule, such as by routing index.html#.png to a static server.

CVE-2023-45539

    HAProxy before 2.8.2 accepts # as part of the URI component, which might allow remote attackers to obtain sensitive information or have unspecified other impact upon misinterpretation of a path_end rule, such as routing index.html#.png to a static server.



