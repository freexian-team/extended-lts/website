---
title: "ELA-982-1 curl security update"
package: curl
version: 7.38.0-4+deb8u27 (jessie), 7.52.1-5+deb9u20 (stretch)
version_map: {"8 jessie": "7.38.0-4+deb8u27", "9 stretch": "7.52.1-5+deb9u20"}
description: "cookie injection"
date: 2023-10-11T13:51:14+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-38546

---

An issue was found in Curl, an easy-to-use client-side URL transfer library
and command line tool, which could lead to cookie injection from a file
named `none` under certain circumstances.
