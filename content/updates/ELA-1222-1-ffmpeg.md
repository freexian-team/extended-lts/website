---
title: "ELA-1222-1 ffmpeg security update"
package: ffmpeg
version: 7:3.2.19-0+deb9u5 (stretch), 7:4.1.11-0+deb10u2 (buster)
version_map: {"9 stretch": "7:3.2.19-0+deb9u5", "10 buster": "7:4.1.11-0+deb10u2"}
description: "multiple vulnerabilities"
date: 2024-10-31T19:52:19+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-20898
  - CVE-2020-22040
  - CVE-2020-22051
  - CVE-2020-22056
  - CVE-2021-38090
  - CVE-2021-38091
  - CVE-2021-38092
  - CVE-2021-38093
  - CVE-2021-38094
  - CVE-2022-48434
  - CVE-2023-49502
  - CVE-2023-50010
  - CVE-2023-51793
  - CVE-2023-51794
  - CVE-2023-51798
  - CVE-2024-31578
  - CVE-2024-32230

---

Multiple vulnerabilities have been fixed in the FFmpeg multimedia framework.

CVE-2020-20898 (buster)

    avfilter/vf_convolution integer overflow

CVE-2020-22040

    avfilter/f_reverse memory leaks

CVE-2020-22051 (buster)

    avfilter/vf_tile memory leak

CVE-2020-22056 (buster)

    avfilter/af_acrossover memory leak

CVE-2021-38090 (buster)

    avfilter/vf_convolution integer overflow

CVE-2021-38091 (buster)

    avfilter/vf_convolution integer overflow

CVE-2021-38092 (buster)

    avfilter/vf_convolution integer overflow

CVE-2021-38093 (buster)

    avfilter/vf_convolution integer overflow

CVE-2021-38094 (buster)

    avfilter/vf_convolution integer overflow

CVE-2022-48434 (buster)

    lavc/pthread_frame hwaccel use-after-free

CVE-2023-49502

    avfilter/bwdif buffer overflow

CVE-2023-50010 (buster)

    avfilter/vf_gradfun buffer overflow

CVE-2023-51793 (buster)

    avfilter/vf_weave buffer overflow

CVE-2023-51794 (buster)

    avfilter/af_stereowiden buffer overflow

CVE-2023-51798 (buster)

    avfilter/vf_minterpolate floating point exception

CVE-2024-31578 (buster)

    avutil/hwcontext use-after-free

CVE-2024-32230

    avcodec/mpegvideo_enc buffer overflow
