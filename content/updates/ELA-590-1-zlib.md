---
title: "ELA-590-1 zlib security update"
package: zlib
version: 1:1.2.8.dfsg-2+deb8u2
distribution: "Debian 8 jessie"
description: "incorrect memory handling"
date: 2022-04-03T10:28:07+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-25032

---


Danilo Ramos discovered that incorrect memory handling in zlib's deflate
handling could result in denial of service or potentially the execution
of arbitrary code if specially crafted input is processed.
