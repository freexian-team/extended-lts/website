---
title: "ELA-1236-1 waitress security update"
package: waitress
version: 1.2.0~b2-2+deb10u2 (buster)
version_map: {"10 buster": "1.2.0~b2-2+deb10u2"}
description: "resource exhaustion"
date: 2024-11-16T23:56:23+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-49769

---

DoS due to resource exhaustion has been fixed in waitress, a Python Web Server Gateway Interface server.
