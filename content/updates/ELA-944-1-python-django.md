---
title: "ELA-944-1 python-django security update"
package: python-django
version: 1:1.10.7-2+deb9u21 (stretch)
version_map: {"9 stretch": "1:1.10.7-2+deb9u21"}
description: "denial of service vulnerability"
date: 2023-09-07T13:33:25-07:00
draft: false
cvelist:
  - CVE-2023-41164
---

It was discovered that there was a potential denial of service vulnerability in
Django, a popular Python-based web development framework.

Upstream reported that there was a potential vulnerability in
`django.utils.encoding.uri_to_iri()`. This method was subject to potential DoS
attack via certain inputs with a very large number of Unicode characters.
