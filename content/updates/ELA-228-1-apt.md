---
title: "ELA-228-1 apt security update"
package: apt
version: 0.9.7.9+deb7u9
distribution: "Debian 7 Wheezy"
description: "out-of-bounds read"
date: 2020-05-14T23:27:32+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-3810

---

Shuaibing Lu discovered that missing input validation in the ar/tar
implementations of APT, the high level package manager, could result in
denial of service when processing specially crafted deb files.
