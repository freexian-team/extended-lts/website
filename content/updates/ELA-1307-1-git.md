---
title: "ELA-1307-1 git security update"
package: git
version: 1:2.1.4-2.1+deb8u15 (jessie), 1:2.11.0-3+deb9u12 (stretch), 1:2.20.1-2+deb10u10 (buster)
version_map: {"8 jessie": "1:2.1.4-2.1+deb8u15", "9 stretch": "1:2.11.0-3+deb9u12", "10 buster": "1:2.20.1-2+deb10u10"}
description: "multiple vulnerabilities"
date: 2025-01-28T16:46:04Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-50349
  - CVE-2024-52006

---

Multiple vulnerabilities were discovered in git, a fast, scalable and
distributed revision control system.

### CVE-2024-50349

When Git asks for credentials via a terminal prompt (i.e. without using any
credential helper), it prints out the host name for which the user is expected
to provide a username and/or a password.  At this stage, any URL-encoded parts
have been decoded already, and are printed verbatim.  This could allow
attackers to craft URLs that contain ANSI escape sequences that the terminal
interpret to confuse users e.g. into providing passwords for trusted Git
hosting sites when in fact they are then sent to untrusted sites that are
under the attacker's control.

### CVE-2024-52006

Git defines a line-based protocol that is used to exchange information between
Git and Git credential helpers.  Some ecosystems (most notably, .NET and
node.js) interpret single Carriage Return characters as newlines, which
renders the protections against CVE-2020-5260 incomplete for credential
helpers that treat Carriage Returns in this way.
