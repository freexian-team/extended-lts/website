---
title: "ELA-798-1 sox security update"
package: sox
version: 14.4.1-5+deb8u5 (jessie), 14.4.1-5+deb9u3 (stretch)
version_map: {"8 jessie": "14.4.1-5+deb8u5", "9 stretch": "14.4.1-5+deb9u3"}
description: "multiple file format vulnerabilities"
date: 2023-02-10T13:36:51+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-13590
  - CVE-2021-3643
  - CVE-2021-23159
  - CVE-2021-23172
  - CVE-2021-23210
  - CVE-2021-33844
  - CVE-2021-40426
  - CVE-2022-31650
  - CVE-2022-31651

---

This update fixes multiple file format validation vulnerabilities that could
result in memory access violations such as buffer overflows and floating point
exceptions. It also fixes a regression in hcom parsing introduced when fixing
CVE-2017-11358.

CVE-2019-13590

    In sox-fmt.h (startread function), there is an integer overflow on the
    result of integer addition (wraparound to 0) fed into the lsx_calloc macro
    that wraps malloc. When a NULL pointer is returned, it is used without a
    prior check that it is a valid pointer, leading to a NULL pointer
    dereference on lsx_readbuf in formats_i.c.

CVE-2021-3643

    The lsx_adpcm_init function within libsox leads to a
    global-buffer-overflow. This flaw allows an attacker to input a malicious
    file, leading to the disclosure of sensitive information.

CVE-2021-23159

    A vulnerability was found in SoX, where a heap-buffer-overflow occurs
    in function lsx_read_w_buf() in formats_i.c file. The vulnerability is
    exploitable with a crafted file, that could cause an application to
    crash.

CVE-2021-23172

    A vulnerability was found in SoX, where a heap-buffer-overflow occurs
    in function startread() in hcom.c file. The vulnerability is
    exploitable with a crafted hcomn file, that could cause an application
    to crash.

CVE-2021-23210

    A floating point exception (divide-by-zero) issue was discovered in
    SoX in functon read_samples() of voc.c file. An attacker with a
    crafted file, could cause an application to crash.

CVE-2021-33844

    A floating point exception (divide-by-zero) issue was discovered in
    SoX in functon startread() of wav.c file. An attacker with a crafted
    wav file, could cause an application to crash.

CVE-2021-40426

    A heap-based buffer overflow vulnerability exists in the sphere.c
    start_read() functionality of Sound Exchange libsox. A specially-crafted
    file can lead to a heap buffer overflow. An attacker can provide a
    malicious file to trigger this vulnerability.

CVE-2022-31650

    There is a floating-point exception in lsx_aiffstartwrite in aiff.c.

CVE-2022-31651

    There is an assertion failure in rate_init in rate.c.
