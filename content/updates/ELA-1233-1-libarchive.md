---
title: "ELA-1233-1 libarchive security update"
package: libarchive
version: 3.1.2-11+deb8u12 (jessie), 3.2.2-2+deb9u5 (stretch), 3.3.3-4+deb10u4 (buster)
version_map: {"8 jessie": "3.1.2-11+deb8u12", "9 stretch": "3.2.2-2+deb9u5", "10 buster": "3.3.3-4+deb10u4"}
description: "out-of-bounds write"
date: 2024-11-11T23:51:03+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-20696

---

RAR reader out-of-bounds write has been fixed in libarchive, a multi-format archive and compression library.
