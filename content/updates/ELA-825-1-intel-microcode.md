---
title: "ELA-825-1 intel-microcode security update"
package: intel-microcode
version: 3.20230214.1~deb8u1 (jessie), 3.20230214.1~deb9u1 (stretch)
version_map: {"8 jessie": "3.20230214.1~deb8u1", "9 stretch": "3.20230214.1~deb9u1"}
description: "multiple vulnerabilities"
date: 2023-04-01T12:13:28+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-21216
  - CVE-2022-21233
  - CVE-2022-33196
  - CVE-2022-33972
  - CVE-2022-38090

---
Multiple potential security vulnerabilities in some Intel® Processors
have been found which may allow information disclosure or may allow
escalation of privilege. Intel is releasing microcode updates to mitigate
this potential vulnerabilities.

Please pay attention that the fix for CVE-2022-33196 might require a
firmware update.

CVE-2022-21216 (INTEL-SA-00700)

    Insufficient granularity of access control in out-of-band
    management in some Intel(R) Atom and Intel Xeon Scalable Processors
    may allow a privileged user to potentially enable escalation of
    privilege via adjacent network access.

CVE-2022-21233 (INTEL-SA-00657)

    Improper isolation of shared resources in some Intel(R) Processors
    may allow a privileged user to potentially enable information
    disclosure via local access.

CVE-2022-33196 (INTEL-SA-00738)	

    Incorrect default permissions in some memory controller
    configurations for some Intel(R) Xeon(R) Processors when using
    Intel(R) Software Guard Extensions which may allow a privileged user
    to potentially enable escalation of privilege via local access.

    This fix may require a firmware update to be effective on some
    processors.

CVE-2022-33972 (INTEL-SA-00730)

    Incorrect calculation in microcode keying mechanism for some 3rd
    Generation Intel(R) Xeon(R) Scalable Processors may allow a
    privileged user to potentially enable information disclosure via
    local acces

CVE-2022-38090 (INTEL-SA-00767)

    Improper isolation of shared resources in some Intel(R) Processors
    when using Intel(R) Software Guard Extensions may allow a privileged
    user to potentially enable information disclosure via local access.
