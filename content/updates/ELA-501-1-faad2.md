---
title: "ELA-501-1 faad2 security update"
package: faad2
version: 2.7-8+deb8u4
distribution: "Debian 8 jessie"
description: "heap buffer overflow and null pointer dereference"
date: 2021-10-24T17:40:32+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-32274
  - CVE-2021-32276
  - CVE-2021-32277
  - CVE-2021-32278

---

Several issues have been found in faad2, a freeware Advanced Audio Decoder
player. They are related to heap buffer overflows or null pointer
dereferences, which both might allow an attacker to execute code by
providing crafted files.
