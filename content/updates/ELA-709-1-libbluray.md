---
title: "ELA-709-1 libbluray bugfix update"
package: libbluray
version: 1:0.6.2-1+deb8u1 (jessie), 1:0.9.3-3+deb9u1 (stretch)
version_map: {"8 jessie": "1:0.6.2-1+deb8u1", "9 stretch": "1:0.9.3-3+deb9u1"}
description: "compatibility with latest Java"
date: 2022-10-25T09:41:37+02:00
draft: false
type: updates
cvelist:

---

The latest Java security updates introduced a change that broke libbluray's
interactive BD-J support. This update addresses that, adding compatibility
with recent Java versions.
