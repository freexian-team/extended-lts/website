---
title: "ELA-1065-1 python2.7 security update"
package: python2.7
version: 2.7.9-2-ds1-1+deb8u12 (jessie), 2.7.13-2+deb9u9 (stretch)
version_map: {"8 jessie": "2.7.9-2-ds1-1+deb8u12", "9 stretch": "2.7.13-2+deb9u9"}
description: "quoted-overlap zipbomb DoS"
date: 2024-03-24T23:42:05+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-0450

---

The zipfile module was vulnerable to “quoted-overlap” zip-bombs
in the Python 2 interpreter.
