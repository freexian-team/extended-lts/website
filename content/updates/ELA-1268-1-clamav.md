---
title: "ELA-1268-1 clamav security update"
package: clamav
version: 0.103.12+dfsg-0+deb8u1 (jessie), 0.103.12+dfsg-0+deb9u1 (stretch), 1.0.7+dfsg-1~deb10u1 (buster)
version_map: {"8 jessie": "0.103.12+dfsg-0+deb8u1", "9 stretch": "0.103.12+dfsg-0+deb9u1", "10 buster": "1.0.7+dfsg-1~deb10u1"}
description: "multiple vulnerabilities"
date: 2024-12-04T19:36:21-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-20505
  - CVE-2024-20506

---

Two vulnerabilities were found in ClamAV, an antivirus toolkit for Unix.

CVE-2024-20505

  Affected versions could allow an unauthenticated, remote attacker to cause a
  denial of service (DoS) condition on an affected device. The vulnerability is
  due to an out of bounds read. An attacker could exploit this vulnerability by
  submitting a crafted PDF file to be scanned by ClamAV on an affected device. An
  exploit could allow the attacker to terminate the scanning process. 

CVE-2024-20506

  Affected versions could allow an authenticated, local attacker to corrupt
  critical system files. The vulnerability is due to allowing the ClamD process
  to write to its log file while privileged without checking if the logfile has
  been replaced with a symbolic link. An attacker could exploit this
  vulnerability if they replace the ClamD log file with a symlink to a critical
  system file and then find a way to restart the ClamD process. An exploit could
  allow the attacker to corrupt a critical system file by appending ClamD log
  messages after restart.

On Debian 10 (Buster), clamav was updated to version 1.0.7+dfsg-1~deb10u1. In
order to properly built it, new source packages and their binaries were
introduced to Debian 10 (Buster):

- cmake-latest/3.18.4-2~deb10u1
- llvm-toolchain-16/1:16.0.6-15~deb10u1
- rustc-web/1.78.0+dfsg1-2~deb10u1

Due to the library soname bump, the reverse dependencies of libclamav9 were
also rebuilt against libclamav11. The following source packages were updated:

- c-icap-modules/1:0.5.3-1+deb10u2
- cyrus-imapd/3.0.8-6+deb10u7
- havp/0.93-2+deb10u1
- pg-snakeoil/1.1-1+deb10u1
- python-clamav/0.4.1-11+deb10u1

