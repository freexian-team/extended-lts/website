---
title: "ELA-196-1 icu security update"
package: icu
version: 4.8.1.1-12+deb7u8
distribution: "Debian 7 Wheezy"
description: "double free"
date: 2019-11-27T22:50:41+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-14952

---

An issue has been found in icu, a package containing International Components for Unicode.

By not doing a double free in createMetazoneMappings() a crash of the application that uses this function can be avoided.
