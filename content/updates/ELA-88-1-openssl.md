---
title: "ELA-88-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb7u8
distribution: "Debian 7 Wheezy"
description: "padding oracle attack"
date: 2019-03-03T18:00:45+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-1559

---

Juraj Somorovsky, Robert Merget and Nimrod Aviram discovered a padding
oracle attack in OpenSSL.
If an application encounters a fatal protocol error and then calls
SSL_shutdown() twice (once to send a close_notify, and once to receive one)
then OpenSSL can respond differently to the calling application if a 0 byte
record is received with invalid padding compared to if a 0 byte record is
received with an invalid MAC. If the application then behaves differently
based on that in a way that is detectable to the remote peer, then this
amounts to a padding oracle that could be used to decrypt data.

In order for this to be exploitable "non-stitched" ciphersuites must be in
use. Stitched ciphersuites are optimised implementations of certain
commonly used ciphersuites. Also the application must call SSL_shutdown()
twice even if a protocol error has occurred (applications should not do
this but some do anyway). AEAD ciphersuites are not impacted.


