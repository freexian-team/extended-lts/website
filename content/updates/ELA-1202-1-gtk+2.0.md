---
title: "ELA-1202-1 gtk+2.0 security update"
package: gtk+2.0
version: 2.24.25-3+deb8u3 (jessie), 2.24.31-2+deb9u1 (stretch), 2.24.32-3+deb10u1 (buster)
version_map: {"8 jessie": "2.24.25-3+deb8u3", "9 stretch": "2.24.31-2+deb9u1", "10 buster": "2.24.32-3+deb10u1"}
description: "cwd module loading"
date: 2024-10-07T14:00:53+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-6655

---

Modules were also searched in the current working directory in the GNOME toolkit gtk+2.0, allowing library injection.
