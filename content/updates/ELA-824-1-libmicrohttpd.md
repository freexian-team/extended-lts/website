---
title: "ELA-824-1 libmicrohttpd security update"
package: libmicrohttpd
version: 0.9.37+dfsg-1+deb8u1 (jessie), 0.9.51-1+deb9u1 (stretch)
version_map: {"8 jessie": "0.9.37+dfsg-1+deb8u1", "9 stretch": "0.9.51-1+deb9u1"}
description: "denial of service"
date: 2023-03-30T23:21:47+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-27371

---

An issue has been found in linmicrohttpd, a library embedding HTTP server
functionality. Parsing crafted POST requests result in an out of bounds
read, which might cause a DoS (Denial of Service).

