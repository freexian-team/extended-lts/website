---
title: "ELA-1089-1 less security update"
package: less
version: 458-3+deb8u1 (jessie), 481-2.1+deb9u1 (stretch)
version_map: {"8 jessie": "458-3+deb8u1", "9 stretch": "481-2.1+deb9u1"}
description: "arbitrary command execution"
date: 2024-05-08T23:27:52+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-48624
  - CVE-2024-32487

---

Several vulnerabilities were discovered in less, a file pager, which may result
in the execution of arbitrary commands if a file with a specially crafted file
name is processed.
