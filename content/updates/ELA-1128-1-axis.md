---
title: "ELA-1128-1 axis security update"
package: axis
version: 1.4-21+deb8u1 (jessie)
version_map: {"8 jessie": "1.4-21+deb8u1"}
description: "two vulnerabilities"
date: 2024-07-11T17:08:17+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-8032
  - CVE-2023-40743
---

Two vulnerabilities were discovered in Apache Axis, an XML-based web service
framework for Java.

CVE-2018-8032: Fix a cross-site scripting (XSS) attack in the default
servlet/services. ([#905328](https://bugs.debian.org/905328))

CVE-2023-40743: Fix an issue in `ServiceFactory.getService` that allowed
potentially dangerous lookup mechanisms. When passing untrusted input to this
API method, this could have exposed the application to DoS, SSRF and even
attacks leading to remote code execution. ([#1051288](https://bugs.debian.org/1051288))
