---
title: "ELA-1038-1 openssh security update"
package: openssh
version: 1:7.4p1-10+deb9u9 (stretch)
version_map: {"9 stretch": "1:7.4p1-10+deb9u9"}
description: "multiple vulnerabilities"
date: 2024-01-30T18:36:48-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-41617
  - CVE-2023-48795
  - CVE-2023-51385

---
 
Several vulnerabilities have been discovered in OpenSSH, an implementation of
the SSH protocol suite.

CVE-2021-41617

    It was discovered that sshd failed to correctly initialise supplemental
    groups when executing an AuthorizedKeysCommand or
    AuthorizedPrincipalsCommand, where a AuthorizedKeysCommandUser or
    AuthorizedPrincipalsCommandUser directive has been set to run the command
    as a different user. Instead these commands would inherit the groups that
    sshd was started with.

CVE-2023-48795

    Fabian Baeumer, Marcus Brinkmann and Joerg Schwenk discovered that the SSH
    protocol is prone to a prefix truncation attack, known as the "Terrapin
    attack". This attack allows a MITM attacker to effect a limited break of the
    integrity of the early encrypted SSH transport protocol by sending extra
    messages prior to the commencement of encryption, and deleting an equal
    number of consecutive messages immediately after encryption starts.

    Details can be found at https://terrapin-attack.com/

CVE-2023-51385

    It was discovered that if an invalid user or hostname that contained shell
    metacharacters was passed to ssh, and a ProxyCommand, LocalCommand
    directive or "match exec" predicate referenced the user or hostname via
    expansion tokens, then an attacker who could supply arbitrary
    user/hostnames to ssh could potentially perform command injection. The
    situation could arise in case of git repositories with submodules, where the
    repository could contain a submodule with shell characters in its user or
    hostname.
