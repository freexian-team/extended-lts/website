---
title: "ELA-132-1 bzip2 security update"
package: bzip2
version: 1.0.6-4+deb7u1
distribution: "Debian 7 Wheezy"
description: "fix for out-of-bounds write and use-after-free"
date: 2019-06-22T23:14:03+02:00
draft: false
type: updates
cvelist:
  - CVE-2016-3189
  - CVE-2019-12900

---

Two issues in bzip2, a high-quality block-sorting file compressor, have been fixed. One, CVE-2019-12900, is a out-of-bounds write when using a crafted compressed file. The other, CVE-2016-3189, is a potential user-after-free.
