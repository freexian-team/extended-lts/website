---
title: "ELA-191-1 libice security update"
package: libice
version: 2:1.0.8-2+deb7u1
distribution: "Debian 7 Wheezy"
description: "weak entropy used for keys"
date: 2019-11-23T19:51:27+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-2626

---

It has been found, that libice, an X11 Inter-Client Exchange library, 
uses weak entropy to generate keys.
Using arc4random_buf() from libbsd should avoid this flaw.
