---
title: "ELA-1195-1 libxml2 security update"
package: libxml2
version: 2.9.4+dfsg1-7+deb10u8 (buster)
version_map: {"10 buster": "2.9.4+dfsg1-7+deb10u8"}
description: "XML External Entity attack"
date: 2024-10-03T23:48:50+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-9318

---

An XML External Entity (XXE) attack via crafted documents has been fixed in the XML library libxml2.
