---
title: "ELA-434-1 djvulibre security update"
package: djvulibre
version: 3.5.25.4-4+deb8u3
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-05-26T17:29:56+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3500
  - CVE-2021-32490
  - CVE-2021-32491
  - CVE-2021-32492
  - CVE-2021-32493

---

Several vulnerabilities were discovered in djvulibre, a library and
set of tools to handle documents in the DjVu format. An attacker could
crash document viewers and possibly execute arbitrary code through
crafted DjVu files.
