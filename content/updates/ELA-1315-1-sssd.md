---
title: "ELA-1315-1 sssd security update"
package: sssd
version: 1.15.0-3+deb9u3 (stretch), 1.16.3-3.2+deb10u3 (buster)
version_map: {"9 stretch": "1.15.0-3+deb9u3", "10 buster": "1.16.3-3.2+deb10u3"}
description: "multiple vulnerabilities"
date: 2025-02-01T02:39:03+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-10852
  - CVE-2018-16838
  - CVE-2019-3811
  - CVE-2023-3758

---

CVE-2018-10852

:   It was discovered that when SSSD created the UNIX pipe for
    communication between sudo and the sssd-sudo responder,
    the `umask()` call was set to be too permissive, which resulted in
    the pipe being readable and writable.  Then, if an attacker used the
    same communication protocol that sudo uses to talk to SSSD, they
    could obtain the list of sudo rules for any user who stores their
    sudo rules in a remote directory.

    While the sudo responder is not started by default by SSSD itself,
    utilities like ipa-client-install configure the sudo responder to be
    started.

CVE-2018-16838

:   It was discovered that when the Group Policy Objects (GPO) are not
    readable by SSSD due to a too strict permission settings on the
    server side, SSSD allows all authenticated users to login instead of
    denying access.

    A new boolean setting `ad_gpo_ignore_unreadable` (defaulting to
    `False`) is introduced for environments where attributes in the
    `groupPolicyContainer` are not readable and changing the permissions
    on the GPO objects is not possible or desirable.  See sssd-ad(5).

CVE-2019-3811

:   It was discovered that if a user was configured with no home
    directory set, then sssd(8) returns `/` (i.e., the root directory)
    instead of the empty string (meaning no home directory).  This could
    impact services that restrict the user's filesystem access to within
    their home directory through `chroot()` or similar.

CVE-2023-3758

:   A race condition flaw was found in SSSD where the GPO policy is not
    consistently applied for authenticated users.  This may lead to
    improper authorization issues, granting access to resources
    inappropriately.

(sssd 1.16.3-3.2+deb10u3 only fixes CVE-2023-3758 as the previous
version was already immune to the other vulnerabilities.)
