---
title: "ELA-1160-1 tiff security update"
package: tiff
version: 4.0.3-12.3+deb8u17 (jessie), 4.0.8-2+deb9u12 (stretch)
version_map: {"8 jessie": "4.0.3-12.3+deb8u17", "9 stretch": "4.0.8-2+deb9u12"}
description: "segmentation fault and memory leak"
date: 2024-08-24T00:59:15+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-3576
  - CVE-2023-52356

---

Two issues have been found in tiff, a Tag Image File Format (TIFF) library with tools.
Using crafted TIFF files an attacker would be able to cause a segmentation fault or
a memory leak, which may result in an application crash and denial of service.
