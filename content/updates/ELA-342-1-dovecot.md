---
title: "ELA-342-1 dovecot security update"
package: dovecot
version: 1:2.2.13-12~deb8u9
distribution: "Debian 8 Jessie"
description: "denial of service (DoS) vulnerability"
date: 2021-01-05T17:03:57+00:00
draft: false
type: updates
cvelist:
  - CVE-2020-25275
---

A vulnerability was discovered in the Dovecot IMAP server where a malicious
sender could crash Dovecot repeatedly by sending messages with more than 10,000
MIME parts.
