---
title: "ELA-473-2 openjdk-7 regression update"
package: openjdk-7
version: 7u311-2.6.27-0+deb8u2
distribution: "Debian 8 jessie"
description: "regression update"
date: 2021-08-18T11:46:17+02:00
draft: false
type: updates
cvelist:

---

A recent update for OpenJDK 7 introduced a regression that made applications
crash if they opened the java.security configuration file.
