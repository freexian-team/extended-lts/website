---
title: "ELA-1317-1 ark security update"
package: ark
version: 4:18.08.3-1+deb10u3 (buster)
version_map: {"10 buster": "4:18.08.3-1+deb10u3"}
description: "extraction to absolute paths"
date: 2025-02-08T19:53:19+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-57966

---

A flaw was discovered in ark, an archive utility for the KDE platform. Ark
extracted archives with absolute paths to the corresponding location on
the user's file system. Absolute paths are now treated as relative paths to
prevent overwriting of sensitive information.
