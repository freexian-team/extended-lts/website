---
title: "ELA-921-1 unrar-nonfree security update"
package: unrar-nonfree
version: 1:5.6.6-1+deb9u1 (stretch)
version_map: {"9 stretch": "1:5.6.6-1+deb9u1"}
description: "extraction of files outside destination directory"
date: 2023-08-16T12:01:46+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-12938
  - CVE-2017-12940
  - CVE-2017-12941
  - CVE-2017-12942
  - CVE-2017-20006
  - CVE-2018-25018
  - CVE-2022-30333
  - CVE-2022-48579

---

It was discovered that UnRAR, an unarchiver for rar files, allows extraction of
files outside of the destination folder via symlink chains. Programming flaws
like heap-based buffer overflows or out-of-bounds reads may also cause a denial
of service (application crash) if a malformed rar archive is extracted.

