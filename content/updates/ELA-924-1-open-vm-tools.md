---
title: "ELA-924-1 open-vm-tools security update"
package: open-vm-tools
version: 2:9.4.6-1770165-8+deb8u1 (jessie), 2:10.1.5-5055683-4+deb9u4 (stretch)
version_map: {"8 jessie": "2:9.4.6-1770165-8+deb8u1", "9 stretch": "2:10.1.5-5055683-4+deb9u4"}
description: "authentication bypass vulnerability"
date: 2023-08-17T09:49:42+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-20867

---

open-vm-tools is a package that provides Open VMware Tools for virtual
machines hosted on VMware.

It was discovered that Open VM Tools incorrectly handled certain
authentication requests. A fully compromised ESXi host can force Open
VM Tools to fail to authenticate host-to-guest operations, impacting
the confidentiality and integrity of the guest virtual machine.
