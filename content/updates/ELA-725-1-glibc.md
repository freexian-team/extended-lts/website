---
title: "ELA-725-1 glibc security update"
package: glibc
version: 2.19-18+deb8u11 (jessie)
version_map: {"8 jessie": "2.19-18+deb8u11"}
description: "multiple vulnerabilities"
date: 2022-11-07T12:32:37+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-12132
  - CVE-2017-12133
  - CVE-2017-15670
  - CVE-2017-15671
  - CVE-2017-15804
  - CVE-2017-16997
  - CVE-2017-1000408
  - CVE-2017-1000409
  - CVE-2018-6485
  - CVE-2018-6551
  - CVE-2018-11236
  - CVE-2018-1000001
  - CVE-2019-9169
  - CVE-2019-25013
  - CVE-2020-1752
  - CVE-2020-10029
  - CVE-2020-27618
  - CVE-2020-29573
  - CVE-2021-3326
  - CVE-2021-3999
  - CVE-2021-33574
  - CVE-2021-35942
  - CVE-2022-23218
  - CVE-2022-23219

---

This update fixes a significant number of minor to important vulnerabilities in
glibc.

CVE-2017-12132

    The DNS stub resolver in the GNU C Library, when EDNS support is
    enabled, will solicit large UDP responses from name servers,
    potentially simplifying off-path DNS spoofing attacks due to IP
    fragmentation.

CVE-2017-12133

    Use-after-free vulnerability in the clntudp_call function in
    sunrpc/clnt_udp.c in the GNU C Library allows remote attackers to
    have unspecified impact via vectors related to error path.

CVE-2017-15670

    The GNU C Library contains an off-by- one error leading to a
    heap-based buffer overflow in the glob function in glob.c, related
    to the processing of home directories using the ~ operator followed
    by a long string.

CVE-2017-15671

    The glob function in glob.c in the GNU C Library, when invoked with
    GLOB_TILDE, could skip freeing allocated memory when processing the
    ~ operator with a long user name, potentially leading to a denial of
    service (memory leak).

CVE-2017-15804

    The glob function in glob.c in the GNU C Library contains a buffer
    overflow during unescaping of user names with the ~ operator.

CVE-2017-16997

    elf/dl-load.c in the GNU C Library mishandles RPATH and RUNPATH
    containing $ORIGIN for a privileged (setuid or AT_SECURE) program,
    which allows local users to gain privileges via a Trojan horse
    library in the current working directory, related to the
    fillin_rpath and decompose_rpath functions.  This is associated with
    misinterpretion of an empty RPATH/RUNPATH token as the "./"
    directory. NOTE: this configuration of RPATH/RUNPATH for a
    privileged program is apparently very uncommon; most likely, no such
    program is shipped with any common Linux distribution.

CVE-2017-1000408

    A memory leak in glibc can be reached and amplified through the
    LD_HWCAP_MASK environment variable. Please note that many versions
    of glibc are not vulnerable to this issue if patched for
    CVE-2017-1000366.

CVE-2017-1000409

    A buffer overflow in glibc can be triggered through the
    LD_LIBRARY_PATH environment variable.

CVE-2018-6485

    An integer overflow in the implementation of the posix_memalign in
    memalign functions in the GNU C Library could cause these functions
    to return a pointer to a heap area that is too small, potentially
    leading to heap corruption.

CVE-2018-6551

    The malloc implementation in the GNU C Library on powerpc and i386
    did not properly handle malloc calls with arguments close to
    SIZE_MAX and could return a pointer to a heap region that is smaller
    than requested, eventually leading to heap corruption.

CVE-2018-11236

    stdlib/canonicalize.c in the GNU C Library when processing very long
    pathname arguments to the realpath function, could encounter an
    integer overflow on 32-bit architectures, leading to a stack-based
    buffer overflow and, potentially, arbitrary code execution.

CVE-2018-1000001

    In glibc there is confusion in the usage of getcwd() by realpath()
    which can be used to write before the destination buffer leading to
    a buffer underflow and potential code execution.

CVE-2019-9169

    In the GNU C Library, proceed_next_node in posix/regexec.c has a
    heap-based buffer over-read via an attempted case-insensitive
    regular-expression match.

CVE-2019-25013

    The iconv feature in the GNU C Library, when processing invalid
    multi-byte input sequences in the EUC-KR encoding, may have a buffer
    over-read.

CVE-2020-1752

    A use-after-free vulnerability introduced in glibc was found in the
    way the tilde expansion was carried out.  Directory paths containing
    an initial tilde followed by a valid username were affected by this
    issue. A local attacker could exploit this flaw by creating a
    specially crafted path that, when processed by the glob function,
    would potentially lead to arbitrary code execution.

CVE-2020-10029

    The GNU C Library could overflow an on-stack buffer during range
    reduction if an input to an 80-bit long double function contains a
    non-canonical bit pattern, a seen when passing a
    0x5d414141414141410000 value to sinl on x86 targets. This is related
    to sysdeps/ieee754/ldbl-96/e_rem_pio2l.c.

CVE-2020-27618

    The iconv function in the GNU C Library, when processing invalid
    multi-byte input sequences in IBM1364, IBM1371, IBM1388, IBM1390,
    and IBM1399 encodings, fails to advance the input state, which could
    lead to an infinite loop in applications, resulting in a denial of
    service, a different vulnerability from CVE-2016-10228.

CVE-2020-29573

    sysdeps/i386/ldbl2mpn.c in the GNU C Library on x86 targets has a
    stack-based buffer overflow if the input to any of the printf family
    of functions is an 80-bit long double with a non-canonical bit
    pattern, as seen when passing a
    \x00\x04\x00\x00\x00\x00\x00\x00\x00\x04 value to sprintf.

CVE-2021-3326

    The iconv function in the GNU C Library, when processing invalid
    input sequences in the ISO-2022-JP-3 encoding, fails an assertion in
    the code path and aborts the program, potentially resulting in a
    denial of service.

CVE-2021-3999

    A flaw was found in glibc. An off-by-one buffer overflow and underflow
    in getcwd() may lead to memory corruption when the size of the buffer
    is exactly 1. A local attacker who can control the input buffer and
    size passed to getcwd() in a setuid program could use this flaw to
    potentially execute arbitrary code and escalate their privileges on
    the system.

CVE-2021-33574

    The mq_notify function in the GNU C Library has a use-after-free. It
    may use the notification thread attributes object (passed through
    its struct sigevent parameter) after it has been freed by the
    caller, leading to a denial of service (application crash) or
    possibly unspecified other impact.

CVE-2021-35942

    The wordexp function in the GNU C Library may crash or read
    arbitrary memory in parse_param (in posix/wordexp.c) when called
    with an untrusted, crafted pattern, potentially resulting in a
    denial of service or disclosure of information. This occurs because
    atoi was used but strtoul should have been used to ensure correct
    calculations.

CVE-2022-23218

    The deprecated compatibility function svcunix_create in the sunrpc
    module of the GNU C Library copies its path argument on the stack
    without validating its length, which may result in a buffer
    overflow, potentially resulting in a denial of service or (if an
    application is not built with a stack protector enabled) arbitrary
    code execution.

CVE-2022-23219

    The deprecated compatibility function clnt_create in the sunrpc
    module of the GNU C Library copies its hostname argument on the
    stack without validating its length, which may result in a buffer
    overflow, potentially resulting in a denial of service or (if an
    application is not built with a stack protector enabled) arbitrary
    code execution.

