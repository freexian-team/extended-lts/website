---
title: "ELA-116-1 openjdk-7 security update"
package: openjdk-7
version: 7u221-2.6.18-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2019-05-10T18:36:11+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-2602
  - CVE-2019-2684
  - CVE-2019-2698

---

Several vulnerabilities have been discovered in OpenJDK, an
implementation of the Oracle Java platform, resulting in denial of
service, sandbox bypass, information disclosure or the execution
of arbitrary code.
