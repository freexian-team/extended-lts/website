---
title: "ELA-967-1 ncurses security update"
package: ncurses
version: 5.9+20140913-1+deb8u5 (jessie), 6.0+20161126-1+deb9u4 (stretch)
version_map: {"8 jessie": "5.9+20140913-1+deb8u5", "9 stretch": "6.0+20161126-1+deb9u4"}
description: "out-of-bounds read problem"
date: 2023-09-28T14:48:47+01:00
draft: false
type: updates
tags:
- update
cvelist:
- CVE-2020-19189
---


An out-of-bounds read problem was found in the postprocess_terminfo function
of ncurses, a text-based user interface toolkit, which could potentially lead
to an exposure of sensitive information or denial of service.
