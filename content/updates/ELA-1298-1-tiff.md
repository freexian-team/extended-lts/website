---
title: "ELA-1298-1 tiff security update"
package: tiff
version: 4.0.3-12.3+deb8u18 (jessie), 4.0.8-2+deb9u13 (stretch), 4.1.0+git191117-2~deb10u10 (buster)
version_map: {"8 jessie": "4.0.3-12.3+deb8u18", "9 stretch": "4.0.8-2+deb9u13", "10 buster": "4.1.0+git191117-2~deb10u10"}
description: "NULL pointer dereference"
date: 2025-01-20T17:24:16+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-7006

---

NULL pointer dereference in TIFFReadDirectory()/TIFFReadCustomDirectory() has been fixed in tiff, a library and tools providing support for the Tag Image File Format (TIFF).

Additionally, issues with the earlier fixes for CVE-2023-52356 and CVE-2023-25433 have been resolved.
