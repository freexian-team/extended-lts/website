---
title: "ELA-1252-1 libmodule-scandeps-perl security update"
package: libmodule-scandeps-perl
version: 1.16-1+deb8u1 (jessie), 1.23-1+deb9u1 (stretch), 1.27-1+deb10u1 (buster)
version_map: {"8 jessie": "1.16-1+deb8u1", "9 stretch": "1.23-1+deb9u1", "10 buster": "1.27-1+deb10u1"}
description: "arbitrary code execution"
date: 2024-11-28T17:29:38-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-10224

---

The Qualys Threat Research Unit discovered that libmodule-scandeps-perl,
a Perl module to recursively scan Perl code for dependencies, allows an
attacker to execute arbitrary shell commands via specially crafted file
names.

Details can be found in the Qualys advisory at
https://www.qualys.com/2024/11/19/needrestart/needrestart.txt
