---
title: "ELA-941-1 gst-plugins-ugly1.0 security update"
package: gst-plugins-ugly1.0
version: 1.10.4-1+deb9u2 (stretch)
version_map: {"9 stretch": "1.10.4-1+deb9u2"}
description: "vulnerabilities in RealMedia demuxer"
date: 2023-08-31T23:56:13+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

Demuxer vulnerabilities have been fixed in the RealMedia demuxers for
the GStreamer media framework
