---
title: "ELA-795-1 heimdal security update"
package: heimdal
version: 7.1.0+dfsg-13+deb9u4 (stretch)
version_map: {"9 stretch": "7.1.0+dfsg-13+deb9u4"}
description: "7 vulnerabilities"
date: 2023-02-08T13:15:06+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-14870
  - CVE-2021-3671
  - CVE-2021-44758
  - CVE-2022-3437
  - CVE-2022-41916
  - CVE-2022-42898
  - CVE-2022-44640

---

This update fixes several vulnerabilities in heimdal, an implementation of
kerberos.

CVE-2019-14870

    Improper validation of forwarded kerberos tickets.

CVE-2021-3671

    A null pointer de-reference was found in the way heimdal kdc handled
    missing sname in TGS-REQ (Ticket Granting Server - Request). An
    authenticated user could use this flaw to crash the kdc.

CVE-2021-44758

    Heimdal allows attackers to cause a NULL pointer dereference in a SPNEGO
    acceptor via a preferred_mech_type of GSS_C_NO_OID and a nonzero
    initial_response value to send_accept.    

CVE-2022-3437

    A heap-based buffer overflow vulnerability was found within the GSSAPI
    unwrap_des() and unwrap_des3() routines of Heimdal. The DES and Triple-DES
    decryption routines in the Heimdal GSSAPI library allow a length-limited
    write buffer overflow on malloc() allocated memory when presented with a
    maliciously small packet. This flaw allows a remote user to send specially
    crafted malicious data to the application, possibly resulting in a denial
    of service (DoS) attack.

CVE-2022-41916

    Heimdal is an implementation of ASN.1/DER, PKIX, and Kerberos. It was
    vulnerable to a denial of service vulnerability in the PKI certificate
    validation library, affecting the KDC (via PKINIT) and kinit (via PKINIT),
    as well as any third-party applications using Heimdal's libhx509.

CVE-2022-42898

    PAC parsing in heimdal has integer overflows that may lead to remote code
    execution (in KDC, kadmind, or a GSS or Kerberos application server) on
    32-bit platforms (which have a resultant heap-based buffer overflow), and
    cause a denial of service on other platforms. This occurs in krb5_pac_parse
    in lib/krb5/krb/pac.c in MIT Kerberos. The bug for heimdal is similar.

CVE-2022-44640

    Heimdal allows remote attackers to execute arbitrary code because of an
    invalid free in the ASN.1 codec used by the Key Distribution Center (KDC).
