---
title: "ELA-875-1 libxpm security update"
package: libxpm
version: 1:3.5.12-0+deb8u2 (jessie), 1:3.5.12-1+deb9u1 (stretch)
version_map: {"8 jessie": "1:3.5.12-0+deb8u2", "9 stretch": "1:3.5.12-1+deb9u1"}
description: "multiple vulnerabilities"
date: 2023-06-20T20:11:42Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-4883
  - CVE-2022-44617
  - CVE-2022-46285

---

Multiple vulnerabilities were found in libxpm, a library handling X PixMap image format (so called xpm files).
xpm files are an extension of the monochrome X BitMap format specified in the X protocol, and are commonly used in traditional X applications.

CVE-2022-4883

    When processing files with .Z or .gz extensions, the library calls external programs to compress and uncompress files, relying on the PATH environment variable to find these programs, which could allow a malicious user to execute other programs by manipulating the PATH environment variable.

CVE-2022-44617

    When processing a file with width of 0 and a very large height, some parser functions will be called repeatedly and can lead to an infinite loop, resulting in a Denial of Service in the application linked to the library.

CVE-2022-46285

    When parsing a file with a comment not closed an end-of-file condition will not be detected, leading to an infinite loop and resulting in a Denial of Service in the application linked to the library.
