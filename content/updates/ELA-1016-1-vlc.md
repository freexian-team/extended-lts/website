---
title: "ELA-1016-1 vlc security update"
package: vlc
version: 3.0.20-0+deb9u1 (stretch)
version_map: {"9 stretch": "3.0.20-0+deb9u1"}
description: "vulnerabilities in the MMSH module"
date: 2023-11-30T23:52:01Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-47359
  - CVE-2023-47360

---

Two vulnerabilities in the MMS over HTTP protocol have been fixed in the
VLC media player, which has also been upgraded to the latest upstream version.

CVE-2023-47359

    Heap buffer overflow in the MMSH module.

CVE-2023-47360

    Integer underflow in the MMSH module.

