---
title: "ELA-1115-1 glib2.0 security update"
package: glib2.0
version: 2.50.3-2+deb9u6 (stretch)
version_map: {"9 stretch": "2.50.3-2+deb9u6"}
description: "spoofing"
date: 2024-06-27T11:16:30+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-34397

---

Alicia Boya Garcia reported that the GDBus signal subscriptions in the
GLib library are prone to a spoofing vulnerability. A local attacker can
take advantage of this flaw to cause a GDBus-based client to behave
incorrectly, with an application-dependent impact.
