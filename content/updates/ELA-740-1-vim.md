---
title: "ELA-740-1 vim security update"
package: vim
version: 2:8.0.0197-4+deb9u9 (stretch)
version_map: {"9 stretch": "2:8.0.0197-4+deb9u9"}
description: "multiple memory access violations"
date: 2022-11-25T08:00:52+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-1897
  - CVE-2022-1942
  - CVE-2022-2000
  - CVE-2022-2129
  - CVE-2022-3235
  - CVE-2022-3256
  - CVE-2022-3352

---

This update fixes multiple memory access violations in vim.

CVE-2022-1897

    Out-of-bounds Write

CVE-2022-1942

    Heap-based Buffer Overflow

CVE-2022-2000

    Out-of-bounds Write

CVE-2022-2129

    Out-of-bounds Write

CVE-2022-3235

    Use After Free

CVE-2022-3256

    Use After Free

CVE-2022-3352

    Use After Free

