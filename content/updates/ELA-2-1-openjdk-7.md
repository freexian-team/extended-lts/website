---
title: "ELA-2-1 openjdk-7 security update"
package: openjdk-7
version: 7u181-2.6.14-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2018-06-15T08:54:30+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-2790 CVE-2018-2794 CVE-2018-2795 CVE-2018-2796 CVE-2018-2797 CVE-2018-2798 CVE-2018-2799 CVE-2018-2800 CVE-2018-2814 CVE-2018-2815
---

Several vulnerabilities have been discovered in OpenJDK, an
implementation of the Oracle Java platform, resulting in denial of
service, sandbox bypass, execution of arbitrary code or bypass of JAR
signature validation.
