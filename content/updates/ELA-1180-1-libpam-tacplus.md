---
title: "ELA-1180-1 libpam-tacplus security update"
package: libpam-tacplus
version: 1.3.8-2+deb10u2 (buster)
version_map: {"10 buster": "1.3.8-2+deb10u2"}
description: "possible information disclosure"
date: 2024-09-15T23:39:19+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-20014

---

Missing zeroing of a structure has been fixed in libpam-tacplus, a PAM module for using TACACS+ as an authentication service.
