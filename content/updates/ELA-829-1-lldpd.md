---
title: "ELA-829-1 lldpd security update"
package: lldpd
version: 0.9.6-1+deb9u1 (stretch)
version_map: {"9 stretch": "0.9.6-1+deb9u1"}
description: "denial of service (DoS) vulnerability"
date: 2023-08-07T14:13:04+01:00
draft: false
cvelist:
  - CVE-2020-27827
  - CVE-2021-43612
---

It was discovered that there were two potential denial of service (DoS) attacks
in `lldpd`, a implementation of the IEEE 802.1ab (LLDP) protocol used to
administer and monitor networking devices.
