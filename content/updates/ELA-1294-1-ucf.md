---
title: "ELA-1294-1 ucf security update"
package: ucf
version: 3.0030+deb8u1 (jessie), 3.0036+deb9u1 (stretch), 3.0038+nmu1+deb10u1 (buster)
version_map: {"8 jessie": "3.0030+deb8u1", "9 stretch": "3.0036+deb9u1", "10 buster": "3.0038+nmu1+deb10u1"}
description: "command-injection vulnerability"
date: 2025-01-16T17:03:34+00:00
draft: false
type: updates
tags:
- update
---

A potential command-injection vulnerability was discovered in ucf, a tool to
preserve user changes to config files.
