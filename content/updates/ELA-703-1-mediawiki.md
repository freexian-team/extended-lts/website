---
title: "ELA-703-1 mediawiki security update"
package: mediawiki
version: 1:1.27.7-1+deb9u13 (stretch)
version_map: {"9 stretch": "1:1.27.7-1+deb9u13"}
description: "privacy leakage"
date: 2022-10-13T00:08:15+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-41765

---

A privacy flaw was discovered in mediawiki, a website engine for collaborative
work. The HTMLUserTextField exposed the existence of hidden users which gave
more insight than actually intended.

