---
title: "ELA-102-1 libdatetime-timezone-perl new upstream version"
package: libdatetime-timezone-perl
version: 1:1.58-1+2019a
distribution: "Debian 7 Wheezy"
description: "new version update"
date: 2019-04-01T12:10:43+02:00
draft: false
type: updates
cvelist:

---

This update brings the Olson database changes from the 2019a version to
the Perl bindings.
