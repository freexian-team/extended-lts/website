---
title: "ELA-778-1 libde265 security update"
package: libde265
version: 1.0.2-2+deb9u2 (stretch)
version_map: {"9 stretch": "1.0.2-2+deb9u2"}
description: "multiple vulnerabilities"
date: 2023-01-26T16:32:39+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-21594
  - CVE-2020-21595
  - CVE-2020-21596
  - CVE-2020-21597
  - CVE-2020-21598
  - CVE-2020-21600
  - CVE-2020-21601
  - CVE-2020-21602
  - CVE-2020-21603
  - CVE-2020-21604
  - CVE-2020-21605
  - CVE-2020-21606
  - CVE-2022-43235
  - CVE-2022-43236
  - CVE-2022-43237
  - CVE-2022-43238
  - CVE-2022-43239
  - CVE-2022-43240
  - CVE-2022-43241
  - CVE-2022-43242
  - CVE-2022-43243
  - CVE-2022-43244
  - CVE-2022-43245
  - CVE-2022-43248
  - CVE-2022-43249
  - CVE-2022-43250
  - CVE-2022-43252
  - CVE-2022-43253
  - CVE-2022-47655

---

Multiple issues were found in libde265, an open source implementation of the
H.265 video codec, which may result in denial of or have unspecified other
impact.

CVE-2020-21594

    libde265 v1.0.4 contains a heap buffer overflow in the put_epel_hv_fallback
    function, which can be exploited via a crafted a file.

CVE-2020-21595

    libde265 v1.0.4 contains a heap buffer overflow in the mc_luma function,
    which can be exploited via a crafted a file.

CVE-2020-21596

    libde265 v1.0.4 contains a global buffer overflow in the
    decode_CABAC_bit function, which can be exploited via a crafted a
    file.

CVE-2020-21597

    libde265 v1.0.4 contains a heap buffer overflow in the mc_chroma
    function, which can be exploited via a crafted a file.

CVE-2020-21598

    libde265 v1.0.4 contains a heap buffer overflow in the
    ff_hevc_put_unweighted_pred_8_sse function, which can be exploited
    via a crafted a file.

CVE-2020-21600

    libde265 v1.0.4 contains a heap buffer overflow in the
    put_weighted_pred_avg_16_fallback function, which can be exploited via a
    crafted a file.

CVE-2020-21601

    libde265 v1.0.4 contains a stack buffer overflow in the put_qpel_fallback
    function, which can be exploited via a crafted a file.

CVE-2020-21602

    libde265 v1.0.4 contains a heap buffer overflow in the
    put_weighted_bipred_16_fallback function, which can be exploited via a crafted
    a file.

CVE-2020-21603

    libde265 v1.0.4 contains a heap buffer overflow in the
    put_qpel_0_0_fallback_16 function, which can be exploited via a crafted a file.

CVE-2020-21604

    libde265 v1.0.4 contains a heap buffer overflow fault in the
    _mm_loadl_epi64 function, which can be exploited via a crafted a file.

CVE-2020-21605

    libde265 v1.0.4 contains a segmentation fault in the apply_sao_internal
    function, which can be exploited via a crafted a file.

CVE-2020-21606

    libde265 v1.0.4 contains a heap buffer overflow fault in the
    put_epel_16_fallback function, which can be exploited via a crafted a file.

CVE-2022-43235

    Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via ff_hevc_put_hevc_epel_pixels_8_sse in sse-motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS) via a crafted
    video file.

CVE-2022-43236

    Libde265 v1.0.8 was discovered to contain a stack-buffer-overflow
    vulnerability via put_qpel_fallback<unsigned short> in fallback-motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS) via a crafted
    video file.

CVE-2022-43237

    Libde265 v1.0.8 was discovered to contain a stack-buffer-overflow
    vulnerability via void put_epel_hv_fallback<unsigned short> in
    fallback-motion.cc. This vulnerability allows attackers to cause a Denial of
    Service (DoS) via a crafted video file.

CVE-2022-43238

    Libde265 v1.0.8 was discovered to contain an unknown crash via
    ff_hevc_put_hevc_qpel_h_3_v_3_sse in sse-motion.cc. This vulnerability allows
    attackers to cause a Denial of Service (DoS) via a crafted video file.

CVE-2022-43239

    Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via mc_chroma<unsigned short> in motion.cc. This vulnerability
    allows attackers to cause a Denial of Service (DoS) via a crafted video file.

CVE-2022-43240

    Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via ff_hevc_put_hevc_qpel_h_2_v_1_sse in sse-motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS) via a crafted
    video file.

CVE-2022-43241

    Libde265 v1.0.8 was discovered to contain an unknown crash via
    ff_hevc_put_hevc_qpel_v_3_8_sse in sse-motion.cc. This vulnerability allows
    attackers to cause a Denial of Service (DoS) via a crafted video file.

CVE-2022-43242

    Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via mc_luma<unsigned char> in motion.cc. This vulnerability
    allows attackers to cause a Denial of Service (DoS) via a crafted video file.

CVE-2022-43243

    Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via ff_hevc_put_weighted_pred_avg_8_sse in sse-motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS) via a crafted
    video file.

CVE-2022-43244

    Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_qpel_fallback<unsigned short> in fallback-motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS) via a crafted
    video file.

CVE-2022-43245

    Libde265 v1.0.8 was discovered to contain a segmentation violation via
    apply_sao_internal<unsigned short> in sao.cc. This vulnerability allows
    attackers to cause a Denial of Service (DoS) via a crafted video file.

CVE-2022-43248

    Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_weighted_pred_avg_16_fallback in fallback-motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS) via a crafted
    video file.

CVE-2022-43249

    Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_epel_hv_fallback<unsigned short> in fallback-motion.cc.
    This vulnerability allows attackers to cause a Denial of Service (DoS) via a
    crafted video file.

CVE-2022-43250

    Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_qpel_0_0_fallback_16 in fallback-motion.cc.  This
    vulnerability allows attackers to cause a Denial of Service (DoS) via a crafted
    video file.

CVE-2022-43252

    Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_epel_16_fallback in fallback-motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS) via a crafted
    video file.

CVE-2022-43253

    Libde265 v1.0.8 was discovered to contain a heap-buffer-overflow
    vulnerability via put_unweighted_pred_16_fallback in fallback-motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS) via a crafted
    video file.

CVE-2022-47655

    Libde265 1.0.9 is vulnerable to Buffer Overflow in function void
    put_qpel_fallback<unsigned short>
