---
title: "ELA-673-1 ghostscript security update"
package: ghostscript
version: 9.26a~dfsg-0+deb8u10 (jessie), 9.26a~dfsg-0+deb9u10 (stretch)
version_map: {"8 jessie": "9.26a~dfsg-0+deb8u10", "9 stretch": "9.26a~dfsg-0+deb9u10"}
description: "buffer overflow"
date: 2022-09-03T23:24:50+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-27792

---

A heap-based buffer over write vulnerability was found in GhostScript, the GPL
PostScript/PDF interpreter. An attacker could trick a user to open a crafted
PDF file, triggering the heap buffer overflow that could lead to memory
corruption or a denial of service.
