---
title: "ELA-930-1 snapd security update"
package: snapd
version: 2.21-2+deb9u3 (stretch)
version_map: {"9 stretch": "2.21-2+deb9u3"}
description: "local privilege escalation"
date: 2023-08-23T01:11:22+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-3328

---

The Qualys Research Team discovered that a race condition existed in the snapd
snap-confine binary when preparing the private /tmp mount for a snap. A local
attacker could possibly use this issue to escalate privileges and execute
arbitrary code.
