---
title: "ELA-1312-1 openjdk-11 security update"
package: openjdk-11
version: 11.0.26+4-1~deb10u1 (buster)
version_map: {"10 buster": "11.0.26+4-1~deb10u1"}
description: "unauthorized access"
date: 2025-01-31T14:41:21+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2025-21502

---

An issue was found in the OpenJDK Java runtime, which may result in
unauthorized access.
