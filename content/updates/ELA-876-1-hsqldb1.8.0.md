---
title: "ELA-876-1 hsqldb1.8.0 security update"
package: hsqldb1.8.0
version: 1.8.0.10+dfsg-7+deb9u1 (stretch)
version_map: {"9 stretch": "1.8.0.10+dfsg-7+deb9u1"}
description: "arbitrary file write"
date: 2023-06-21T19:00:32+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-1183

---

Gregor Kopf of Secfault Security GmbH discovered that HSQLDB, a Java SQL
database engine, allowed the execution of spurious scripting commands in
.script and .log files. Hsqldb supports a "SCRIPT" keyword which is normally
used to record the commands input by the database admin to output such a
script. In combination with LibreOffice, an attacker could craft an odb
containing a "database/script" file which itself contained a SCRIPT command
where the contents of the file could be written to a new file whose location was
determined by the attacker.
