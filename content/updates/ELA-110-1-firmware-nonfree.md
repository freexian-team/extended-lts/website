---
title: "ELA-110-1 firmware-nonfree security update"
package: firmware-nonfree
version: 20161130-5~deb7u1
distribution: "Debian 7 Wheezy"
description: "information disclosure"
date: 2019-04-22T16:40:49+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-5383

---

Eli Biham and Lior Neumann discovered a cryptographic weakness in the
Bluetooth LE SC pairing protocol, called the Fixed Coordinate Invalid
Curve Attack (CVE-2018-5383).  Depending on the devices used, this
could be exploited by a nearby attacker to obtain sensitive
information, for denial of service, or for other security impact.

This flaw has been fixed in firmware for Intel Wireless 7260 (B3),
7260 (B5), 7265 (D1), and 8264 adapters, and for Qualcomm Atheros
QCA61x4 "ROME" version 3.2 adapters.  Other Bluetooth adapters are
also affected and remain vulnerable.
