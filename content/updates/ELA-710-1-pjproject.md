---
title: "ELA-710-1 pjproject security update"
package: pjproject
version: 2.5.5~dfsg-6+deb9u7 (stretch)
version_map: {"9 stretch": "2.5.5~dfsg-6+deb9u7"}
description: "buffer overflow"
date: 2022-10-26T15:11:59+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-39244

---

PJSIP is a free and open source multimedia communication library written in C.
The PJSIP parser, PJMEDIA RTP decoder, and PJMEDIA SDP parser are affected by a
buffer overflow vulnerability. Users connecting to untrusted clients are at
risk.
