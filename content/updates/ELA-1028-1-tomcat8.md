---
title: "ELA-1028-1 tomcat8 security update"
package: tomcat8
version: 8.5.54-0+deb9u14 (stretch)
version_map: {"9 stretch": "8.5.54-0+deb9u14"}
description: "improper input validation"
date: 2024-01-04T11:54:59Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-46589

---

An improper input validation vulnerability was discovered in Apache Tomcat.
Tomcat did not correctly parse HTTP trailer headers.  A trailer header that
exceeded the header size limit could cause Tomcat to treat a single request as
multiple requests, leading to the possibility of request smuggling when behind
a reverse proxy.

The update for Debian 8 "jessie" is pending.
