---
title: "ELA-1131-1 phpldapadmin security update"
package: phpldapadmin
version: 1.2.2-5.2+deb8u3 (jessie)
version_map: {"8 jessie": "1.2.2-5.2+deb8u3"}
description: "HTTP request smuggling vulnerability"
date: 2024-07-16T09:16:57+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-15039
---

A HTTP request smuggling vulnerability was discovered in `phpldapadmin`, a
web-based interface for administering Lightweight Directory Access Protocol
(LDAP) servers.
