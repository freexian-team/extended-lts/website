---
title: "ELA-100-1 tiff3 security update"
package: tiff3
version: 3.9.6-11+deb7u14
distribution: "Debian 7 Wheezy"
description: "mishandling of reading of TIFF files"
date: 2019-03-28T12:23:53Z
draft: false
type: updates
cvelist:
  - CVE-2018-5360

---

A vulnerability has been discovered in tiff3, an older implementation of
the libtiff library providing support for the Tag Image File Format
(TIFF), a widely used format for storing image data.  Mishandling the
reading of TIFF files has been demonstrated by a heap-based buffer
over-read in the ReadTIFFImage function in coders/tiff.c in
GraphicsMagick.
