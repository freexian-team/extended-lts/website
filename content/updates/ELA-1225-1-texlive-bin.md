---
title: "ELA-1225-1 texlive-bin security update"
package: texlive-bin
version: 2016.20160513.41080.dfsg-2+deb9u2 (stretch), 2018.20181218.49446-1+deb10u3 (buster)
version_map: {"9 stretch": "2016.20160513.41080.dfsg-2+deb9u2", "10 buster": "2018.20181218.49446-1+deb10u3"}
description: "multiple Vulnerabilities"
date: 2024-11-01T22:31:54Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-32668
  - CVE-2024-25262

---

TeXLive, a popular software distribution for the TeX typesetting system
that includes major TeX-related programs, macro packages, and fonts,
was affected by two vulnerabilties.

CVE-2023-32668

    A document (compiled with the default settings)
    was allowed to make arbitrary network requests.
    This occurs because full access to the socket library was
    permitted by default, as stated in the documentation.

CVE-2024-25262

    A heap buffer overflow was found via
    the function ttfLoadHDMX:ttfdump. This vulnerability
    allows attackers to cause a Denial of Service (DoS)
    via supplying a crafted TTF file.
