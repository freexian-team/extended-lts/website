---
title: "ELA-836-1 protobuf security update"
package: protobuf
version: 2.6.1-1+deb8u1 (jessie), 3.0.0-9+deb9u1 (stretch)
version_map: {"8 jessie": "2.6.1-1+deb8u1", "9 stretch": "3.0.0-9+deb9u1"}
description: "multiple vulnerabilities"
date: 2023-04-19T08:58:01+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-22569
  - CVE-2021-22570
  - CVE-2022-1941

---

This update fixes a NULL pointer dereference and two denial of service
conditions in protobuf.

CVE-2021-22569

    An issue in protobuf-java allowed the interleaving of
    com.google.protobuf.UnknownFieldSet fields in such a way that would be
    processed out of order. A small malicious payload can occupy the parser for
    several minutes by creating large numbers of short-lived objects that cause
    frequent, repeated pauses.

CVE-2021-22570

    Nullptr dereference when a null char is present in a proto symbol. The
    symbol is parsed incorrectly, leading to an unchecked call into the proto
    file's name during generation of the resulting error message. Since the
    symbol is incorrectly parsed, the file is nullptr.

CVE-2022-1941

    A parsing vulnerability for the MessageSet type in the ProtocolBuffers can
    lead to out of memory failures. A specially crafted message with multiple
    key-value per elements creates parsing issues, and can lead to a Denial of
    Service against services receiving unsanitized input.
