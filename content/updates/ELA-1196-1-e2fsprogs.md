---
title: "ELA-1196-1 e2fsprogs security update"
package: e2fsprogs
version: 1.42.12-2+deb8u3 (jessie), 1.43.4-2+deb9u3 (stretch), 1.44.5-1+deb10u4 (buster)
version_map: {"8 jessie": "1.42.12-2+deb8u3", "9 stretch": "1.43.4-2+deb9u3", "10 buster": "1.44.5-1+deb10u4"}
description: "out-of-bounds read/write"
date: 2024-10-04T17:25:40+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-1304

---

An out-of-bounds read/write vulnerability has been fixed in the e2fsck tool of the ext2/ext3/ext4 file system utilities e2fsprogs.
