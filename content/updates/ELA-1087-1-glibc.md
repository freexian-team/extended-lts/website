---
title: "ELA-1087-1 glibc security update"
package: glibc
version: 2.19-18+deb8u13 (jessie), 2.24-11+deb9u6 (stretch)
version_map: {"8 jessie": "2.19-18+deb8u13", "9 stretch": "2.24-11+deb9u6"}
description: "out-of-bounds write"
date: 2024-05-04T01:48:46+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-2961

---

Out-of-bounds write in the iconv ISO-2022-CN-EXT module has been fixed  
in the GNU C library.
