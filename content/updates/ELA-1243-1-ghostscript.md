---
title: "ELA-1243-1 ghostscript security update"
package: ghostscript
version: 9.26a~dfsg-0+deb8u13 (jessie), 9.26a~dfsg-0+deb9u13 (stretch), 9.27~dfsg-2+deb10u10 (buster)
version_map: {"8 jessie": "9.26a~dfsg-0+deb8u13", "9 stretch": "9.26a~dfsg-0+deb9u13", "10 buster": "9.27~dfsg-2+deb10u10"}
description: "multiple vulnerabilities"
date: 2024-11-24T23:59:04+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-46951
  - CVE-2024-46953
  - CVE-2024-46955
  - CVE-2024-46956

---
Multiple vulnerabilities have been fixed in the PostScript/PDF
interpreter Ghostscript.

CVE-2024-46951

    PS interpreter unchecked pointer

CVE-2024-46953

    output filename format string integer overflow

CVE-2024-46955

    PS interpreter out-of-bounds

CVE-2024-46956

    PS interpreter out-of-bounds
