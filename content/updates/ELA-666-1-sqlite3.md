---
title: "ELA-666-1 sqlite3 security update"
package: sqlite3
version: 3.8.7.1-1+deb8u7 (jessie)
version_map: {"8 jessie": "3.8.7.1-1+deb8u7"}
description: "missing validation and missing error handling in SELECT...WITH"
date: 2022-08-25T11:49:35+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-16168
  - CVE-2019-20218

---

Multiple fixes for vulnerabilities were backported from Debian stretch to
Debian jessie. The two fixed vulnerabilities could result in crashes when
working with BTree indexes, and in unexpected behaviour after parsing errors
in WITH clauses.

Debian 9 stretch is not affected, the changes have been delivered there
before.
