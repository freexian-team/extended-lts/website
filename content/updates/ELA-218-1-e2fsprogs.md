---
title: "ELA-218-1 e2fsprogs security update"
package: e2fsprogs
version: 1.42.5-1.1+deb7u3
distribution: "Debian 7 Wheezy"
description: "out-of-bounds write on stack"
date: 2020-03-24T19:46:30+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-5188

---

An issue has been found in e2fsprogs, a package that contains ext2/ext3/ext4 file system utilities.
A specially crafted ext4 directory can cause an out-of-bounds write on the stack, resulting in code execution. An attacker can corrupt a partition to trigger this vulnerability.

