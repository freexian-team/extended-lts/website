---
title: "ELA-1176-1 libxml2 security update"
package: libxml2
version: 2.9.1+dfsg1-5+deb8u16 (jessie), 2.9.4+dfsg1-2.2+deb9u11 (stretch), 2.9.4+dfsg1-7+deb10u7 (buster)
version_map: {"8 jessie": "2.9.1+dfsg1-5+deb8u16", "9 stretch": "2.9.4+dfsg1-2.2+deb9u11", "10 buster": "2.9.4+dfsg1-7+deb10u7"}
description: "two vulnerabilities"
date: 2024-09-07T15:51:27+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-3709
  - CVE-2022-2309

---

Two vulnerabilities have been fixed in the XML library libxml2.

CVE-2016-3709 (buster)

    HTML 4 parser cross-site scripting

CVE-2022-2309

    Parser NULL pointer dereference
