---
title: "ELA-698-1 bind9 security update"
package: bind9
version: 1:9.9.5.dfsg-9+deb8u28 (jessie), 1:9.10.3.dfsg.P4-12.3+deb9u13 (stretch)
version_map: {"8 jessie": "1:9.9.5.dfsg-9+deb8u28", "9 stretch": "1:9.10.3.dfsg.P4-12.3+deb9u13"}
description: "denial of service"
date: 2022-10-07T12:21:24+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-2795
  - CVE-2022-38177

---

Several vulnerabilities were discovered in BIND, a DNS server
implementation.

CVE-2022-2795

    Yehuda Afek, Anat Bremler-Barr and Shani Stajnrod discovered that a
    flaw in the resolver code can cause named to spend excessive amounts
    of time on processing large delegations, significantly degrade
    resolver performance and result in denial of service.

CVE-2022-38177

    It was discovered that the DNSSEC verification code for the ECDSA
    algorithm is susceptible to a memory leak flaw. A remote attacker
    can take advantage of this flaw to cause BIND to consume resources,
    resulting in a denial of service.
