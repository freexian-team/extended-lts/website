---
title: "ELA-1227-1 libxml2 security update"
package: libxml2
version: 2.9.1+dfsg1-5+deb8u17 (jessie), 2.9.4+dfsg1-7+deb10u9 (buster)
version_map: {"8 jessie": "2.9.1+dfsg1-5+deb8u17", "10 buster": "2.9.4+dfsg1-7+deb10u9"}
description: "multiple vulnerabilities"
date: 2024-11-03T09:13:19+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-9318
  - CVE-2017-16932
  - CVE-2023-39615
  - CVE-2023-45322
  - CVE-2024-25062

---


Several vulnerabilities were discovered in libxml2, a library providing
support to read, modify and write XML and HTML files, potentially allowing
an attacker to perform denial of service or trigger an use-after-free situation.


CVE-2016-9318 (Debian 8 update only)

    XML External Entity (XXE) attacks via a crafted document.

Note: CVE-2016-9318 has been previously addressed for Debian 10  (buster) in ELA-1195.

CVE-2017-16932

    When expanding a parameter entity in a DTD, infinite recursion could lead to
    an infinite loop or memory exhaustion.

CVE-2023-39615

    Xmlsoft Libxml2 v2.11.0 was discovered to contain an out-of-bounds read via
    the xmlSAX2StartElement() function at /libxml2/SAX2.c. This vulnerability
    allows attackers to cause a Denial of Service (DoS) via supplying a crafted
    XML file.

CVE-2023-45322

    libxml2 through 2.11.5 has a use-after-free that can only occur after a
    certain memory allocation fails. This occurs in xmlUnlinkNode in tree.c. 


CVE-2024-25062

    An issue was discovered in libxml2 before 2.11.7 and 2.12.x before 2.12.5.
    When using the XML Reader interface with DTD validation and XInclude 
    expansion enabled, processing crafted XML documents can lead to an 
    xmlValidatePopElement use-after-free.

