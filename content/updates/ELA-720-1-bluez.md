---
title: "ELA-720-1 bluez security update"
package: bluez
version: 5.43-2+deb9u2~deb8u5 (jessie), 5.43-2+deb9u6 (stretch)
version_map: {"8 jessie": "5.43-2+deb9u2~deb8u5", "9 stretch": "5.43-2+deb9u6"}
description: "denial of service or information leak"
date: 2022-10-30T13:00:58+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-0204
  - CVE-2022-39176
  - CVE-2022-39177

---

Several vulnerabilities have been found in BlueZ, the Linux Bluetooth protocol stack.

CVE-2022-0204

  A heap overflow vulnerability was found in bluez. An attacker with local network access 
  could pass specially crafted files causing an application to halt or crash, leading to 
  a denial of service.

CVE-2022-39176

  BlueZ allows physically proximate attackers to obtain sensitive information because 
  profiles/audio/avrcp.c does not validate params_len.

CVE-2022-39177

  BlueZ allows physically proximate attackers to cause a denial of service because 
  malformed and invalid capabilities can be processed in profiles/audio/avdtp.c.
