---
title: "ELA-128-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u23
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2019-06-03T13:37:26+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-11039
  - CVE-2019-11040

---

Two vulnerabilities were found in PHP, a widely-used open source general
purpose scripting language.

CVE-2019-11039

    An integer underflow in the iconv module could be exploited to trigger
    an out of bounds read.

CVE-2019-11040

    A heap buffer overflow was discovered in the EXIF parsing code.
