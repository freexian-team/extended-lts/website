---
title: "ELA-202-2 gdk-pixbuf regression update"
package: gdk-pixbuf
version: 2.26.1-1+deb7u10
distribution: "Debian 7 Wheezy"
description: "fix for unknown symbol"
date: 2019-12-20T12:59:37+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-6314

---

While preparing a fix for CVE-2017-6314 an unknown symbol g_uint_checked_mul() was introduced.

