---
title: "ELA-817-1 tzdata new timezone database"
package: tzdata
version: 2021a-0+deb8u9 (jessie), 2021a-0+deb9u9 (stretch)
version_map: {"8 jessie": "2021a-0+deb8u9", "9 stretch": "2021a-0+deb9u9"}
description: "updated timezone database"
date: 2023-03-24T13:26:39+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update includes the changes in tzdata 2023b. Notable
changes are:

- Egypt uses DST again, starting on April.
- Palestine and Lebanon delay the start of DST this year.
- Morocco DST will happen a week earlier on April 23.
- Adjustments to Greenland's timezones and DST rules.
