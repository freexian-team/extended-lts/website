---
title: "ELA-766-1 exiv2 security update"
package: exiv2
version: 0.25-3.1+deb9u4 (stretch)
version_map: {"9 stretch": "0.25-3.1+deb9u4"}
description: "several issues with untrusted input"
date: 2023-01-11T10:35:10+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-11591
  - CVE-2017-14859
  - CVE-2017-14862
  - CVE-2017-14864
  - CVE-2017-17669
  - CVE-2017-18005
  - CVE-2018-8976
  - CVE-2018-17581
  - CVE-2018-19107
  - CVE-2018-19108
  - CVE-2018-19535
  - CVE-2018-20097
  - CVE-2019-13110
  - CVE-2019-13112
  - CVE-2019-13114
  - CVE-2019-13504
  - CVE-2019-14369
  - CVE-2019-14370
  - CVE-2019-17402
  - CVE-2020-18771
  - CVE-2021-29458
  - CVE-2021-32815
  - CVE-2021-34334
  - CVE-2021-37620
  - CVE-2021-37621
  - CVE-2021-37622

---

This update fixes a number of memory access violations and other input
validation failures that can be triggered by passing specially crafted files to
exiv2.                                                                                                                                                                                  

CVE-2017-11591

    There is a Floating point exception in the Exiv2::ValueType function
    in Exiv2 0.26 that will lead to a remote denial of service attack via
    crafted input.

CVE-2017-14859

    An Invalid memory address dereference was discovered in
    Exiv2::StringValueBase::read in value.cpp in Exiv2 0.26. The
    vulnerability causes a segmentation fault and application crash, which
    leads to denial of service.

CVE-2017-14862

    An Invalid memory address dereference was discovered in
    Exiv2::DataValue::read in value.cpp in Exiv2 0.26. The vulnerability
    causes a segmentation fault and application crash, which leads to
    denial of service.

CVE-2017-14864

    An Invalid memory address dereference was discovered in
    Exiv2::getULong in types.cpp in Exiv2 0.26. The vulnerability causes a
    segmentation fault and application crash, which leads to denial of
    service.

CVE-2017-17669

    There is a heap-based buffer over-read in the
    Exiv2::Internal::PngChunk::keyTXTChunk function of pngchunk_int.cpp in
    Exiv2 0.26. A crafted PNG file will lead to a remote denial of service
    attack.

CVE-2017-18005

    Exiv2 0.26 has a Null Pointer Dereference in the
    Exiv2::DataValue::toLong function in value.cpp, related to crafted
    metadata in a TIFF file.

CVE-2018-17581

    CiffDirectory::readDirectory() at crwimage_int.cpp in Exiv2 0.26 has
    excessive stack consumption due to a recursive function, leading to
    Denial of service.

CVE-2018-19107

    In Exiv2 0.26, Exiv2::IptcParser::decode in iptc.cpp (called from
    psdimage.cpp in the PSD image reader) may suffer from a denial of
    service (heap-based buffer over-read) caused by an integer overflow
    via a crafted PSD image file.

CVE-2018-19108

    In Exiv2 0.26, Exiv2::PsdImage::readMetadata in psdimage.cpp in the
    PSD image reader may suffer from a denial of service (infinite loop)
    caused by an integer overflow via a crafted PSD image file.

CVE-2018-19535

    In Exiv2 0.26 and previous versions, PngChunk::readRawProfile in
    pngchunk_int.cpp may cause a denial of service (application crash due
    to a heap-based buffer over-read) via a crafted PNG file.

CVE-2018-20097

    There is a SEGV in
    Exiv2::Internal::TiffParserWorker::findPrimaryGroups of
    tiffimage_int.cpp in Exiv2 0.27-RC3. A crafted input will lead to a
    remote denial of service attack.

CVE-2018-8976

    In Exiv2 0.26, jpgimage.cpp allows remote attackers to cause a denial
    of service (image.cpp Exiv2::Internal::stringFormat out-of-bounds
    read) via a crafted file.

CVE-2019-13110

    A CiffDirectory::readDirectory integer overflow and out-of-bounds read
    in Exiv2 through 0.27.1 allows an attacker to cause a denial of
    service (SIGSEGV) via a crafted CRW image file.

CVE-2019-13112

    A PngChunk::parseChunkContent uncontrolled memory allocation in Exiv2
    through 0.27.1 allows an attacker to cause a denial of service (crash
    due to an std::bad_alloc exception) via a crafted PNG image file.

CVE-2019-13114

    http.c in Exiv2 through 0.27.1 allows a malicious http server to cause
    a denial of service (crash due to a NULL pointer dereference) by
    returning a crafted response that lacks a space character.

CVE-2019-13504

    There is an out-of-bounds read in Exiv2::MrwImage::readMetadata in
    mrwimage.cpp in Exiv2 through 0.27.2.

CVE-2019-14369

    Exiv2::PngImage::readMetadata() in pngimage.cpp in Exiv2 0.27.99.0
    allows attackers to cause a denial of service (heap-based buffer over-
    read) via a crafted image file.

CVE-2019-14370

    In Exiv2 0.27.99.0, there is an out-of-bounds read in
    Exiv2::MrwImage::readMetadata() in mrwimage.cpp. It could result in
    denial of service.

CVE-2019-17402

    Exiv2 0.27.2 allows attackers to trigger a crash in Exiv2::getULong in
    types.cpp when called from
    Exiv2::Internal::CiffDirectory::readDirectory in crwimage_int.cpp,
    because there is no validation of the relationship of the total size
    to the offset and size.

CVE-2020-18771

    Exiv2 0.27.99.0 has a global buffer over-read in
    Exiv2::Internal::Nikon1MakerNote::print0x0088 in nikonmn_int.cpp which
    can result in an information leak.

CVE-2021-29458

    Exiv2 is a command-line utility and C++ library for reading, writing,
    deleting, and modifying the metadata of image files. An out-of-bounds
    read was found in Exiv2 versions v0.27.3 and earlier. The out-of-
    bounds read is triggered when Exiv2 is used to write metadata into a
    crafted image file. An attacker could potentially exploit the
    vulnerability to cause a denial of service by crashing Exiv2, if they
    can trick the victim into running Exiv2 on a crafted image file. Note
    that this bug is only triggered when writing the metadata, which is a
    less frequently used Exiv2 operation than reading the metadata. For
    example, to trigger the bug in the Exiv2 command-line application, you
    need to add an extra command-line argument such as insert. The bug is
    fixed in version v0.27.4.

CVE-2021-32815

    Exiv2 is a command-line utility and C++ library for reading, writing,
    deleting, and modifying the metadata of image files. The assertion
    failure is triggered when Exiv2 is used to modify the metadata of a
    crafted image file. An attacker could potentially exploit the
    vulnerability to cause a denial of service, if they can trick the
    victim into running Exiv2 on a crafted image file. Note that this bug
    is only triggered when modifying the metadata, which is a less
    frequently used Exiv2 operation than reading the metadata. For
    example, to trigger the bug in the Exiv2 command-line application, you
    need to add an extra command-line argument such as `fi`. ### Patches
    The bug is fixed in version v0.27.5. ### References Regression test
    and bug fix: #1739 ### For more information Please see our [security
    policy](https://github.com/Exiv2/exiv2/security/policy) for
    information about Exiv2 security.

CVE-2021-34334

    Exiv2 is a command-line utility and C++ library for reading, writing,
    deleting, and modifying the metadata of image files. An infinite loop
    is triggered when Exiv2 is used to read the metadata of a crafted
    image file. An attacker could potentially exploit the vulnerability to
    cause a denial of service, if they can trick the victim into running
    Exiv2 on a crafted image file. The bug is fixed in version v0.27.5.

CVE-2021-37620

    Exiv2 is a command-line utility and C++ library for reading, writing,
    deleting, and modifying the metadata of image files. An out-of-bounds
    read was found in Exiv2 versions v0.27.4 and earlier. The out-of-
    bounds read is triggered when Exiv2 is used to read the metadata of a
    crafted image file. An attacker could potentially exploit the
    vulnerability to cause a denial of service, if they can trick the
    victim into running Exiv2 on a crafted image file. The bug is fixed in
    version v0.27.5.

CVE-2021-37621

    Exiv2 is a command-line utility and C++ library for reading, writing,
    deleting, and modifying the metadata of image files. An infinite loop
    was found in Exiv2 versions v0.27.4 and earlier. The infinite loop is
    triggered when Exiv2 is used to print the metadata of a crafted image
    file. An attacker could potentially exploit the vulnerability to cause
    a denial of service, if they can trick the victim into running Exiv2
    on a crafted image file. Note that this bug is only triggered when
    printing the image ICC profile, which is a less frequently used Exiv2
    operation that requires an extra command line option (`-p C`). The bug
    is fixed in version v0.27.5.

CVE-2021-37622

    Exiv2 is a command-line utility and C++ library for reading, writing,
    deleting, and modifying the metadata of image files. An infinite loop
    was found in Exiv2 versions v0.27.4 and earlier. The infinite loop is
    triggered when Exiv2 is used to modify the metadata of a crafted image
    file. An attacker could potentially exploit the vulnerability to cause
    a denial of service, if they can trick the victim into running Exiv2
    on a crafted image file. Note that this bug is only triggered when
    deleting the IPTC data, which is a less frequently used Exiv2
    operation that requires an extra command line option (`-d I rm`). The
    bug is fixed in version v0.27.5.
