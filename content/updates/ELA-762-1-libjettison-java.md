---
title: "ELA-762-1 libjettison-java security update"
package: libjettison-java
version: 1.5.3-1~deb9u1 (stretch)
version_map: {"9 stretch": "1.5.3-1~deb9u1"}
description: "denial of service"
date: 2022-12-31T18:29:14+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-40150
  - CVE-2022-45685
  - CVE-2022-45693

---

Several flaws have been discovered in libjettison-java, a
collection of StAX parsers and writers for JSON. Specially crafted user input
may cause a denial of service via out-of-memory or stack overflow errors.

In addition a build failure related to the update was fixed in jersey1.

