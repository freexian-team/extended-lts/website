---
title: "ELA-786-1 tiff security update"
package: tiff
version: 4.0.3-12.3+deb8u14 (jessie), 4.0.8-2+deb9u9 (stretch)
version_map: {"8 jessie": "4.0.3-12.3+deb8u14", "9 stretch": "4.0.8-2+deb9u9"}
description: "denial of service"
date: 2023-01-30T23:42:38+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-0865
  - CVE-2022-0891
  - CVE-2022-0907
  - CVE-2022-0908
  - CVE-2022-0909
  - CVE-2022-0924
  - CVE-2022-1355
  - CVE-2022-2056
  - CVE-2022-2057
  - CVE-2022-2058
  - CVE-2022-2867
  - CVE-2022-2868
  - CVE-2022-2869
  - CVE-2022-3570
  - CVE-2022-3597
  - CVE-2022-3598
  - CVE-2022-3599
  - CVE-2022-3626
  - CVE-2022-3627
  - CVE-2022-3970
  - CVE-2022-34526
  - CVE-2022-48281

---

Multiple vulnerabilities were found in tiff, a library and tools providing
support for the Tag Image File Format (TIFF), leading to denial of service
(DoS) and possibly local code execution.
