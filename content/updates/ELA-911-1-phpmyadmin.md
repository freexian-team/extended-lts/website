---
title: "ELA-911-1 phpmyadmin security update"
package: phpmyadmin
version: 4:4.6.6-4+deb9u3 (stretch)
version_map: {"9 stretch": "4:4.6.6-4+deb9u3"}
description: "sql injection"
date: 2023-08-02T00:11:05+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-22452
  - CVE-2023-25727

---

phpMyAdmin is a popular MySQL web administration tool. The following security
vulnerabilities have been addressed:

CVE-2020-22452

    SQL Injection vulnerability in function getTableCreationQuery in
    CreateAddField.php in phpMyAdmin via the tbl_storage_engine or
    tbl_collation parameters to tbl_create.php.

CVE-2023-25727

    In phpMyAdmin an authenticated user can trigger XSS by uploading a crafted
    .sql file through the drag-and-drop interface.

