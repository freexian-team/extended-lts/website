---
title: "ELA-1102-1 gst-plugins-base1.0 security update"
package: gst-plugins-base1.0
version: 1.4.4-2+deb8u5 (jessie), 1.10.4-1+deb9u4 (stretch)
version_map: {"8 jessie": "1.4.4-2+deb8u5", "9 stretch": "1.10.4-1+deb9u4"}
description: "integer overflow"
date: 2024-05-30T23:57:46+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-4453

---

An integer overflow in the EXIF metadata parser has been fixed in the
GStreamer media framework.
