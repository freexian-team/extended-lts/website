---
title: "ELA-630-1 maven-shared-utils security update"
package: maven-shared-utils
version: 0.4-1+deb8u1 (jessie)
version_map: {"8 jessie": "0.4-1+deb8u1"}
description: "shell injection attack"
date: 2022-06-27T13:08:44+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-29599

---

It was discovered that the Commandline class in maven-shared-utils, a
collection of various utility classes for the Maven build system, can emit
double-quoted strings without proper escaping, allowing shell injection
attacks.
