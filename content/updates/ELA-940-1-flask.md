---
title: "ELA-940-1 flask security update"
package: flask
version: 0.12.1-1+deb9u1 (stretch)
version_map: {"9 stretch": "0.12.1-1+deb9u1"}
description: "denial-of-service"
date: 2023-08-29T21:07:55+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-1000656
  - CVE-2019-1010083

---

Flask, a micro web framework for the Python programming language,
contains a improper input validation vulnerability (CWE-20) that can
result in large amount of memory usage, possibly leading to denial of
service. This attack appears to be exploitable through a crafted JSON
data in an incorrect encoding.
NOTE: this may overlap CVE-2019-1010083.
