---
title: "ELA-976-1 exim4 security update"
package: exim4
version: 4.84.2-2+deb8u11 (jessie), 4.89-2+deb9u11 (stretch)
version_map: {"8 jessie": "4.84.2-2+deb8u11", "9 stretch": "4.89-2+deb9u11"}
description: "remote code execution"
date: 2023-10-03T01:08:01+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-42114
  - CVE-2023-42116

---

Several vulnerabilities were discovered in Exim, a mail transport agent,
which could result in remote code execution if the SPA/NTLM authenticators
are used.

