---
title: "ELA-184-1 libarchive security update"
package: libarchive
version: 3.0.4-3+wheezy6+deb7u4
distribution: "Debian 7 Wheezy"
description: "use after free"
date: 2019-10-27T12:32:48+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-18408

---

An issue has been found in libarchive, a multi-format archive and compression library.

In case of a crafted archive containing several parts and one part being corrupt, there would be an use-after-free for the next part of the archive.

