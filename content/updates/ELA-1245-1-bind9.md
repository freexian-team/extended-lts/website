---
title: "ELA-1245-1 bind9 security update"
package: bind9
version: 1:9.10.3.dfsg.P4-12.3+deb9u17 (stretch)
version_map: {"9 stretch": "1:9.10.3.dfsg.P4-12.3+deb9u17"}
description: "denial of service"
date: 2024-11-27T11:56:02+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-1737
  - CVE-2024-1975

---

Several vulnerabilities were discovered in BIND, a DNS server
implementation, which may result in denial of service.
