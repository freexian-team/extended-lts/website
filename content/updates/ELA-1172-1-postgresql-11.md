---
title: "ELA-1172-1 postgresql-11 security update"
package: postgresql-11
version: 11.22-0+deb10u3 (buster)
version_map: {"10 buster": "11.22-0+deb10u3"}
description: "privilege escalation"
date: 2024-09-04T15:42:00-04:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-7348

---

Noah Misch discovered a race condition in the pg\_dump tool included in
PostgreSQL, which may result in privilege escalation.
