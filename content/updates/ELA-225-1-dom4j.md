---
title: "ELA-225-1 dom4j security update"
package: dom4j
version: 1.6.1+dfsg.3-2+deb7u2
distribution: "Debian 7 Wheezy"
description: "XML external entity vulnerability"
date: 2020-04-23T16:29:48+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-10683
---

An XML external external entity vulnerability was discovered in dom4j, a
library for working with XML, XPath and XSLT formats on the Java platform.

This type of attack occurs when XML input containing a reference to an external
entity is processed by a weakly configured XML parser. This attack may lead to
the disclosure of confidential data, denial of service, server side request
forgery, port scanning from the perspective of the machine where the parser is
located as well as other system impacts.
