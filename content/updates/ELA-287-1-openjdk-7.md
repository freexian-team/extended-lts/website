---
title: "ELA-287-1 openjdk-7 security update"
package: openjdk-7
version: 7u271-2.6.23-1~deb8u1
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2020-09-30T09:22:36+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-14577
  - CVE-2020-14578
  - CVE-2020-14579
  - CVE-2020-14581
  - CVE-2020-14583
  - CVE-2020-14593
  - CVE-2020-14621

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
resulting in denial of service, bypass of access/sandbox restrictions or
information disclosure.
