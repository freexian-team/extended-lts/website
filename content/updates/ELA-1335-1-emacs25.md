---
title: "ELA-1335-1 emacs25 security update"
package: emacs25
version: 25.1+1-4+deb9u6 (stretch)
version_map: {"9 stretch": "25.1+1-4+deb9u6"}
description: "multiple vulnerabilities"
date: 2025-02-28T17:03:41+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-53920
  - CVE-2025-1244

---

Multiple problems were discovered in GNU Emacs, the extensible, customisable,
self-documenting real-time display editor.

### CVE-2024-53920

Several ways to trigger arbitrary code execution were discovered in Emacs's
support for editing files in its own dialect of Lisp. These include arbitrary
code execution upon opening an otherwise innocent-looking file, with any (or
no) file extension, for editing.

### CVE-2025-1244

Improper handling of custom 'man' URI schemes could allow an attacker to
execute arbitrary shell commands by tricking users into visiting a specially
crafted website, or an HTTP URL with a redirect.
