---
title: "ELA-1313-1 ffmpeg security update"
package: ffmpeg
version: 7:4.1.11-0+deb10u3 (buster)
version_map: {"10 buster": "7:4.1.11-0+deb10u3"}
description: "integer overflow, double-free, out-of-bounds access, incomplete checks"
date: 2025-02-01T00:28:05+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-35366
  - CVE-2024-35367
  - CVE-2024-35368
  - CVE-2024-36616
  - CVE-2024-36617
  - CVE-2024-36618

---

Several issues have been found in ffmpeg, a package that contains tools
for transcoding, streaming and playing of multimedia files
Those issues are related to possible integer overflows, double-free on
errors, out-of-bounds access, seeks beyond 64bit and an incomplete
check of negative durations.



