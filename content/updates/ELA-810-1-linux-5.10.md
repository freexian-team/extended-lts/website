---
title: "ELA-810-1 linux-5.10 security update"
package: linux-5.10
version: 5.10.162-1~deb9u1 (stretch)
version_map: {"9 stretch": "5.10.162-1~deb9u1"}
description: "linux kernel update"
date: 2023-03-03T09:05:41+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-2873
  - CVE-2022-3545
  - CVE-2022-3623
  - CVE-2022-4696
  - CVE-2022-36280
  - CVE-2022-41218
  - CVE-2022-45934
  - CVE-2022-47929
  - CVE-2023-0179
  - CVE-2023-0240
  - CVE-2023-0266
  - CVE-2023-0394
  - CVE-2023-23454
  - CVE-2023-23455
  - CVE-2023-23586

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2022-2873

    Zheyu Ma discovered that an out-of-bounds memory access flaw in
    the Intel iSMT SMBus 2.0 host controller driver may result in
    denial of service (system crash).

CVE-2022-3545

    It was discovered that the Netronome Flow Processor (NFP) driver
    contained a use-after-free flaw in area_cache_get(), which may
    result in denial of service or the execution of arbitrary code.

CVE-2022-3623

    A race condition when looking up a CONT-PTE/PMD size hugetlb page
    may result in denial of service or an information leak.

CVE-2022-4696

    A use-after-free vulnerability was discovered in the io_uring
    subsystem.

CVE-2022-36280

    An out-of-bounds memory write vulnerability was discovered in the
    vmwgfx driver, which may allow a local unprivileged user to cause
    a denial of service (system crash).

CVE-2022-41218

    Hyunwoo Kim reported a use-after-free flaw in the Media DVB core
    subsystem caused by refcount races, which may allow a local user
    to cause a denial of service or escalate privileges.

CVE-2022-45934

    An integer overflow in l2cap_config_req() in the Bluetooth
    subsystem was discovered, which may allow a physically proximate
    attacker to cause a denial of service (system crash).

CVE-2022-47929

    Frederick Lawler reported a NULL pointer dereference in the
    traffic control subsystem allowing an unprivileged user to cause a
    denial of service by setting up a specially crafted traffic
    control configuration.

CVE-2023-0179

    Davide Ornaghi discovered incorrect arithmetics when fetching VLAN
    header bits in the netfilter subsystem, allowing a local user to
    leak stack and heap addresses or potentially local privilege
    escalation to root.

CVE-2023-0240

    A flaw was discovered in the io_uring subsystem that could lead
    to a use-after-free.  A local user could exploit this to cause
    a denial of service (crash or memory corruption) or possibly for
    privilege escalation.

CVE-2023-0266

    A use-after-free flaw in the sound subsystem due to missing
    locking may result in denial of service or privilege escalation.

CVE-2023-0394

    Kyle Zeng discovered a NULL pointer dereference flaw in
    rawv6_push_pending_frames() in the network subsystem allowing a
    local user to cause a denial of service (system crash).

CVE-2023-23454

    Kyle Zeng reported that the Class Based Queueing (CBQ) network
    scheduler was prone to denial of service due to interpreting
    classification results before checking the classification return
    code.

CVE-2023-23455

    Kyle Zeng reported that the ATM Virtual Circuits (ATM) network
    scheduler was prone to a denial of service due to interpreting
    classification results before checking the classification return
    code.

CVE-2023-23586

    A flaw was discovered in the io_uring subsystem that could lead to
    an information leak.  A local user could exploit this to obtain
    sensitive information from the kernel or other users.

This update also fixes Debian bugs #825141, #1008501, #1027430, and
#1027483, and includes many more bug fixes from stable updates
5.10.159-5.10.162 inclusive.
