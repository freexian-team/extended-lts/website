---
title: "ELA-841-1 distro-info-data database update"
package: distro-info-data
version: 0.36~bpo8+3 (jessie), 0.41+deb10u2~bpo9+3 (stretch)
version_map: {"8 jessie": "0.36~bpo8+3", "9 stretch": "0.41+deb10u2~bpo9+3"}
description: "non-security-related database update"
date: 2023-04-30T21:57:18-04:00
draft: false
type: updates
tags:
- update
cvelist:

---

This is a routine update of the distro-info-data database for Debian ELTS
users.

It includes the expected release date for Debian 12, adds Debian 14, adds
Ubuntu 23.10, and some minor updates to EoL dates for Ubuntu releases.
