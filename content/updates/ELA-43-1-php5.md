---
title: "ELA-43-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u16
distribution: "Debian 7 Wheezy"
description: "XSS in Apache2 component"
date: 2018-09-20T02:54:01Z
draft: false
type: updates
cvelist:
  - CVE-2018-17082
---

A vulnerability has been discovered in php5, a server-side,
HTML-embedded scripting language.  The Apache2 component allows XSS via
the body of a "Transfer-Encoding: chunked" request because of a defect
in request handling.
