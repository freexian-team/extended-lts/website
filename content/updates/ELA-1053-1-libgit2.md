---
title: "ELA-1053-1 libgit2 security update"
package: libgit2
version: 0.25.1+really0.24.6-1+deb9u3 (stretch)
version_map: {"9 stretch": "0.25.1+really0.24.6-1+deb9u3"}
description: "arbitrary code execution"
date: 2024-03-03T16:39:42+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-24577

---

Arbitrary code execution in git_index_add has been fixed in libgit2,
a library implementing the Git core methods.
