---
title: "ELA-1133-2 imagemagick regression update"
package: imagemagick
version: 8:6.9.10.23+dfsg-2.1+deb10u9 (buster)
version_map: {"10 buster": "8:6.9.10.23+dfsg-2.1+deb10u9"}
description: "regression update"
date: 2024-07-23T23:02:21Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-34151

---

The Imagemagick security update issued as ELA 1133-1 addressed the
vulnerability identified by CVE-2023-34151. The fix for that CVE introduced a
regression.

A Magick Vector Graphics file including a pattern operator could return an
incorrect bounding box, and thus generate a corrupted pattern.
