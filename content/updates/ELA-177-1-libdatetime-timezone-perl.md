---
title: "ELA-177-1 libdatetime-timezone-perl new upstream version"
package: libdatetime-timezone-perl
version: 1:1.58-1+2019c
distribution: "Debian 7 Wheezy"
description: "new version update"
date: 2019-10-14T13:31:02+02:00
draft: false
type: updates
cvelist:

---

This update brings the Olson database changes from the 2019c version to
the Perl bindings.
