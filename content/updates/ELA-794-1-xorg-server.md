---
title: "ELA-794-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u10 (jessie), 2:1.19.2-1+deb9u13 (stretch)
version_map: {"8 jessie": "2:1.16.4-1+deb8u10", "9 stretch": "2:1.19.2-1+deb9u13"}
description: "use after free"
date: 2023-02-07T11:23:05+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-0494

---

Jan-Niklas Sohn, working with Trend Micro Zero Day Initiative, discovered
a vulnerability in the X.Org X server.
A potential use after free mighty result in local privilege escalation if
the X server is running privileged or remote code execution during ssh X
forwarding sessions.
