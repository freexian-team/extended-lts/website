---
title: "ELA-874-1 glibc security update"
package: glibc
version: 2.19-18+deb8u12 (jessie)
version_map: {"8 jessie": "2.19-18+deb8u12"}
description: "denial of service"
date: 2023-06-20T14:21:57+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2015-20109

---

This update fixes a denial of service condition in fnmatch. This is a variant
of CVE-2015-8984, which has been associated with
[BZ#18032](https://sourceware.org/bugzilla/show_bug.cgi?id=18032). This variant
is reported as
[BZ#18036](https://sourceware.org/bugzilla/show_bug.cgi?id=18036), but has not
been fixed together with the original problem.
