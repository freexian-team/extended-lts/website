---
title: "ELA-1242-1 intel-microcode security update"
package: intel-microcode
version: 3.20240910.1~deb8u1 (jessie), 3.20240910.1~deb9u1 (stretch), 3.20240910.1~deb10u1 (buster)
version_map: {"8 jessie": "3.20240910.1~deb8u1", "9 stretch": "3.20240910.1~deb9u1", "10 buster": "3.20240910.1~deb10u1"}
description: "microcode update"
date: 2024-11-24T15:45:15+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-23984
  - CVE-2024-24968

---

A microcode update has been released for Intel processors, addressing
multiple vulnerabilties which potentially could cause information
disclosue or local DoS.

CVE-2024-23984

    Observable discrepancy in RAPL interface for some Intel(R)
    Processors may allow a privileged user to potentially enable
    information disclosure via local access.

CVE-2024-24968

    Improper finite state machines (FSMs) in hardware logic in some
    Intel(R) Processors may allow an privileged user to potentially
    enable a denial of service via local access.

