---
title: "ELA-1052-1 wireshark security update"
package: wireshark
version: 2.6.20-0+deb9u7 (stretch)
version_map: {"9 stretch": "2.6.20-0+deb9u7"}
description: "multiple vulnerabilities"
date: 2024-02-29T23:56:36Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-4511
  - CVE-2023-4513
  - CVE-2023-6175
  - CVE-2024-0208

---

Multiple vulnerabilities have been fixed in the network traffic analyzer Wireshark.

CVE-2023-4511

    BT SDP dissector infinite loop

CVE-2023-4513

    BT SDP dissector memory leak

CVE-2023-6175

    NetScreen file parser crash

CVE-2024-0208

    GVCP dissector crash

