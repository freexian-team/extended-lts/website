---
title: "ELA-1135-1 python3.7 security update"
package: python3.7
version: 3.7.3-2+deb10u8 (buster)
version_map: {"10 buster": "3.7.3-2+deb10u8"}
description: "multiple vulnerabilities"
date: 2024-07-22T17:53:36+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-0397
  - CVE-2024-4032

---

Multiple vulnerabilities have been fixed in the Python3 interpreter.

CVE-2024-0397

    Race condition in ssl.SSLContext


CVE-2024-4032

    Incorrect information about private addresses in the ipaddress module

