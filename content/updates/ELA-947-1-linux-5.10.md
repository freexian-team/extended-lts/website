---
title: "ELA-947-1 linux-5.10 security update"
package: linux-5.10
version: 5.10.179-5~deb9u1 (stretch)
version_map: {"9 stretch": "5.10.179-5~deb9u1"}
description: "linux kernel update"
date: 2023-09-20T10:12:32+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-40982
  - CVE-2023-20569

---

CVE-2022-40982

    Daniel Moghimi discovered Gather Data Sampling (GDS), a hardware
    vulnerability for Intel CPUs which allows unprivileged speculative
    access to data which was previously stored in vector registers.

    This mitigation requires updated CPU microcode provided in the
    intel-microcode package.

    For details please refer to <https://downfall.page/> and
    <https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html>.

CVE-2023-20569

    Daniel Trujillo, Johannes Wikner and Kaveh Razavi discovered
    INCEPTION, also known as Speculative Return Stack Overflow (SRSO),
    a transient execution attack that leaks arbitrary data on all AMD
    Zen CPUs. An attacker can mis-train the CPU BTB to predict non-
    architectural CALL instructions in kernel space and use this to
    control the speculative target of a subsequent kernel RET,
    potentially leading to information disclosure via a speculative
    side-channel.

    For details please refer to
    <https://comsec.ethz.ch/research/microarch/inception/> and
    <https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-7005>.
