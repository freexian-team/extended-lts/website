---
title: "ELA-1164-1 python-django security update"
package: python-django
version: 1:1.11.29-1+deb10u12 (buster)
version_map: {"10 buster": "1:1.11.29-1+deb10u12"}
description: "multiple vulnerabilities"
date: 2024-08-27T15:41:47+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-41989
  - CVE-2024-41991
  - CVE-2024-42005
---

*(Release for `buster` only)*

A number of vulnerabilities were discovered in Django, a popular Python-based web development framework:

* CVE-2024-41989: The `floatformat` template filter was subject to significant memory consumption when given a string representation of a number in scientific notation with a large exponent.

* CVE-2024-41991: Fix an issue where the `urlize` and `urlizetrunc` template filters (as well as the `AdminURLFieldWidget` widget) were subject to a potential denial-of-service attack via certain inputs with a very large number of Unicode characters.

* CVE-2024-42005: Fix an issue where the `QuerySet.values()` and `values_list()` methods on models with a `JSONFields` were subject to a SQL injection attack through column aliases via a crafted JSON object key.
