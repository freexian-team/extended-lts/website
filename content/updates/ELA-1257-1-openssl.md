---
title: "ELA-1257-1 openssl security update"
package: openssl
version: 1.1.0l-1~deb9u10 (stretch)
version_map: {"9 stretch": "1.1.0l-1~deb9u10"}
description: "multiple vulnerabilities"
date: 2024-11-30T19:14:29+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-5678
  - CVE-2024-0727
  - CVE-2024-2511
  - CVE-2024-9143

---

Multiple vulnerabilities were discovered in OpenSSL, the Secure Sockets Layer
toolkit.

### CVE-2023-5678

A denial of service could occur with excessively long X9.42 DH keys.

### CVE-2024-0727

A denial of service could occur with a null field in a PKCS12 file.

### CVE-2024-2511

A denial of service could occur when the `SSL_OP_NO_TICKET` flag is set, with
TLSv1.3.

### CVE-2024-9143

Use of the low-level `GF(2^m)` elliptic curve APIs with untrusted explicit
values for the field polynomial can lead to out-of-bounds memory reads or
writes.  This could lead to information disclosure or possibly remote code
execution.
