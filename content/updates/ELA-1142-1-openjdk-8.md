---
title: "ELA-1142-1 openjdk-8 security update"
package: openjdk-8
version: 8u422-b05-1~deb8u1 (jessie), 8u422-b05-1~deb9u1 (stretch)
version_map: {"8 jessie": "8u422-b05-1~deb8u1", "9 stretch": "8u422-b05-1~deb9u1"}
description: "multiple vulnerabilities"
date: 2024-07-30T10:55:20+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-21131
  - CVE-2024-21138
  - CVE-2024-21140
  - CVE-2024-21144
  - CVE-2024-21145
  - CVE-2024-21147

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
which may result in bypass of sandbox restrictions, information
disclosure or denial of service.
