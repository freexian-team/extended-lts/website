---
title: "ELA-712-1 tzdata new timezone database"
package: tzdata
version: 2021a-0+deb8u7 (jessie), 2021a-0+deb9u7 (stretch)
version_map: {"8 jessie": "2021a-0+deb8u7", "9 stretch": "2021a-0+deb9u7"}
description: "updated timezone database"
date: 2022-10-26T19:46:10+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update includes the changes in tzdata 2022e. Notable
changes are:

- Syria and Jordan are abandoning the DST regime and are changing to
  permanent +03, so they will not fall back from +03 to +02 on
  2022-10-28.

In addition, the jessie version is building tzdata-java again, to make
OpenJDK 7 installable again. Note that that version is unsupported
security-wise though.
