---
title: "ELA-1234-1 apache2 security update"
package: apache2
version: 2.4.59-1~deb10u4 (buster)
version_map: {"10 buster": "2.4.59-1~deb10u4"}
description: "authentication bypass"
date: 2024-11-15T08:36:15Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-38473

---

A vulnerability was found in apache2, a popular web server.

An encoding problem in mod\_proxy allowed request URLs with incorrect encoding to be sent
to backend services, potentially bypassing authentication via crafted requests.

This affects configurations where mechanisms other than ProxyPass/ProxyPassMatch
or RewriteRule with the 'P' flag are used to configure a request to be proxied,
such as SetHandler or inadvertent proxying via CVE-2024-39573.

Note that these alternate mechanisms may be used within .htaccess.
