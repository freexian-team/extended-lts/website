---
title: "ELA-1001-1 postgresql-9.6 security update"
package: postgresql-9.6
version: 9.6.24-0+deb9u5 (stretch)
version_map: {"9 stretch": "9.6.24-0+deb9u5"}
description: "multiple security vulnerabilities"
date: 2023-11-16T00:38:29+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-5868
  - CVE-2023-5869
  - CVE-2023-5870
  - CVE-2023-39417

---

Several security vulnerabilities have been found in PostgreSQL, an advanced
open source database.

CVE-2023-5868

    Certain aggregate function calls receiving "unknown"-type arguments could
    disclose bytes of server memory from the end of the "unknown"-type value to
    the next zero byte. One typically gets an "unknown"-type value via a string
    literal having no type designation.

CVE-2023-5869

    While modifying certain SQL array values, missing overflow checks let
    authenticated database users write arbitrary bytes to a memory area that
    facilitates arbitrary code execution. Missing overflow checks also let
    authenticated database users read a wide area of server memory. The
    CVE-2021-32027 fix covered some attacks of this description, but it missed
    others.

CVE-2023-5870

    Documentation says the pg_signal_backend role cannot signal "a backend
    owned by a superuser". On the contrary, it can signal background workers,
    including the logical replication launcher. It can signal autovacuum
    workers and the autovacuum launcher. Signaling autovacuum workers and those
    two launchers provides no meaningful exploit, so exploiting this
    vulnerability requires a non-core extension with a less-resilient
    background worker. For example, a non-core background worker that does not
    auto-restart would experience a denial of service with respect to that
    particular background worker.

CVE-2023-39417

    In the EXTENSION SCRIPT, a SQL Injection vulnerability was found in
    PostgreSQL if it uses @extowner@, @extschema@, or @extschema:...@ inside a
    quoting construct (dollar quoting, '', or ""). If an administrator has
    installed files of a vulnerable, trusted, non-bundled extension, an
    attacker with database-level CREATE privilege can execute arbitrary code as
    the bootstrap superuser.
