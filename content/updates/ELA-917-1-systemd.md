---
title: "ELA-917-1 systemd security update"
package: systemd
version: 215-17+deb8u15 (jessie)
version_map: {"8 jessie": "215-17+deb8u15"}
description: "local privilege escalation"
date: 2023-08-10T00:24:34+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-3821
  - CVE-2023-26604

---

Systemd is a system and service manager. The following security vulnerabilities
have been fixed.

CVE-2023-26604

    systemd does not adequately block local privilege escalation for
    some Sudo configurations, e.g., plausible sudoers files in which the
    "systemctl status" command may be executed. Specifically, systemd does not
    set LESSSECURE to 1, and thus other programs may be launched from the less
    program. This presents a substantial security risk when running systemctl
    from Sudo, because less executes as root when the terminal size is too
    small to show the complete systemctl output.

    This update introduces a new systemd environment variable called
    $SYSTEMD_PAGERSECURE. By default it is set to true which means LESSSECURE
    is set to 1. However only the less pager implements such a security
    feature and thus will be used whenever $SYSTEMD_PAGERSECURE is true. You
    can disable this feature by setting $SYSTEMD_PAGERSECURE to false.

    As a general precaution we recommend to carefully review an existing
    sudoers file and reassess if certain privileges are still required for
    normal users.

CVE-2022-3821

    An off-by-one error issue was discovered in Systemd in format_timespan()
    function of time-util.c. An attacker could supply specific values for time
    and accuracy that leads to buffer overrun in format_timespan(), leading to
    a Denial of Service.

