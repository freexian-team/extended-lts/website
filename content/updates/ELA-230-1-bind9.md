---
title: "ELA-230-1 bind9 security update"
package: bind9
version: 1:9.8.4.dfsg.P1-6+nmu2+deb7u24
distribution: "Debian 7 Wheezy"
description: "fix for denial of service due to bad checks"
date: 2020-05-30T22:39:36+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-8616
  - CVE-2020-8617

---

Several vulnerabilities were discovered in BIND, a DNS server implementation.

CVE-2020-8616

    It was discovered that BIND does not sufficiently limit the number
    of fetches performed when processing referrals. An attacker can take
    advantage of this flaw to cause a denial of service (performance
    degradation) or use the recursing server in a reflection attack with
    a high amplification factor.

CVE-2020-8617

    It was discovered that a logic error in the code which checks TSIG
    validity can be used to trigger an assertion failure, resulting in
    denial of service.


