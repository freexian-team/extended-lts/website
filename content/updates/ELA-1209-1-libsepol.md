---
title: "ELA-1209-1 libsepol security update"
package: libsepol
version: 2.6-2+deb9u1 (stretch), 2.8-1+deb10u1 (buster)
version_map: {"9 stretch": "2.6-2+deb9u1", "10 buster": "2.8-1+deb10u1"}
description: "multiple vulnerabilities"
date: 2024-10-22T14:01:17+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-36084
  - CVE-2021-36085
  - CVE-2021-36086
  - CVE-2021-36087

---

Multiple vulnerabilities were discovered in libsepol, a set of userspace
utilities and libraries for manipulating SELinux policies.

### CVE-2021-36084, CVE-2021-36085, CVE-2021-36086

Three use-after-free problems were discovered in the CIL compiler.  These
could lead to data corruption, denial of service or possibly arbitrary code
execution.

### CVE-2021-36087

A heap-based buffer over-read was discovered in the CIL compiler.  This could
lead to confidentiality or integrity violations, or crashes.

