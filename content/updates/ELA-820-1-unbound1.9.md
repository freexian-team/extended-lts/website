---
title: "ELA-820-1 unbound1.9 security update"
package: unbound1.9
version: 1.9.0-2+deb10u2~deb9u3 (stretch)
version_map: {"9 stretch": "1.9.0-2+deb10u2~deb9u3"}
description: "NRDelegation Attack"
date: 2023-03-29T00:29:24+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-3204
  - CVE-2022-30698
  - CVE-2022-30699

---

Several security vulnerabilities have been discovered in unbound, a validating,
recursive, caching DNS resolver.

CVE-2022-3204

    A vulnerability named 'Non-Responsive Delegation Attack' (NRDelegation
    Attack) has been discovered in various DNS resolving software. The
    NRDelegation Attack works by having a malicious delegation with a
    considerable number of non responsive nameservers. The attack starts by
    querying a resolver for a record that relies on those unresponsive
    nameservers. The attack can cause a resolver to spend a lot of
    time/resources resolving records under a malicious delegation point where a
    considerable number of unresponsive NS records reside. It can trigger high
    CPU usage in some resolver implementations that continually look in the
    cache for resolved NS records in that delegation. This can lead to degraded
    performance and eventually denial of service in orchestrated attacks.
    Unbound does not suffer from high CPU usage, but resources are still needed
    for resolving the malicious delegation. Unbound will keep trying to resolve
    the record until hard limits are reached. Based on the nature of the attack
    and the replies, different limits could be reached. From now on Unbound
    introduces fixes for better performance when under load, by cutting
    opportunistic queries for nameserver discovery and DNSKEY prefetching and
    limiting the number of times a delegation point can issue a cache lookup
    for missing records.

CVE-2022-30698 and CVE-2022-30699

    NLnet Labs Unbound is vulnerable to a novel type of the "ghost domain
    names" attack. The vulnerability works by targeting an Unbound instance.
    Unbound is queried for a rogue domain name when the cached delegation
    information is about to expire. The rogue nameserver delays the response so
    that the cached delegation information is expired. Upon receiving the
    delayed answer containing the delegation information, Unbound overwrites
    the now expired entries. This action can be repeated when the delegation
    information is about to expire making the rogue delegation information
    ever-updating. From now on Unbound stores the start time for a query and
    uses that to decide if the cached delegation information can be
    overwritten.
