---
title: "ELA-574-1 expat security update"
package: expat
version: 2.1.0-6+deb8u8
distribution: "Debian 8 jessie"
description: "arbitrary code execution"
date: 2022-03-07T14:59:48+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-23852
  - CVE-2022-25235
  - CVE-2022-25236
  - CVE-2022-25313
  - CVE-2022-25315

---

Several vulnerabilities have been discovered in Expat, an XML parsing C
library, which could result in denial of service or potentially the
execution of arbitrary code, if a malformed XML file is processed.
