---
title: "ELA-780-2 curl regression update"
package: curl
version: 7.38.0-4+deb8u25 (jessie)
version_map: {"8 jessie": "7.38.0-4+deb8u25"}
description: "regression in libcurl"
date: 2023-02-10T09:53:20-05:00
draft: false
type: updates
tags:
- update
cvelist:
- CVE-2022-27774

---

The patches for CVE-2022-27774 caused a regression in libcurl which could
result in a segmentation fault.  The root cause has been identified and the
patches have been revised.
