---
title: "ELA-1127-1 dns-root-data security update"
package: dns-root-data
version: 2024041801~deb8u1 (jessie), 2024041801~deb9u1 (stretch), 2024041801~deb10u1 (buster)
version_map: {"8 jessie": "2024041801~deb8u1", "9 stretch": "2024041801~deb9u1", "10 buster": "2024041801~deb10u1"}
description: "DNS root zone data update"
date: 2024-07-08T13:00:27-03:00
draft: false
type: updates
tags:
- update
cvelist:

---

The dns-root-data package contains various DNS root zone related data as
published by IANA to be used by various DNS software as a common source of DNS
root zone data. This release includes updates such as the new A and AAAA
records of the B root server. Without this update, users could face slowdowns
when doing DNS queries after the old B root server's IP addresses cease
functioning, on November 27th 2024.
