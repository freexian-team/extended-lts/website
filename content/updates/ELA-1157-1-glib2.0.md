---
title: "ELA-1157-1 glib2.0 security update"
package: glib2.0
version: 2.42.1-1+deb8u7 (jessie)
version_map: {"8 jessie": "2.42.1-1+deb8u7"}
description: "spoofing"
date: 2024-08-19T08:54:25+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-34397

---

Alicia Boya Garcia reported that the GDBus signal subscriptions in the GLib
library are prone to a spoofing vulnerability. A local attacker can take
advantage of this flaw to cause a GDBus-based client to behave incorrectly,
with an application-dependent impact.
