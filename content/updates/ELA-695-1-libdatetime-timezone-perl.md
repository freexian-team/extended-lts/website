---
title: "ELA-695-1 libdatetime-timezone-perl new timezone database"
package: libdatetime-timezone-perl
version: 1:1.75-2+2022d (jessie), 1:2.09-1+2022d (stretch)
version_map: {"8 jessie": "1:1.75-2+2022d", "9 stretch": "1:2.09-1+2022d"}
description: "update to tzdata 2022d"
date: 2022-10-03T14:24:52+02:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2022d for the
Perl bindings.
