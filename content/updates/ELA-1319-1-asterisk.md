---
title: "ELA-1319-1 asterisk security update"
package: asterisk
version: 1:13.14.1~dfsg-2+deb9u11 (stretch), 1:16.28.0~dfsg-0+deb10u6 (buster)
version_map: {"9 stretch": "1:13.14.1~dfsg-2+deb9u11", "10 buster": "1:16.28.0~dfsg-0+deb10u6"}
description: "path traversal vulnerability"
date: 2025-02-12T02:13:01+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-53566

---

A vulnerability was discovered in asterisk, an Open Source Private Branch
Exchange.

CVE-2024-53566

    It is possible to access files outside the configuration directory via AMI
    and path traversal even when live_dangerously is not enabled.
