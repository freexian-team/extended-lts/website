---
title: "ELA-195-1 libvorbis security update"
package: libvorbis
version: 1.3.2-1.3+deb7u2
distribution: "Debian 7 Wheezy"
description: "fix for forgotten checks"
date: 2019-11-27T22:45:02+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-14160
  - CVE-2018-10392
  - CVE-2018-10393

---

Several issues have been found in libvorbis, a decoder library for Vorbis
General Audio Compression Codec.

The fix for CVE-2017-14160 and CVE-2018-10393 improve the bound checking
for very low sample rates.

CVE-2018-10392 was found because the number of channels was not validated
and a remote attacker could cause a denial of service.

