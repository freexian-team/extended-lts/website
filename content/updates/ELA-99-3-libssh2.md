---
title: "ELA-99-3 libssh2 security update"
package: libssh2
version: 1.4.2-1.1+deb7u7
distribution: "Debian 7 Wheezy"
description: "more boundary checks"
date: 2019-07-30T20:53:39+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-3859
  - CVE-2019-3860
  - CVE-2019-13115

---

CVE-2019-3859: While investigating the impact of CVE-2019-13115 in Debian
jessie's version of libssh2, it was discovered that issues around
CVE-2019-3859 had not been fully resolved in Debian jessie's version of
libssh2. A thorough manual (read, analyze, and copy code changes if
needed) comparison of upstream code and code in Debian jessie's version
of libssh2 was done and various more boundary checks and integer overflow
protections got added to the package.

CVE-2019-13115: Kevin Backhouse from semmle.com discovered that initial
fixes for the CVE series CVE-2019-3855 - 2019-3863 introduced several
regressions about signedness of length return values into the upstream
code. While working on the CVE-2019-3859 update mentioned above, it was
paid attention to not introduce these upstream regression registered as
CVE-2019-13115.

CVE-2019-3860: Several more boundary checks have been backported to
src/sftp.c. Furthermore, all boundary checks in src/sftp.c now result in
an LIBSSH2_ERROR_BUFFER_TOO_SMALL error code, rather than a
LIBSSH2_ERROR_ OUT_OF_BOUNDARY error code. This e.g. avoids a segfault in
kftpgrabber's SFTP code.

As a side note, it was discovered that libssh2's SFTP implementation from
Debian wheezy only works well against openssh SFTP servers from Debian
wheezy, tests against newer openssh versions (such as available in Debian
jessie and beyond) failed with SFTP protocol error "Error opening remote
file".
