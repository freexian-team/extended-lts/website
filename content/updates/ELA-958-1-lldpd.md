---
title: "ELA-958-1 lldpd security update"
package: lldpd
version: 0.9.6-1+deb9u2 (stretch)
version_map: {"9 stretch": "0.9.6-1+deb9u2"}
description: "read overflow"
date: 2023-09-23T23:57:35+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-41910

---

Matteo Memelli discovered a flaw in lldpd, an implementation of the IEEE
802.1ab protocol. By crafting a CDP PDU packet with specific CDP_TLV_ADDRESSES
TLVs, a malicious actor can remotely force the lldpd daemon to perform an
out-of-bounds read on heap memory.
