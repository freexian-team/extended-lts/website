---
title: "ELA-31-1 bind9 security update"
package: bind9
version: 1:9.8.4.dfsg.P1-6+nmu2+deb7u21
distribution: "Debian 7 Wheezy"
description: "fix assertion failure"
date: 2018-08-30T22:15:48+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-5740
---
The "deny-answer-aliases" feature in BIND has a flaw which can cause named to exit with an assertion failure.
