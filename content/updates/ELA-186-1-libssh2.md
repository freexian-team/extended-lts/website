---
title: "ELA-186-1 libssh2 security update"
package: libssh2
version: 1.4.2-1.1+deb7u8
distribution: "Debian 7 Wheezy"
description: "denial-of-service"
date: 2019-11-04T20:40:18+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-17498

---

In libssh2, the SSH_MSG_DISCONNECT logic in packet.c has an integer overflow in
a bounds check, enabling an attacker to specify an arbitrary (out-of-bounds)
offset for a subsequent memory read. A crafted SSH server may be able to
disclose sensitive information or cause a denial of service condition on the
client system when a user connects to the server.

