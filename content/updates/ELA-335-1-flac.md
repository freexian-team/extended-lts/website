---
title: "ELA-335-1 flac security update"
package: flac
version: 1.3.0-3+deb8u1
distribution: "Debian 8 jessie"
description: "out-of-bounds read"
date: 2020-12-22T17:50:08+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-0499

---

In `FLAC__bitreader_read_rice_signed_block` of `bitreader.c`,
there is a possible out-of-bounds read due to a heap buffer
overflow.

This could lead to remote information disclosure with no
additional execution privileges needed. However, user
interaction is needed for exploitation.
