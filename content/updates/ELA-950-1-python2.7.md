---
title: "ELA-950-1 python2.7 security update"
package: python2.7
version: 2.7.9-2-ds1-1+deb8u11 (jessie), 2.7.13-2+deb9u8 (stretch)
version_map: {"8 jessie": "2.7.9-2-ds1-1+deb8u11", "9 stretch": "2.7.13-2+deb9u8"}
description: "vulnerabilities in multiple modules"
date: 2023-09-20T21:44:40+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-0391
  - CVE-2022-48560
  - CVE-2022-48565
  - CVE-2022-48566
  - CVE-2023-24329
  - CVE-2023-40217

---

This update fixes multiple vulnerabilities concerning the `urlparse` module as
well as vulnerabilities concerning the `heapq`, `hmac`, `plistlib` and `ssl`
modules.

CVE-2022-0391

    The `urlparse` module helps break Uniform Resource Locator (URL) strings
    into components. The issue involves how the `urlparse` method does not
    sanitize input and allows characters like `'\r'` and `'\n'` in the URL
    path.  This flaw allows an attacker to input a crafted URL, leading to
    injection attacks.

CVE-2022-48560
    
    A use-after-free exists in Python via `heappushpop` in `heapq`.

CVE-2022-48565
    
    An XML External Entity (XXE) issue was discovered in Python.  The
    `plistlib` module no longer accepts entity declarations in XML plist files
    to avoid XML vulnerabilities.

CVE-2022-48566
    
    An issue was discovered in `compare_digest` in `Lib/hmac.py` in Python.
    Constant-time-defeating optimisations were possible in the accumulator
    variable in `hmac.compare_digest`.

CVE-2023-24329
    
    An issue in the `urlparse` component of Python allows attackers to bypass
    blocklisting methods by supplying a URL that starts with blank characters.

CVE-2023-40217
    
    The issue primarily affects servers written in Python (such as HTTP
    servers) that use TLS client authentication. If a TLS server-side socket is
    created, receives data into the socket buffer, and then is closed quickly,
    there is a brief window where the `SSLSocket` instance will detect the
    socket as "not connected" and won't initiate a handshake, but buffered data
    will still be readable from the socket buffer.  This data will not be
    authenticated if the server-side TLS peer is expecting client certificate
    authentication, and is indistinguishable from valid TLS stream data. Data
    is limited in size to the amount that will fit in the buffer. (The TLS
    connection cannot directly be used for data exfiltration because the
    vulnerable code path requires that the connection be closed on
    initialization of the `SSLSocket`.)
