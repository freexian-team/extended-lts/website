---
title: "ELA-1175-1 dovecot security update"
package: dovecot
version: 1:2.2.27-3+deb9u8 (stretch), 1:2.3.4.1-5+deb10u8 (buster)
version_map: {"9 stretch": "1:2.2.27-3+deb9u8", "10 buster": "1:2.3.4.1-5+deb10u8"}
description: "denial of service (DoS) vulnerability"
date: 2024-09-07T02:57:04+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-23185

---

A Denial of Service (DoS) vulnerability was discovered in the IMAP
implementation of the Dovecot mail server: Very large headers could
cause resource exhaustion when parsing message.
