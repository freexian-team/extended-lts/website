---
title: "ELA-848-1 php7.0 security update"
package: php7.0
version: 7.0.33-0+deb9u14 (stretch)
version_map: {"9 stretch": "7.0.33-0+deb9u14"}
description: "multiple vulnerabilities"
date: 2023-05-13T01:25:09+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-31631
  - CVE-2023-0567
  - CVE-2023-0568
  - CVE-2023-0662

---

Multiple security issues were found in PHP, a widely-used open source
general purpose scripting language, which could result in denial of
service or incorrect validation of BCrypt hashes.

CVE-2022-31631

    Due to an uncaught integer overflow, `PDO::quote()` of PDO_SQLite
    may return an improperly quoted string.  The exact details likely
    depend on the implementation of `sqlite3_snprintf()`, but with some
    versions it is possible to force the function to return a single
    apostrophe, if the function is called on user supplied input without
    any length restrictions in place.

CVE-2023-0567

    Tim Düsterhus discovered that malformed BCrypt hashes that include a
    `$` within their salt part trigger a buffer overread and may
    erroneously validate any password as valid.  (`Password_verify()`
    always return `true` with such inputs.)

CVE-2023-0568

    1-byte array overrun when appending slash to paths during path
    resolution.

CVE-2023-0662

    Jakob Ackermann discovered a Denial of Service vulnerability when
    parsing multipart request body: the request body parsing in PHP
    allows any unauthenticated attacker to consume a large amount of CPU
    time and trigger excessive logging.

