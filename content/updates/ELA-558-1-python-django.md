---
title: "ELA-558-1 python-django security update"
package: python-django
version: 1.7.11-1+deb8u15
distribution: "Debian 8 Jessie"
description: "two vulnerabilities"
date: 2022-02-01T11:13:05-08:00
draft: false
type: updates
cvelist:
  - CVE-2022-22818
  - CVE-2022-23833
---

It was discovered that there were two vulnerabilities in Django, the popular
Python-based web development framework:

* CVE-2022-22818: Possible XSS via the `{% debug %}` template tag.

  The `{% debug %}` template tag didn't properly encode the current context,
  posing an XSS attack vector.

  In order to avoid this vulnerability, `{% debug %}` no longer outputs
  information when the `DEBUG` setting is False, and it ensures all context
  variables are correctly escaped when the `DEBUG` setting is `True`.

* CVE-2022-23833: Denial-of-service possibility in file uploads

  Passing certain inputs to multipart forms could result in an infinite loop
  when parsing files.
