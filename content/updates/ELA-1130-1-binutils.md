---
title: "ELA-1130-1 binutils security update"
package: binutils
version: 2.25-5+deb8u2 (jessie), 2.28-5+deb9u1 (stretch), 2.31.1-16+deb10u1 (buster)
version_map: {"8 jessie": "2.25-5+deb8u2", "9 stretch": "2.28-5+deb9u1", "10 buster": "2.31.1-16+deb10u1"}
description: "two vulnerabilities"
date: 2024-07-15T10:02:51+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-12934
  - CVE-2018-1000876

---

Two vulnerabilities have been fixed in binutils, the GNU assembler, linker and binary utilities.

Note that the fix for CVE-2018-12934 removes demangling support for some ancient (e.g. GCC 2.x) mangling schemes


CVE-2018-12934

    OOM in c++filt

CVE-2018-1000876

    Integer Overflow in objdump

