---
title: "ELA-871-1 sqlparse security update"
package: sqlparse
version: 0.2.2-1+deb9u1 (stretch)
version_map: {"9 stretch": "0.2.2-1+deb9u1"}
description: "denial of service"
date: 2023-06-19T17:50:45+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-30608

---

Erik Krogh Kristensen discovered that sqlparse, a non-validating SQL parser,
contained a regular expression that is vulnerable to ReDoS (Regular Expression
Denial of Service).
