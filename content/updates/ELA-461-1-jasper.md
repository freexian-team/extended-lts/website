---
title: "ELA-461-1 jasper security update"
package: jasper
version: 1.900.1-debian1-2.4+deb8u11
distribution: "Debian 8 jessie"
description: "divide by zero"
date: 2021-07-22T23:52:02+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-27845

---

An issue has been found in jasper, a JPEG-2000 runtime library.
A Divide-by-zero vulnerability exists in JasPer Image Coding Toolkit 2.0 in jasper/src/libjasper/jpc/jpc_enc.c 
