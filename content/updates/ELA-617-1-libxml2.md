---
title: "ELA-617-1 libxml2 security update"
package: libxml2
version: 2.9.1+dfsg1-5+deb8u13
distribution: "Debian 8 jessie"
description: "integer overflow"
date: 2022-05-18T22:18:40+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-29824

---

Felix Wilhelm discovered that libxml2, the GNOME XML library, did not correctly
check for integer overflows or used wrong types for buffer sizes. This could
result in out-of-bounds writes or other memory errors when working on large,
multi-gigabyte buffers.

