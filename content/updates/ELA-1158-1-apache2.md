---
title: "ELA-1158-1 apache2 security update"
package: apache2
version: 2.4.10-10+deb8u28 (jessie)
version_map: {"8 jessie": "2.4.10-10+deb8u28"}
description: "multiple vulnerabilities"
date: 2024-08-22T20:09:55Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-38476
  - CVE-2024-38477
  - CVE-2024-39573
  - CVE-2024-39884
  - CVE-2024-40725

---

Multiple vulnerabilities were found on apache, a popular webserver.

CVE-2024-38476

    Backend application whose reponse headers are malicious
    rendered apache2 vulnerable to SSRF
    (Server-side Request Forgery) and local script execution.

CVE-2024-38477

    A NULL pointer dereference was found in
    mod_proxy allowing an attacker to crash the server via
    a malicious request.

CVE-2024-39573

    A potential SSRF in mod_rewrite allowed an
    attacker to cause unsafe RewriteRules to unexpectedly
    setup URL's to be handled by mod_proxy.

CVE-2024-39884

    A regression of CVE-2024-38476 in the core of Apache
    HTTP Server ignores some use of the legacy content-type based
    configuration of handlers. "AddType" and similar configuration,
    under some circumstances where files are requested indirectly,
    result in source code disclosure of local content. For example,
    PHP scripts may be served instead of interpreted.

CVE-2024-40725

    A partial fix for CVE-2024-38476 in the core of
    Apache HTTP Server ignores some use of the legacy content-type based
    configuration of handlers. "AddType" and similar configuration,
    under some circumstances where files are requested indirectly,
    result in source code disclosure of local content. For example,
    PHP scripts may be served instead of interpreted.

Moreover a functionality bug was fixed in webdav list of well known
browser by adding dolphin and Konqueror/5 browsers.
