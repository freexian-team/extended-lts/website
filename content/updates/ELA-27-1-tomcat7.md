---
title: "ELA-27-1 tomcat7 security update"
package: tomcat7
version: 7.0.28-4+deb7u19
distribution: "Debian 7 Wheezy"
description: "denial of service"
date: 2018-08-19T18:35:30+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-1336
---

It was discovered that Tomcat incorrectly handled decoding certain UTF-8
strings which can lead to an infinite loop in the decoder causing a Denial of
Service.
