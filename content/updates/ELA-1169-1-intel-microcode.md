---
title: "ELA-1169-1 intel-microcode security update"
package: intel-microcode
version: 3.20240813.1~deb8u1 (jessie), 3.20240813.1~deb9u1 (stretch), 3.20240813.1~deb10u1 (buster)
version_map: {"8 jessie": "3.20240813.1~deb8u1", "9 stretch": "3.20240813.1~deb9u1", "10 buster": "3.20240813.1~deb10u1"}
description: "multiple vulnerabilities (privilege escalation, DoS, information disclosure)"
date: 2024-08-29T03:11:50+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-42667
  - CVE-2023-45733
  - CVE-2023-45745
  - CVE-2023-46103
  - CVE-2023-47855
  - CVE-2023-49141
  - CVE-2024-24853
  - CVE-2024-24980
  - CVE-2024-25939

---

This update ships updated CPU microcode for some types of Intel CPUs and
provides mitigations for vulnerabilities that may allow a privileged user to
potentially enable escalation of privilege, partial information disclosure, or
denial of service via local access.
