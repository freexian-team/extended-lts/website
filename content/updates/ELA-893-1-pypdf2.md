---
title: "ELA-893-1 pypdf2 security update"
package: pypdf2
version: 1.26.0-2+deb9u2 (stretch)
version_map: {"9 stretch": "1.26.0-2+deb9u2"}
description: "quadratic runtime with malformed PDFs"
date: 2023-07-14T23:43:15+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-36810

---

Quadratic runtime with malformed PDFs missing xref marker has been fixed 
in PyPDF2, a pure Python PDF library.
