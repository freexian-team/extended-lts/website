---
title: "ELA-1272-1 libsoup2.4 security update"
package: libsoup2.4
version: 2.48.0-1+deb8u3 (jessie), 2.56.0-2+deb9u3 (stretch), 2.64.2-2+deb10u1 (buster)
version_map: {"8 jessie": "2.48.0-1+deb8u3", "9 stretch": "2.56.0-2+deb9u3", "10 buster": "2.64.2-2+deb10u1"}
description: "multiple vulnerabilities"
date: 2024-12-12T20:25:42+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-52530
  - CVE-2024-52531
  - CVE-2024-52532

---

Multiple vulnerabilities were discovered in libsoup2.4, an HTTP library
for Gtk+ programs.

### CVE-2024-52530

In some configurations, HTTP request smuggling is possible because null
characters at the end of the names of HTTP headers were ignored.

### CVE-2024-52531

There was a buffer overflow in applications that perform conversion to
UTF-8 in `soup_header_parse_param_list_strict`.  This could lead to memory
corruption, crashes or information disclosure.  (Contrary to the CVE
description, it is now believed that input received over the network could
trigger this.)

### CVE-2024-52532

An infinite loop in the processing of WebSocket data from clients could
lead to a denial-of-service problem through memory exhaustion.

