---
title: "ELA-988-1 redis security update"
package: redis
version: 2:2.8.17-1+deb8u12 (jessie), 3:3.2.6-3+deb9u12 (stretch)
version_map: {"8 jessie": "2:2.8.17-1+deb8u12", "9 stretch": "3:3.2.6-3+deb9u12"}
description: "authorisation bypass vulnerability"
date: 2023-10-23T13:49:03+01:00
draft: false
cvelist:
  - CVE-2023-45145
---

It was discovered that there was an authentication bypass vulnerability in
Redis, a popular key-value database similar to memcached.

On startup, Redis began listening on a Unix socket before adjusting its
permissions to the user-provided configuration. If a permissive `umask(2)` was
used, this created a race condition that enabled, during a short period of
time, another process to establish an otherwise unauthorized connection.
