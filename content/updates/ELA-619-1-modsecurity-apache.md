---
title: "ELA-619-1 modsecurity-apache security update"
package: modsecurity-apache
version: 2.8.0-3+deb8u1
distribution: "Debian 8 Jessie"
description: "resource exhaustion vulnerability"
date: 2022-05-28T09:00:45+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-42717
---

It was discovered that there was a potential resource exhaustion attack in
`modsecurity-apache`, an Apache module which inspects HTTP requests with the
aim of preventing typical web application attacks such as XSS and SQL.

