---
title: "ELA-1309-1 libgit2 security update"
package: libgit2
version: 0.21.1-3+deb8u2 (jessie)
version_map: {"8 jessie": "0.21.1-3+deb8u2"}
description: "multiple vulnerabilities"
date: 2025-01-30T19:44:11+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-8568
  - CVE-2016-8569
  - CVE-2016-10128
  - CVE-2016-10129
  - CVE-2018-8099
  - CVE-2018-10887
  - CVE-2018-10888
  - CVE-2020-12278
  - CVE-2020-12279
  - CVE-2024-24577
---

Multiple vulnerabilities were discovered in libgit2.

* **CVE-2016-8568**

    The `git_commit_message` function in oid.c allows remote attackers
    to cause a denial of service (out-of-bounds read) via a cat-file
    command with a crafted object file.

* **CVE-2016-8569**

    The `git_oid_nfmt` function in commit.c allows remote attackers to
    cause a denial of service (`NULL` pointer dereference) via a cat-file
    command with a crafted object file.

* **CVE-2016-10128**

    Buffer overflow in the `git_pkt_parse_line` function in
    `transports/smart_pkt.c` in the Git Smart Protocol support in libgit2
    allows remote attackers to have unspecified impact via a crafted
    non-flush packet.

* **CVE-2016-10129**

    The Git Smart Protocol support in libgit2 allows remote attackers
    to cause a denial of service (`NULL` pointer dereference) via an empty
    packet line.

* **CVE-2018-8099**

    Incorrect returning of an error code in the `index.c:read_entry()`
    function leads to a double free in libgit2, which allows an attacker
    to cause a denial of service via a crafted repository index file.

* **CVE-2018-10887**

    An unexpected sign extension in `git_delta_apply` function in `delta.c`
    file may lead to an integer overflow which in turn leads to an out of
    bound read, allowing to read before the base object. An attacker may
    use this flaw to leak memory addresses or cause a Denial of Service.

* **CVE-2018-10888**

    A missing check in `git_delta_apply` function in delta.c file, may
    lead to an out-of-bound read while reading a binary delta file. An
    attacker may use this flaw to cause a Denial of Service.

* **CVE-2020-12278**

    path.c mishandles equivalent filenames that exist because of NTFS
    Alternate Data Streams. This may allow remote code execution when
    cloning a repository.

* **CVE-2020-12279**

    checkout.c mishandles equivalent filenames that exist because of
    NTFS short names. This may allow remote code execution when cloning
    a repository

* **CVE-2024-24577**

    Using crafted inputs to the `git_index_add` function could cause
    heap corruption, and this had the potential to permit arbitrary
    code execution.

