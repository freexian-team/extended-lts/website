---
title: "ELA-1197-1 ntfs-3g security update"
package: ntfs-3g
version: 1:2016.2.22AR.1+dfsg-1+deb9u5 (stretch), 1:2017.3.23AR.3-4+deb11u4~deb10u1 (buster)
version_map: {"9 stretch": "1:2016.2.22AR.1+dfsg-1+deb9u5", "10 buster": "1:2017.3.23AR.3-4+deb11u4~deb10u1"}
description: "use-after-free"
date: 2024-10-04T23:29:03+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-52890

---

Use-after-free in ntfs_uppercase_mbs() has been fixed in ntfs-3g, a read/write driver for the NTFS filesystem.
