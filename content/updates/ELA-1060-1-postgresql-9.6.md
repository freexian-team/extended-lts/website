---
title: "ELA-1060-1 postgresql-9.6 security update"
package: postgresql-9.6
version: 9.6.24-0+deb9u6 (stretch)
version_map: {"9 stretch": "9.6.24-0+deb9u6"}
description: "privilege escalation"
date: 2024-03-20T22:57:09+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-0985

---

In the PostgreSQL database server, a late privilege drop in the
REFRESH MATERIALIZED VIEW CONCURRENTLY command could allow an
attacker to trick a user with higher privileges to run SQL commands.
