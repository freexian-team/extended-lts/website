---
title: "ELA-1105-1 gst-plugins-base0.10 security update"
package: gst-plugins-base0.10
version: 0.10.36-2+deb8u4 (jessie)
version_map: {"8 jessie": "0.10.36-2+deb8u4"}
description: "integer overflow"
date: 2024-06-06T09:51:25+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-4453

---

An integer overflow in the EXIF metadata parsing was discovered in the
GStreamer media framework, which may result in denial of service or
potentially the execution of arbitrary code if a malformed file is
processed.
