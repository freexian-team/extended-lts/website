---
title: "ELA-1078-1 util-linux security update"
package: util-linux
version: 2.26.2-6+deb8u1 (jessie), 2.29.2-1+deb9u2 (stretch)
version_map: {"8 jessie": "2.26.2-6+deb8u1", "9 stretch": "2.29.2-1+deb9u2"}
description: "buffer overflow vulnerability"
date: 2024-04-26T12:44:26+01:00
draft: false
cvelist:
  - CVE-2021-37600
---

An integer overflow attack was discovered in `util-linux` which could
have caused a buffer overflow if an attacker were able to use system resources
in a way that leads to a large number in the `/proc/sysvipc/sem` file.
