---
title: "ELA-75-1 wireshark security update"
package: wireshark
version: 1.12.1+g01b65bf-4+deb8u6~deb7u13
distribution: "Debian 7 Wheezy"
description: "several issues in different dissectors"
date: 2019-01-20T16:47:19+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-7703
  - CVE-2017-7746
  - CVE-2017-7747
  - CVE-2017-9766
  - CVE-2017-11406
  - CVE-2017-11407
  - CVE-2017-11409
  - CVE-2017-13765
  - CVE-2017-15191
  - CVE-2017-17935
  - CVE-2017-17997
  - CVE-2018-7325
  - CVE-2018-7331
  - CVE-2018-9256
  - CVE-2018-9259
  - CVE-2018-9262
  - CVE-2018-11356
  - CVE-2018-11357
  - CVE-2018-11359
  - CVE-2018-16057
  - CVE-2018-16058
  - CVE-2018-19622
  - CVE-2018-19623
  - CVE-2018-19624
  - CVE-2018-19625
  - CVE-2018-19626

---

Several issues in wireshark, a tool that captures and analyzes packets off the wire, have been found by different people. These are basically issues with length checks or invalid memory access in different dissectors. This could result in infinite loops or crashes by malicious packets.

