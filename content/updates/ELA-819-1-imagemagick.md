---
title: "ELA-819-1 imagemagick security update"
package: imagemagick
version: 8:6.8.9.9-5+deb8u25 (jessie), 8:6.9.7.4+dfsg-11+deb9u18 (stretch)
version_map: {"8 jessie": "8:6.8.9.9-5+deb8u25", "9 stretch": "8:6.9.7.4+dfsg-11+deb9u18"}
description: "multiple vulnerabilities"
date: 2023-03-24T13:39:40Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-18028
  - CVE-2020-27767
  - CVE-2021-3574
  - CVE-2021-20224
  - CVE-2022-44267

---

Multiple vulnerability were found in ImageMagick, an image processing
software, that could result in deny of service, or memory leaks.

CVE-2017-18028

    A memory exhaustion vulnerability was found in the function
    ReadTIFFImage in coders/tiff.c, which allow remote attackers to
    cause a denial of service via a crafted file.

CVE-2020-27767

    A flaw was found in ImageMagick in MagickCore/quantum.h.
    An attacker who submits a crafted file that is processed by ImageMagick
    could trigger undefined behavior in the form of values outside the range
    of types `float` and `unsigned char`. This would most likely lead to
    an impact to application availability, but could potentially cause
    other problems related to undefined behavior.

CVE-2021-3574

    A flaw was found in ImageMagick, executing a crafted file with
    the convert command, will leak memory.

CVE-2021-20224

    An integer overflow issue was discovered in ImageMagick's
    ExportIndexQuantum() function in MagickCore/quantum-export.c.
    Function calls to GetPixelIndex() could result in values
    outside the range of representable for the 'unsigned char'.
    When ImageMagick processes a crafted pdf file,
    this could lead to an undefined behaviour or a crash.

CVE-2022-44267

    ImageMagick was vulnerable to Denial of Service.
    When it parses a PNG image (e.g., for resize), the convert process
    could be left waiting for stdin input.

