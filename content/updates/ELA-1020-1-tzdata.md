---
title: "ELA-1020-1 tzdata security update"
package: tzdata
version: 2021a-0+deb8u11 (jessie), 2021a-0+deb9u11 (stretch)
version_map: {"8 jessie": "2021a-0+deb8u11", "9 stretch": "2021a-0+deb9u11"}
description: "updated timezone database"
date: 2023-12-15T16:48:45+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update includes the latest changes to the leap second list,
including an update to its expiry date, which was set for the end of
December.
