---
title: "ELA-889-1 dpdk security update"
package: dpdk
version: 16.11.11-1+deb9u3 (stretch)
version_map: {"9 stretch": "16.11.11-1+deb9u3"}
description: "buffer overflow"
date: 2023-07-08T18:31:04-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-2132

---

A buffer overflow was discovered in the vhost code of DPDK, a set of libraries
for fast packet processing, which could result in denial of service or the
execution of arbitrary code by malicious guests/containers.
