---
title: "ELA-834-1 keepalived security update"
package: keepalived
version: 1:1.3.2-1+deb9u1 (stretch)
version_map: {"9 stretch": "1:1.3.2-1+deb9u1"}
description: "access-control bypass"
date: 2023-04-18T01:12:21+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-19115
  - CVE-2021-44225

---

Two security vulnerabilities were found in keepalived, a failover and
monitoring daemon for LVS clusters.

CVE-2018-19115

    keepalived has a heap-based buffer overflow when parsing HTTP
    status codes resulting in DoS or possibly unspecified other impact, because
    extract_status_code in lib/html.c has no validation of the status code and
    instead writes an unlimited amount of data to the heap.

CVE-2021-44225

    A flaw was found in keepalived where an improper authentication
    vulnerability allows an unprivileged user to change properties that could
    lead to an access-control bypass.
