---
title: "ELA-343-1 phpmyadmin security update"
package: phpmyadmin
version: 4:4.2.12-2+deb8u11
distribution: "Debian 8 jessie"
description: "XSS and CRSF"
date: 2021-02-02T09:24:26+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-2045
  - CVE-2016-5097
  - CVE-2016-5702
  - CVE-2016-6623
  - CVE-2019-12922

---

Several security vulnerabilities were addressed in phpmyadmin, a popular MySQL
web administration tool.

CVE-2016-2045

    Vulnerability in the SQL editor: A remote authenticated attacker is able to
    inject arbitrary web script or HTML via a SQL query that triggers JSON data
    in a response.

CVE-2016-5097

    phpMyAdmin places tokens in query strings and does not arrange for them to
    be stripped before external navigation, which allows remote attackers to
    obtain sensitive information by reading (1) HTTP requests or (2) server
    logs.

CVE-2016-6623

    An authorized user can cause a denial-of-service (DoS) attack on a server
    by passing large values to a loop.

CVE-2016-5702

    phpMyAdmin, when the environment lacks a PHP_SELF value, allows remote
    attackers to conduct cookie-attribute injection attacks via a crafted URI.

CVE-2019-12922

    CSRF vulnerability that allows an attacker to delete a server when using
    the setup script. Note: The setup script is disabled by default.


