---
title: "ELA-1154-1 util-linux security update"
package: util-linux
version: 2.26.2+really2.25.2-6+deb8u2 (jessie), 2.29.2-1+deb9u3 (stretch)
version_map: {"8 jessie": "2.26.2+really2.25.2-6+deb8u2", "9 stretch": "2.29.2-1+deb9u3"}
description: "information disclosure"
date: 2024-08-15T21:02:29-04:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-28085

---

Skyler Ferrante discovered that the `wall(1)` utility found in
`util-linux`, a collection of system utilities for Linux, does not
filter escape sequences from command line arguments.  This allows
unprivileged local users to put arbitrary text on other users
terminals if `mesg` is set to ‘y’ and the `wall` executable is setgid,
which could lead to information disclosure.

With this update the wall executable is no longer installed setgid
`tty`.
