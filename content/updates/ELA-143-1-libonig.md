---
title: "ELA-143-1 libonig security update"
package: libonig
version: 5.9.1-1+deb7u2
distribution: "Debian 7 Wheezy"
description: "use-after-free"
date: 2019-07-17T02:27:01+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-13224

---

A use-after-free in onig_new_deluxe() in regext.c allows attackers to potentially
cause information disclosure, denial of service, or possibly code execution by
providing a crafted regular expression. The attacker provides a pair of a regex
pattern and a string, with a multi-byte encoding that gets handled by
onig_new_deluxe().
