---
title: "ELA-1177-1 bluez security update"
package: bluez
version: 5.43-2+deb9u8 (stretch), 5.50-1.2~deb10u6 (buster)
version_map: {"9 stretch": "5.43-2+deb9u8", "10 buster": "5.50-1.2~deb10u6"}
description: "multiple vulnerabilities"
date: 2024-09-07T17:07:49+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-27349
  - CVE-2023-50229
  - CVE-2023-50230

---

Multiple vulnerabilities have been fixed in bluez, a library, tools and daemons for using Bluetooth devices.

CVE-2023-27349 (stretch)

    AVRCP crash while handling unsupported events

CVE-2023-50229

    Phone Book Access profile Heap-based Buffer Overflow

CVE-2023-50230

    Phone Book Access profile Heap-based Buffer Overflow

