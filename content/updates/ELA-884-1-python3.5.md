---
title: "ELA-884-1 python3.5 security update"
package: python3.5
version: 3.5.3-1+deb9u7 (stretch)
version_map: {"9 stretch": "3.5.3-1+deb9u7"}
description: "several vulnerabilities"
date: 2023-06-30T23:51:40+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2015-20107
  - CVE-2021-4189
  - CVE-2022-45061

---

Several vulnerabilities were fixed in the Python3 interpreter.

CVE-2015-20107

    The mailcap module did not add escape characters into commands discovered in the system mailcap file.

CVE-2021-4189

    Make ftplib not trust the PASV response.

CVE-2022-45061

    Quadratic time in the IDNA decoder.

