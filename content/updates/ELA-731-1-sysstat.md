---
title: "ELA-731-1 sysstat security update"
package: sysstat
version: 11.0.1-1+deb8u1 (jessie), 11.4.3-2+deb9u1 (stretch)
version_map: {"8 jessie": "11.0.1-1+deb8u1", "9 stretch": "11.4.3-2+deb9u1"}
description: "remote code execution"
date: 2022-11-14T03:30:06+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-39377

---

On 32 bit systems, allocate_structures contains a size_t overflow
in sa_common.c. The allocate_structures function insufficiently
checks bounds before arithmetic multiplication, allowing for an
overflow in the size allocated for the buffer representing system
activities. This issue may lead to Remote Code Execution (RCE).
