---
title: "ELA-646-1 python-babel security update"
package: python-babel
version: 1.3+dfsg.1-5+deb8u1 (jessie)
version_map: {"8 jessie": "1.3+dfsg.1-5+deb8u1"}
description: "arbitrary code-execution vulnerability"
date: 2022-07-17T11:45:22+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-42771
---

An arbitrary code execution vulnerability was discovered in `python-babel`, a
library for internationalizing Python applications.

Attackers could load arbitrary locale `.data` files (containing serialized
Python objects) via a directory traversal attack, leading to code execution.
