---
title: "ELA-1214-1 distro-info-data database update"
package: distro-info-data
version: 0.36~bpo8+6 (jessie), 0.41+deb10u2~bpo9+6 (stretch), 0.41+deb10u10 (buster)
version_map: {"8 jessie": "0.36~bpo8+6", "9 stretch": "0.41+deb10u2~bpo9+6", "10 buster": "0.41+deb10u10"}
description: "non-security-related database update"
date: 2024-10-25T09:49:27-07:00
draft: false
type: updates
tags:
- update
cvelist:

---

This is a routine update of the distro-info-data database for Debian
ELTS users.

It adds Ubuntu 25.04.
