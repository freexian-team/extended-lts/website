---
title: "ELA-860-1 cups security update"
package: cups
version: 1.7.5-11+deb8u10 (jessie), 2.2.1-8+deb9u9 (stretch)
version_map: {"8 jessie": "1.7.5-11+deb8u10", "9 stretch": "2.2.1-8+deb9u9"}
description: "buffer overflow might cause DoS"
date: 2023-06-01T12:14:29+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-32324

---

An issue has been found in cups, the Common UNIX Printing System.
Due to a buffer overflow vulnerability in the function format_log_line()
a remote attackers could cause a denial-of-service(DoS). The vulnerability
can be triggered when the configuration file cupsd.conf sets the value of
"loglevel" to "DEBUG".

