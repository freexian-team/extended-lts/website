---
title: "ELA-681-1 mako security update"
package: mako
version: 1.0.0+dfsg-0.1+deb8u1 (jessie), 1.0.6+ds1-2+deb9u1 (stretch)
version_map: {"8 jessie": "1.0.0+dfsg-0.1+deb8u1", "9 stretch": "1.0.6+ds1-2+deb9u1"}
description: "denial of service"
date: 2022-09-21T18:07:13+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-40023

---

It was found that Mako, a Python template library, was vulnerable to a
denial of service attack via crafted regular expressions.
