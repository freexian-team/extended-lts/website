---
title: "ELA-1049-1 evince security update"
package: evince
version: 3.22.1-3+deb9u3 (stretch)
version_map: {"9 stretch": "3.22.1-3+deb9u3"}
description: "command injection"
date: 2024-02-29T20:38:21+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-51698

---

A security vulnerability was found in Evince, a document viewer, which may grant
an attacker immediate access to the target system when the target user opens a
crafted document or clicks on a crafted link/URL using a maliciously crafted
CBT (comic book archive) document which is a TAR archive. The comic book
backend of Evince uses libarchive now, which handles CBT and other comic book
archives correctly.
