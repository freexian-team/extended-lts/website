---
title: "ELA-1156-1 indent security update"
package: indent
version: 2.2.12-1+deb11u1~deb10u1 (buster)
version_map: {"10 buster": "2.2.12-1+deb11u1~deb10u1"}
description: "multiple issues"
date: 2024-08-17T23:57:21+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-40305
  - CVE-2024-0911

---

Multiple issues have been fixed in GNU indent, a C source code formatter.
