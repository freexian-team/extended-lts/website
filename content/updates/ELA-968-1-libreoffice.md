---
title: "ELA-968-1 libreoffice security update"
package: libreoffice
version: 1:6.1.5-3~deb9u1 (stretch)
version_map: {"9 stretch": "1:6.1.5-3~deb9u1"}
description: "multiple vulnerabilities"
date: 2023-09-28T16:25:40Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-25636
  - CVE-2022-3140
  - CVE-2022-26305
  - CVE-2022-26306
  - CVE-2022-26307
  - CVE-2022-38745
  - CVE-2023-0950
  - CVE-2023-2255

---

Multiple vulnerabilities were found in LibreOffice, an office productivity software suite, leading to arbitrary script execution, improper certificate validation, and weak encryption of password storage in the user's configuration database.

The changes required to fix all the open vulnerabilities, especially
those affecting the Graphical User Interface (GUI), were too invasive to
be backported individually, and the risk of regressions was too high,
due to large amounts of source code that needed to be
modified or rewritten, including an internal library.

A risk analysis was carried out, and it was determined that the best
available solution was to backport the buster version of LibreOffice to
stretch. This decision means that upon installing this update users of
LibreOffice in stretch will be moving from a LibreOffice version of 5.x
to 6.1.5. Additionally, this backport required the introduction of libxmlsec1
as new dependency.


 CVE-2021-25636

    Only use X509Data LibreOffice supports digital signatures of ODF documents and macros within documents, presenting visual aids that no alteration of the document occurred since the last signing and that the signature is valid. An Improper Certificate Validation vulnerability in LibreOffice allowed an attacker to create a digitally signed ODF document, by manipulating the documentsignatures.xml or macrosignatures.xml stream within the document to contain both "X509Data" and "KeyValue" children of the "KeyInfo" tag, which when opened caused LibreOffice to verify using the "KeyValue" but to report verification with the unrelated "X509Data" value.

CVE-2022-3140

    Insufficient validation of "vnd.libreoffice.command" URI schemes. LibreOffice supports Office URI Schemes to enable browser integration of LibreOffice with MS SharePoint server. An additional scheme "vnd.libreoffice.command" specific to LibreOffice was added. In the affected versions of LibreOffice links using that scheme could be constructed to call internal macros with arbitrary arguments. Which when clicked on, or activated by document events, could result in arbitrary script execution without warning.
    
CVE-2022-26305

    Compare authors using Thumbprint An Improper Certificate Validation vulnerability in LibreOffice existed where determining if a macro was signed by a trusted author was done by only matching the serial number and issuer string of the used certificate with that of a trusted certificate. This is not sufficient to verify that the macro was actually signed with the certificate. An adversary could therefore create an arbitrary certificate with a serial number and an issuer string identical to a trusted certificate which LibreOffice would present as belonging to the trusted author, potentially leading to the user to execute arbitrary code contained in macros improperly trusted.

CVE-2022-26306

    LibreOffice supports the storage of passwords for web connections in the user’s configuration database. The stored passwords are encrypted with a single master key provided by the user. A flaw in LibreOffice existed where the required initialization vector for encryption was always the same which weakens the security of the encryption making them vulnerable if an attacker has access to the user's configuration data.

CVE-2022-26307

    Add Initialization Vectors to password storage. LibreOffice supports the storage of passwords for web connections in the user’s configuration database. The stored passwords are encrypted with a single master key provided by the user. A flaw in LibreOffice existed where master key was poorly encoded resulting in weakening its entropy from 128 to 43 bits making the stored passwords vulerable to a brute force attack if an attacker has access to the users stored config.

CVE-2022-38745

    Libreoffice may be configured to add an empty entry to the Java class path. This may lead to run arbitrary Java code from the current directory.

CVE-2023-0950

    Improper Validation of Array Index vulnerability in the spreadsheet component allows an attacker to craft a spreadsheet document that will cause an array index underflow when loaded. In the affected versions of LibreOffice certain malformed spreadsheet formulas, such as AGGREGATE, could be created with less parameters passed to the formula interpreter than it expected, leading to an array index underflow, in which case there is a risk that arbitrary code could be executed.

CVE-2023-2255

    Improper access control in editor components of LibreOffice allowed an attacker to craft a document that would cause external links to be loaded without prompt. In the affected versions of LibreOffice documents that used "floating frames" linked to external files, would load the contents of those frames without prompting the user for permission to do so. This was inconsistent with the treatment of other linked content in LibreOffice.
