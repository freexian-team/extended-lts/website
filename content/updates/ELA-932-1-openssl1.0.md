---
title: "ELA-932-1 openssl1.0 security update"
package: openssl1.0
version: 1.0.2u-1~deb9u8 (stretch)
version_map: {"9 stretch": "1.0.2u-1~deb9u8"}
description: "multiple issues"
date: 2023-08-25T23:29:15+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-1292
  - CVE-2022-2068
  - CVE-2023-0215
  - CVE-2023-0286
  - CVE-2023-0464
  - CVE-2023-0465
  - CVE-2023-0466
  - CVE-2023-2650

---

Several issues have been found in openssl1.0, a Secure Sockets Layer toolkit.

CVE-2022-1292, CVE-2022-2068

    The c_rehash script does not properly sanitise shell metacharacters to prevent
    command injection. This script is executed by update-ca-certificates,
    from ca-certificates, to re-hash certificates in /etc/ssl/certs/. An attacker
    able to place files in this directory could execute arbitrary commands with
    the privileges of the script.

CVE-2023-0215, CVE-2023-0286

    Multiple vulnerabilities may result in incomplete encryption, side channel attacks,
    denial of service or information disclosure.

CVE-2023-0464

    David Benjamin reported a flaw related to the verification of X.509 certificate
    chains that include policy constraints, which may result in denial of service.

CVE-2023-0465

    David Benjamin reported that invalid certificate policies in leaf certificates
    are silently ignored. A malicious CA could take advantage of this flaw to
    deliberately assert invalid certificate policies in order to circumvent policy
    checking on the certificate altogether.

CVE-2023-0466

    David Benjamin discovered that the implementation of the
    X509_VERIFY_PARAM_add0_policy() function does not enable the check which
    allows certificates with invalid or incorrect policies to pass the certificate
    verification (contrary to its documentation).

CVE-2023-2650

    It was discovered that processing malformed ASN.1 object identifiers or data
    may result in denial of service.

