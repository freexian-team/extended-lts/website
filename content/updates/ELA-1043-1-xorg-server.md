---
title: "ELA-1043-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u15 (jessie)
version_map: {"8 jessie": "2:1.16.4-1+deb8u15"}
description: "privilege escalation"
date: 2024-02-10T23:46:09+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-6816
  - CVE-2024-0229
  - CVE-2024-0408
  - CVE-2024-0409
  - CVE-2024-21885
  - CVE-2024-21886

---

Several vulnerabilities were discovered in the Xorg X server, which may
result in privilege escalation if the X server is running privileged
or denial of service.
