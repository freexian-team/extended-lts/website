---
title: "ELA-1184-1 zeromq3 security update"
package: zeromq3
version: 4.3.1-4+deb10u3 (buster)
version_map: {"10 buster": "4.3.1-4+deb10u3"}
description: "multiple vulnerabilities"
date: 2024-09-28T02:49:36+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-20234
  - CVE-2021-20235
  - CVE-2021-20237

---

Multiple vulnerabilities have been fixed in the messaging library ZeroMQ.

CVE-2021-20234

    Memory leak in client induced by malicious server(s)

CVE-2021-20235

    Heap overflow when receiving malformed ZMTP v1 packets

CVE-2021-20237

    Memory leak in PUB server induced by malicious client(s)

