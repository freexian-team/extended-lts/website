---
title: "ELA-1014-1 python-urllib3 security update"
package: python-urllib3
version: 1.9.1-3+deb8u2 (jessie), 1.19.1-1+deb9u2 (stretch)
version_map: {"8 jessie": "1.9.1-3+deb8u2", "9 stretch": "1.19.1-1+deb9u2"}
description: "multiple vulnerabilities"
date: 2023-11-30T16:24:26Z
draft: false
type: updates
tags:
- update
cvelist:
- CVE-2018-20060
- CVE-2018-25091
- CVE-2023-43804
- CVE-2023-45803
---

Multiple vulnerabilities were found in python-urllib3, a user-friendly HTTP
library for Python.

### CVE-2018-20060

It was discovered that the Authorization HTTP header was not removed when
following a cross-origin redirect (i.e., a redirect that differs in host,
port, or scheme).  This could allow for credentials in the Authorization
header to be exposed to unintended hosts or transmitted in cleartext.

### CVE-2018-25091

Yoshida Katsuhiko discovered that the fix for CVE-2018-20060 did not cover
non-titlecase request headers; for instance “authorization” request headers
were not removed during cross-origin redirects.  (Per RFC7230 sec. 3.2
header fields are to be treated case-insensitively.)

### CVE-2023-43804

It was discovered that the Cookie request header was not stripped during
cross-origin redirects.  It is therefore possible for a user specifying a
Cookie header to unknowingly leak information via HTTP redirects to a
different origin, unless the user disables redirects explicitly.
The issue is similar to CVE-2018-20060, but for the Cookie request header rather
than Authorization.

### CVE-2023-45803

It was discovered that the HTTP request body was not removed when an HTTP
redirect response using status 301, 302, or 303 after the request had its
method changed from one that could accept a request body, like POST, to GET,
as required by the HTTP RFCs.  This could lead to information disclosure.
