---
title: "ELA-360-1 gdisk security update"
package: gdisk
version: 0.8.10-2+deb8u1
distribution: "Debian 8 jessie"
description: "out-of-bounds write"
date: 2021-02-08T13:47:44+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-0256
  - CVE-2021-0308

---

CVE-2020-0256

    In LoadPartitionTable of gpt.cc, there is a possible
    out of bounds write due to a missing bounds check. This
    could lead to local escalation of privilege with no
    additional execution privileges needed.

CVE-2021-0308

    In ReadLogicalParts of basicmbr.cc, there is a possible
    out of bounds write due to a missing bounds check. This
    could lead to local escalation of privilege with no
    additional execution privileges needed.
