---
title: "ELA-892-1 yajl security update"
package: yajl
version: 2.1.0-2+deb8u2 (jessie), 2.1.0-2+deb9u2 (stretch)
version_map: {"8 jessie": "2.1.0-2+deb8u2", "9 stretch": "2.1.0-2+deb9u2"}
description: "multiple vulnerabilties"
date: 2023-07-11T18:56:29+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-16516
  - CVE-2022-24795
  - CVE-2023-33460

---

Multiple vulnerabilties have been found in yajl, a JSON parser / small 
validating JSON generator# written in ANSI C, which potentially can
cause memory corruption or DoS.

The CVE-20117-16516 had been addressed already in ELA-888-1, however 
the fix has been found to be incomplete as it missed an additional memory leak.
This update fixes that problem.

CVE-2017-16516

    When a crafted JSON file is supplied to yajl, the process might
    crash with a SIGABRT in the yajl_string_decode function in 
    yajl_encode.c. This results potentially in a denial of service.

CVE-2022-24795

    The 1.x branch and the 2.x branch of `yajl` contain an integer overflow
    which leads to subsequent heap memory corruption when dealing with large
    (~2GB) inputs.

CVE-2023-33460

    There's a memory leak in yajl 2.1.0 with use of yajl_tree_parse function,
    which potentially cause out-of-memory in server and cause crash.
