---
title: "ELA-922-1 rar security update"
package: rar
version: 2:6.20-0.1~deb9u1 (stretch)
version_map: {"9 stretch": "2:6.20-0.1~deb9u1"}
description: "directory traversal"
date: 2023-08-16T19:48:04+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-30333

---

The RAR archiver allows directory traversal to write to files during an extract
(aka unpack) operation, as demonstrated by creating a ~/.ssh/authorized_keys
file.
