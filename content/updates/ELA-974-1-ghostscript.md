---
title: "ELA-974-1 ghostscript security update"
package: ghostscript
version: 9.26a~dfsg-0+deb8u12 (jessie), 9.26a~dfsg-0+deb9u12 (stretch)
version_map: {"8 jessie": "9.26a~dfsg-0+deb8u12", "9 stretch": "9.26a~dfsg-0+deb9u12"}
description: "multiple vulnerabilities"
date: 2023-09-30T22:58:01+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-21710
  - CVE-2020-21890
  - CVE-2023-38559

---

CVE-2020-21710

	Divide by zero in eps_print_page()

CVE-2020-21890

	Buffer overflow in clj_media_size()

CVE-2023-38559

	Buffer overflow in devn_pcx_write_rle()

