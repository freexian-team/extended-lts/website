---
title: "ELA-200-2 intel-microcode regression update"
package: intel-microcode
version: 3.20191115.2~deb7u1
distribution: "Debian 7 Wheezy"
description: "firmware update"
date: 2019-12-28T17:21:39+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-11135
  - CVE-2019-11139

---

This update ships updated CPU microcode for CFL-S (Coffe Lake Desktop) models
of Intel CPUs which were not yet included in the Intel microcode update
released as ELA-200-1. For details please refer to

https://www.intel.com/content/dam/www/public/us/en/security-advisory/documents/IPU-2019.2-microcode-update-guidance-v1.01.pdf

Additionally this update rolls back CPU microcode for HEDT and Xeon processors
with signature 0x50654 which were affected by a regression causing hangs on
warm reboots (Cf. #946515).
