---
title: "ELA-1203-1 samba security update"
package: samba
version: 2:4.5.16+dfsg-1+deb9u5 (stretch)
version_map: {"9 stretch": "2:4.5.16+dfsg-1+deb9u5"}
description: "multiple vulnerabilities"
date: 2024-10-12T09:43:32-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-2124
  - CVE-2020-25717
  - CVE-2021-44142
  - CVE-2022-2127
  - CVE-2022-3437
  - CVE-2022-32742
  - CVE-2023-4091

---

Several vulnerabilities were discovered in Samba, SMB/CIFS file, 
print, and login server for Unix.

CVE-2016-2124

    A flaw was found in the way samba implemented SMB1 authentication. An
    attacker could use this flaw to retrieve the plaintext password sent over
    the wire even if Kerberos authentication was required.

CVE-2020-25717

    Andrew Bartlett reported that Samba may map domain users to local
    users in an undesired way, allowing for privilege escalation. The
    update introduces a new parameter "min domain uid" (default to 1000)
    to not accept a UNIX uid below this value.

CVE-2021-44142

    Orange Tsai reported an out-of-bounds heap write vulnerability in
    the VFS module vfs_fruit, which could result in remote execution of
    arbitrary code as root.

CVE-2022-2127

    Out-of-bounds read in winbind AUTH_CRAP.

CVE-2022-3437

    Heimdal des/des3 heap-based buffer overflow.

CVE-2022-32742

    Server memory information leak via SMB1.

CVE-2023-4091

    Client can truncate files even with read-only permissions.
