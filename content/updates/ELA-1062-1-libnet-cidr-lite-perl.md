---
title: "ELA-1062-1 libnet-cidr-lite-perl security update"
package: libnet-cidr-lite-perl
version: 0.21-1+deb9u1 (stretch)
version_map: {"9 stretch": "0.21-1+deb9u1"}
description: "bypass access check based on IP addresses"
date: 2024-03-23T17:28:24+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-47154

---


An issue has been found in libnet-cidr-lite-perl, a module for merging
IPv4 or IPv6 CIDR address ranges.

Extraneous zero characters at the beginning of an IP address string
might allow attackers to bypass access control that is based on IP
addresses.

Please check your application whether it accidentally allows such leading
zero characters (that are normally meant to indicate octal numbers).

