---
title: "ELA-1215-1 python-cryptography security update"
package: python-cryptography
version: 2.6.1-3+deb10u5 (buster)
version_map: {"10 buster": "2.6.1-3+deb10u5"}
description: "bleichenbacher attack"
date: 2024-10-26T23:02:59+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-25659

---

Mitigation for Bleichenbacher attacks on RSA decryption has been added in python-cryptography, a Python library for cryptographic algorithms.
