---
title: "ELA-764-1 leptonlib security update"
package: leptonlib
version: 1.74.1-1+deb9u2 (stretch)
version_map: {"9 stretch": "1.74.1-1+deb9u2"}
description: "fix privilege escalations and disable unsafe debug functions"
date: 2023-01-10T15:03:08+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-18196
  - CVE-2018-3836
  - CVE-2018-7186
  - CVE-2018-7441
  - CVE-2018-7442
  - CVE-2022-38266

---

This update fixes several issues related to unsafe processing of untrusted
input and dealing with predictable paths below /tmp. Part of the affected
functionality is primarily intended for debugging leptonlib. This functionality
has been disabled rather than fixed. It can reenabled by issuing a call to
setLeptDebugOK(1). This change in behaviour was performed upstream and in
Debian 10 and later.

CVE-2017-18196

    Leptonica constructs unintended pathnames (containing duplicated path
    components) when operating on files in /tmp subdirectories, which might
    allow local users to bypass intended file restrictions by leveraging access
    to a directory located deeper within the /tmp directory tree, as
    demonstrated by /tmp/ANY/PATH/ANY/PATH/input.tif.

CVE-2018-3836

    An exploitable command injection vulnerability exists in the
    gplotMakeOutput function. A specially crafted gplot rootname argument can
    cause a command injection resulting in arbitrary code execution. An
    attacker can provide a malicious path as input to an application that
    passes attacker data to this function to trigger this vulnerability.

CVE-2018-7186

    Leptonica does not limit the number of characters in a %s format argument
    to fscanf or sscanf, which allows remote attackers to cause a denial of
    service (stack-based buffer overflow) or possibly have unspecified other
    impact via a long string, as demonstrated by the gplotRead and
    ptaReadStream functions.

CVE-2018-7441

    Leptonica uses hardcoded /tmp pathnames, which might allow local users to
    overwrite arbitrary files or have unspecified other impact by creating
    files in advance or winning a race condition, as demonstrated by
    /tmp/junk_split_image.ps in prog/splitimage2pdf.c.

CVE-2018-7442

    The gplotMakeOutput function does not block '/' characters in the gplot
    rootname argument, potentially leading to path traversal and arbitrary file
    overwrite.

CVE-2022-38266

    An issue in the Leptonica linked library allows attackers to cause an
    arithmetic exception leading to a Denial of Service (DoS) via a crafted
    JPEG file.
