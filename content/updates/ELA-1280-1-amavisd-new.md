---
title: "ELA-1280-1 amavisd-new security update"
package: amavisd-new
version: 1:2.10.1-4+deb9u1 (stretch), 1:2.11.0-6.1+deb10u1 (buster)
version_map: {"9 stretch": "1:2.10.1-4+deb9u1", "10 buster": "1:2.11.0-6.1+deb10u1"}
description: "interpretation conflict"
date: 2024-12-26T20:52:43+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-28054

---

Amavis has an interpretation conflict when there are ambiguous
boundary delimiters in a MIME email message. An attacker can send
crafted emails that avoid checks for banned files or malware.

Amavis now treats such emails as UNCHECKED, and this new behavior can
be configured, see:

 * https://gitlab.com/amavis/amavis/-/blob/v2.12.3/RELEASE_NOTES

 * https://gitlab.com/amavis/amavis/-/blob/v2.12.3/README_FILES/README.CVE-2024-28054
