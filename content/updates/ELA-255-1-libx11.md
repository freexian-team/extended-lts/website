---
title: "ELA-255-1 libx11 security update"
package: libx11
version: 2:1.6.2-3+deb8u3
distribution: "Debian 8 jessie"
description: "heap corruption in the X input method"
date: 2020-08-03T02:03:45+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-14344

---

The X Input Method (XIM) client implementation in libX11 has some
integer overflows and signed/unsigned comparison issues that can
lead to heap corruption when handling malformed messages from an
input method.
