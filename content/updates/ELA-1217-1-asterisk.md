---
title: "ELA-1217-1 asterisk security update"
package: asterisk
version: 1:16.28.0~dfsg-0+deb10u5 (buster)
version_map: { "10 buster": "1:16.28.0~dfsg-0+deb10u5"}
description: "privilege escalation and crash"
date: 2024-10-27T18:41:30+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-42365
  - CVE-2024-42491

---

Two issues have been found in asterisk, an Open Source Private Branch Exchange.

CVE-2024-42365

    Due to a privilege escalation, remote code execution and/or
    blind server-side request forgery with arbitrary protocol are
    possible.

CVE-2024-42491

    Due to bad handling of malformed Contact or Record-Route URI in an
    incoming  SIP request, Asterisk might crash when res_resolver_unbound
    is used.

Thanks to Niels Galjaard, a minor privilege escalation has been fixed. More information about ths can be found at:
https://alioth-lists.debian.net/pipermail/pkg-voip-maintainers/2024-July/038664.html

Please be aware that this fix explicitly sets the gid of the asterisk process to "asterisk".
In case you added the user asterisk to other groups, please update your systemd service file accordingly.
