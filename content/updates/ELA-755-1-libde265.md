---
title: "ELA-755-1 libde265 security update"
package: libde265
version: 1.0.2-2+deb9u1 (stretch)
version_map: {"9 stretch": "1.0.2-2+deb9u1"}
description: "multiple vulnerabilities"
date: 2022-12-16T15:46:57+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-21599
  - CVE-2021-35452
  - CVE-2021-36409
  - CVE-2021-36410
  - CVE-2021-36411

---

Multiple issues were found in libde265, an open source implementation of the
h.265 video codec, which may result in denial of service, or have unspecified
other impact.


CVE-2020-21599

    libde265 v1.0.4 contains a heap buffer overflow in the
    de265_image::available_zscan function, which can be exploited via a crafted
    a file.

CVE-2021-35452

    An Incorrect Access Control vulnerability exists in libde265 v1.0.8 due to
    a SEGV in slice.cc.

CVE-2021-36409

    There is an Assertion `scaling_list_pred_matrix_id_delta==1' failed at
    sps.cc:925 in libde265 v1.0.8 when decoding file, which allows attackers to
    cause a Denial of Service (DoS) by running the application with a crafted
    file or possibly have unspecified other impact.

CVE-2021-36410

    A stack-buffer-overflow exists in libde265 v1.0.8 via fallback-motion.cc in
    function put_epel_hv_fallback when running program dec265.

CVE-2021-36411

    An issue has been found in libde265 v1.0.8 due to incorrect access control.
    A SEGV caused by a READ memory access in function derive_boundaryStrength of
    deblock.cc has occurred. The vulnerability causes a segmentation fault and
    application crash, which leads to remote denial of service.

