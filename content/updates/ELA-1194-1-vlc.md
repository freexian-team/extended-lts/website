---
title: "ELA-1194-1 vlc security update"
package: vlc
version: 3.0.21-0+deb9u1 (stretch), 3.0.21-0+deb10u1 (buster)
version_map: {"9 stretch": "3.0.21-0+deb9u1", "10 buster": "3.0.21-0+deb10u1"}
description: "buffer overflow"
date: 2024-10-03T23:27:44+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-46461

---

A buffer overflow with MMS streams has been fixed by upgrading the VLC media player to the latest upstream version.
