---
title: "ELA-403-1 jackson-databind security update"
package: jackson-databind
version: 2.4.2-2+deb8u16
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-04-14T09:27:03+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-24616
  - CVE-2020-24750
  - CVE-2020-25649
  - CVE-2020-35490
  - CVE-2020-35491
  - CVE-2020-35728
  - CVE-2020-36179
  - CVE-2020-36180
  - CVE-2020-36181
  - CVE-2020-36182
  - CVE-2020-36183
  - CVE-2020-36184
  - CVE-2020-36185
  - CVE-2020-36186
  - CVE-2020-36187
  - CVE-2020-36188
  - CVE-2020-36189
  - CVE-2021-20190

---

Multiple security vulnerabilities were found in Jackson Databind.

CVE-2020-24616

     FasterXML jackson-databind 2.x before 2.9.10.6 mishandles the
     interaction between serialization gadgets and typing, related
     to br.com.anteros.dbcp.AnterosDBCPDataSource (aka Anteros-DBCP).

CVE-2020-24750

    FasterXML jackson-databind 2.x before 2.9.10.6 mishandles the
    interaction between serialization gadgets and typing, related
    to com.pastdev.httpcomponents.configuration.JndiConfiguration.

CVE-2020-25649

    A flaw was found in FasterXML Jackson Databind, where it did not
    have entity expansion secured properly. This flaw allows
    vulnerability to XML external entity (XXE) attacks. The highest
    threat from this vulnerability is data integrity.

CVE-2020-35490

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.apache.commons.dbcp2.datasources.PerUserPoolDataSource.

CVE-2020-35491

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.apache.commons.dbcp2.datasources.SharedPoolDataSource.

CVE-2020-35728

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    com.oracle.wls.shaded.org.apache.xalan.lib.sql.JNDIConnectionPool
    (aka embedded Xalan in org.glassfish.web/javax.servlet.jsp.jstl).

CVE-2020-36179

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to oadd.org.apache.commons.dbcp.cpdsadapter.DriverAdapterCPDS.

CVE-2020-36180

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.apache.commons.dbcp2.cpdsadapter.DriverAdapterCPDS.

CVE-2020-36181

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.apache.tomcat.dbcp.dbcp.cpdsadapter.DriverAdapterCPDS.

CVE-2020-36182

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.apache.tomcat.dbcp.dbcp2.cpdsadapter.DriverAdapterCPDS.

CVE-2020-36183

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.docx4j.org.apache.xalan.lib.sql.JNDIConnectionPool.

CVE-2020-36184

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.tomcat.dbcp.dbcp2.datasources.PerUserPoolDataSource.

CVE-2020-36185

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.tomcat.dbcp.dbcp2.datasources.SharedPoolDataSource.

CVE-2020-36186

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.tomcat.dbcp.dbcp.datasources.PerUserPoolDataSource.

CVE-2020-36187

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.tomcat.dbcp.dbcp.datasources.SharedPoolDataSource.

CVE-2020-36188

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    com.newrelic.agent.deps.ch.qos.logback.core.db.JNDIConnectionSource.

CVE-2020-36189

    FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    com.newrelic.agent.deps.ch.qos.logback.core.db.DriverManagerConnectionSource.

CVE-2021-20190

    A flaw was found in jackson-databind before 2.9.10.7. FasterXML
    mishandles the interaction between serialization gadgets and
    typing. The highest threat from this vulnerability is to data
    confidentiality and integrity as well as system availability.
