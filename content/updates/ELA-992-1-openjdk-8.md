---
title: "ELA-992-1 openjdk-8 security update"
package: openjdk-8
version: 8u392-ga-1~deb8u1 (jessie), 8u392-ga-1~deb9u1 (stretch)
version_map: {"8 jessie": "8u392-ga-1~deb8u1", "9 stretch": "8u392-ga-1~deb9u1"}
description: "multiple vulnerabilities"
date: 2023-10-27T08:41:34+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-22067
  - CVE-2023-22081

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
which may result in authentication bypass, information disclosure or denial
of service.
