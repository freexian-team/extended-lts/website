---
title: "ELA-654-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u7 (jessie), 2:1.19.2-1+deb9u10 (stretch)
version_map: {"8 jessie": "2:1.16.4-1+deb8u7", "9 stretch": "2:1.19.2-1+deb9u10"}
description: "out of bounds write"
date: 2022-07-26T17:23:00+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-2319
  - CVE-2022-2320

---

Jan-Niklas Sohn discovered two out of bound memory writes in X.Org Server's
ProcXkbSetGeometry and ProcXkbSetDeviceInfo Xkb extensions. These issues could
be exploited by an attacker to cause denial of service, privilege escalation
or arbitrary code execution.
