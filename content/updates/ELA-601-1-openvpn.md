---
title: "ELA-601-1 openvpn security update"
package: openvpn
version: 2.3.4-5+deb8u3
distribution: "Debian 8 jessie"
description: "authentication bypass"
date: 2022-04-28T14:10:59+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-12166
  - CVE-2020-15078
  - CVE-2022-0547

---

Several issues were discovered in OpenVPN, a Virtual Private Network server
and client, that could lead to authentication bypass when using deferred
auth plugins.

Note that this upload disables support for multiple deferred auth plugins,
following the upstream fix for CVE-2022-0547.
