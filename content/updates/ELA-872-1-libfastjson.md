---
title: "ELA-872-1 libfastjson security update"
package: libfastjson
version: 0.99.4-1+deb9u1 (stretch)
version_map: {"9 stretch": "0.99.4-1+deb9u1"}
description: "out-of-bounds write"
date: 2023-06-20T19:42:17+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-12762

---

An issue has been found in libfastjson, a fast json library for C.
Due to missing checks, out-of-bounds write might happen when parsing large JSON files.
