---
title: "ELA-1198-1 cups security update"
package: cups
version: 2.2.10-6+deb10u11 (buster)
version_map: {"10 buster": "2.2.10-6+deb10u11"}
description: "stronger validation of input data from external printers"
date: 2024-10-06T18:30:58+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-47175

---

An issue has been found in cups, the Common UNIX Printing System(tm).
This update introduces stronger validations of input data from external printers.

Please be aware that now bugs in the firmware of the printer might be detected. In case of problems, that should appear in the error.log, please update this firmware first.

This ELA also contains an update of CVE-2024-35235, where problems could arise when only domain sockets are used to send data to the printer.
