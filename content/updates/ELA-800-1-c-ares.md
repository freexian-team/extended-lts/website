---
title: "ELA-800-1 c-ares security update"
package: c-ares
version: 1.10.0-2+deb8u4 (jessie), 1.12.0-1+deb9u3 (stretch)
version_map: {"8 jessie": "1.10.0-2+deb8u4", "9 stretch": "1.12.0-1+deb9u3"}
description: "denial of service"
date: 2023-02-19T00:36:28+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-4904

---

It was discovered that in c-ares, an asynchronous name resolver library, the
config_sortlist function is missing checks about the validity of the input
string, which allows a possible arbitrary length stack overflow and thus may
cause a denial of service.
