---
title: "ELA-938-1 rar security update"
package: rar
version: 2:6.23-1~deb9u1 (stretch)
version_map: {"9 stretch": "2:6.23-1~deb9u1"}
description: "arbitrary code execution"
date: 2023-08-29T00:18:44+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-40477

---

A specific flaw within the processing of recovery volumes exists in RAR,
an archive program for rar files. It allows remote attackers to execute
arbitrary code on affected installations. User interaction is required to
exploit this vulnerability. The target must visit a malicious page or open a
malicious rar file.
