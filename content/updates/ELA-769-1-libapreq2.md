---
title: "ELA-769-1 libapreq2 security update"
package: libapreq2
version: 2.13-7~deb9u2 (stretch)
version_map: {"9 stretch": "2.13-7~deb9u2"}
description: "denial of service"
date: 2023-01-14T19:46:27+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-22728

---

A flaw in Apache libapreq2 versions 2.16 and earlier could cause a
buffer overflow while processing multipart form uploads. A remote
attacker could send a request causing a process crash which could lead
to a denial of service attack.
