---
title: "ELA-768-1 viewvc security update"
package: viewvc
version: 1.1.26-1+deb9u1 (stretch)
version_map: {"9 stretch": "1.1.26-1+deb9u1"}
description: "XSS vulnerability"
date: 2023-01-11T10:19:31+00:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-22456
  - CVE-2023-22464

---

It was discovered that there were two issues in *viewvc*, a web-based interface
for browsing Subversion and CVS repositories. The attack vectors involved files
with unsafe names; names that, when embedded into an HTML stream, could cause
the browser to run unwanted code.
