---
title: "ELA-1073-1 expat security update"
package: expat
version: 2.1.0-6+deb8u11 (jessie), 2.2.0-2+deb9u8 (stretch)
version_map: {"8 jessie": "2.1.0-6+deb8u11", "9 stretch": "2.2.0-2+deb9u8"}
description: "denial of service"
date: 2024-04-20T11:05:45+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-52425

---

Expat, an XML parsing C library has been found to have an vulnerability that
allows an attacker to perform a denial of service (resource consumption), when
many full reparsings are required in the case of a large tokens.

When parsing a really big token that requires multiple buffer fills to
complete, expat has to re-parse the token from start multiple times, which
takes time. These patches introduce a heuristic that, when having failed on the
same token multiple times, defers further parsing until there's significantly
more data available.

The patch also introduces an optional API, XML_SetReparseDeferralEnabled() to
disable the new heuristic.
