---
title: "ELA-807-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb8u20 (jessie), 1.1.0l-1~deb9u8 (stretch)
version_map: {"8 jessie": "1.0.1t-1+deb8u20", "9 stretch": "1.1.0l-1~deb9u8"}
description: "multiple vulnerabilities"
date: 2023-02-22T10:25:53+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-0215
  - CVE-2023-0286

---

Multiple vulnerabilities have been discovered in OpenSSL, a Secure
Sockets Layer toolkit, which may result in denial of service or
information disclosure.

Additional details can be found [in the upstream advisory](https://www.openssl.org/news/secadv/20230207.txt).

