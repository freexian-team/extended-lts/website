---
title: "ELA-1188-1 wireshark security update"
package: wireshark
version: 2.6.20-0+deb10u9~deb9u1 (stretch), 2.6.20-0+deb10u9 (buster)
version_map: {"9 stretch": "2.6.20-0+deb10u9~deb9u1", "10 buster": "2.6.20-0+deb10u9"}
description: "multiple vulnerabilities"
date: 2024-09-30T10:42:32+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-0667
  - CVE-2023-3649
  - CVE-2023-4512
  - CVE-2024-0211
  - CVE-2024-2955
  - CVE-2024-4853
  - CVE-2024-4854
  - CVE-2024-8250
  - CVE-2024-8645

---

Multiple vulnerabilities have been fixed in the network traffic analyzer Wireshark.

CVE-2023-0667

    MSMMS dissector buffer overflow

CVE-2023-3649

    iSCSI dissector crash

CVE-2023-4512

    CBOR dissector crash

CVE-2024-0211

    DOCSIS dissector crash

CVE-2024-2955

    T.38 dissector crash

CVE-2024-4853

    Editcap byte chopping crash

CVE-2024-4854

    MONGO dissector infinite loop

CVE-2024-8250

    NTLMSSP dissector crash

CVE-2024-8645

    SPRT dissector crash
