---
title: "ELA-1212-1 samba security update"
package: samba
version: 2:4.2.14+dfsg-0+deb8u16 (jessie)
version_map: {"8 jessie": "2:4.2.14+dfsg-0+deb8u16"}
description: "multiple vulnerabilities"
date: 2024-10-23T12:29:57-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-2124
  - CVE-2021-44142
  - CVE-2022-2127
  - CVE-2022-3437
  - CVE-2022-32742
  - CVE-2023-4091

---

Several vulnerabilities were discovered in Samba, SMB/CIFS file, 
print, and login server for Unix.

CVE-2016-2124

    A flaw was found in the way samba implemented SMB1 authentication. An
    attacker could use this flaw to retrieve the plaintext password sent over
    the wire even if Kerberos authentication was required.

CVE-2021-44142

    Orange Tsai reported an out-of-bounds heap write vulnerability in
    the VFS module vfs_fruit, which could result in remote execution of
    arbitrary code as root.

CVE-2022-2127

    Out-of-bounds read in winbind AUTH_CRAP.

CVE-2022-3437

    Heimdal des/des3 heap-based buffer overflow.

CVE-2022-32742

    Server memory information leak via SMB1.

CVE-2023-4091

    Client can truncate files even with read-only permissions.
