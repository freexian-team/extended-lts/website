---
title: "ELA-201-1 ruby1.9.1 security update"
package: ruby1.9.1
version: 1.9.3.194-8.1+deb7u10
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2019-12-18T18:33:28+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-15845
  - CVE-2019-16201
  - CVE-2019-16254
  - CVE-2019-16255

---

Several vulnerabilities have been discovered in the interpreter for
the Ruby language, which could result in unauthorized access by
bypassing intended path matchings, denial of service, or the execution
of arbitrary code.

* CVE-2019-16255

    Ruby allows code injection if the first argument (aka the
    "command" argument) to Shell#[] or Shell#test in lib/shell.rb is
    untrusted data. An attacker can exploit this to call an arbitrary
    Ruby method.

* CVE-2019-15845

    Ruby mishandles path checking within File.fnmatch functions.

* CVE-2019-16254

    Ruby allows HTTP Response Splitting. If a program using WEBrick
    inserts untrusted input into the response header, an attacker can
    exploit it to insert a newline character to split a header, and
    inject malicious content to deceive clients. NOTE: this issue
    exists because of an incomplete fix for CVE-2017-17742, which
    addressed the CRLF vector, but did not address an isolated CR or
    an isolated LF.

* CVE-2019-16201

    WEBrick::HTTPAuth::DigestAuth in Ruby has a regular expression
    Denial of Service cause by looping/backtracking. A victim must
    expose a WEBrick server that uses DigestAuth to the Internet or a
    untrusted network.
