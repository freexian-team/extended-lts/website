---
title: "ELA-1120-1 linux-5.10 security update"
package: linux-5.10
version: 5.10.218-1~deb8u1 (jessie), 5.10.218-1~deb9u1 (stretch)
version_map: {"8 jessie": "5.10.218-1~deb8u1", "9 stretch": "5.10.218-1~deb9u1"}
description: "linux kernel update"
date: 2024-07-02T10:37:25+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-48655
  - CVE-2023-52585
  - CVE-2023-52882
  - CVE-2024-26900
  - CVE-2024-27398
  - CVE-2024-27399
  - CVE-2024-27401
  - CVE-2024-35848
  - CVE-2024-35947
  - CVE-2024-36017
  - CVE-2024-36031
  - CVE-2024-36883
  - CVE-2024-36886
  - CVE-2024-36889
  - CVE-2024-36902
  - CVE-2024-36904
  - CVE-2024-36905
  - CVE-2024-36916
  - CVE-2024-36919
  - CVE-2024-36929
  - CVE-2024-36933
  - CVE-2024-36934
  - CVE-2024-36939
  - CVE-2024-36940
  - CVE-2024-36941
  - CVE-2024-36946
  - CVE-2024-36950
  - CVE-2024-36953
  - CVE-2024-36954
  - CVE-2024-36957
  - CVE-2024-36959
  - CVE-2024-36960

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.
