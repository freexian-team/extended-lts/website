---
title: "ELA-926-1 opendmarc security update"
package: opendmarc
version: 1.3.2+ds-0+deb9u1 (stretch)
version_map: {"9 stretch": "1.3.2+ds-0+deb9u1"}
description: "authentication bypass"
date: 2023-08-18T02:00:34+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-20790
  - CVE-2020-12272

---

CVE-2019-20790

    OpenDMARC when used with pypolicyd-spf 2.0.2, allows attacks that bypass
    SPF and DMARC authentication in situations where the HELO field is
    inconsistent with the MAIL FROM field.

CVE-2020-12272

    OpenDMARC allows attacks that inject authentication results to provide
    false information about the domain that originated an e-mail message. This
    is caused by incorrect parsing and interpretation of SPF/DKIM
    authentication results, as demonstrated by the example.net(.example.com
    substring.

