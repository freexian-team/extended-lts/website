---
title: "ELA-798-2 sox regression update"
package: sox
version: 14.4.1-5+deb8u6 (jessie), 14.4.1-5+deb9u4 (stretch)
version_map: {"8 jessie": "14.4.1-5+deb8u6", "9 stretch": "14.4.1-5+deb9u4"}
description: "WAV gsm regression fix"
date: 2023-03-20T11:19:41+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

One of the security fixes released as ELA 798 introduced a regression in the
processing WAV files with variable bitrate encoding. Updated sox packages are
available to correct this issue.
