---
title: "ELA-1146-1 openjdk-11 security update"
package: openjdk-11
version: 11.0.24+8-2~deb10u1 (buster)
version_map: {"10 buster": "11.0.24+8-2~deb10u1"}
description: "multiple vulnerabilities"
date: 2024-08-07T09:41:32+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-21131
  - CVE-2024-21138
  - CVE-2024-21140
  - CVE-2024-21144
  - CVE-2024-21145
  - CVE-2024-21147

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
which may result in denial of service, information disclosure or bypass
of Java sandbox restrictions.
