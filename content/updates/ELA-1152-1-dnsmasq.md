---
title: "ELA-1152-1 dnsmasq security update"
package: dnsmasq
version: 2.80-1+deb10u2 (buster)
version_map: {"10 buster": "2.80-1+deb10u2"}
description: "multiple vulnerabilities"
date: 2024-08-17T18:10:21+02:00
draft: false
type: updates
tags:
    - update
cvelist:
    - CVE-2019-14834
    - CVE-2021-3448
    - CVE-2022-0934
    - CVE-2023-28450
---

Multiple vulnerabilities have been fixed in the dnsmasq package, the small caching DNS proxy and DHCP/TFTP server.

### CVE-2019-14834

A vulnerability was found in dnsmasq before version 2.81, where the memory leak allows remote attackers to cause a
denial of service (memory consumption) via vectors involving DHCP response creation.

### CVE-2021-3448

A flaw was found in dnsmasq in versions before 2.85. When configured to use a specific server for a given network
interface, dnsmasq uses a fixed port while forwarding queries. An attacker on the network, able to find the outgoing
port used by dnsmasq, only needs to guess the random transmission ID to forge a reply and get it accepted by
dnsmasq. This flaw makes a DNS Cache Poisoning attack much easier. The highest threat from this vulnerability is to
data integrity.

### CVE-2022-0934

A single-byte, non-arbitrary write/use-after-free flaw was found in dnsmasq. This flaw allows an attacker who sends
a crafted packet processed by dnsmasq, potentially causing a denial of service.

### CVE-2023-28450

An issue was discovered in Dnsmasq before 2.90. The default maximum EDNS.0 UDP packet size was set to 4096 but
should be 1232 because of DNS Flag Day 2020.
