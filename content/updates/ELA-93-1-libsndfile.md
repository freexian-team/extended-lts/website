---
title: "ELA-93-1 libsndfile security update"
package: libsndfile
version: 1.0.25-9.1+deb7u6
distribution: "Debian 7 Wheezy"
description: "heap buffer overflow"
date: 2019-03-13T13:56:58+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-3832

---

It was found that the fix for CVE-2018-19758 was incomplete. That
has been addressed in this update. For completeness, the description
for CVE-2018-19758 follows:

    A heap-buffer-overflow vulnerability was discovered in libsndfile, the
    library for reading and writing files containing sampled sound. This flaw
    might be triggered by remote attackers to cause denial of service (out of
    bounds read and application crash).
