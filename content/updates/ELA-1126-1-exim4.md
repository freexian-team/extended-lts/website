---
title: "ELA-1126-1 exim4 security update"
package: exim4
version: 4.84.2-2+deb8u12 (jessie), 4.89-2+deb9u12 (stretch)
version_map: {"8 jessie": "4.84.2-2+deb8u12", "9 stretch": "4.89-2+deb9u12"}
description: "induce embedded message in pipeline"
date: 2024-07-07T18:36:35+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-51766

---

It was discovered that Exim, a mail transport agent, can be induced to accept a
second message embedded as part of the body of a first message in certain
configurations where PIPELINING or CHUNKING on incoming connections is offered.
