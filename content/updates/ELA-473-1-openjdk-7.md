---
title: "ELA-473-1 openjdk-7 security update"
package: openjdk-7
version: 7u311-2.6.27-0+deb8u1
distribution: "Debian 8 jessie"
description: "sandbox bypass"
date: 2021-08-09T18:39:34+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-2341
  - CVE-2021-2369
  - CVE-2021-2432

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
resulting in bypass of sandbox restrictions.
