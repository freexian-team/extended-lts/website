---
title: "ELA-771-1 libxstream-java security update"
package: libxstream-java
version: 1.4.11.1-1+deb8u6 (jessie), 1.4.11.1-1+deb9u6 (stretch)
version_map: {"8 jessie": "1.4.11.1-1+deb8u6", "9 stretch": "1.4.11.1-1+deb9u6"}
description: "denial of service"
date: 2023-01-16T22:20:47+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-41966

---

XStream serializes Java objects to XML and back again. Versions prior to
this update may allow a remote attacker to terminate the application with a
stack overflow error, resulting in a denial of service only via manipulation of
the processed input stream. The attack uses the hash code implementation for
collections and maps to force recursive hash calculation causing a stack
overflow. This update handles the stack overflow and raises an
InputManipulationException instead.
