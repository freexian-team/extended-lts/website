---
title: "ELA-1149-1 ruby2.3 security update"
package: ruby2.3
version: 2.3.3-1+deb9u12 (stretch)
version_map: {"9 stretch": "2.3.3-1+deb9u12"}
description: "multiple vulnerabilities"
date: 2024-08-13T13:06:50+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-28965
  - CVE-2021-33621
  - CVE-2022-28739
  - CVE-2023-28755
  - CVE-2023-28756
  - CVE-2023-36617
  - CVE-2024-27281
  - CVE-2024-27282

---

Several vulnerabilities have been discovered in the interpreter for
the Ruby language, which may result in denial-of-service (DoS),
information leak, HTTP response splitting, XML round-trip issues, and
remote code execution.

* CVE-2021-28965

    The REXML gem does not properly address XML round-trip issues. An
    incorrect document can be produced after parsing and serializing.

* CVE-2021-33621

    The cgi gem allows HTTP response splitting. This is relevant to
    applications that use untrusted user input either to generate an
    HTTP response or to create a CGI::Cookie object.

* CVE-2022-28739

    Buffer over-read occurs in String-to-Float conversion, including
    Kernel#Float and String#to_f.

* CVE-2023-28755, CVE-2023-36617

    A ReDoS issue was discovered in the URI component. The URI parser
    mishandles invalid URLs that have specific characters. It causes
    an increase in execution time for parsing strings to URI objects.

* CVE-2023-28756

    A ReDoS issue was discovered in the Time component. The Time
    parser mishandles invalid URLs that have specific characters. It
    causes an increase in execution time for parsing strings to Time
    objects.

* CVE-2024-27281

    When parsing .rdoc_options (used for configuration in RDoc) as a
    YAML file, object injection and resultant remote code execution
    are possible because there are no restrictions on the classes that
    can be restored. (When loading the documentation cache, object
    injection and resultant remote code execution are also possible if
    there were a crafted cache.)

* CVE-2024-27282

    If attacker-supplied data is provided to the Ruby regex compiler,
    it is possible to extract arbitrary heap data relative to the
    start of the text, including pointers and sensitive strings.
