---
title: "ELA-546-2 pillow regression update"
package: pillow
version: 2.6.1-2+deb8u8 (jessie)
version_map: {"8 jessie": "2.6.1-2+deb8u8"}
description: "regression update"
date: 2023-02-14T09:27:39+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-28675

---

The patch to address CVE-2021-28675 in Pillow 2.6.1-2+deb8u7 raised
OSError exceptions when processing truncated files. This version has
been updated to raise IOError exceptions instead, which makes Pillow
itself handle the error, making it more transparent to users.
