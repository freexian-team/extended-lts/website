---
title: "ELA-120-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u22
distribution: "Debian 7 Wheezy"
description: "read past allocated buffer"
date: 2019-05-25T12:35:52+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-11036

---

A read past allocated buffer vulnerability was discovered in the PHP5 programming language within the Exif image module.
