---
title: "ELA-985-2 tomcat8 regression update"
package: tomcat8
version: 8.5.54-0+deb9u13 (stretch)
version_map: {"9 stretch": "8.5.54-0+deb9u13"}
description: "HTTP2 protocol regression"
date: 2023-10-17T00:25:04+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

A regression was discovered in the Http2UpgradeHandler class of Tomcat 8
introduced by the patch to fix CVE-2023-44487 (Rapid Reset Attack). A wrong
value for the overheadcount variable forced HTTP2 connections to close early.

