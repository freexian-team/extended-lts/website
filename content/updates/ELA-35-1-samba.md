---
title: "ELA-35-1 samba security update"
package: samba
version: 2:3.6.6-6+deb7u17
distribution: "Debian 7 Wheezy"
description: "memory corruption"
date: 2018-09-14T19:18:55+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-10858
---

Svyatoslav Phirsov discovered that the libsmbclient contains an error that
could allow a malicious server to overwrite client heap memory by returning an
extra long filename in a directory listing.
