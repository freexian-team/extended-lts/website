---
title: "ELA-76-1 apt security update"
package: apt
version: 0.9.7.9+deb7u8
distribution: "Debian 7 Wheezy"
description: "redirect header injection vulnerability"
date: 2019-01-22T21:08:30+00:00
draft: false
type: updates
cvelist:
  - CVE-2019-3462
---

The HTTP redirects handling code did not properly sanitise fields transmitted
over the wire. This vulnerability could be used by an man-in-the-middle
attacker between APT and a mirror to inject malicious content in the HTTP
connection. This content would then be recognised as a valid package by APT and
used later for potential code execution with root privileges on the target
machine.

Since the vulnerability is present in the package manager itself it is
recommended to disable redirects in order to prevent exploitation (during
this upgrade only):

    apt -o Acquire::http::AllowRedirect=false update
    apt -o Acquire::http::AllowRedirect=false upgrade
