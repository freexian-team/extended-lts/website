---
title: "ELA-1327-1 libxml2 security update"
package: libxml2
version: 2.9.1+dfsg1-5+deb8u18 (jessie), 2.9.4+dfsg1-2.2+deb9u12 (stretch), 2.9.4+dfsg1-7+deb10u10 (buster)
version_map: {"8 jessie": "2.9.1+dfsg1-5+deb8u18", "9 stretch": "2.9.4+dfsg1-2.2+deb9u12", "10 buster": "2.9.4+dfsg1-7+deb10u10"}
description: "multiple vulnerabilities"
date: 2025-02-24T18:58:28+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-49043
  - CVE-2023-39615
  - CVE-2023-45322
  - CVE-2024-25062
  - CVE-2024-56171
  - CVE-2025-24928
  - CVE-2025-27113

---

Multiple vulnerabilities have been found in libxml2, a library providing
support to read, modify and write XML and HTML files. These
vulnerabilities could potentially lead to denial of servie or other
unintended behaviors.

CVE-2022-49043

    xmlXIncludeAddNode in xinclude.c in libxml2 before 2.11.0 has a use-after-free.

CVE-2023-39615 (Stretch only)

    libxml2 v2.11.0 was discovered to contain an out-of-bounds read via the xmlSAX2StartElement() function at 
    /libxml2/SAX2.c. This vulnerability allows attackers to cause a Denial of Service (DoS) via supplying a crafted XML file. NOTE: the vendor's position is that the product does not support the legacy SAX1 interface with custom callbacks; there is a crash even without crafted input.

CVE-2023-45322 (Stretch only)

    libxml2 through 2.11.5 has a use-after-free that can only occur after a certain memory allocation fails. This occurs in xmlUnlinkNode in tree.c. NOTE: the vendor's position is "I don't think these issues are critical enough to warrant a CVE ID ... because an attacker typically can't control when memory allocations fail."

CVE-2024-25062 (Stretch only)

    An issue was discovered in libxml2 before 2.11.7 and 2.12.x before 2.12.5. When using the XML Reader interface with DTD validation and XInclude expansion enabled, processing crafted XML documents can lead to an xmlValidatePopElement use-after-free.

CVE-2024-56171 

    libxml2 before 2.12.10 and 2.13.x before 2.13.6 has a use-after-free in xmlSchemaIDCFillNodeTables and xmlSchemaBubbleIDCNodeTables in xmlschemas.c. To exploit this, a crafted XML document must be validated against an XML schema with certain identity constraints, or a crafted XML schema must be used.

CVE-2025-24928

    libxml2 before 2.12.10 and 2.13.x before 2.13.6 has a stack-based buffer overflow in xmlSnprintfElements in valid.c. To exploit this, DTD validation must occur for an untrusted document or untrusted DTD. NOTE: this is similar to CVE-2017-9047. 

CVE-2025-27113

    libxml2 before 2.12.10 and 2.13.x before 2.13.6 has a NULL pointer dereference in xmlPatMatch in pattern.c.

