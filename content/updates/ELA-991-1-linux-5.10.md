---
title: "ELA-991-1 linux-5.10 security update"
package: linux-5.10
version: 5.10.197-1~deb8u1 (jessie), 5.10.197-1~deb9u1 (stretch)
version_map: {"8 jessie": "5.10.197-1~deb8u1", "9 stretch": "5.10.197-1~deb9u1"}
description: "linux kernel update"
date: 2023-10-26T10:08:51+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-4269
  - CVE-2022-39189
  - CVE-2023-1206
  - CVE-2023-1380
  - CVE-2023-2002
  - CVE-2023-2007
  - CVE-2023-2124
  - CVE-2023-2269
  - CVE-2023-2898
  - CVE-2023-3090
  - CVE-2023-3111
  - CVE-2023-3141
  - CVE-2023-3212
  - CVE-2023-3268
  - CVE-2023-3338
  - CVE-2023-3389
  - CVE-2023-3609
  - CVE-2023-3611
  - CVE-2023-3772
  - CVE-2023-3773
  - CVE-2023-3776
  - CVE-2023-3863
  - CVE-2023-4004
  - CVE-2023-4128
  - CVE-2023-4132
  - CVE-2023-4147
  - CVE-2023-4194
  - CVE-2023-4244
  - CVE-2023-4273
  - CVE-2023-4622
  - CVE-2023-4623
  - CVE-2023-4921
  - CVE-2023-20588
  - CVE-2023-21255
  - CVE-2023-21400
  - CVE-2023-31084
  - CVE-2023-34256
  - CVE-2023-34319
  - CVE-2023-35788
  - CVE-2023-35823
  - CVE-2023-35824
  - CVE-2023-40283
  - CVE-2023-42753
  - CVE-2023-42755
  - CVE-2023-42756

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2022-4269

    William Zhao discovered that a flaw in the Traffic Control (TC)
    subsystem when using a specific networking configuration
    (redirecting egress packets to ingress using TC action "mirred"),
    may allow a local unprivileged user to cause a denial of service
    (triggering a CPU soft lockup).

CVE-2022-39189

    Jann Horn discovered that TLB flush operations are mishandled in
    the KVM subsystem in certain KVM_VCPU_PREEMPTED situations, which
    may allow an unprivileged guest user to compromise the guest
    kernel.

CVE-2023-1206

    It was discovered that the networking stack permits attackers to
    force hash collisions in the IPv6 connection lookup table, which
    may result in denial of service (significant increase in the cost
    of lookups, increased CPU utilization).

CVE-2023-1380

    Jisoo Jang reported a heap out-of-bounds read in the brcmfmac
    Wi-Fi driver. On systems using this driver, a local user could
    exploit this to read sensitive information or to cause a denial of
    service.

CVE-2023-2002

    Ruiahn Li reported an incorrect permissions check in the Bluetooth
    subsystem. A local user could exploit this to reconfigure local
    Bluetooth interfaces, resulting in information leaks, spoofing, or
    denial of service (loss of connection).

CVE-2023-2007

    Lucas Leong and Reno Robert discovered a
    time-of-check-to-time-of-use flaw in the dpt_i2o SCSI controller
    driver. A local user with access to a SCSI device using this
    driver could exploit this for privilege escalation.

    This flaw has been mitigated by removing support for the I2OUSRCMD
    operation.

CVE-2023-2124

    Kyle Zeng, Akshay Ajayan and Fish Wang discovered that missing
    metadata validation may result in denial of service or potential
    privilege escalation if a corrupted XFS disk image is mounted.

CVE-2023-2269

    Zheng Zhang reported that improper handling of locking in the
    device mapper implementation may result in denial of service.

CVE-2023-2898

    It was discovered that missing sanitising in the f2fs file system
    may result in denial of service if a malformed file system is
    accessed.

CVE-2023-3090

    It was discovered that missing initialization in ipvlan networking
    may lead to an out-of-bounds write vulnerability, resulting in
    denial of service or potentially the execution of arbitrary code.

CVE-2023-3111

    The TOTE Robot tool found a flaw in the Btrfs filesystem driver
    that can lead to a use-after-free. It's unclear whether an
    unprivileged user can exploit this.

CVE-2023-3141

    A flaw was discovered in the r592 memstick driver that could lead
    to a use-after-free after the driver is removed or unbound from a
    device. The security impact of this is unclear.

CVE-2023-3212

    Yang Lan discovered that missing validation in the GFS2 filesystem
    could result in denial of service via a NULL pointer dereference
    when mounting a malformed GFS2 filesystem.

CVE-2023-3268

    It was discovered that an out-of-bounds memory access in relayfs
    could result in denial of service or an information leak.

CVE-2023-3338

    Davide Ornaghi discovered a flaw in the DECnet protocol
    implementation which could lead to a null pointer dereference or
    use-after-free. A local user can exploit this to cause a denial of
    service (crash or memory corruption) and probably for privilege
    escalation.

    This flaw has been mitigated by removing the DECnet protocol
    implementation.

CVE-2023-3389

    Querijn Voet discovered a use-after-free in the io_uring
    subsystem, which may result in denial of service or privilege
    escalation.

CVE-2023-3609, CVE-2023-3776. CVE-2023-4128

    It was discovered that a use-after-free in the cls_fw, cls_u32,
    cls_route and network classifiers may result in denial of service
    or potential local privilege escalation.

CVE-2023-3611

    It was discovered that an out-of-bounds write in the traffic
    control subsystem for the Quick Fair Queueing scheduler (QFQ) may
    result in denial of service or privilege escalation.

CVE-2023-3772

    Lin Ma discovered a NULL pointer dereference flaw in the XFRM
    subsystem which may result in denial of service.

CVE-2023-3773

    Lin Ma discovered a flaw in the XFRM subsystem, which may result
    in denial of service for a user with the CAP_NET_ADMIN capability
    in any user or network namespace.

CVE-2023-3863

    It was discovered that a use-after-free in the NFC implementation
    may result in denial of service, an information leak or potential
    local privilege escalation.

CVE-2023-4004

    It was discovered that a use-after-free in Netfilter's
    implementation of PIPAPO (PIle PAcket POlicies) may result in
    denial of service or potential local privilege escalation for a
    user with the CAP_NET_ADMIN capability in any user or network
    namespace.

CVE-2023-4132

    A use-after-free in the driver for Siano SMS1xxx based MDTV
    receivers may result in local denial of service.

CVE-2023-4147

    Kevin Rich discovered a use-after-free in Netfilter when adding a
    rule with NFTA_RULE_CHAIN_ID, which may result in local privilege
    escalation for a user with the CAP_NET_ADMIN capability in any
    user or network namespace.

CVE-2023-4194

    A type confusion in the implementation of TUN/TAP network devices
    may allow a local user to bypass network filters.

CVE-2023-4244

    A race condition was found in the nftables subsystem that could
    lead to a use-after-free.  A local user could exploit this to
    cause a denial of service (crash), information leak, or possibly
    for privilege escalation.

CVE-2023-4273

    Maxim Suhanov discovered a stack overflow in the exFAT driver,
    which may result in local denial of service via a malformed file
    system.

CVE-2023-4622

    Bing-Jhong Billy Jheng discovered a use-after-free within the Unix
    domain sockets component, which may result in local privilege
    escalation.

CVE-2023-4623

    Budimir Markovic reported a missing configuration check in the
    sch_hfsc network scheduler that could lead to a use-after-free or
    other problems.  A local user with the CAP_NET_ADMIN capability in
    any user or network namespace could exploit this to cause a denial
    of service (crash or memory corruption) or possibly for privilege
    escalation.

CVE-2023-4921

    "valis" reported flaws in the sch_qfq network scheduler that could
    lead to a use-after-free.  A local user with the CAP_NET_ADMIN
    capability in any user or network namespace could exploit this to
    cause a denial of service (crash or memory corruption) or possibly
    for privilege escalation.

CVE-2023-20588

    Jana Hofmann, Emanuele Vannacci, Cedric Fournet, Boris Koepf and
    Oleksii Oleksenko discovered that on some AMD CPUs with the Zen1
    micro architecture an integer division by zero may leave stale
    quotient data from a previous division, resulting in a potential
    leak of sensitive data.

CVE-2023-21255

    A use-after-free was discovered in the Android binder driver,
    which may result in local privilege escalation on systems where
    the binder driver is loaded.

CVE-2023-21400

    Ye Zhang and Nicolas Wu discovered a double-free in the io_uring
    subsystem, which may result in denial of service or privilege
    escalation.

CVE-2023-31084

    It was discovered that the DVB Core driver does not properly
    handle locking of certain events, allowing a local user to cause a
    denial of service.

CVE-2023-34256

    The syzbot tool found a time-of-check-to-time-of-use flaw in the
    ext4 filesystem driver. An attacker able to mount a disk image or
    device that they can also write to directly could exploit this to
    cause an out-of-bounds read, possibly resulting in a leak of
    sensitive information or denial of service (crash).

CVE-2023-34319

    Ross Lagerwall discovered a buffer overrun in Xen's netback driver
    which may allow a Xen guest to cause denial of service to the
    virtualisation host by sending malformed packets.

CVE-2023-35788

    Hangyu Hua discovered that an off-by-one in the Flower traffic
    classifier may result in local denial of service or the execution
    of privilege escalation.

CVE-2023-35823

    A flaw was discovered in the saa7134 media driver that could lead
    to a use-after-free after the driver is removed or unbound from a
    device. The security impact of this is unclear.

CVE-2023-35824

    A flaw was discovered in the dm1105 media driver that could lead
    to a use-after-free after the driver is removed or unbound from a
    device. The security impact of this is unclear.

CVE-2023-40283

    A use-after-free was discovered in Bluetooth L2CAP socket
    handling.

CVE-2023-42753

    Kyle Zeng discovered an off-by-one error in the netfilter ipset
    subsystem which could lead to out-of-bounds memory access.  A
    local user with the CAP_NET_ADMIN capability in any user or
    network namespace could exploit this to cause a denial of service
    (memory corruption or crash) and possibly for privilege
    escalation.

CVE-2023-42755

    Kyle Zeng discovered missing configuration validation in the
    cls_rsvp network classifier which could lead to out-of-bounds
    reads.  A local user with the CAP_NET_ADMIN capability in any user
    or network namespace could exploit this to cause a denial of
    service (crash) or to leak sensitive information.

    This flaw has been mitigated by removing the cls_rsvp classifier.

CVE-2023-42756

    Kyle Zeng discovered a race condition in the netfiler ipset
    subsystem which could lead to an assertion failure.  A local user
    with the CAP_NET_ADMIN capability in any user or network namespace
    could exploit this to cause a denial of service (crash).

This update additionally fixes Debian bugs
#871216, #1035359, #1036543, #1044518, and #1050622; and includes many
more bug fixes from stable updates 5.10.180-5.10.197 inclusive.
