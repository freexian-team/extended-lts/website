---
title: "ELA-952-1 gsl security update"
package: gsl
version: 1.16+dfsg-2+deb8u1 (jessie), 2.3+dfsg-1+deb9u1 (stretch)
version_map: {"8 jessie": "1.16+dfsg-2+deb8u1", "9 stretch": "2.3+dfsg-1+deb9u1"}
description: "denial of service"
date: 2023-09-22T00:30:37+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-35357

---

A buffer overflow can occur when calculating the quantile value using the
Statistics Library of GSL (GNU Scientific Library). Processing a
maliciously crafted input data for gsl_stats_quantile_from_sorted_data of
the library may lead to unexpected application termination or arbitrary
code execution.
