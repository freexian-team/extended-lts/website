---
title: "ELA-1117-1 gunicorn security update"
package: gunicorn
version: 19.6.0-10+deb9u3 (stretch)
version_map: {"9 stretch": "19.6.0-10+deb9u3"}
description: "request smuggling"
date: 2024-06-29T11:42:09+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-1135

---

Gunicorn, an event-based HTTP/WSGI server, fails to properly validate
Transfer-Encoding headers, leading to HTTP Request Smuggling (HRS)
vulnerabilities. By crafting requests with conflicting Transfer-Encoding
headers, attackers can bypass security restrictions and access restricted
endpoints. This issue is due to Gunicorn's handling of Transfer-Encoding
headers, where it incorrectly processes requests with multiple, conflicting
Transfer-Encoding headers, treating them as chunked regardless of the final
encoding specified. This vulnerability allows for a range of attacks including
cache poisoning, session manipulation, and data exposure.
