---
title: "ELA-971-1 libwebp security update"
package: libwebp
version: 0.5.2-1+deb9u3 (stretch)
version_map: {"9 stretch": "0.5.2-1+deb9u3"}
description: "heap buffer overflow"
date: 2023-09-29T21:41:17-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-4863

---

A buffer overflow in parsing WebP images may result in the execution of arbitrary code.
