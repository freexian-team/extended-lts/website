---
title: "ELA-428-1 graphviz security update"
package: graphviz
version: 2.38.0-7+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-05-13T15:10:11+05:30
draft: false
type: updates
cvelist:
  - CVE-2018-10196
  - CVE-2020-18032

---

CVE-2018-10196

    NULL pointer dereference vulnerability in the rebuild_vlists
    function in lib/dotgen/conc.c in the dotgen library allows
    remote attackers to cause a denial of service (application
    crash) via a crafted file.

CVE-2020-18032

    A buffer overflow was discovered in Graphviz, which could
    potentially result in the execution of arbitrary code when
    processing a malformed file.
