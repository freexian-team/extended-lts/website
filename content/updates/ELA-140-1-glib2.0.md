---
title: "ELA-140-1 glib2.0 security update"
package: glib2.0
version: 2.33.12+really2.32.4-5+deb7u2
distribution: "Debian 7 Wheezy"
description: "insecure permissions"
date: 2019-07-05T23:27:40+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-16428
  - CVE-2018-16429
  - CVE-2019-13012

---

Several flaws were corrected in glib2.0, a general-purpose C library.

CVE-2018-16428

    A NULL pointer dereference may lead to a denial-of-service (application
    crash) when parsing a document.

CVE-2018-16429

    While parsing an invalid string an out-of-bounds read may occur which can
    lead to an access violation error or may have other unspecified impact.

CVE-2019-13012

    The keyfile settings backend in GNOME GLib creates directories and files
    with insecure permissions. This is similar to CVE-2019-12450.
