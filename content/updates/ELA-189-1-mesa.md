---
title: "ELA-189-1 mesa security update"
package: mesa
version: 3.5.25.3-1+deb7u3
distribution: "Debian 7 Wheezy"
description: "shared memory permissions vulnerability"
date: 2019-11-15T13:29:19+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-5068

---

Tim Brown discovered a shared memory permissions vulnerability in the
Mesa 3D graphics library.  Some Mesa X11 drivers use shared-memory
XImages to implement back buffers for improved performance, but Mesa
creates shared memory regions with permission mode 0777.  An attacker
can access the shared memory without any specific permissions.
