---
title: "ELA-890-1 php-cas security update"
package: php-cas
version: 1.3.3-4+deb9u1 (stretch)
version_map: {"9 stretch": "1.3.3-4+deb9u1"}
description: "authentication issues"
date: 2023-07-09T17:55:46+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-1000071
  - CVE-2022-39369

---

Two vulnerabilities has been found in phpCAS, a Central Authentication
Service client library in php, which may allow an attacker to gain
access to a victim's account on a vulnerable CASified service without
victim's knowledge, when the victim visits attacker's website while
being logged in to the same CAS server and an possible 
authentication bypass vulnerability, if used with an vulnerable CAS
server suspectible to XML injection.

The fix for this vulnerabilty requires an API breaking change in php-cas
and will require that software using the library be updated.

The only package depending on php-cas in stretch, package fusiondirectory,
is not supported by ELTS und therefore has not been updated. It will
stop working if configured to use CAS.

For software using php-cas, please see the upstream instructions how to
update this software [1]:

phpCAS now requires an additional service base URL argument when constructing
the client class. It accepts any argument of:

1. A service base URL string. The service URL discovery will always use this
   server name (protocol, hostname and port number) without using any external
   host names.
2. An array of service base URL strings. The service URL discovery will check
   against this list before using the auto discovered base URL. If there is no
   match, the first base URL in the array will be used as the default. This
   option is helpful if your PHP website is accessible through multiple domains
   without a canonical name, or through both HTTP and HTTPS.
3. A class that implements CAS_ServiceBaseUrl_Interface. If you need to
   customize the base URL discovery behavior, you can pass in a class that
   implements the interface.

Constructing the client class is usually done with phpCAS::client().

For example, using the first possiblity:  
`phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);`  
could become:  
`phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context, "https://casified-service.example.org:8080");`



Details of the vulnerabilities:

CVE-2022-39369

    The phpCAS library uses HTTP headers to determine the service URL used
    to validate tickets. This allows an attacker to control the host header
    and use a valid ticket granted for any authorized service in the same
    SSO realm (CAS server) to authenticate to the service protected by
    phpCAS.  Depending on the settings of the CAS server service registry in
    worst case this may be any other service URL (if the allowed URLs are
    configured to "^(https)://.*") or may be strictly limited to known and
    authorized services in the same SSO federation if proper URL service
    validation is applied.

CVE-2017-1000071

    Jasig phpCAS version 1.3.4 is vulnerable to an authentication bypass 
    in the validateCAS20 function when configured to authenticate
    against an old CAS server (which needs to be suspectible to XML tag)

[1] https://github.com/apereo/phpCAS/blob/f3db27efd1f5020e71f2116f637a25cc9dbda1e3/docs/Upgrading#L1C1-L1C1

