---
title: "ELA-1282-1 gst-plugins-base1.0 security update"
package: gst-plugins-base1.0
version: 1.4.4-2+deb8u6 (jessie), 1.10.4-1+deb9u5 (stretch), 1.14.4-2+deb10u4 (buster)
version_map: {"8 jessie": "1.4.4-2+deb8u6", "9 stretch": "1.10.4-1+deb9u5", "10 buster": "1.14.4-2+deb10u4"}
description: "multiple vulnerabilities"
date: 2024-12-28T21:19:33Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-47538
  - CVE-2024-47541
  - CVE-2024-47542
  - CVE-2024-47600
  - CVE-2024-47607
  - CVE-2024-47615
  - CVE-2024-47835

---

gstreamer a multimedia framework was affected by multiple vulnerabilities.

CVE-2024-47538

    A stack-buffer overflow has been detected
    in the `vorbis_handle_identification_packet`
    function within `gstvorbisdec.c`

CVE-2024-47541

    An Out of Bound write vulnerability has been
    identified in the gst_ssa_parse_remove_override_codes
    function of the gstssaparse.c file.

CVE-2024-47542

    A null pointer dereference has been
    discovered in the id3v2_read_synch_uint function, located
    in id3v2.c

CVE-2024-47600

    An Out of Bound read vulnerability has been
    detected in the format_channel_mask function in
    gst-discoverer.c

CVE-2024-47607

    A stack-buffer overflow has been
    detected in the gst_opus_dec_parse_header function
    within `gstopusdec.c'.

CVE-2024-47615

    An Out Of Bound Write has been detected
    in the function gst_parse_vorbis_setup_packet within
    vorbis_parse.c.

CVE-2024-47835

    A null pointer dereference vulnerability
    has been detected in the parse_lrc function within
    gstsubparse.c
