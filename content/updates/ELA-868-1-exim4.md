---
title: "ELA-868-1 exim4 security update"
package: exim4
version: 4.84.2-2+deb8u10 (jessie), 4.89-2+deb9u10 (stretch)
version_map: {"8 jessie": "4.84.2-2+deb8u10", "9 stretch": "4.89-2+deb9u10"}
description: "response injection"
date: 2023-06-12T06:00:54+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-38371

---

A flaw was found in Exim, a Mail Transport Agent (MTA). The STARTTLS feature in
Exim allows response injection (buffering) during MTA SMTP sending. The program
will fail with an appropriate error message if such a behavior is detected now.

