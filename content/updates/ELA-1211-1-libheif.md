---
title: "ELA-1211-1 libheif security update"
package: libheif
version: 1.3.2-2+deb10u2 (buster)
version_map: {"10 buster": "1.3.2-2+deb10u2"}
description: "out-of-bounds read vulnerability"
date: 2024-10-22T16:05:48-07:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-41311

---

It was discovered that there was a potential out-of-bounds read vulnerability
in [libheif](https://github.com/strukturag/libheif), a decoder and encoder for
the HEIF and AVIF image formats.

Insufficient checks in `ImageOverlay::parse()` could have been exploited by an
overlay image with forged offsets which could in turn have led to undefined
behaviour.
