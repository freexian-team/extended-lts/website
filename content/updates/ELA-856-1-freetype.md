---
title: "ELA-856-1 freetype security update"
package: freetype
version: 2.5.2-3+deb8u6 (jessie), 2.6.3-3.2+deb9u3 (stretch)
version_map: {"8 jessie": "2.5.2-3+deb8u6", "9 stretch": "2.6.3-3.2+deb9u3"}
description: "segmentation violation"
date: 2023-05-27T18:18:05+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-27405
  - CVE-2022-27406

---

Two issues have been found in freetype, a FreeType 2 font engine.
Both issues are related to segmentation violations in different functions: ft_open_face_internal() and FT_Request_Size().
