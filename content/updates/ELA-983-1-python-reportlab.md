---
title: "ELA-983-1 python-reportlab security update"
package: python-reportlab
version: 3.3.0-2+deb9u2 (stretch)
version_map: {"9 stretch": "3.3.0-2+deb9u2"}
description: "multiple vulnerabilities"
date: 2023-10-15T11:46:51+01:00
draft: false
type: updates
tags:
- update
cvelist:
- CVE-2019-19450
- CVE-2020-28463
---

Vulnerabilities were found in python-reportlab, a Python library for creating
PDF documents.

### CVE-2019-19450

The `start_unichar` function in paraparser.py was found to evaluate untrusted
user input, which could permit remote code execution.

### CVE-2020-28463

It was discovered that img tags could be used for Server-side Request Forgery
(SSRF).  The issue can be mitigated by using the new `trustedSchemes` and
`trustedHosts` rl_config variables.  See "Inline Images" in ch. 6 of the
reportlab user manual.
