---
title: "ELA-1111-1 pymongo security update"
package: pymongo
version: 2.7.2-1+deb8u1 (jessie), 3.4.0-1+deb9u1 (stretch)
version_map: {"8 jessie": "2.7.2-1+deb8u1", "9 stretch": "3.4.0-1+deb9u1"}
description: "information leakage"
date: 2024-06-17T19:36:53Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-5629

---

An out-of-bounds read in the 'bson' module of PyMongo allowed deserialization of malformed BSON provided by a Server to raise an exception which may contain arbitrary application memory.
