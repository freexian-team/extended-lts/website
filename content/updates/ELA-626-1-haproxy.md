---
title: "ELA-626-1 haproxy security update"
package: haproxy
version: 1.5.8-3+deb8u3
distribution: "Debian 8 jessie"
description: "HTTP request smuggling"
date: 2022-06-15T01:04:24+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-18277

---

Nathan Davison discovered that HAProxy, a load balancing reverse proxy, did not
correctly reject requests or responses featuring a transfer-encoding header
missing the "chunked" value which could facilitate a HTTP request smuggling
attack.

Furthermore two issues have been addressed which never received a final CVE.
There was a risk of reading past the end of a buffer in src/proto_http.c. This
could lead to a denial of service (segmentation fault and application crash)

