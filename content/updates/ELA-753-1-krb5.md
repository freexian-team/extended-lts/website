---
title: "ELA-753-1 krb5 security update"
package: krb5
version: 1.12.1+dfsg-19+deb8u7 (jessie), 1.15-1+deb9u4 (stretch)
version_map: {"8 jessie": "1.12.1+dfsg-19+deb8u7", "9 stretch": "1.15-1+deb9u4"}
description: "integer overflow"
date: 2022-12-08T14:49:07+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-42898

---

It was discovered that there was a potential Denial of Service (DoS)
attack against krb5, a suite of tools implementing the Kerberos
authentication system. An integer overflow in PAC parsing could have
been exploited if a cross-realm entity acted maliciously.
