---
title: "ELA-505-1 tzdata new upstream version"
package: tzdata
version: 2021a-0+deb8u2
distribution: "Debian 8 jessie"
description: "timezone database update"
date: 2021-10-29T12:03:32+02:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2021e. Notable
changes are:

- Fiji suspends DST for the 2021/2022 season.
- Palestine falls back 2021-10-29 (not 2021-10-30) at 01:00.
