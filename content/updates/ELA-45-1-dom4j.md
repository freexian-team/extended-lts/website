---
title: "ELA-45-1 dom4j security update"
package: dom4j
version: 1.6.1+dfsg.3-2+deb7u1
distribution: "Debian 7 Wheezy"
description: "XML Injection vulnerability"
date: 2018-09-24T22:47:30+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-1000632
---

Mario Areias discovered that dom4j, a XML framework for Java, was vulnerable to
a XML injection attack. An attacker able to specify attributes or elements in
the XML document might be able to modify the whole XML document.

This update also removes non-free files from the source package and the
dependency on backport-util-concurrent. It requires the new dependency
libmsv-java though.
