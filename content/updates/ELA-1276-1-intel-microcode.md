---
title: "ELA-1276-1 intel-microcode security update"
package: intel-microcode
version: 3.20241112.1~deb8u1 (jessie), 3.20241112.1~deb9u1 (stretch), 3.20241112.1~deb10u1 (buster)
version_map: {"8 jessie": "3.20241112.1~deb8u1", "9 stretch": "3.20241112.1~deb9u1", "10 buster": "3.20241112.1~deb10u1"}
description: "multiple vulnerabilities"
date: 2024-12-23T20:33:41+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-21820
  - CVE-2024-21853
  - CVE-2024-23918
  - CVE-2024-23984

---

A microcode update has been released for Intel processors, addressing multiple
vulnerabilties which potentially could cause local privileged escalation or
local DoS.

CVE-2024-21820

    Incorrect default permissions in some Intel(R) Xeon(R) processor memory
    controller configurations when using Intel(R) SGX may allow a privileged user
    to potentially enable escalation of privilege via local access.
    (INTEL-SA-01079)

CVE-2024-21853

    Improper finite state machines (FSMs) in the hardware logic in some 4th and 5th
    Generation Intel(R) Xeon(R) Processors may allow an authorized user to
    potentially enable denial of service via local access. (INTEL-SA-01101)

CVE-2024-23918

    Improper conditions check in some Intel(R) Xeon(R) processor memory controller
    configurations when using Intel(R) SGX may allow a privileged user to
    potentially enable escalation of privilege via local access. (INTEL-SA-01079)


CVE-2024-23984 (already adressed in a previous upload, this upload adds more processor models.)

    Observable discrepancy in RAPL interface for some Intel(R) Processors may allow
    a privileged user to potentially enable information disclosure via local
    access.


