---
title: "ELA-1281-1 gstreamer1.0 security update"
package: gstreamer1.0
version: 1.4.4-2+deb8u2 (jessie), 1.10.4-1+deb9u1 (stretch), 1.14.4-1+deb10u1 (buster)
version_map: {"8 jessie": "1.4.4-2+deb8u2", "9 stretch": "1.10.4-1+deb9u1", "10 buster": "1.14.4-1+deb10u1"}
description: "integer overflow"
date: 2024-12-27T10:29:36Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-47606

---

gstreamer a multimedia framework was affected by a vulnerability.

The vulnerability occurs due to an underflow of the `gint` `size` variable, which causes
`size` to hold a large unintended value when cast to an unsigned integer.
This 32-bit negative value is then cast to a 64-bit unsigned integer (0xfffffffffffffffa) in a
call to `gst_buffer_new_and_alloc`.
The function `gst_buffer_new_allocate` then attempts to allocate memory, eventually
calling `_sysmem_new_block`.
The function `_sysmem_new_block` adds alignment and header size to the (unsigned) size,
causing the overflow of the '`slice_size`' variable.
