---
title: "ELA-696-1 linux-4.19 security update"
package: linux-4.19
version: 4.19.260-1~deb8u1 (jessie), 4.19.260-1~deb9u1 (stretch)
version_map: {"8 jessie": "4.19.260-1~deb8u1", "9 stretch": "4.19.260-1~deb9u1"}
description: "linux kernel update"
date: 2022-10-04T09:31:12+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-4159
  - CVE-2021-33655
  - CVE-2021-33656
  - CVE-2022-1462
  - CVE-2022-1679
  - CVE-2022-2153
  - CVE-2022-2318
  - CVE-2022-2586
  - CVE-2022-2588
  - CVE-2022-2663
  - CVE-2022-3028
  - CVE-2022-26365
  - CVE-2022-26373
  - CVE-2022-33740
  - CVE-2022-33741
  - CVE-2022-33742
  - CVE-2022-33744
  - CVE-2022-36879
  - CVE-2022-36946
  - CVE-2022-39188
  - CVE-2022-39842
  - CVE-2022-40307

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to privilege escalation, denial of service or information
leaks.

CVE-2021-4159

    A flaw was found in the eBPF verifier which could lead to an
    out-of-bounds read.  If unprivileged use of eBPF is enabled, this
    could leak sensitive information.  This was already disabled by
    default, which would fully mitigate the vulnerability.

CVE-2021-33655

    A user with access to a framebuffer console device could cause a
    memory out-of-bounds write via the FBIOPUT_VSCREENINFO ioctl.

CVE-2021-33656

    A user with access to a framebuffer console device could cause a
    memory out-of-bounds write via some font setting ioctls.  These
    obsolete ioctls have been removed.

CVE-2022-1462

    一只狗 reported a race condition in the pty (pseudo-terminal)
    subsystem that can lead to a slab out-of-bounds write.  A local
    user could exploit this to cause a denial of service (crash or
    memory corruption) or possibly for privilege escalation.

CVE-2022-1679

    The syzbot tool found a race condition in the ath9k_htc driver
    which can lead to a use-after-free.  This might be exploitable to
    cause a denial service (crash or memory corruption) or possibly
    for privilege escalation.

CVE-2022-2153

    "kangel" reported a flaw in the KVM implementation for x86
    processors which could lead to a null pointer dereference. A local
    user permitted to access /dev/kvm could exploit this to cause a
    denial of service (crash).

CVE-2022-2318

    A use-after-free in the Amateur Radio X.25 PLP (Rose) support may
    result in denial of service.

CVE-2022-2586

    A use-after-free in the Netfilter subsystem may result in local
    privilege escalation for a user with the CAP_NET_ADMIN capability
    in any user or network namespace.

CVE-2022-2588

    Zhenpeng Lin discovered a use-after-free flaw in the cls_route
    filter implementation which may result in local privilege
    escalation for a user with the CAP_NET_ADMIN capability in any
    user or network namespace.

CVE-2022-2663

    David Leadbeater reported flaws in the nf_conntrack_irc
    connection-tracking protocol module.  When this module is enabled
    on a firewall, an external user on the same IRC network as an
    internal user could exploit its lax parsing to open arbitrary TCP
    ports in the firewall, to reveal their public IP address, or to
    block their IRC connection at the firewall.

CVE-2022-3028

    Abhishek Shah reported a race condition in the AF_KEY subsystem,
    which could lead to an out-of-bounds write or read.  A local user
    could exploit this to cause a denial of service (crash or memory
    corruption), to obtain sensitive information, or possibly for
    privilege escalation.

CVE-2022-26365, CVE-2022-33740, CVE-2022-33741, CVE-2022-33742

    Roger Pau Monne discovered that Xen block and network PV device
    frontends don't zero out memory regions before sharing them with
    the backend, which may result in information disclosure.
    Additionally it was discovered that the granularity of the grant
    table doesn't permit sharing less than a 4k page, which may also
    result in information disclosure.

CVE-2022-26373

    It was discovered that on certain processors with Intel's Enhanced
    Indirect Branch Restricted Speculation (eIBRS) capabilities there
    are exceptions to the documented properties in some situations,
    which may result in information disclosure.

    Intel's explanation of the issue can be found at
    https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/post-barrier-return-stack-buffer-predictions.html

CVE-2022-33744

    Oleksandr Tyshchenko discovered that ARM Xen guests can cause a
    denial of service to the Dom0 via paravirtual devices.

CVE-2022-36879

    A flaw was discovered in xfrm_expand_policies in the xfrm
    subsystem which can cause a reference count to be dropped twice.

CVE-2022-36946

    Domingo Dirutigliano and Nicola Guerrera reported a memory
    corruption flaw in the Netfilter subsystem which may result in
    denial of service.

CVE-2022-39188

    Jann Horn reported a race condition in the kernel's handling of
    unmapping of certain memory ranges.  When a driver created a
    memory mapping with the VM_PFNMAP flag, which many GPU drivers do,
    the memory mapping could be removed and freed before it was
    flushed from the CPU TLBs.  This could result in a page use-after-
    free.  A local user with access to such a device could exploit
    this to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.

CVE-2022-39842

    An integer overflow was discovered in the pxa3xx-gcu video driver
    which could lead to a heap out-of-bounds write.

    This driver is not enabled in Debian's official kernel
    configurations.

CVE-2022-40307

    A race condition was discovered in the EFI capsule-loader driver,
    which could lead to use-after-free.  A local user permitted to
    access this device (/dev/efi_capsule_loader) could exploit this to
    cause a denial of service (crash or memory corruption) or possibly
    for privilege escalation.  However, this device is normally only
    accessible by the root user.
