---
title: "ELA-410-1 gst-plugins-good1.0 security update"
package: gst-plugins-good1.0
version: 1.4.4-2+deb8u4
distribution: "Debian 8 jessie"
description: "use-after-free"
date: 2021-04-26T11:55:13+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3497

---

A use-after-free vulnerability was found in the Matroska plugin of the
the GStreamer media framework, which may result in denial of service or
potentially the execution of arbitrary code if a malformed media file
is opened.
