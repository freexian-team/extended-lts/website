---
title: "ELA-1072-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u16 (jessie), 2:1.19.2-1+deb9u19 (stretch)
version_map: {"8 jessie": "2:1.16.4-1+deb8u16", "9 stretch": "2:1.19.2-1+deb9u19"}
description: "multiple vulnerabilities"
date: 2024-04-16T21:40:00+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-31080
  - CVE-2024-31081
  - CVE-2024-31083

---

Multiple vulnerabilities have been fixed in the Xorg X server.

CVE-2024-31080

    Heap buffer overread in ProcXIGetSelectedEvents()

CVE-2024-31081

    Heap buffer overread in ProcXIPassiveGrabDevice()

CVE-2024-31083

    Use-after-free in ProcRenderAddGlyphs()

