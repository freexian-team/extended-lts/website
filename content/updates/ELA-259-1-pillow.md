---
title: "ELA-259-1 pillow security update"
package: pillow
version: 2.6.1-2+deb8u5
distribution: "Debian 8 jessie"
description: "mutiple out-of-bounds reads"
date: 2020-08-08T23:35:05+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-10177

---

It was noticed that in Pillow before 7.1.0, there are multiple
out-of-bounds reads in libImaging/FliDecode.c.
