---
title: "ELA-923-1 libssh security update"
package: libssh
version: 0.6.3-4+deb8u6 (jessie), 0.7.3-2+deb9u4 (stretch)
version_map: {"8 jessie": "0.6.3-4+deb8u6", "9 stretch": "0.7.3-2+deb9u4"}
description: "multiple vulnerabilities"
date: 2023-08-16T19:19:49-04:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-14889
  - CVE-2023-1667

---

Two security issues have been discovered in libssh, a tiny C SSH
library, which may allow a remote authenticated user to cause a denial
of service or inject arbitrary commands.

CVE-2019-14889

    A flaw was found with the libssh API function ssh_scp_new() in
    versions before 0.9.3 and before 0.8.8. When the libssh SCP client
    connects to a server, the scp command, which includes a
    user-provided path, is executed on the server-side. In case the
    library is used in a way where users can influence the third
    parameter of the function, it would become possible for an attacker
    to inject arbitrary commands, leading to a compromise of the remote
    target.

    Note that this CVE was previously fixed in jessie and that it has
    now been fixed in stretch.

CVE-2023-1667

    A NULL pointer dereference was found In libssh during re-keying with
    algorithm guessing. This issue may allow an authenticated client to
    cause a denial of service.

