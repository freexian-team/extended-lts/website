---
title: "ELA-894-1 python-git security update"
package: python-git
version: 2.1.1-2+deb9u1 (stretch)
version_map: {"9 stretch": "2.1.1-2+deb9u1"}
description: "shell injection"
date: 2023-07-15T19:51:20+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-24439

---

python-git, a Python library to interact with Git repositories, is
vulnerable to shell injection due to improper user input validation,
which makes it possible to inject a maliciously crafted remote URL
into the clone command.