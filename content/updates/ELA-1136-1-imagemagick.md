---
title: "ELA-1136-1 imagemagick security update"
package: imagemagick
version: 8:6.9.7.4+dfsg-11+deb9u20 (stretch)
version_map: {"9 stretch": "8:6.9.7.4+dfsg-11+deb9u20"}
description: "multiple vulnerabilities"
date: 2024-07-23T12:30:47Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-11752
  - CVE-2017-12566
  - CVE-2017-18022
  - CVE-2018-11655
  - CVE-2022-48541
  - CVE-2023-1289
  - CVE-2023-5341
  - CVE-2023-34151

---
Imagemagick, an image processing toolking was vulnerable.

CVE-2017-11752

	The ReadMAGICKImage function allows remote attackers to cause
	a denial of service (memory leak) via a crafted file.

CVE-2017-12566

	A memory leak vulnerability was found in the function ReadMVGImage
	in mvg coder, which allows attackers to cause a denial of service.

CVE-2017-18022

	A memory leak vulnerability was found in MontageImageCommand.

CVE-2018-11655

	A memory leak vulnerability was found in the function GetImagePixelCache
	which allows attackers to cause a denial of service via a crafted
	CALS image file.

CVE-2022-48541

	A memory leak in was found that allows a remote attackers to perform
	a denial of service via the "identify -help" command.

CVE-2023-1289

	Loading a specially created SVG file may cause a segmentation fault.
	When ImageMagick crashes, it generates a lot of trash files. These trash
	files can be large if the SVG file contains many render actions, and could
	result in a denial of service.

CVE-2023-5341

	A heap use-after-free flaw was found in coders/bmp.c

CVE-2023-34151

	Undefined behaviors of casting double to size_t in svg, mvg and other
	coders.
