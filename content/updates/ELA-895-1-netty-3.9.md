---
title: "ELA-895-1 netty-3.9 security update"
package: netty-3.9
version: 3.9.9.Final-1+deb9u2 (stretch)
version_map: {"9 stretch": "3.9.9.Final-1+deb9u2"}
description: "information disclosure"
date: 2023-07-16T18:45:29+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-21290

---

It was discovered that there was an insecure temporary file issue that could
have lead to disclosure of arbitrary local files.
