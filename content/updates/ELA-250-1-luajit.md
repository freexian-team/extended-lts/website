---
title: "ELA-250-1 luajit security update"
package: luajit
version: 2.0.3+dfsg-3+deb8u1
distribution: "Debian 8 jessie"
description: "out-of-bounds read"
date: 2020-07-27T16:23:59+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-15890

---

An issue has been found in luajit, a just in time compiler for Lua.

An out-of-bounds read could happen because __gc handler frame traversal is mishandled.
