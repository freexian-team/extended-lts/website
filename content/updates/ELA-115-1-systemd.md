---
title: "ELA-115-1 systemd security update"
package: systemd
version: 44-11+deb7u7
distribution: "Debian 7 Wheezy"
description: "sanitize environment fix in pam_systemd.so"
date: 2019-04-30T16:42:31+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-18078
  - CVE-2019-3842

---

Two vulnerabilities have been addressed in the systemd components
systemd-tmpfiles and pam_systemd.so.

CVE-2017-18078:
    systemd-tmpfiles in systemd attempted to support ownership/permission
    changes on hardlinked files even if the fs.protected_hardlinks sysctl
    is turned off, which allowed local users to bypass intended access
    restrictions via vectors involving a hard link to a file for which
    the user lacked write access.

CVE-2019-3842:
    It was discovered that pam_systemd did not properly sanitize the
    environment before using the XDG_SEAT variable. It was possible for
    an attacker, in some particular configurations, to set a XDG_SEAT
    environment variable which allowed for commands to be checked against
    polkit policies using the "allow_active" element rather than
    "allow_any".
