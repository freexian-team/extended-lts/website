---
title: "ELA-738-1 postgresql-9.4 security update"
package: postgresql-9.4
version: 9.4.26-0+deb8u6
version_map: {"8 jessie": "9.4.26-0+deb8u6"}
description: "arbitrary code execution"
date: 2022-11-23T15:38:53-05:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-2625
  - CVE-2022-1552
---

* CVE-2022-2625

    Sven Klemm found that some extensions in the PostgreSQL database
    system could replace objects not belonging to the extension. An
    attacker could leverage this to run arbitrary commands as another
    user.

* CVE-2022-1552

    Alexander Lakhin discovered that the autovacuum feature and multiple
    commands could escape the "security-restricted operation" sandbox.
