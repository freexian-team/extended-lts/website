---
title: "ELA-1239-1 qtbase-opensource-src security update"
package: qtbase-opensource-src
version: 5.3.2+dfsg-4+deb8u7 (jessie), 5.7.1+dfsg-3+deb9u5 (stretch), 5.11.3+dfsg1-1+deb10u7 (buster)
version_map: {"8 jessie": "5.3.2+dfsg-4+deb8u7", "9 stretch": "5.7.1+dfsg-3+deb9u5", "10 buster": "5.11.3+dfsg1-1+deb10u7"}
description: "multiple vulnerabilities"
date: 2024-11-22T23:43:54+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-24607
  - CVE-2023-32763
  - CVE-2023-33285
  - CVE-2023-34410
  - CVE-2023-37369
  - CVE-2023-38197

---

Multiple vulnerabilities have been fixed in qtbase-opensource-src, the core part of the Qt 5 application framework.

CVE-2023-24607 (jessie)

    Qt SQL ODBC driver DoS

CVE-2023-32763 (jessie)

    Qt SVG buffer overflow

CVE-2023-33285 (jessie)

    QDnsLookup buffer over-read

CVE-2023-34410

    certificate validation for TLS did not always consider whether the root of a chain is a configured CA certificate

CVE-2023-37369 (jessie)

    QXmlStreamReader buffer overflow

CVE-2023-38197 (jessie)

    QXmlStreamReader buffer overflow
