---
title: "ELA-864-1 linux-5.10 security update"
package: linux-5.10
version: 5.10.179-1~deb9u1 (stretch)
version_map: {"9 stretch": "5.10.179-1~deb9u1"}
description: "linux kernel update"
date: 2023-06-07T09:10:18+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-0386
  - CVE-2023-31436
  - CVE-2023-32233

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2023-0386

    It was discovered that under certain conditions the overlayfs
    filesystem implementation did not properly handle copy up
    operations. A local user permitted to mount overlay mounts in user
    namespaces can take advantage of this flaw for local privilege
    escalation.

CVE-2023-31436

    Gwangun Jung reported a a flaw causing heap out-of-bounds
    read/write errors in the traffic control subsystem for the Quick
    Fair Queueing scheduler (QFQ) which may result in information
    leak, denial of service or privilege escalation.

CVE-2023-32233

    Patryk Sondej and Piotr Krysiuk discovered a use-after-free flaw
    in the Netfilter nf_tables implementation when processing batch
    requests, which may result in local privilege escalation for a
    user with the CAP_NET_ADMIN capability in any user or network
    namespace.
