---
title: "ELA-1286-1 sympa security update"
package: sympa
version: 6.2.40~dfsg-1+deb10u2 (buster)
version_map: {"10 buster": "6.2.40~dfsg-1+deb10u2"}
description: "authentication bypass"
date: 2025-01-06T11:15:21+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-55919

---

A flaw was found in Sympa's web interface, a modern mailing list
manager. An attacker may bypass authentication by using an arbitrary e-mail
address when the generic SSO loging feature was enabled.
