---
title: "ELA-986-1 tomcat7 security update"
package: tomcat7
version: 7.0.56-3+really7.0.109-1+deb8u5 (jessie)
version_map: {"8 jessie": "7.0.56-3+really7.0.109-1+deb8u5"}
description: "multiple issues"
date: 2023-10-16T13:44:31+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-42795
  - CVE-2023-45648

---

Two security vulnerabilities have been discovered in the Tomcat servlet and JSP
engine.

CVE-2023-42795

    Information Disclosure. When recycling various internal objects, including
    the request and the response, prior to re-use by the next request/response,
    an error could cause Tomcat to skip some parts of the recycling process
    leading to information leaking from the current request/response to the
    next.

CVE-2023-45648

    Request smuggling. Tomcat did not correctly parse HTTP trailer headers. A
    specially crafted, invalid trailer header could cause Tomcat to treat a
    single request as multiple requests leading to the possibility of request
    smuggling when behind a reverse proxy.
