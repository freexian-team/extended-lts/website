---
title: "ELA-340-1 cairo security update"
package: cairo
version: 1.14.0-2.1+deb8u3
distribution: "Debian 8 jessie"
description: "stack smashing in cairo's composite_boxes"
date: 2020-12-30T20:59:54+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-35492

---

LibreOffice slideshow aborts with stack smashing in cairo's composite_boxes.
