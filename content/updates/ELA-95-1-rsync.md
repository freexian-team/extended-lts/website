---
title: "ELA-95-1 rsync security update"
package: rsync
version: 3.0.9-4+deb7u3
distribution: "Debian 7 Wheezy"
description: "several issues found"
date: 2019-03-24T22:51:02+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-9840
  - CVE-2016-9841
  - CVE-2016-9843

---

Trail of Bits used the automated vulnerability discovery tools developed
for the DARPA Cyber Grand Challenge to audit zlib. As rsync, a fast,
versatile, remote (and local) file-copying tool, uses an embedded copy of
zlib, those issues are also present in rsync.

CVE-2016-9840
     In order to avoid undefined behavior, remove offset pointer
     optimization, as this is not compliant with the C standard.

CVE-2016-9841
     Only use post-increment to be compliant with the C standard.

CVE-2016-9843
     In order to avoid undefined behavior, do not pre-decrement a pointer
     in big-endian CRC calculation, as this is not compliant with the
     C standard.

