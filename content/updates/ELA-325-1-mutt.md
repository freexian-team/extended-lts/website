---
title: "ELA-325-1 mutt security update"
package: mutt
version: 1.5.23-3+deb8u4
distribution: "Debian 8 jessie"
description: "man-in-the-middle attack"
date: 2020-12-01T03:55:30+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-28896

---

In Mutt, a text-based Mail User Agent, invalid IMAP server responses 
were not properly handled, potentially resulting in authentication 
credentials being exposed or man-in-the-middle attacks.
