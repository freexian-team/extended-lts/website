---
title: "ELA-741-1 vim security update"
package: vim
version: 2:7.4.488-7+deb8u9 (jessie)
version_map: {"8 jessie": "2:7.4.488-7+deb8u9"}
description: "multiple memory access violations"
date: 2022-11-25T08:00:34+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-1785
  - CVE-2022-1897
  - CVE-2022-1942
  - CVE-2022-2000
  - CVE-2022-2129
  - CVE-2022-3235
  - CVE-2022-3256

---

This update fixes multiple memory access violations in vim.

CVE-2022-1785

    Out-of-bounds Write

CVE-2022-1897

    Out-of-bounds Write

CVE-2022-1942

    Heap-based Buffer Overflow

CVE-2022-2000

    Out-of-bounds Write

CVE-2022-2129

    Out-of-bounds Write

CVE-2022-3235

    Use After Free

CVE-2022-3256

    Use After Free

