---
title: "ELA-1162-1 suricata security update"
package: suricata
version: 1:4.1.2-2+deb10u2 (buster)
version_map: {"10 buster": "1:4.1.2-2+deb10u2"}
description: "multiple vulnerabilities"
date: 2024-08-27T12:17:40+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-10050
  - CVE-2019-10051
  - CVE-2019-10052
  - CVE-2019-10053
  - CVE-2019-10054
  - CVE-2019-10055
  - CVE-2019-10056
  - CVE-2019-15699
  - CVE-2019-16410
  - CVE-2019-16411
  - CVE-2019-18625
  - CVE-2019-18792
  - CVE-2019-1010279
  - CVE-2021-35063
  - CVE-2021-37592
  - CVE-2024-37151

---

Multiple vulnerabilities have been fixed in intrusion detection system (IDS) and intrusion prevention system (IPS) Suricata.

CVE-2019-10050

    Buffer over-read in DecodeMPLS()

CVE-2019-10051

    Incorrect SMB1 filename parsing

CVE-2019-10052

    Incorrect DHCP parsing

CVE-2019-10053

    Heap overflow in SSHParseBanner()

CVE-2019-10054

    Integer overflow in NFS process_reply_record_v3()

CVE-2019-10055

    Crash in ftp_pasv_response()

CVE-2019-10056

    Crash in DecodeEthernet()

CVE-2019-15699

    Memory overread in TLSDecodeHSHelloExtensions()

CVE-2019-16410

    Memory overread in Defrag4Reassemble()

CVE-2019-16411

    Overread in IPV4OptValidateTimestamp()

CVE-2019-18625

    SYN_SENT RST/FIN injection

CVE-2019-18792

    Reject broken TCP ACK packets

CVE-2019-1010279

    TCP/HTTP detection bypass

CVE-2021-35063

    TCP evasion

CVE-2021-37592

    TCP evasion

CVE-2024-37151

    id reuse can lead to invalid reassembly

