---
title: "ELA-1235-1 unbound security update"
package: unbound
version: 1.9.0-2+deb10u5 (buster)
version_map: {"10 buster": "1.9.0-2+deb10u5"}
description: "multiple vulnerabilities"
date: 2024-11-15T14:02:20+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-8508
  - CVE-2024-43167
  - CVE-2024-43168

---

Multiple vulnerabilities were discovered in unbound, a validating,
recursive, caching DNS resolver.

CVE-2024-8508

    When handling replies with very large RRsets that unbound needs to perform
    name compression for, it can spend a considerable time applying name
    compression to downstream replies, potentially leading to degraded
    performance and eventually denial of service in well orchestrated attacks.

CVE-2024-43167

    A NULL pointer dereference flaw was found in the ub_ctx_set_fwd function in
    Unbound. This issue could allow an attacker who can invoke specific
    sequences of API calls to cause a segmentation fault. When certain API
    functions such as ub_ctx_set_fwd and ub_ctx_resolvconf are called in a
    particular order, the program attempts to read from a NULL pointer,
    leading to a crash. This issue can result in a denial of service by causing
    the application to terminate unexpectedly.

CVE-2024-43168

    A heap-buffer-overflow flaw was found in the cfg_mark_ports function within
    Unbound's config_file.c, which can lead to memory corruption. This issue
    could allow an attacker with local access to provide specially crafted
    input, potentially causing the application to crash or allowing arbitrary
    code execution. This could result in a denial of service or unauthorized
    actions on the system.
