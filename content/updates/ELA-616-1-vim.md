---
title: "ELA-616-1 vim security update"
package: vim
version: 2:7.4.488-7+deb8u6
distribution: "Debian 8 jessie"
description: "buffer overflows"
date: 2022-05-18T16:32:55+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-0261
  - CVE-2022-0351
  - CVE-2022-0413
  - CVE-2022-0443
  - CVE-2022-0572
  - CVE-2022-1154
  - CVE-2022-1616
  - CVE-2022-1619
  - CVE-2022-1621
  - CVE-2022-1720

---

Multiple security vulnerabilities have been discovered in vim, an enhanced
vi editor. Buffer overflows, out-of-bounds reads and use-after-free may
lead to a denial-of-service (application crash) or other unspecified
impact.
