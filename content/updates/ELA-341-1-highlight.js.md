---
title: "ELA-341-1 highlight.js security update"
package: highlight.js
version: 8.2+ds-4+deb8u1
distribution: "Debian 8 jessie"
description: "probable crash of application"
date: 2020-12-30T17:29:26+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-26237

---

An issue has been found in highlight.js, a JavaScript library for syntax highlighting.
If a website or application renders user provided data it might be affected by a Prototype Pollution. This might result in strange behavior or crashes of applications that do not correctly handle unknown properties.
