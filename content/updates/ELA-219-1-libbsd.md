---
title: "ELA-219-1 libbsd security update"
package: libbsd
version: 0.4.2-1+deb7u1
distribution: "Debian 7 Wheezy"
description: "out-of-bounds read vulnerability"
date: 2020-03-26T13:26:23+00:00
draft: false
type: updates
cvelist:
  - CVE-2019-20367
---

An out-of-bounds read vulnerability during string comparisons was discovered in
`libbsd`, a library of functions commonly available on BSD systems but not on
others such as GNU.
