---
title: "ELA-933-1 php5 security update"
package: php5
version: 5.6.40+dfsg-0+deb8u18 (jessie)
version_map: {"8 jessie": "5.6.40+dfsg-0+deb8u18"}
description: "information disclosure and memory corruption"
date: 2023-08-26T21:38:25+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-3823
  - CVE-2023-3824

---

Two security vulnerabilities have been found in PHP, a server-side, HTML-embedded
scripting language.


CVE-2023-3823

    In PHP various XML functions rely on libxml global state to track
    configuration variables, like whether external entities are loaded. This
    state is assumed to be unchanged unless the user explicitly changes it by
    calling appropriate function. However, since the state is process-global,
    other modules - such as ImageMagick - may also use this library within the
    same process, and change that global state for their internal purposes, and
    leave it in a state where external entities loading is enabled. This can
    lead to the situation where external XML is parsed with external entities
    loaded, which can lead to disclosure of any local files accessible to PHP.
    This vulnerable state may persist in the same process across many requests,
    until the process is shut down.

CVE-2023-3824

    In PHP when loading phar file, while reading PHAR directory entries,
    insufficient length checking may lead to a stack buffer overflow, leading
    potentially to memory corruption or RCE.

