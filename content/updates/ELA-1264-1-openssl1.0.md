---
title: "ELA-1264-1 openssl1.0 security update"
package: openssl1.0
version: 1.0.2u-1~deb9u10 (stretch)
version_map: {"9 stretch": "1.0.2u-1~deb9u10"}
description: "multiple vulnerabilities"
date: 2024-12-01T10:32:47+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-5678
  - CVE-2024-0727

---

Multiple vulnerabilities were discovered in OpenSSL, the Secure Sockets Layer
toolkit.

### CVE-2023-5678

A denial of service could occur with excessively long X9.42 DH keys.

### CVE-2024-0727

A denial of service could occur with a null field in a PKCS12 file.
