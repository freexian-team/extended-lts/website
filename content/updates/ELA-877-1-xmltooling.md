---
title: "ELA-877-1 xmltooling security update"
package: xmltooling
version: 1.5.3-2+deb8u5 (jessie)
version_map: {"8 jessie": "1.5.3-2+deb8u5"}
description: "server-side request forgery"
date: 2023-06-23T15:01:59-03:00
draft: false
type: updates
tags:
- update
cvelist:
- CVE-2023-36661
---

Jurien de Jong discovered that the parsing of KeyInfo elements within the
XMLTooling library may result in server-side request forgery.
