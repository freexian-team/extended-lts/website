---
title: "ELA-1082-1 phpmyadmin security update"
package: phpmyadmin
version: 4:4.2.12-2+deb8u12 (jessie)
version_map: {"8 jessie": "4:4.2.12-2+deb8u12"}
description: "SQL injection vulnerability"
date: 2024-04-30T13:26:28+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-22452

---

A potential SQL injection vulnerability was discovered in phpmyadmin, the
popular MySQL web administration tool.

This could have been exploited by a malicious storage engine value.
