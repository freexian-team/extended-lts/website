---
title: "ELA-961-1 linux-5.10 new linux version"
package: linux-5.10
version: 5.10.179-5~deb8u1 (jessie)
version_map: {"8 jessie": "5.10.179-5~deb8u1"}
description: "new linux kernel backport"
date: 2023-09-25T09:39:11+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update introduces Linux kernel 4.19 to Debian 8 jessie. Linux kernel 4.19 is still supported. Instructions on how to
update to 5.10 can be found [in the kernel backports page](../../docs/kernel-backport).
