---
title: "ELA-1088-1 intel-microcode security update"
package: intel-microcode
version: 3.20240312.1~deb8u1 (jessie), 3.20240312.1~deb9u1 (stretch)
version_map: {"8 jessie": "3.20240312.1~deb8u1", "9 stretch": "3.20240312.1~deb9u1"}
description: "microcode update"
date: 2024-05-05T13:19:36+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-22655
  - CVE-2023-28746
  - CVE-2023-38575
  - CVE-2023-39368
  - CVE-2023-43490

---

Intel has released microcode updates, addressing serveral vulnerabilties:

CVE-2023-22655

    Protection mechanism failure in some 3rd and 4th Generation Intel(R)
    Xeon(R) Processors when using Intel(R) SGX or Intel(R) TDX may allow
    a privileged user to potentially enable escalation of privilege via
    local access.

CVE-2023-28746

    Information exposure through microarchitectural state after
    transient execution from some register files for some Intel(R)
    Atom(R) Processors may allow an authenticated user to potentially
    enable information disclosure via local access.

CVE-2023-38575

    Non-transparent sharing of return predictor targets between contexts
    in some Intel(R) Processors may allow an authorized user to
    potentially enable information disclosure via local access.

CVE-2023-39368

    Protection mechanism failure of bus lock regulator for some Intel(R)
    Processors may allow an unauthenticated user to potentially enable
    denial of service via network access.

CVE-2023-43490

    Incorrect calculation in microcode keying mechanism for some
    Intel(R) Xeon(R) D Processors with Intel(R) SGX may allow a
    privileged user to potentially enable information disclosure via
    local access.
