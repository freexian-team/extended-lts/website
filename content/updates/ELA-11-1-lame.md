---
title: "ELA-11-1 lame security update"
package: lame
version: 3.99.5+repack1-3+deb7u2
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities in lame, the MP3 encoder"
date: 2018-07-01T10:38:36-04:00
draft: false
type: updates
cvelist:
  - CVE-2017-9870
  - CVE-2017-9871
  - CVE-2017-9872
  - CVE-2017-15018
  - CVE-2017-15045
  - CVE-2017-15046
---

Multiple vulnerabilities have been discovered in lame:

* CVE-2017-9870

    The III_i_stereo function in layer3.c in mpglib as used in LAME 3.99.5,
    allows remote attackers to cause a denial of service (buffer over-read
    and application crash) via a crafted audio file.

* CVE-2017-9871

    The III_i_stereo function in layer3.c in mpglib as used in LAME 3.99.5
    allows remote attackers to cause a denial of service (stack-based buffer
    overflow and application crash) or possibly have unspecified other impact
    via a crafted audio file.

* CVE-2017-9872

    The III_dequantize_sample function in layer3.c in mpglib as used in LAME
    3.99.5 allows remote attackers to cause a denial of service (stack-based
    buffer overflow and application crash) or possibly have unspecified other
    impact via a crafted audio file.

* CVE-2017-15018

    LAME 3.99.5 is vulnerable to a heap-based buffer over-read when handling a
    malformed file in k_34_4 in vbrquantize.c. Remote attackers might leverage
    this flaw to cause a denial of service or possibly have unspecified other
    impact via a crafted audio file.

* CVE-2017-15045

    LAME 3.99.5 is vulnerable to a heap-based buffer over-read in fill_buffer
    in libmp3lame/util.c, related to lame_encode_buffer_sample_t in
    libmp3lame/lame.c. Remote attackers might leverage this flaw to cause a
    denial of service or possibly have unspecified other impact via a crafted
    audio file.

* CVE-2017-15046

    LAME 3.99.5 is vulnerable to a stack-based buffer overflow in
    unpack_read_samples in frontend/get_audio.c. Remote attackers might leverage
    this flaw to cause a denial of service or possibly have unspecified other
    impact via a crafted audio file.
