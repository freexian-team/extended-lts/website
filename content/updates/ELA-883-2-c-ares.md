---
title: "ELA-883-2 c-ares regression update"
package: c-ares
version: 1.10.0-2+deb8u6 (jessie), 1.12.0-1+deb9u5 (stretch)
version_map: {"8 jessie": "1.10.0-2+deb8u6", "9 stretch": "1.12.0-1+deb9u5"}
description: "regression update"
date: 2023-07-04T21:06:10+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

The previous security update of c-ares, issued as ELA-883-1, causes a regression
on both Jessie and Stretch suites.

This update fixes this regression.
