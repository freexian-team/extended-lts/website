---
title: "ELA-993-1 gst-plugins-bad1.0 security update"
package: gst-plugins-bad1.0
version: 1.4.4-2.1+deb8u6 (jessie), 1.10.4-1+deb9u4 (stretch)
version_map: {"8 jessie": "1.4.4-2.1+deb8u6", "9 stretch": "1.10.4-1+deb9u4"}
description: "multiple vulnerabilities"
date: 2023-10-28T18:52:50+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-40474
  - CVE-2023-40475
  - CVE-2023-40476

---

Multiple vulnerabilities were discovered in plugins for the GStreamer
media framework and its codecs and demuxers, which may result in denial
of service or potentially the execution of arbitrary code if a malformed
media file is opened.
