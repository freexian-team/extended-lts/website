---
title: "ELA-22-1 mutt security update"
package: mutt
version: 1.5.21-6.2+deb7u4
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2018-07-28T05:00:58Z
draft: false
type: updates
cvelist:
  - CVE-2018-14349
  - CVE-2018-14350
  - CVE-2018-14351
  - CVE-2018-14352
  - CVE-2018-14353
  - CVE-2018-14354
  - CVE-2018-14355
  - CVE-2018-14356
  - CVE-2018-14357
  - CVE-2018-14358
  - CVE-2018-14359
  - CVE-2018-14362
---

Several vulnerabilities have been discovered in mutt, a sophisticated
text-based Mail User Agent, resulting in denial of service, stack-based
buffer overflow, arbitrary command execution, and directory traversal
flaws.
