---
title: "ELA-32-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u15
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2018-08-31T12:45:14Z
draft: false
type: updates
cvelist:
  - CVE-2018-14851
  - CVE-2018-14883
---

Two vulnerabilities have been discovered in php5, a server-side,
HTML-embedded scripting language.  One (CVE-2018-14851) results in a
potential denial of service (out-of-bounds read and application crash)
via a crafted JPEG file.  The other (CVE-2018-14883) is an Integer
Overflow that leads to a heap-based buffer over-read.
