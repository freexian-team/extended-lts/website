---
title: "ELA-989-1 dbus security update"
package: dbus
version: 1.10.32-0+deb9u3 (stretch)
version_map: {"9 stretch": "1.10.32-0+deb9u3"}
description: "denial of service"
date: 2023-10-23T15:56:20+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-34969

---

It was found that D-Bus, a simple interprocess messaging system, was
susceptible to a denial of service vulnerability if a monitor was being
run.
