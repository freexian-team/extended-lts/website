---
title: "ELA-1002-1 vim security update"
package: vim
version: 2:7.4.488-7+deb8u11 (jessie), 2:8.0.0197-4+deb9u11 (stretch)
version_map: {"8 jessie": "2:7.4.488-7+deb8u11", "9 stretch": "2:8.0.0197-4+deb9u11"}
description: "multiple vulnerabilities"
date: 2023-11-18T23:52:37+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-4752
  - CVE-2023-4781
  - CVE-2023-5344

---

Multiple vulnerabilities have been fixed in the editor vim.

  CVE-2023-4752
    Heap use after free in ins_compl_get_exp()

  CVE-2023-4781
    Heap buffer-overflow in vim_regsub_both()

  CVE-2023-5344
    Heap buffer-overflow in trunc_string()

