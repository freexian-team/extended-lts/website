---
title: "ELA-826-1 firmware-nonfree security update"
package: firmware-nonfree
version: 20190114+really20220913-0+deb8u1 (jessie), 20190114+really20220913-0+deb9u1 (stretch)
version_map: {"8 jessie": "20190114+really20220913-0+deb8u1", "9 stretch": "20190114+really20220913-0+deb9u1"}
description: "multiple vulnerabilties and firmware updates"
date: 2023-04-02T11:03:46+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-12362
  - CVE-2020-12363
  - CVE-2020-12364
  - CVE-2020-24586
  - CVE-2020-24587
  - CVE-2020-24588
  - CVE-2021-23168
  - CVE-2021-23223
  - CVE-2021-37409
  - CVE-2021-44545
  - CVE-2022-21181

---

The firmware-nonfree package has been updated to include addtional firmware
that may be requested by some drivers in newer Linux kernels.

Some of the updated firmware files adresses security vulnerabilities, which may
allow Escalation of Privileges, Denial of Services and Information Disclosures.

For best support, we recommend to utilize [the backported (Extended) LTS kernels.](https://www.freexian.com/lts/extended/docs/kernel-backport)


CVE-2020-24586 (INTEL-SA-00473)

    The 802.11 standard that underpins Wi-Fi Protected Access (WPA,
    WPA2, and WPA3) and Wired Equivalent Privacy (WEP) doesn't require
    that received fragments be cleared from memory after (re)connecting
    to a network. Under the right circumstances, when another device
    sends fragmented frames encrypted using WEP, CCMP, or GCMP, this can
    be abused to inject arbitrary network packets and/or exfiltrate user
    data.

CVE-2020-24587  (INTEL-SA-00473)

    The 802.11 standard that underpins Wi-Fi Protected Access (WPA,
    WPA2, and WPA3) and Wired Equivalent Privacy (WEP) doesn't require
    that all fragments of a frame are encrypted under the same key. An
    adversary can abuse this to decrypt selected fragments when another
    device sends fragmented frames and the WEP, CCMP, or GCMP encryption
    key is periodically renewed.

CVE-2020-24588  (INTEL-SA-00473)

    The 802.11 standard that underpins Wi-Fi Protected Access (WPA,
    WPA2, and WPA3) and Wired Equivalent Privacy (WEP) doesn't require
    that the A-MSDU flag in the plaintext QoS header field is
    authenticated. Against devices that support receiving non-SSP A-MSDU
    frames (which is mandatory as part of 802.11n), an adversary can
    abuse this to inject arbitrary network packets.

CVE-2021-23168  (INTEL-SA-00621)

    Out of bounds read for some Intel(R) PROSet/Wireless WiFi and
    Killer(TM) WiFi products may allow an unauthenticated user to
    potentially enable denial of service via adjacent access.

CVE-2021-23223 (INTEL-SA-00621)

    Improper initialization for some Intel(R) PROSet/Wireless WiFi and
    Killer(TM) WiFi products may allow a privileged user to potentially
    enable escalation of privilege via local access.

CVE-2021-37409 (INTEL-SA-00621)

    Improper access control for some Intel(R) PROSet/Wireless WiFi and
    Killer(TM) WiFi products may allow a privileged user to potentially
    enable escalation of privilege via local access.

CVE-2021-44545 (INTEL-SA-00621)

    Improper input validation for some Intel(R) PROSet/Wireless WiFi and
    Killer(TM) WiFi products may allow an unauthenticated user to
    potentially enable denial of service via adjacent access.

CVE-2022-21181 (INTEL-SA-00621)

    Improper input validation for some Intel(R) PROSet/Wireless WiFi and
    Killer(TM) WiFi products may allow a privileged user to potentially
    enable escalation of privilege via local access.

The following advisories are also fixed by this upload, but needs an
updated Linux kernel to load the updated firmware:

CVE-2020-12362 (INTEL-SA-00438)

    Integer overflow in the firmware for some Intel(R) Graphics Drivers
    for Windows * before version 26.20.100.7212 and before Linux kernel
    version 5.5 may allow a privileged user to potentially enable an
    escalation of privilege via local access.

CVE-2020-12363 (INTEL-SA-00438)

    Improper input validation in some Intel(R) Graphics Drivers for
    Windows* before version 26.20.100.7212 and before Linux kernel
    version 5.5 may allow a privileged user to potentially enable a
    denial of service via local access.

CVE-2020-12364 (INTEL-SA-00438)

    Null pointer reference in some Intel(R) Graphics Drivers for
    Windows* before version 26.20.100.7212 and before version Linux
    kernel version 5.5 may allow a privileged user to potentially enable
    a denial of service via local access.

