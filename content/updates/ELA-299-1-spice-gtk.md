---
title: "ELA-299-1 spice-gtk security update"
package: spice-gtk
version: 0.25-1+deb8u2
distribution: "Debian 8 jessie"
description: "multiple buffer overflow"
date: 2020-10-12T05:31:20+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-14355

---

Multiple buffer overflow vulnerabilities were found in the QUIC image
decoding process of the SPICE remote display system.

Both the SPICE client (spice-gtk) and server are affected by these flaws.
These flaws allow a malicious client or server to send specially crafted
messages that, when processed by the QUIC image compression algorithm,
result in a process crash or potential code execution.
