---
title: "ELA-338-1 libdatetime-timezone-perl new upstream version"
package: libdatetime-timezone-perl
version: 1:1.75-2+2020e
distribution: "Debian 8 jessie"
description: "update to tzdata 2020e"
date: 2020-12-29T20:50:33+01:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2020e for the
Perl bindings.
