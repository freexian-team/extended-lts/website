---
title: "ELA-543-1 qtsvg-opensource-src security update"
package: qtsvg-opensource-src
version: 5.3.2-2+deb8u1
distribution: "Debian 8 jessie"
description: "out-of-bounds vulnerability"
date: 2022-01-24T02:21:34+05:30
draft: false
type: updates
cvelist:
  - CVE-2018-19869
  - CVE-2021-3481
  - CVE-2021-45930

---

Multiple out-of-bounds error were discovered in qtsvg-opensource-src.
The highest threat from CVE-2021-3481 (at least) is to data
confidentiality the application availability.
