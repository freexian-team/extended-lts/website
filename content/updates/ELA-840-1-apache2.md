---
title: "ELA-840-1 apache2 security update"
package: apache2
version: 2.4.10-10+deb8u25 (jessie), 2.4.25-3+deb9u15 (stretch)
version_map: {"8 jessie": "2.4.10-10+deb8u25", "9 stretch": "2.4.25-3+deb9u15"}
description: "multiple vulnerabilities"
date: 2023-04-30T23:47:57Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2006-20001
  - CVE-2022-36760
  - CVE-2022-37436
  - CVE-2023-25690

---

Several vulnerabilities have been discovered in apache2, a webserver that may be used as front-end proxy for other applications. These vulnerabilities may lead to HTTP request smuggling, and thus to front-end security controls being bypassed.

Unfortunately, fixing these security vulnerabilities may require changes to configuration files. Some out-of-specification RewriteRule directives that were previously silently accepted, are now rejected with error AH10409. For instance, some RewriteRules that included a back-reference and the flags "[L,NC]" will need to be written with extra escaping flags such as "[B= ?,BNP,QSA]".

CVE-2006-20001
	
	A carefully crafted If: request header can cause a memory read, or write of a single zero byte, in a pool (heap) memory location beyond the header value sent. This could cause the process to crash.
	
CVE-2022-36760

	An Inconsistent Interpretation of HTTP Requests ('HTTP Request Smuggling') vulnerability in mod_proxy_ajp of Apache HTTP Server allowed an attacker to smuggle requests to the AJP server it forwards requests to.
	
CVE-2022-37436
	
	A malicious backend can cause the response headers to be truncated early, resulting in some headers being incorporated into the response body. If the later headers have any security purpose, they will not be interpreted by the client.

CVE-2023-25690

    Some mod_proxy configurations allow an HTTP request Smuggling attack. Configurations are affected when mod_proxy is enabled along with some form of RewriteRule or ProxyPassMatch in which a non-specific pattern matches some portion of the user-supplied request-target (URL) data and is then re-inserted into the proxied request-target using variable substitution.
