---
title: "ELA-990-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u12 (jessie), 2:1.19.2-1+deb9u15 (stretch)
version_map: {"8 jessie": "2:1.16.4-1+deb8u12", "9 stretch": "2:1.19.2-1+deb9u15"}
description: "privilege escalation"
date: 2023-10-25T17:42:22+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-5367
  - CVE-2023-5380

---

Jan-Niklas Sohn discovered several vulnerabilities in the Xorg X server,
which may result in privilege escalation if the X server is running
privileged.
