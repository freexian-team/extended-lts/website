---
title: "ELA-1275-1 libpgjava regression update"
package: libpgjava
version: 42.2.5-2+deb10u5 (buster)
version_map: {"10 buster": "42.2.5-2+deb10u5"}
description: "regression update"
date: 2024-12-20T08:17:29+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

A regression in PgResultSet.refreshRow() introduced by the CVE-2022-31197 fix in 42.2.5-2+deb10u2 has been fixed in the PostgreSQL JDBC Driver.
