---
title: "ELA-1291-1 tomcat7 security update"
package: tomcat7
version: 7.0.56-3+really7.0.109-1+deb8u7 (jessie)
version_map: {"8 jessie": "7.0.56-3+really7.0.109-1+deb8u7"}
description: "denial of service"
date: 2025-01-15T00:31:00+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-23672

---

A denial-of-service vulnerability was found in Tomcat 7, a Java based web
server, servlet and JSP engine. It was possible for WebSocket clients to keep
WebSocket connections open leading to increased resource consumption.
