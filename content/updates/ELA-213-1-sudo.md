---
title: "ELA-213-1 sudo security update"
package: sudo
version: 1.8.5p2-1+nmu3+deb7u6
distribution: "Debian 7 Wheezy"
description: "privilege escalation vulnerability"
date: 2020-02-06T21:57:09+00:00
draft: false
type: updates
cvelist:
  - CVE-2019-18634
---

A privilege escalation vulnerability was discovered in sudo, a tool to allow
users to run programs with the security privileges of another user.

If `pwfeedback` was enabled in `/etc/sudoers`, users could trigger a
stack-based buffer overflow in the privileged sudo process.

Note that whilst `pwfeedback` is a default setting in some distributions (eg.
*Linux Mint* and *elementary OS*) it is not the upstream default and thus
should only exist if enabled by an administrator.
