---
title: "ELA-827-1 tomcat7 security update"
package: tomcat7
version: 7.0.56-3+really7.0.109-1+deb8u3 (jessie)
version_map: {"8 jessie": "7.0.56-3+really7.0.109-1+deb8u3"}
description: "information disclosure"
date: 2023-04-10T14:50:52+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-28708

---

A flaw has been found in the Tomcat servlet and JSP engine. When using the
RemoteIpFilter with requests received from a reverse proxy via HTTP that
include the X-Forwarded-Proto header set to https, session cookies created by
Apache Tomcat did not include the secure attribute. This could result in the
user agent transmitting the session cookie over an insecure channel.
