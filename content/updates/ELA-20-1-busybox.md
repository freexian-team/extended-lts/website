---
title: "ELA-20-1 busybox security update"
package: busybox
version: 1:1.20.0-7+deb7u1
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2018-07-22T17:06:56+02:00
draft: false
type: updates
cvelist:
  - CVE-2011-5325
  - CVE-2013-1813
  - CVE-2014-4607
  - CVE-2014-9645
  - CVE-2015-9261
  - CVE-2016-2147
  - CVE-2016-2148
  - CVE-2017-15873
  - CVE-2017-16544
  - CVE-2018-1000517
---

CVE-2011-5325

    A path traversal vulnerability was found in Busybox implementation of tar.
    tar will extract a symlink that points outside of the current working
    directory and then follow that symlink when extracting other files. This
    allows for a directory traversal attack when extracting untrusted tarballs.

CVE-2013-1813

    When device node or symlink in /dev should be created inside 2-or-deeper
    subdirectory (/dev/dir1/dir2.../node), the intermediate directories are
    created with incorrect permissions.

CVE-2014-4607

    An integer overflow may occur when processing any variant of a "literal
    run" in the lzo1x_decompress_safe function. Each of these three locations
    is subject to an integer overflow when processing zero bytes. This exposes
    the code that copies literals to memory corruption.

CVE-2014-9645

    The add_probe function in modutils/modprobe.c in BusyBox allows local users
    to bypass intended restrictions on loading kernel modules via a / (slash)
    character in a module name, as demonstrated by an "ifconfig /usbserial up"
    command or a "mount -t /snd_pcm none /" command.

CVE-2015-9261

    Unziping a specially crafted zip file results in a computation of an
    invalid pointer and a crash reading an invalid address.

CVE-2016-2147

    Integer overflow in the DHCP client (udhcpc) in BusyBox allows remote
    attackers to cause a denial of service (crash) via a malformed
    RFC1035-encoded domain name, which triggers an out-of-bounds heap write.

CVE-2016-2148

    Heap-based buffer overflow in the DHCP client (udhcpc) in BusyBox allows
    remote attackers to have unspecified impact via vectors involving
    OPTION_6RD parsing.

CVE-2017-15873

    The get_next_block function in archival/libarchive/decompress_bunzip2.c in
    BusyBox has an Integer Overflow that may lead to a write access violation.

CVE-2017-16544

    In the add_match function in libbb/lineedit.c in BusyBox, the tab
    autocomplete feature of the shell, used to get a list of filenames in a
    directory, does not sanitize filenames and results in executing any escape
    sequence in the terminal. This could potentially result in code execution,
    arbitrary file writes, or other attacks.

CVE-2018-1000517

    BusyBox project BusyBox wget contains a Buffer Overflow vulnerability in
    Busybox wget that can result in heap buffer overflow. This attack appear to
    be exploitable via network connectivity.
