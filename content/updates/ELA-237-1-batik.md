---
title: "ELA-237-1 batik security update"
package: batik
version: 1.7+dfsg-5+deb8u2
distribution: "Debian 8 jessie"
description: "server-side request forgery"
date: 2020-07-02T12:00:13+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-17566

---

The Apache Batik library can be made to perform arbitrary GET requests
via xlink:href attributes on SVG files. Since there can be legitimate
use cases for xlink:href attributes, this update introduces a new option,
-blockExternalResources, that can be used to prevent fetching external
resources.
