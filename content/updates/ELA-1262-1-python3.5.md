---
title: "ELA-1262-1 python3.5 security update"
package: python3.5
version: 3.5.3-1+deb9u11 (stretch)
version_map: {"9 stretch": "3.5.3-1+deb9u11"}
description: "multiple vulnerabilities"
date: 2024-11-30T21:04:07Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-27043
  - CVE-2024-6232
  - CVE-2024-6923
  - CVE-2024-7592
  - CVE-2024-9287
  - CVE-2024-11168

---

Multiple vulnerabilities were found in python3.5, an interactive high-level
object-oriented language.

CVE-2023-27043:

    The email module of Python
    incorrectly parsed e-mail addresses that contain
    a special character. The wrong portion of an
    RFC2822 header was identified as the value of the addr-spec.
    In some applications, an attacker could bypass a protection
    mechanism in which application access is granted only after
    verifying receipt of e-mail to a specific domain (e.g.,
    only @company.example.com addresses may be used for signup).

CVE-2024-6232:

    Regular expressions that allowed excessive
    backtracking during tarfile.TarFile header parsing were vulnerable
    to ReDoS via specifically-crafted tar archives.

CVE-2024-6923

    The email module didn’t properly quote
    newlines for email headers when serializing an email message,
    allowing for header injection when an email is serialized.

CVE-2024-7592

    When parsing cookies that contained
    backslashes for quoted characters in the cookie value,
    the parser would use an algorithm with quadratic complexity,
    resulting in excess CPU resources being used while parsing
    the value

CVE-2024-9287

    A vulnerability has been found in the `venv`
    module and CLI where path names provided when creating a
    virtual environment were not quoted properly, allowing the
    creator to inject commands into virtual environment "activation"
    scripts (ie "source venv/bin/activate").

CVE-2024-11168

    The urllib.parse.urlsplit() and urlparse()
    functions improperly validated bracketed hosts (`[]`),
    allowing hosts that weren't IPv6 or IPvFuture. This behavior
    was not conformant to RFC 3986 and potentially enabled SSRF
    if a URL is processed by more than one URL parser.

