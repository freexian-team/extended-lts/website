---
title: "ELA-687-1 liblouis security update"
package: liblouis
version: 3.0.0-3+deb9u5 (stretch)
version_map: {"9 stretch": "3.0.0-3+deb9u5"}
description: "buffer overwrites"
date: 2022-09-27T13:41:42+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-26981
  - CVE-2022-31783

---

Two buffer overwrite vulnerabilities were found in liblouis, a braille
translator library, that could cause denial of service or have other
unspecified impact.
