---
title: "ELA-363-2 xterm regression update"
package: xterm
version: 312-2+deb8u2
distribution: "Debian 8 jessie"
description: "regression update"
date: 2021-03-21T14:48:02+05:30
draft: false
type: updates
cvelist:

---

ELA 363-1 backported a part of the upstream patch which fails
to deal with the realloc failures in Debian stretch. This update
reverts that part of the patch since it's not really needed and
just focuses on fixing CVE-2021-27135.
