---
title: "ELA-1010-1 minizip security update"
package: minizip
version: 1.1-8+deb9u1 (stretch)
version_map: {"9 stretch": "1.1-8+deb9u1"}
description: "heap based buffer overflow"
date: 2023-11-27T23:35:50+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-45853

---

An issue has been found in minizip, a compression library.
When using long filenames, an integer overflow might happen, which results in a heap-based buffer overflow in zipOpenNewFileInZip4_64().
