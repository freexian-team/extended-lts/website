---
title: "ELA-808-1 git security update"
package: git
version: 1:2.1.4-2.1+deb8u13 (jessie), 1:2.11.0-3+deb9u10 (stretch)
version_map: {"8 jessie": "1:2.1.4-2.1+deb8u13", "9 stretch": "1:2.11.0-3+deb9u10"}
description: "multiple vulnerabilities"
date: 2023-02-24T14:39:21+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-22490
  - CVE-2023-23946

---

Several vulnerabilities have been discovered in git, a fast, scalable
and distributed revision control system.

CVE-2023-22490

    yvvdwf found a data exfiltration vulnerability while performing a local
    clone from a malicious repository even using a non-local transport.

CVE-2023-23946

    Joern Schneeweisz found a path traversal vulnerbility in git-apply
    that a path outside the working tree can be overwritten as the acting
    user.
