---
title: "ELA-1173-1 postgresql-9.6 security update"
package: postgresql-9.6
version: 9.6.24-0+deb9u7 (stretch)
version_map: {"9 stretch": "9.6.24-0+deb9u7"}
description: "privilege escalation"
date: 2024-09-04T15:42:10-04:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-7348

---

Noah Misch discovered a race condition in the pg\_dump tool included in
PostgreSQL, which may result in privilege escalation.
