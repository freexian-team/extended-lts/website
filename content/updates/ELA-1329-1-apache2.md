---
title: "ELA-1329-1 apache2 security update"
package: apache2
version: 2.4.25-3+deb9u20 (stretch)
version_map: {"9 stretch": "2.4.25-3+deb9u20"}
description: "proxy authentication bypass"
date: 2025-02-25T16:37:18Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-38473

---

apache2 a popular webserver was affected by a vulnerability.

Encoding problem allows request URLs with incorrect encoding to be sent
to backend services, potentially bypassing authentication via crafted
requests.
