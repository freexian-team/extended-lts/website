---
title: "ELA-509-1 bind9 security update"
package: bind9
version: 1:9.9.5.dfsg-9+deb8u23
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-11-02T01:53:43+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-25219

---

Kishore Kumar Kothapalli discovered that the lame server cache in BIND, a DNS
server implementation, can be abused by an attacker to significantly degrade
resolver performance, resulting in denial of service (large delays for
responses for client queries and DNS timeouts on client hosts).
