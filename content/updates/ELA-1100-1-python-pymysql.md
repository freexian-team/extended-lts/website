---
title: "ELA-1100-1 python-pymysql security update"
package: python-pymysql
version: 0.7.10-1+deb9u1 (stretch)
version_map: {"9 stretch": "0.7.10-1+deb9u1"}
description: "SQL injection vulnerability"
date: 2024-05-27T23:39:27+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-36039

---

It was discovered that there was a potential SQL injection attack in
`python-pymysql`, a MySQL client library for Python. This was exploitable when
`python-pymysql` was used with untrusted JSON input as keys were not escaped by
the `escape_dict` routine.
