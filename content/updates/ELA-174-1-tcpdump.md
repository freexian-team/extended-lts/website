---
title: "ELA-174-1 tcpdump security update"
package: tcpdump
version: 4.9.3-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2019-10-11T22:19:14+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-10103
  - CVE-2018-10105
  - CVE-2018-14461
  - CVE-2018-14462
  - CVE-2018-14463
  - CVE-2018-14464
  - CVE-2018-14465
  - CVE-2018-14466
  - CVE-2018-14467
  - CVE-2018-14468
  - CVE-2018-14469
  - CVE-2018-14470
  - CVE-2018-14879
  - CVE-2018-14880
  - CVE-2018-14881
  - CVE-2018-14882
  - CVE-2018-16227
  - CVE-2018-16228
  - CVE-2018-16229
  - CVE-2018-16230
  - CVE-2018-16300
  - CVE-2018-16451
  - CVE-2018-16452
  - CVE-2019-15166

---

Several vulnerabilities have been discovered in tcpdump, a command-line network
traffic analyzer. These security vulnerabilities might result in denial of
service or, potentially, execution of arbitrary code.
