---
title: "ELA-891-1 nsis security update"
package: nsis
version: 2.51-1+deb9u1
version_map: {"9 stretch": "2.51-1+deb9u1"}
description: "mishandling of directory access control"
date: 2023-07-11T09:00:27+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-37378
---

It was discovered that the Nullsoft Scriptable Install System (NSIS)
before version 3.09 mishandles access control for the uninstaller
directory.
