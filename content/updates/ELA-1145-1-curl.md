---
title: "ELA-1145-1 curl security update"
package: curl
version: 7.38.0-4+deb8u28 (jessie), 7.52.1-5+deb9u22 (stretch), 7.64.0-4+deb10u10 (buster)
version_map: {"8 jessie": "7.38.0-4+deb8u28", "9 stretch": "7.52.1-5+deb9u22", "10 buster": "7.64.0-4+deb10u10"}
description: "denial of service"
date: 2024-08-05T21:13:04+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-7264

---

A denial-of-service vulnerability was found in cURL, an easy-to-use client-side
URL transfer library. libcurl's ASN1 parser code has the GTime2str() function,
used for parsing an ASN.1 Generalized Time field. If given an syntactically
incorrect field, the parser might end up crashing but this flaw can also lead
to heap contents getting returned to the application when CURLINFO_CERTINFO is
used.
