---
title: "ELA-1058-1 kde4libs security update"
package: kde4libs
version: 4:4.14.26-2+deb9u1 (stretch)
version_map: {"9 stretch": "4:4.14.26-2+deb9u1"}
description: "arbitrary code execution"
date: 2024-03-19T02:19:15+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-14744

---

Dominik Penner discovered a flaw in how KConfig interpreted shell commands
in desktop files and other configuration files. An attacker may trick users
into installing specially crafted files which could then be used to execute
arbitrary code, e.g. a file manager trying to find out the icon for a file
or any application using KConfig. Thus the entire feature of supporting
shell commands in KConfig entries has been removed.
