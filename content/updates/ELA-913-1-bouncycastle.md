---
title: "ELA-913-1 bouncycastle security update"
package: bouncycastle
version: 1.56-1+deb9u4 (stretch)
version_map: {"9 stretch": "1.56-1+deb9u4"}
description: "LDAP injection vulnerability"
date: 2023-08-02T18:40:10Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-33201

---

Bouncy Castle is a collection of APIs used in cryptography. It includes APIs for both the Java and the C# programming languages.

Bouncy Castle was vulnerable due to a LDAP injection in X509 certificates handling.  The vulnerability only affects applications that use an LDAP CertStore from Bouncy Castle to validate X.509 certificates. During the certificate validation process, Bouncy Castle inserts the certificate's Subject Name into an LDAP search filter without any escaping, which leads to an LDAP injection vulnerability.
