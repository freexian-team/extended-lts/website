---
title: "ELA-846-1 openimageio security update"
package: openimageio
version: 1.4.14~dfsg0-1+deb8u1 (jessie), 1.6.17~dfsg0-1+deb9u1 (stretch)
version_map: {"8 jessie": "1.4.14~dfsg0-1+deb8u1", "9 stretch": "1.6.17~dfsg0-1+deb9u1"}
description: "denial of service"
date: 2023-05-03T11:53:26+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-36354
  - CVE-2022-41838
  - CVE-2022-41999
  - CVE-2022-43592
  - CVE-2022-43593
  - CVE-2022-43594
  - CVE-2022-43595
  - CVE-2022-43596
  - CVE-2022-43597
  - CVE-2022-43598
  - CVE-2022-43599
  - CVE-2022-43600
  - CVE-2022-43601
  - CVE-2022-43602
  - CVE-2022-43603

---

Multiple security vulnerabilities have been discovered in OpenImageIO, a
library for reading and writing images. Buffer overflows and out-of-bounds read
and write programming errors may lead to a denial of service (application
crash) or the execution of arbitrary code if a malformed image file is
processed.
