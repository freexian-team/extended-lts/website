---
title: "ELA-306-1 openldap security update"
package: openldap
version: 2.4.40+dfsg-1+deb8u7
distribution: "Debian 8 jessie"
description: "slapd normalization handling"
date: 2020-11-03T11:42:28+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-25692

---

A vulnerability in the handling of normalization with modrdn was
discovered in OpenLDAP, a free implementation of the Lightweight
Directory Access Protocol. An unauthenticated remote attacker can
use this flaw to cause a denial of service (slapd daemon crash)
via a specially crafted packet.
