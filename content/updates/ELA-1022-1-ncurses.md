---
title: "ELA-1022-1 ncurses security update"
package: ncurses
version: 5.9+20140913-1+deb8u6 (jessie), 6.0+20161126-1+deb9u5 (stretch)
version_map: {"8 jessie": "5.9+20140913-1+deb8u6", "9 stretch": "6.0+20161126-1+deb9u5"}
description: "setuid/setgid mitigation"
date: 2023-12-18T15:29:43+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-29491

---

Loading of custom terminfo entries in setuid/setgid programs has been disabled to mitigate memory corruption via malformed data in terminfo database files.
