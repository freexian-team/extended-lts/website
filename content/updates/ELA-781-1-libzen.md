---
title: "ELA-781-1 libzen security update"
package: libzen
version: 0.4.34-1+deb9u1 (stretch)
version_map: {"9 stretch": "0.4.34-1+deb9u1"}
description: "null pointer dereference"
date: 2023-01-29T01:16:40+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-36646

---

Crafted arguments to a function could lead to an unchecked return value
and a null pointer dereference.
