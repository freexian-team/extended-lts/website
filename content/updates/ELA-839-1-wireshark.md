---
title: "ELA-839-1 wireshark security update"
package: wireshark
version: 2.6.20-0+deb9u5 (stretch)
version_map: {"9 stretch": "2.6.20-0+deb9u5"}
description: "multiple vulnerabilities"
date: 2023-04-29T23:41:05+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-1161
  - CVE-2023-1992
  - CVE-2023-1993
  - CVE-2023-1994

---

Several vulnerabilities were fixed in the network traffic analyzer Wireshark.

CVE-2023-1161

    ISO 15765 dissector crash

CVE-2023-1992

    RPCoRDMA dissector crash

CVE-2023-1993

    LISP dissector large loop vulnerability

CVE-2023-1994

    GQUIC dissector crash

