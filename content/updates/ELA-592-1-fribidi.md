---
title: "ELA-592-1 fribidi security update"
package: fribidi
version: 0.19.6-3+deb8u1
distribution: "Debian 8 jessie"
description: "several buffer overflows"
date: 2022-04-10T12:30:08+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-25308
  - CVE-2022-25309
  - CVE-2022-25310

---

Several issues have been found in fribidi, a free Implementation of the Unicode BiDi algorithm.
The issues are related to stack-buffer-overflow, heap-buffer-overflow, and a SEGV.

CVE-2022-25308
     stack-buffer-overflow issue in main()

CVE-2022-25309
     heap-buffer-overflow issue in fribidi_cap_rtl_to_unicode()

CVE-2022-25310
     SEGV issue in fribidi_remove_bidi_marks()
