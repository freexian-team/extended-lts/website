---
title: "ELA-789-1 openjdk-8 security update"
package: openjdk-8
version: 8u362-ga-1~deb8u1 (jessie), 8u362-ga-1~deb9u1 (stretch)
version_map: {"8 jessie": "8u362-ga-1~deb8u1", "9 stretch": "8u362-ga-1~deb9u1"}
description: "sandbox bypass"
date: 2023-01-31T16:15:46+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-21830
  - CVE-2023-21843

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
resulting in bypass of sandbox restrictions.
