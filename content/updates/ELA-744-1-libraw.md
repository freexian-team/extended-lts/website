---
title: "ELA-744-1 libraw security update"
package: libraw
version: 0.16.0-9+deb8u5 (jessie)
version_map: {"8 jessie": "0.16.0-9+deb8u5"}
description: "multiple memory access violations"
date: 2022-11-29T10:18:39+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-13735
  - CVE-2017-14265
  - CVE-2017-14608
  - CVE-2017-16909
  - CVE-2017-16910
  - CVE-2018-5804
  - CVE-2018-5805
  - CVE-2018-5806
  - CVE-2018-5813
  - CVE-2018-10528
  - CVE-2018-10529
  - CVE-2020-35530
  - CVE-2020-35531
  - CVE-2020-35532
  - CVE-2020-35533

---

This update fixes multiple memory access violations.

CVE-2017-13735

    There is a floating point exception in the kodak_radc_load_raw
    function in dcraw_common.cpp. It will lead to a remote denial of
    service attack.

CVE-2017-14265

    A Stack-based Buffer Overflow was discovered in xtrans_interpolate in
    internal/dcraw_common.cpp. It could allow a remote denial of service
    or code execution attack.

CVE-2017-14608

    An out of bounds read flaw related to kodak_65000_load_raw has been
    reported in dcraw/dcraw.c and internal/dcraw_common.cpp. An attacker
    could possibly exploit this flaw to disclose potentially sensitive
    memory or cause an application crash.

CVE-2017-16909

    An error related to the "LibRaw::panasonic_load_raw()" function
    (dcraw_common.cpp) can be exploited to cause a heap-based buffer
    overflow and subsequently cause a crash via a specially crafted TIFF
    image.

CVE-2017-16910

    An error within the "LibRaw::xtrans_interpolate()" function
    (internal/dcraw_common.cpp) can be exploited to cause an invalid read
    memory access and subsequently a Denial of Service condition.

CVE-2018-10528

    There is a stack-based buffer overflow in the utf2char function in
    libraw_cxx.cpp.

CVE-2018-10529

    There is an out-of-bounds read affecting the X3F property table list
    implementation in libraw_x3f.cpp and libraw_cxx.cpp.

CVE-2018-5804

    A type confusion error within the "identify()" function
    (internal/dcraw_common.cpp) can be exploited to trigger a division by
    zero.

CVE-2018-5805

    A boundary error within the "quicktake_100_load_raw()" function
    (internal/dcraw_common.cpp) can be exploited to cause a stack-based
    buffer overflow and subsequently cause a crash.

CVE-2018-5806

    An error within the "leaf_hdr_load_raw()" function
    (internal/dcraw_common.cpp) can be exploited to trigger a NULL pointer
    dereference.

CVE-2018-5813

    An error within the "parse_minolta()" function (dcraw/dcraw.c) can be
    exploited to trigger an infinite loop via a specially crafted file.

CVE-2020-35530

    There is an out-of-bounds write vulnerability within the "new_node()"
    function (libraw\src\x3f\x3f_utils_patched.cpp) that can be triggered
    via a crafted X3F file.

CVE-2020-35531

    An out-of-bounds read vulnerability exists within the get_huffman_diff()
    function (libraw\src\x3f\x3f_utils_patched.cpp) when reading data from
    an image file.

CVE-2020-35532

    An out-of-bounds read vulnerability exists within the
    "simple_decode_row()" function (libraw\src\x3f\x3f_utils_patched.cpp)
    which can be triggered via an image with a large row_stride field.

CVE-2020-35533

    An out-of-bounds read vulnerability exists within the
    "LibRaw::adobe_copy_pixel()" function (libraw\src\decoders\dng.cpp)
    when reading data from the image file.
