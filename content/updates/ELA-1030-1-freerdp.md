---
title: "ELA-1030-1 freerdp security update"
package: freerdp
version: 1.1.0~git20140921.1.440916e+dfsg1-13+deb9u6 (stretch)
version_map: {"9 stretch": "1.1.0~git20140921.1.440916e+dfsg1-13+deb9u6"}
description: "multiple vulnerabilities"
date: 2024-01-17T11:04:38+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-11524
  - CVE-2022-39282
  - CVE-2022-39318
  - CVE-2022-39319
  - CVE-2022-39347
  - CVE-2022-41877
  - CVE-2023-39353
  - CVE-2023-39354
  - CVE-2023-39356
  - CVE-2023-40188

---

Multiple vulnerabilities have been found in freerdp2, a free
implementation of the Remote Desktop Protocol (RDP). An attacker
(e.g. through a malicious RDP server) could launch DoS
(denial-of-service) attacks through multiple vectors typically
crashing the client, exploit buffer overflows that could lead to
command execution, or access files outside of a shared directory.

This update also fixes two regressions related to the CVE-2020-11096
and CVE-2020-11089 fixes in ELA-717-1.

* CVE-2020-11524

    libfreerdp/codec/interleaved.c has an Out-of-bounds Write.

* CVE-2022-39282

    FreeRDP based clients on unix systems using `/parallel` command
    line switch might read uninitialized data and send it to the
    server the client is currently connected to.

* CVE-2022-39318

    Missing input validation in `urbdrc` channel. A malicious server
    can trick a FreeRDP based client to crash with division by zero.

* CVE-2022-39319

    Missing input length validation in the `urbdrc` channel. A
    malicious server can trick a FreeRDP based client to read out of
    bound data and send it back to the server.

* CVE-2022-39347

    missing path canonicalization and base path check for `drive`
    channel. A malicious server can trick a FreeRDP based client to
    read files outside the shared directory.

* CVE-2022-41877

    Missing input length validation in `drive` channel. A malicious
    server can trick a FreeRDP based client to read out of bound data
    and send it back to the server.

* CVE-2023-39353

    Missing offset validation leading to Out Of Bound Read. In the
    `libfreerdp/codec/rfx.c` file there is no offset validation in
    `tile->quantIdxY`, `tile->quantIdxCb`, and `tile->quantIdxCr`. As
    a result crafted input can lead to an out of bounds read access
    which in turn will cause a crash.

* CVE-2023-39354

    Out-Of-Bounds Read in the `nsc_rle_decompress_data` function. The
    Out-Of-Bounds Read occurs because it processes `context->Planes`
    without checking if it contains data of sufficient length. Should
    an attacker be able to leverage this vulnerability they may be
    able to cause a crash.

* CVE-2023-39356

    Missing offset validation may lead to an Out Of Bound Read in the
    function `gdi_multi_opaque_rect`. In particular there is no code
    to validate if the value `multi_opaque_rect->numRectangles` is
    less than 45. Looping through `multi_opaque_rect->`numRectangles
    without proper boundary checks can lead to Out-of-Bounds Read
    errors which will likely lead to a crash.

* CVE-2023-40188

    Out-Of-Bounds Read in the `nsc_rle_decode` function. This
    Out-Of-Bounds Read occurs because processing is done on the `in`
    variable without checking if it contains data of sufficient
    length. Insufficient data for the `in` variable may cause errors
    or crashes.
