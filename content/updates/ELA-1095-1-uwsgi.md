---
title: "ELA-1095-1 uwsgi security update"
package: uwsgi
version: 2.0.14+20161117-3+deb9u7 (stretch)
version_map: {"9 stretch": "2.0.14+20161117-3+deb9u7"}
description: "HTTP Response splitting"
date: 2024-05-19T09:01:10Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-24795

---

uWSGI, a Web Server Gateway Interface that mainly interfaces between a web server and a python application, allowed an attacker that can inject malicious response headers into backend applications to cause an HTTP desynchronization attack.
