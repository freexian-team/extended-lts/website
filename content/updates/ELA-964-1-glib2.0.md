---
title: "ELA-964-1 glib2.0 security update"
package: glib2.0
version: 2.42.1-1+deb8u6 (jessie), 2.50.3-2+deb9u5 (stretch)
version_map: {"8 jessie": "2.42.1-1+deb8u6", "9 stretch": "2.50.3-2+deb9u5"}
description: "multiple security issues"
date: 2023-09-25T17:05:47-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-29499
  - CVE-2023-32611
  - CVE-2023-32665

---

Several security vulnerabilities were found in GLib, a general-purpose utility
library, used by projects such as GTK+, GIMP, and GNOME.

CVE-2023-29499

    GVariant deserialization fails to validate that the input conforms to the
    expected format, leading to denial of service.

CVE-2023-32611

    GVariant deserialization is vulnerable to a slowdown issue where a crafted
    GVariant can cause excessive processing, leading to denial of service.

CVE-2023-32665

    GVariant deserialization is vulnerable to an exponential blowup issue where
    a crafted GVariant can cause excessive processing, leading to denial of
    service.
