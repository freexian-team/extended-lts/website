---
title: "ELA-855-1 bzip2 security update"
package: bzip2
version: 1.0.6-8.1+deb9u1 (stretch)
version_map: {"9 stretch": "1.0.6-8.1+deb9u1"}
description: "out-of-bounds write"
date: 2023-05-26T16:34:10-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-12900

---

A vulnerability has been fixed in bzip2, a high-quality block-sorting file
compressor. CVE-2019-12900 is a out-of-bounds write when using a crafted
compressed file.

This vulnerability was fixed in Debian Jessie, with bzip2 version
1.0.6-4+deb7u1
