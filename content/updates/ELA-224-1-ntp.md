---
title: "ELA-224-1 ntp security update"
package: ntp
version: 1:4.2.6.p5+dfsg-2+deb7u8
distribution: "Debian 7 Wheezy"
description: "denial-of-service vulnerability"
date: 2020-04-23T11:28:31+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-11868
---

A Denial of Service (DoS) vulnerability was discovered in the network time
protocol server/client, ntp.

ntp allowed an "off-path" attacker to block unauthenticated synchronisation via
a server mode packet with a spoofed source IP address because transmissions
were rescheduled even if a packet lacked a valid "origin timestamp".
