---
title: "ELA-3-1 file security update"
package: file
version: 5.11-2+deb7u10
distribution: "Debian 7 Wheezy"
description: "denial of service"
date: 2018-06-21T08:23:59+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-10360
---

do_core_note() in readelf.c allows remote attackers to cause a denial of service (out-of-bounds read and application crash) via a crafted ELF file.
