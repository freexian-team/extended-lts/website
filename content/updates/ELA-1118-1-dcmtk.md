---
title: "ELA-1118-1 dcmtk security update"
package: dcmtk
version: 3.6.1~20160216-4+deb10u1 (stretch)
version_map: {"9 stretch": "3.6.1~20160216-4+deb10u1"}
description: "multiple vulnerabilities"
date: 2024-06-30T23:51:53+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-1010228
  - CVE-2021-41687
  - CVE-2021-41688
  - CVE-2021-41689
  - CVE-2021-41690
  - CVE-2022-2121
  - CVE-2022-43272
  - CVE-2024-28130
  - CVE-2024-34508
  - CVE-2024-34509

---

Multiple vulnerabilities have been fixed in DCMTK, a collection of
libraries and applications implementing large parts the DICOM standard
for medical images.

CVE-2019-1010228

    Buffer overflow in DcmRLEDecoder::decompress()

CVE-2021-41687

    Incorrect freeing of memory

CVE-2021-41688

    Incorrect freeing of memory

CVE-2021-41689

    NULL pointer dereference

CVE-2021-41690

    Incorrect freeing of memory

CVE-2022-2121

    NULL pointer dereference

CVE-2022-43272

    Memory leak in single process mode

CVE-2024-28130

    Segmentation faults due to incorrect typecast

CVE-2024-34508

    Segmentation fault via invalid DIMSE message

CVE-2024-34509

    Segmentation fault via invalid DIMSE message

