---
title: "ELA-53-1 mysql-5.5 security update"
package: mysql-5.5
version: 5.5.62-0+deb7u1
distribution: "Debian 7 Wheezy"
description: "fix multiple issues"
date: 2018-11-05T14:38:41Z
draft: false
type: updates
cvelist:
  - CVE-2018-2767
  - CVE-2018-3058
  - CVE-2018-3063
  - CVE-2018-3066
  - CVE-2018-3070
  - CVE-2018-3081
  - CVE-2018-3133
  - CVE-2018-3174
  - CVE-2018-3282
---

Several issues have been discovered in the MySQL database server. The
vulnerabilities are addressed by upgrading MySQL to the new upstream
version 5.5.62, which includes additional changes. Please see the MySQL
5.5 Release Notes and Oracle's Critical Patch Update advisory for
further details:

 * https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-61.html
 * https://www.oracle.com/technetwork/security-advisory/cpujul2018-4258247.html
 * https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-62.html
 * https://www.oracle.com/technetwork/security-advisory/cpuoct2018-4428296.html
