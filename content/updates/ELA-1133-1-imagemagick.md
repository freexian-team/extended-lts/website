---
title: "ELA-1133-1 imagemagick security update"
package: imagemagick
version: 8:6.9.10.23+dfsg-2.1+deb10u8 (buster)
version_map: {"10 buster": "8:6.9.10.23+dfsg-2.1+deb10u8"}
description: "incomplete fixes"
date: 2024-07-19T21:01:26Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-1289
  - CVE-2023-34151

---

The security fixes for two security vulnerabilities in Imagemagick, an image
processing toolking, were found to be incomplete.

CVE-2023-1289

    Loading a specially created SVG file may cause a segmentation fault.
    When ImageMagick crashes, it generates a lot of trash files. These trash
    files can be large if the SVG file contains many render actions, and could
    result in a denial of service.

CVE-2023-34151

    Undefined behaviors of casting double to size_t in svg, mvg and other
    coders.

These vulnerabilities were previously addressed in Debian 10 buster during
its Debian Long Term Support period, as announced via the [DLA 3737-1]:

[DLA 3737-1] https://lists.debian.org/debian-lts-announce/2024/02/msg00007.html
