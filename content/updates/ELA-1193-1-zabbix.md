---
title: "ELA-1193-1 zabbix security update"
package: zabbix
version: 2.2.23+dfsg-0+deb8u8 (jessie), 1:3.0.32+dfsg-0+deb9u7 (stretch)
version_map: {"8 jessie": "2.2.23+dfsg-0+deb8u8", "9 stretch": "1:3.0.32+dfsg-0+deb9u7"}
description: "multiple vulnerabilties"
date: 2024-10-03T19:42:05+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-22114
  - CVE-2024-22116
  - CVE-2024-22122
  - CVE-2024-22123

---

Several security vulnerabilities have been discovered in zabbix, a network
monitoring solution.

CVE-2024-22114

    A user with no permission to any of the Hosts can access and view host
    count & other statistics through System Information Widget in Global
    View Dashboard.

CVE-2024-22116

    An administrator with restricted permissions can exploit the script
    execution functionality within the Monitoring Hosts section. The lack of
    default escaping for script parameters enabled this user ability to
    execute arbitrary code via the Ping script, thereby compromising
    infrastructure.

CVE-2024-22119

    Stored XSS in graph items select form

CVE-2024-22122

    Zabbix allows to configure SMS notifications. AT command injection
    occurs on "Zabbix Server" because there is no validation of "Number"
    field on Web nor on Zabbix server side. Attacker can run test of SMS
    providing specially crafted phone number and execute additional AT
    commands on the modem.

CVE-2024-22123

    Setting SMS media allows to set GSM modem file. Later this file is used
    as Linux device. But due everything is a file for Linux, it is possible
    to set another file, e.g. log file and zabbix_server will try to
    communicate with it as modem. As a result, log file will be broken with
    AT commands and small part for log file content will be leaked to UI.

