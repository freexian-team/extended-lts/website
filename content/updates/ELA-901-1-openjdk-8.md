---
title: "ELA-901-1 openjdk-8 security update"
package: openjdk-8
version: 8u382-ga-1~deb8u1 (jessie), 8u382-ga-1~deb9u1 (stretch)
version_map: {"8 jessie": "8u382-ga-1~deb8u1", "9 stretch": "8u382-ga-1~deb9u1"}
description: "multiple vulnerabilities"
date: 2023-07-27T16:24:24+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-22045
  - CVE-2023-22049

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
which may result in bypass of sandbox restrictions, information
disclosure or denial of service.
