---
title: "ELA-1265-1 mariadb-10.1 security update"
package: mariadb-10.1
version: 10.1.48-0+deb9u6 (stretch)
version_map: {"9 stretch": "10.1.48-0+deb9u6"}
description: "denial-of-service"
date: 2024-12-01T10:29:50Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-38791

---

A Denial-of-service vulnerability was found in MariaDB, a popular database
server.
It was found that the mariabackup tool did not correctly handle a mutex
primitive, making it possible for local users to trigger a deadlock.
