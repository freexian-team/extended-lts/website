---
title: "ELA-691-1 wkhtmltopdf security update"
package: wkhtmltopdf
version: 0.12.1-2+deb8u1 (jessie), 0.12.3.2-3+deb9u1 (stretch)
version_map: {"8 jessie": "0.12.1-2+deb8u1", "9 stretch": "0.12.3.2-3+deb9u1"}
description: "directory traversal vulnerability"
date: 2022-10-01T05:35:36+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-21365

---

Directory traversal vulnerability in wkhtmltopdf, a set of CLI utilities
to convert html to pdf or image using WebKit, allows remote attackers to
read local files and disclose sensitive information via a crafted html
file running with the default configurations.

Do note that it's a _breaking change_, in the way that the local
filesystem access will be blocked by default. In case you need to enable
or allow it, use `--enable-local-file-access`. Another option would be to
use `--allow <path>` to specify the folder(s) from which local files are
allowed to be loaded.
