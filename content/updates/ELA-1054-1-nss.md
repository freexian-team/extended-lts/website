---
title: "ELA-1054-1 nss security update"
package: nss
version: 2:3.26-1+debu8u18 (jessie), 2:3.26.2-1.1+deb9u7 (stretch)
version_map: {"8 jessie": "2:3.26-1+debu8u18", "9 stretch": "2:3.26.2-1.1+deb9u7"}
description: "multiple vulnerabilities"
date: 2024-03-11T06:54:24+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-4421
  - CVE-2023-5388
  - CVE-2024-0743

---

Multiple vulnerabilities were found in nss, a set of libraries designed
to support cross-platform development of security-enabled client and
server applications.

CVE-2023-4421

    A fuzzing project discovered vulnerabilities to Bleichenbacher
    timing attacks in NSS's facilities for RSA cryptography.

CVE-2023-5388

    A timing attack against RSA decryption in TLS. This vulnerablity has been
    named The MArvin Attack a Bleichenbacher-like vulernability.

CVE-2024-0743

    An unchecked return value in TLS handshake code could have caused a
    potentially exploitable crash.

