---
title: "ELA-715-1 expat security update"
package: expat
version: 2.1.0-6+deb8u10 (jessie), 2.2.0-2+deb9u7 (stretch)
version_map: {"8 jessie": "2.1.0-6+deb8u10", "9 stretch": "2.2.0-2+deb9u7"}
description: "use-after free"
date: 2022-10-28T07:17:05+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-43680

---

In src:expat, an XML parsing C library, there is a use-after free
caused by overeager destruction of a shared DTD in
XML_ExternalEntityParserCreate in out-of-memory situations.
