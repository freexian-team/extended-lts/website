---
title: "ELA-859-1 python-ipaddress security update"
package: python-ipaddress
version: 1.0.17-1+deb9u1 (stretch)
version_map: {"9 stretch": "1.0.17-1+deb9u1"}
description: "denial of service (DoS) vulnerability"
date: 2023-05-30T11:40:21-04:00
draft: false
cvelist:
  - CVE-2020-14422
---

A potential denial of service (DoS) vulnerability was discovered in
`python-ipaddress`, a backport of Python 3's `ipaddress` module for creating
and manipulating IPv4 and IPv6 internet addresses (eg.  `127.0.0.1`).

This was caused by improperly computing hash values in the `IPv4Interface` and
`IPv6Interface` classes: if an application was affected by the performance of a
dictionary containing `IPv4Interface` or `IPv6Interface` objects, an attacker
could have caused many dictionary entries to be created.
