---
title: "ELA-910-1 amd64-microcode security update"
package: amd64-microcode
version: 3.20230719.1~deb8u1 (jessie), 3.20230719.1~deb9u1 (stretch)
version_map: {"8 jessie": "3.20230719.1~deb8u1", "9 stretch": "3.20230719.1~deb9u1"}
description: "leak register contents of Zen 2 CPUs"
date: 2023-08-01T10:41:18+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-9836
  - CVE-2023-20592
  - CVE-2023-20593

---

Tavis Ormandy discovered that under specific microarchitectural
circumstances, a vector register in "Zen 2" CPUs may not be written to 0
correctly. This flaw allows an attacker to leak register contents across
concurrent processes, hyper threads and virtualized guests.

For details please refer to
https://lock.cmpxchg8b.com/zenbleed.html
https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8

The initial microcode release by AMD only provides updates for second
generation EPYC CPUs: Various Ryzen CPUs are also affected, but no
updates are available yet. Fixes will be provided in a later update once
they are released.

Ruiyi Zhang, Lukas Gerlach, Daniel Weber, Lorenz Hetterich, Youheng Lü, Andreas Kogler
and Michael Schwarz discovered a software-based fault injection attack on SEV VMs,
leading to a potential loss of guest virtual machine memory integrity.

For details please refer to
https://cachewarpattack.com/
https://www.amd.com/en/resources/product-security/bulletin/amd-sb-3005.html


For more specific details and target dates please refer to the AMD
advisory at
https://www.amd.com/en/resources/product-security/bulletin/amd-sb-7008.html
