---
title: "ELA-803-1 git security update"
package: git
version: 1:2.1.4-2.1+deb8u12 (jessie)
version_map: {"8 jessie": "1:2.1.4-2.1+deb8u12"}
description: "multiple vulnerabilities"
date: 2023-02-21T14:41:10+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-23521
  - CVE-2022-24765
  - CVE-2022-29187
  - CVE-2022-39253
  - CVE-2022-39260
  - CVE-2022-41903

---

Multiple issues were found in Git, a distributed revision control system.
An attacker may trigger remote code execution, cause local users into
executing arbitrary commands, leak information from the local filesystem,
and bypass restricted shell.

This update includes two changes of behavior that may affect certain setup:
  - It stops when directory traversal changes ownership from the current
    user while looking for a top-level git directory, a user could make an
    exception by using the new safe.directory configuration.
  - The default of protocol.file.allow has been changed from "always" to
    "user".
