---
title: "ELA-747-1 libraw security update"
package: libraw
version: 0.17.2-6+deb9u4 (stretch)
version_map: {"9 stretch": "0.17.2-6+deb9u4"}
description: "two memory access violations"
date: 2022-12-01T18:02:07+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-16909
  - CVE-2020-15503

---

This update fixes two more memory access violations. CVE-2017-16909 was
reported as fixed via DLA-2903-1 earlier, but that update really fixed
CVE-2017-16910.

CVE-2017-16909

    An error related to the "LibRaw::panasonic_load_raw()" function
    (dcraw_common.cpp) can be exploited to cause a heap-based buffer
    overflow and subsequently cause a crash via a specially crafted
    TIFF image.

CVE-2020-15503

    LibRaw lacks a thumbnail size range check. This affects
    decoders/unpack_thumb.cpp, postprocessing/mem_image.cpp, and
    utils/thumb_utils.cpp. For example,
    malloc(sizeof(libraw_processed_image_t)+T.tlength) occurs without
    validating T.tlength.
