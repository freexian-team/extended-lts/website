---
title: "ELA-560-1 libphp-adodb security update"
package: libphp-adodb
version: 5.15-1+deb8u2
distribution: "Debian 8 jessie"
description: "authentication bypass"
date: 2022-02-06T14:20:19+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-3850

---

It was found that in libphp-adodb, a PHP database abstraction layer
library, an attacker can inject values into the PostgreSQL connection
string by bypassing adodb_addslashes(). The function can be bypassed
in phppgadmin, for example, by surrounding the username in quotes and
submitting with other parameters injected in between.
