---
title: "ELA-1170-1 roundcube security update"
package: roundcube
version: 1.3.17+dfsg.1-1~deb10u7 (buster)
version_map: {"10 buster": "1.3.17+dfsg.1-1~deb10u7"}
description: "multiple vulnerabilities (privilege escalation, information disclosure, DoS)"
date: 2024-08-30T18:38:05+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-42008
  - CVE-2024-42009
  - CVE-2024-42010

---

Multiple cross-site scripting (XSS) vulnerabilities were discovered in
Roundcube, a skinnable AJAX based webmail solution for IMAP servers,
which could lead to privilege escalation, information disclosure or
denial of service.

* CVE-2024-42008: Oskar Zeino-Mahmalat discovered that Roundcube allows
  XSS in serving of attachments other than HTML or SVG.

* CVE-2024-42009: Oskar Zeino-Mahmalat discovered that Roundcube allows
  XSS in post-processing of sanitized HTML content.

* CVE-2024-42010: Oskar Zeino-Mahmalat discovered an information leak
  (access to remote content) due to insufficient CSS filtering.
