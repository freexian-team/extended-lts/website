---
title: "ELA-136-1 expat security update"
package: expat
version: 2.1.0-1+deb7u6
distribution: "Debian 7 Wheezy"
description: "high amount of RAM and CPU consumption in XML parser"
date: 2019-06-27T22:03:26+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-20843

---

In libexpat in Expat, XML input including XML names that contained a
large number of colons could have made the XML parser consume a high
amount of RAM and CPU resources while processing (enough to be usable for
denial-of-service attacks).
