---
title: "ELA-260-1 dovecot security update"
package: dovecot
version: 1:2.2.13-12~deb8u8
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2020-08-15T22:10:21Z
draft: false
type: updates
cvelist:
  - CVE-2020-12100
  - CVE-2020-12673
  - CVE-2020-12674
---

Several vulnerabilities have been discovered in the Dovecot email
server.

CVE-2020-12100

    Receiving mail with deeply nested MIME parts leads to resource
    exhaustion as Dovecot attempts to parse it.

CVE-2020-12673

    Dovecot's NTLM implementation does not correctly check message
    buffer size, which leads to a crash when reading past allocation.

CVE-2020-12674

    Dovecot's RPA mechanism implementation accepts zero-length message,
    which leads to assert-crash later on.
