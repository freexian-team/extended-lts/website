---
title: "ELA-953-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb8u21 (jessie), 1.1.0l-1~deb9u9 (stretch)
version_map: {"8 jessie": "1.0.1t-1+deb8u21", "9 stretch": "1.1.0l-1~deb9u9"}
description: "multiple vulnerabilities"
date: 2023-09-22T10:01:40+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-0464
  - CVE-2023-0465
  - CVE-2023-0466
  - CVE-2023-2650
  - CVE-2023-3446

---

Multiple vulnerabilities have been discovered in OpenSSL, a Secure
Sockets Layer toolkit.

CVE-2023-0464

    David Benjamin reported a flaw related to the verification of X.509
    certificate chains that include policy constraints, which may result
    in denial of service.

CVE-2023-0465

    David Benjamin reported that invalid certificate policies in leaf
    certificates are silently ignored. A malicious CA could take
    advantage of this flaw to deliberately assert invalid certificate
    policies in order to circumvent policy checking on the certificate
    altogether.

CVE-2023-0466

    David Benjamin discovered that the implementation of the
    X509_VERIFY_PARAM_add0_policy() function does not enable the check
    which allows certificates with invalid or incorrect policies to pass
    the certificate verification (contrary to its documentation).

CVE-2023-2650

    It was discovered that processing malformed ASN.1 object identifiers
    or data may result in denial of service.

CVE-2023-3446

    It was found that checking excessively long DH keys or parameters
    could lead to denial of service.

In addition, the stretch update addresses the following issues:

CVE-2022-4304

    A timing based side channel attack was found on the RSA decryption
    implementation.

CVE-2023-3817

    It was found that checking excessively long DH keys or parameters
    could lead to denial of service.
