---
title: "ELA-1216-1 graphicsmagick security update"
package: graphicsmagick
version: 1.3.20-3+deb8u14 (jessie)
version_map: {"8 jessie": "1.3.20-3+deb8u14"}
description: "out-of-bounds write"
date: 2024-10-27T14:49:53+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-21679

---

It was discovered that a buffer overflow in GraphicsMagick, a collection
of image processing tools, could result in denial of service or potentially
in the execution of arbitrary code when converting crafted images to the PCX
format.

