---
title: "ELA-1012-1 strongswan security update"
package: strongswan
version: 5.2.1-6+deb8u11 (jessie)
version_map: {"8 jessie": "5.2.1-6+deb8u11"}
description: " vulnerability"
date: 2023-11-29T12:57:37+00:00
draft: false
cvelist:
  - CVE-2023-41913
---

It was discovered that there was a potential buffer overflow in `strongswan`, a
popular IPsec-based VPN (Virtual Private Network) server.

A vulnerability related to processing public Diffie-Hellman key exchange values
could have resulted in a buffer overflow and potentially remote code execution
as a consequence.
