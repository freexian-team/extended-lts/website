---
title: "ELA-1143-1 aom security update"
package: aom
version: 1.0.0-3+deb10u2 (buster)
version_map: {"10 buster": "1.0.0-3+deb10u2"}
description: "integer overflows"
date: 2024-07-31T23:55:34+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-5171

---

Integer overflows have been fixed in aom, an AV1 Codec Library.
