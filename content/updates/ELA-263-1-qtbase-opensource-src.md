---
title: "ELA-263-1 qtbase-opensource-src security update"
package: qtbase-opensource-src
version: 5.3.2+dfsg-4+deb8u5
distribution: "Debian 8 Jessie"
description: "buffer overread vulnerability"
date: 2020-08-21T15:10:59+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-17507
---

A vulnerability was discovered in `qtbase-opensource-src`, the cross-platform
C++ application framework. A specially-crafted XBM image file could have caused
a buffer overread.
