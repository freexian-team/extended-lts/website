---
title: "ELA-809-1 freeradius security update"
package: freeradius
version: 3.0.17+dfsg-1.1+deb9u1 (stretch)
version_map: {"9 stretch": "3.0.17+dfsg-1.1+deb9u1"}
description: "multiple vulnerabilities"
date: 2023-02-24T22:00:14+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-11234
  - CVE-2019-11235
  - CVE-2019-13456
  - CVE-2019-17185
  - CVE-2022-41859
  - CVE-2022-41860
  - CVE-2022-41861

---

Several flaws were found in freeradius, a high-performance and highly
configurable RADIUS server.


CVE-2022-41859

    In freeradius, the EAP-PWD function compute_password_element() leaks
    information about the password which allows an attacker to substantially
    reduce the size of an offline dictionary attack.

CVE-2022-41860

    In freeradius, when an EAP-SIM supplicant sends an unknown SIM option, the
    server will try to look that option up in the internal dictionaries. This
    lookup will fail, but the SIM code will not check for that failure.
    Instead, it will dereference a NULL pointer, and cause the server to crash.

CVE-2022-41861

    A flaw was found in freeradius. A malicious RADIUS client or home server
    can send a malformed attribute which can cause the server to crash.

CVE-2019-11234

    FreeRADIUS does not prevent use of reflection for authentication spoofing,
    aka a "Dragonblood" issue, a similar issue to CVE-2019-9497.

CVE-2019-11235

    FreeRADIUS mishandles the "each participant verifies that the received
    scalar is within a range, and that the received group element is a valid
    point on the curve being used" protection mechanism, aka a "Dragonblood"
    issue, a similar issue to CVE-2019-9498 and CVE-2019-9499.

CVE-2019-13456

    In FreeRADIUS 3.0 on average 1 in every 2048 EAP-pwd handshakes fails
    because the password element cannot be found within 10 iterations of the
    hunting and pecking loop. This leaks information that an attacker can use
    to recover the password of any user. This information leakage is similar to
    the "Dragonblood" attack and CVE-2019-9494.

CVE-2019-17185

    In FreeRADIUS 3.0.x the EAP-pwd module used a global OpenSSL
    BN_CTX instance to handle all handshakes. This mean multiple threads use the
    same BN_CTX instance concurrently, resulting in crashes when concurrent
    EAP-pwd handshakes are initiated. This can be abused by an adversary as a
    Denial-of-Service (DoS) attack.
