---
title: "ELA-822-1 amanda security update"
package: amanda
version: 1:3.3.9-5+deb9u1 (stretch)
version_map: {"9 stretch": "1:3.3.9-5+deb9u1"}
description: "privilege escalation"
date: 2023-03-30T15:38:32+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-37704

---

It was discovered that there was a potential privilege escalation vulnerability
in the "amanda" backup utility. The SUID binary located at /lib/amanda/rundump
executed /usr/sbin/dump as root with arguments controlled by the attacker,
which may have led to an escalation of privileges, denial of service (DoS) or
information disclosure.
