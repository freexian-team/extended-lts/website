---
title: "ELA-1273-1 zabbix security update"
package: zabbix
version: 1:2.2.23+dfsg-0+deb8u9 (jessie), 1:3.0.32+dfsg-0+deb9u8 (stretch)
version_map: {"8 jessie": "1:2.2.23+dfsg-0+deb8u9", "9 stretch": "1:3.0.32+dfsg-0+deb9u8"}
description: "multiple vulnerabilities"
date: 2024-12-15T16:25:14+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-22117
  - CVE-2024-36464
  - CVE-2024-42332
  - CVE-2024-42333

---

Several security vulnerabilities have been discovered in zabbix, a network
monitoring solution, potentially among other effects allowing denial of
service, information disclosure, log tampering or buffer over-read.

CVE-2024-22117

    When a URL is added to the map element, it is recorded in the database
    with sequential IDs. Upon adding a new URL, the system retrieves the
    last sysmapelementurlid value and increments it by one. However, an
    issue arises when a user manually changes the sysmapelementurlid value
    by adding sysmapelementurlid + 1. This action prevents others from
    adding URLs to the map element.

CVE-2024-36464

    When exporting media types, the password is exported in the YAML in
    plain text. This appears to be a best practices type issue and may
    have no actual impact. The user would need to have permissions to
    access the media types and therefore would be expected to have
    access to these passwords.

CVE-2024-42332

    The researcher is showing that due to the way the SNMP trap log is
    parsed, an attacker can craft an SNMP trap with additional lines of
    information and have forged data show in the Zabbix UI. This attack
    requires SNMP auth to be off and/or the attacker to know the
    community/auth details. The attack requires an SNMP item to be
    configured as text on the target host.

CVE-2024-42333

    The researcher is showing that it is possible to leak a small amount
    of Zabbix Server memory using an out of bounds read in
    src/libs/zbxmedia/email.c

