---
title: "ELA-1056-1 python3.4 security update"
package: python3.4
version: 3.4.2-1+deb8u16 (jessie)
version_map: {"8 jessie": "3.4.2-1+deb8u16"}
description: "multiple vulnerabilities"
date: 2024-03-18T17:10:38+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-48560
  - CVE-2022-48564
  - CVE-2022-48565
  - CVE-2022-48566
  - CVE-2023-40217

---

Multiple vulnerabilities were found in python3.4, an interactive
high-level object-oriented language. An attacker could cause DoS
(denial-of-service) situations, exfiltrate private information, and
possibly execute arbitrary code.

* CVE-2022-48560

    A use-after-free exists via heappushpop in heapq.

* CVE-2022-48564

    read_ints in plistlib.py is vulnerable to a potential DoS attack
    via CPU and RAM exhaustion when processing malformed Apple
    Property List files in binary format.

* CVE-2022-48565

    An XML External Entity (XXE) issue was discovered. The
    plistlib module no longer accepts entity declarations in XML plist
    files to avoid XML vulnerabilities.

* CVE-2022-48566

    In compare_digest in Lib/hmac.py, constant-time-defeating
    optimisations were possible in the accumulator variable in
    hmac.compare_digest.

* CVE-2023-40217

    If a TLS server-side socket is created, receives data into the
    socket buffer, and then is closed quickly, there is a brief window
    where the SSLSocket instance will detect the socket as "not
    connected" and won't initiate a handshake, but buffered data will
    still be readable from the socket buffer.
