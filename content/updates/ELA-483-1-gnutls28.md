---
title: "ELA-483-1 gnutls28 security update"
package: gnutls28
version: 3.3.30-0+deb8u2
distribution: "Debian 8 jessie"
description: "alternate chains verification"
date: 2021-09-17T22:00:26+02:00
draft: false
type: updates
cvelist:

---

GnuTLS, a portable cryptography library, fails to validate alternate
trust chains in some conditions.  In particular this breaks connecting
to servers that use Let's Encrypt certificates, starting 2021-10-01.
