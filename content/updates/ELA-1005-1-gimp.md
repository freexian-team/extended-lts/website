---
title: "ELA-1005-1 gimp security update"
package: gimp
version: 2.8.18-1+deb9u2 (stretch)
version_map: {"9 stretch": "2.8.18-1+deb9u2"}
description: "multiple vulnerabilities"
date: 2023-11-21T17:17:56+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-30067
  - CVE-2023-44442
  - CVE-2023-44444

---

Multiple vulnerabilities were fixed in GIMP,
the GNU Image Manipulation Program.

CVE-2022-30067

    Out-of-memory with crafted XCF file.

CVE-2023-44442

    PSD file parsing buffer overflow.

CVE-2023-44444

    PSP file parsing buffer overflow.


