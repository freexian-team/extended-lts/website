---
title: "ELA-30-1 libx11 security update"
package: libx11
version: 2:1.5.0-1+deb7u5
distribution: "Debian 7 Wheezy"
description: "denial of service"
date: 2018-08-29T22:35:09+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-14598
  - CVE-2018-14599
  - CVE-2018-14600
---

Several issues were discovered in libx11, the client interface to the X
Windows System. The functions XGetFontPath, XListExtensions, and XListFonts are
vulnerable to an off-by-one override on malicious server responses. A malicious
server could also send a reply in which the first string overflows, causing a
variable set to NULL that will be freed later on, leading to a segmentation
fault and Denial of Service. The function XListExtensions in ListExt.c
interprets a variable as signed instead of unsigned, resulting in an
out-of-bounds write (of up to 128 bytes), leading to a Denial of Service or
possibly remote code execution.

