---
title: "ELA-949-1 mutt security update"
package: mutt
version: 1.5.23-3+deb8u7 (jessie), 1.7.2-1+deb9u7 (stretch)
version_map: {"8 jessie": "1.5.23-3+deb8u7", "9 stretch": "1.7.2-1+deb9u7"}
description: "denial of service"
date: 2023-09-20T14:49:22-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-4874
  - CVE-2023-4875

---

Two NULL pointer dereference flaws were discovered in Mutt, a text-based
mailreader supporting MIME, GPG, PGP and threading, which may result in denial
of service (application crash) when viewing a specially crafted email or when
composing from a specially crafted draft message.
