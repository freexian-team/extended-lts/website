---
title: "ELA-997-1 python3.5 security update"
package: python3.5
version: 3.5.3-1+deb9u8 (stretch)
version_map: {"9 stretch": "3.5.3-1+deb9u8"}
description: "multiple vulnerabilities"
date: 2023-11-03T08:48:21Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-3177
  - CVE-2022-48560
  - CVE-2022-48564
  - CVE-2022-48565
  - CVE-2022-48566
  - CVE-2023-40217

---

Multiple vulnerabilities were found in python3.5, an interactive high-level
object-oriented language.

CVE-2021-3177:

	A regression was fixed in CVE-2021-3177: ISO C90 forbids mixed
	declarations and code, that could lead to compilation errors in
	some contexts.

CVE-2022-48560:

	A use-after-free existed in Python via heappushpop function
	in heapq.

CVE-2022-48564:

	A DoS attack via CPU and RAM exhaustion
	when processing malformed Apple Property List files
	in binary format was fixed. This needed a backport of GH-4455.

CVE-2022-48565:

	An XML External Entity (XXE) issue
	was discovered in Python. The plistlib module no longer
	accepts entity declarations in XML plist files to
	avoid XML vulnerabilities.

CVE-2022-48566:

	An issue was discovered in compare_digest
	in Lib/hmac.py in Python. Constant-time-defeating
	optimisations were possible in the accumulator variable
	in hmac.compare_digest, that would facilitate a side
	channel type attack.

CVE-2023-40217:

	A race condition was fixed in TLS handling.
	If a TLS server-side socket is created, receives data
	into the socket buffer, and then is closed quickly,
	there is a brief window where the SSLSocket instance
	will detect the socket as "not connected" and
	won't initiate a handshake, but buffered data will
	still be readable from the socket buffer.
	This data will not be authenticated if the server-side
	TLS peer is expecting client certificate authentication,
	and is indistinguishable from valid TLS stream data.
	Data is limited in size to the amount that will fit in the buffer.
	The TLS connection cannot directly be used for data
	exfiltration because the vulnerable code path requires
	that the connection be closed on initialization of the SSLSocket.
