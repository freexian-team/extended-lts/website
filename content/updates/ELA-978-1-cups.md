---
title: "ELA-978-1 cups security update"
package: cups
version: 1.7.5-11+deb8u12 (jessie), 2.2.1-8+deb9u11 (stretch)
version_map: {"8 jessie": "1.7.5-11+deb8u12", "9 stretch": "2.2.1-8+deb9u11"}
description: "buffer overflow and bad default configuration"
date: 2023-10-03T10:32:08+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-4504
  - CVE-2023-32360

---

Two issues have been found in cups, the Common UNIX Printing System(tm).

CVE-2023-4504

  Due to missing boundary checks a heap-based buffer overflow and code
  execution might be possible by using crafted postscript documents.

CVE-2023-32360

  Unauthorized users might be allowed to fetch recently printed documents.

  Since this is a configuration fix, it might be that it does not reach
  you if you are updating the package.
  Please double check your /etc/cups/cupsd.conf file, whether it limits
  the access to CUPS-Get-Document with something like the following
  >  <Limit CUPS-Get-Document>
  >    AuthType Default
  >    Require user @OWNER @SYSTEM
  >    Order deny,allow
  >   </Limit>
  (The important line is the 'AuthType Default' in this section)
