---
title: "ELA-54-1 curl security update"
package: curl
version: 7.26.0-1+wheezy25+deb7u3
distribution: "Debian 7 Wheezy"
description: "buffer overflow"
date: 2018-11-06T22:33:05+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-16842
---

Brian Carpenter discovered that the logic in the curl tool to wrap error
messages at 80 columns is flawed, leading to a read buffer overflow if a single
word in the message is itself longer than 80 bytes.
