---
title: "ELA-323-1 lxml security update"
package: lxml
version: 3.4.0-1+deb8u2
distribution: "Debian 8 jessie"
description: "cross-site scripting"
date: 2020-12-01T09:09:42+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-19787
  - CVE-2020-27783

---

It was discovered that the `clean_html()` function of lxml, a Python library
for HTML and XML processing, performed insufficient sanitisation for embedded
Javascript code. This could lead to cross-site scripting or possibly the
execution of arbitrary code.
