---
title: "ELA-887-1 cups security update"
package: cups
version: 1.7.5-11+deb8u11 (jessie), 2.2.1-8+deb9u10 (stretch)
version_map: {"8 jessie": "1.7.5-11+deb8u11", "9 stretch": "2.2.1-8+deb9u10"}
description: "use after free"
date: 2023-07-01T00:33:54+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-34241

---

An issue has been found in cups, the Common UNIX Printing System(tm).
Due to a use-after-free bug an attacker could cause a denial-of-service.
In case of having access to the log files, an attacker could also
exfiltrate private keys or other sensitive information from the cups
daemon.

