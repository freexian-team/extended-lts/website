---
title: "ELA-467-1 wordpress security update"
package: wordpress
version: 4.1.33+dfsg-0+deb8u1
distribution: "Debian 8 jessie"
description: "object injection"
date: 2021-08-04T19:25:29+05:30
draft: false
type: updates
cvelist:

---

One security issue affects WordPress, a weblog manager, versions
between 3.7 and 5.7. This update fixes the following security issues:
Object injection in PHPMailer (CVE-2020-36326 and CVE-2018-19296).
