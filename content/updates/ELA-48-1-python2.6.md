---
title: "ELA-48-1 python2.6 security update"
package: python2.6
version: 2.6.8-1.1+deb7u2
distribution: "Debian 7 Wheezy"
description: "fixes for command injection, REDOS vulnerabilities and uninitialized Expat's hash"
date: 2018-09-30T22:49:40+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-1000802
  - CVE-2018-1060
  - CVE-2018-1061
  - CVE-2018-14647

---

CVE-2018-1000802
     fix command injection in shutil module

CVE-2018-1060 and CVE-2018-1061
     fix REDOS vulnerabilities in poplib and difflib modules

CVE-2018-14647
     fix uninitialized Expat's hash
