---
title: "ELA-862-1 wireshark security update"
package: wireshark
version: 2.6.20-0+deb9u6 (stretch)
version_map: {"9 stretch": "2.6.20-0+deb9u6"}
description: "several vulnerabilities"
date: 2023-06-03T23:54:12+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-2856
  - CVE-2023-2858
  - CVE-2023-2879
  - CVE-2023-2952

---

Several vulnerabilities were fixed in the network traffic analyzer Wireshark.

CVE-2023-2856

    VMS TCPIPtrace file parser crash

CVE-2023-2858

    NetScaler file parser crash

CVE-2023-2879

    GDSDB infinite loop

CVE-2023-2952

    XRA dissector infinite loop

