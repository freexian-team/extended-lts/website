---
title: "ELA-984-1 nghttp2 security update"
package: nghttp2
version: 1.18.1-1+deb9u3 (stretch)
version_map: {"9 stretch": "1.18.1-1+deb9u3"}
description: "denial of service"
date: 2023-10-15T15:20:32+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-44487

---

CVE-2023-44487 describes a flaw in the HTTP2 protocol that allows an attacker to rapidly create and cancel streams by sending a HEADERS frame
immediately followed by a RST_STREAM. This can cause a denial of service due to resource exhaustion.

The applied patches mitigate this flaw by rate limiting the cancellation of streams and disconnecting the client when this limit is exceeded.

