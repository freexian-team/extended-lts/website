---
title: "ELA-487-1 uwsgi security update"
package: uwsgi
version: 2.0.7-1+deb8u4
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-09-29T21:06:33+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-36160

---

It was discovered that the uwsgi proxy module for Apache2
(mod_proxy_uwsgi) can read above the allocated memory when processing
a request with a carefully crafted uri-path. An attacker may cause the
server to crash (DoS).
