---
title: "ELA-857-1 libtasn1-6 security update"
package: libtasn1-6
version: 4.2-3+deb8u5 (jessie), 4.10-1.1+deb9u2 (stretch)
version_map: {"8 jessie": "4.2-3+deb8u5", "9 stretch": "4.10-1.1+deb9u2"}
description: "off-by-one array size check"
date: 2023-05-27T19:26:04+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-46848

---

It was discovered that there was an off-by-one array size issue in
libtasn1-6, a library to manage the generic ASN.1 data structure.
