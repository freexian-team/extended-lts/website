---
title: "ELA-108-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u21
distribution: "Debian 7 Wheezy"
description: "heap-buffer overflow vulnerabilities"
date: 2019-04-19T10:52:17+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-1000019
  - CVE-2019-1000020
---

Two heap-buffer overflow vulnerabilities were discovered in the PHP5 programming language within the Exif image module.
