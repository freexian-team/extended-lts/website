---
title: "ELA-915-1 linux-5.10 security update"
package: linux-5.10
version: 5.10.179-3~deb9u1 (stretch)
version_map: {"9 stretch": "5.10.179-3~deb9u1"}
description: "linux kernel update"
date: 2023-08-04T14:56:22+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-2156
  - CVE-2023-3390
  - CVE-2023-3610
  - CVE-2023-20593
  - CVE-2023-31248
  - CVE-2023-35001

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2023-2156

    It was discovered that a flaw in the handling of the RPL protocol
    may allow an unauthenticated remote attacker to cause a denial of
    service if RPL is enabled (not by default in Debian).

CVE-2023-3390

    A use-after-free flaw in the netfilter subsystem caused by
    incorrect error path handling may result in denial of service or
    privilege escalation.

CVE-2023-3610

    A use-after-free flaw in the netfilter subsystem caused by
    incorrect refcount handling on the table and chain destroy path
    may result in denial of service or privilege escalation.

CVE-2023-20593

    Tavis Ormandy discovered that under specific microarchitectural
    circumstances, a vector register in AMD "Zen 2" CPUs may not be
    written to 0 correctly.  This flaw allows an attacker to leak
    sensitive information across concurrent processes, hyper threads
    and virtualized guests.

    For details please refer to
    <https://lock.cmpxchg8b.com/zenbleed.html> and
    <https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8>.

    This issue can also be mitigated by a microcode update through the
    amd64-microcode package or a system firmware (BIOS/UEFI) update.
    However, the initial microcode release by AMD only provides
    updates for second generation EPYC CPUs.  Various Ryzen CPUs are
    also affected, but no updates are available yet.

CVE-2023-31248

    Mingi Cho discovered a use-after-free flaw in the Netfilter
    nf_tables implementation when using nft_chain_lookup_byid, which
    may result in local privilege escalation for a user with the
    CAP_NET_ADMIN capability in any user or network namespace.

CVE-2023-35001

    Tanguy DUBROCA discovered an out-of-bounds reads and write flaw in
    the Netfilter nf_tables implementation when processing an
    nft_byteorder expression, which may result in local privilege
    escalation for a user with the CAP_NET_ADMIN capability in any
    user or network namespace.
