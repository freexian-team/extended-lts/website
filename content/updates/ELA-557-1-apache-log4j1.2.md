---
title: "ELA-557-1 apache-log4j1.2 security update"
package: apache-log4j1.2
version: 1.2.17-5+deb8u2
distribution: "Debian 8 jessie"
description: "remote code execution"
date: 2022-02-01T12:48:59+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-4104
  - CVE-2022-23302
  - CVE-2022-23305
  - CVE-2022-23307

---

Multiple security vulnerabilities have been discovered in Apache Log4j 1.2, a
Java logging framework, when it is configured to use JMSSink, JDBCAppender,
JMSAppender or Apache Chainsaw which could be exploited for remote code
execution.

Note that a possible attacker requires write access to the Log4j configuration
and the aforementioned features are not enabled by default. In order to
completely mitigate against these type of vulnerabilities the related classes
have been removed from the resulting jar file.
