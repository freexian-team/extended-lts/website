---
title: "ELA-1304-1 postgresql-9.4 security update"
package: postgresql-9.4
version: 9.4.26-0+deb8u11 (jessie)
version_map: {"8 jessie": "9.4.26-0+deb8u11"}
description: "multiple vulnerabilities"
date: 2025-01-25T12:25:39-05:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-5870
  - CVE-2024-10977
  - CVE-2024-10978
  - CVE-2024-10979

---

Multiple security issues were discovered in PostgreSQL, which may result in
the execution of arbitrary code, privilege escalation, log manipulation, or
denial of service.

CVE-2023-5870

    A flaw was found in PostgreSQL involving the pg_cancel_backend role that
    signals background workers, including the logical replication launcher,
    autovacuum workers, and the autovacuum launcher. Successful exploitation
    requires a non-core extension with a less-resilient background worker
    and would affect that specific background worker only.

CVE-2024-10977

    Client use of server error message in PostgreSQL allows a server not
    trusted under current SSL or GSS settings to furnish arbitrary non-NUL
    bytes to the libpq application. For example, a man-in-the-middle attacker
    could send a long error message that a human or screen-scraper user of
    psql mistakes for valid query results. 

CVE-2024-10978

    Incorrect privilege assignment in PostgreSQL allows a less-privileged
    application user to view or change different rows from those intended. An
    attack requires the application to use SET ROLE, SET SESSION
    AUTHORIZATION, or an equivalent feature.

CVE-2024-10979

    Incorrect control of environment variables in PostgreSQL PL/Perl allows
    an unprivileged database user to change sensitive process environment
    variables (e.g. PATH).
