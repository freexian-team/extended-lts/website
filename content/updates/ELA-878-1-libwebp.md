---
title: "ELA-878-1 libwebp security update"
package: libwebp
version: 0.5.2-1+deb9u2 (stretch)
version_map: {"9 stretch": "0.5.2-1+deb9u2"}
description: "double free"
date: 2023-06-26T11:28:39+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-1999

---

Double free which may result in denial of service was fixed in
the libwebp image compression library.
