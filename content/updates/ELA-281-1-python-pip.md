---
title: "ELA-281-1 python-pip security update"
package: python-pip
version: 1.5.6-5+deb8u1
distribution: "Debian 8 Jessie"
description: "directory traversal vulnerability"
date: 2020-09-11T11:24:37+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-20916
---

It was discovered that there was a directory traversal attack in pip, the
Python package installer.

When an URL was given in an install command, as a `Content-Disposition` HTTP
header was permitted to have `../` components in its filename, arbitrary local
files (eg. `/root/.ssh/authorized_keys`) could be overidden.
