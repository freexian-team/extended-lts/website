---
title: "ELA-749-1 vlc security update"
package: vlc
version: 3.0.17.4-0+deb9u2 (stretch)
version_map: {"9 stretch": "3.0.17.4-0+deb9u2"}
description: "buffer overflow"
date: 2022-12-03T04:52:16+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-41325

---

Mitsurugi Heishiro found out that in VLC, multimedia player and streamer,
a potential buffer overflow in the vnc module could trigger remote code
execution if a malicious vnc URL is deliberately played.
