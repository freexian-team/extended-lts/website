---
title: "ELA-570-1 htmldoc security update"
package: htmldoc
version: 1.8.27-8+deb8u3
distribution: "Debian 8 jessie"
description: "out-of-bounds read or buffer overflows"
date: 2022-02-26T12:15:48+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-40985
  - CVE-2021-43579
  - CVE-2022-0534

---

Several issues have been found in htmldoc, an HTML processor that generates indexed HTML, PS, and PDF.

CVE-2022-0534

     A crafted GIF file could lead to a stack out-of-bounds read,
     which could result in a crash (segmentation fault).

CVE-2021-43579

     Converting an HTML document, which links to a crafted BMP file,
     could lead to a stack-based buffer overflow, which could result
     in remote code execution.

CVE-2021-40985

     A crafted BMP image could lead to a buffer overflow, which could
     cause a denial of service.
