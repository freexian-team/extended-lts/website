---
title: "ELA-699-1 asterisk security update"
package: asterisk
version: 1:13.14.1~dfsg-2+deb9u7 (stretch)
version_map: {"9 stretch": "1:13.14.1~dfsg-2+deb9u7"}
description: "denial of service"
date: 2022-10-07T23:55:49+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-35776
  - CVE-2022-26651

---

Several security vulnerabilities have been discovered in Asterisk, an Open
Source Private Branch Exchange.

CVE-2022-26651

    The func_odbc module provides possibly inadequate escaping functionality
    for backslash characters in SQL queries, resulting in user-provided data
    creating a broken SQL query or possibly a SQL injection.

CVE-2020-35776

    A buffer overflow in res_pjsip_diversion.c allows remote attackers to crash
    Asterisk by deliberately misusing SIP 181 responses.
