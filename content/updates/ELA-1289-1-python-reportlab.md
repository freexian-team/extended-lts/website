---
title: "ELA-1289-1 python-reportlab security update"
package: python-reportlab
version: 3.1.8-3+deb8u3 (jessie)
version_map: {"8 jessie": "3.1.8-3+deb8u3"}
description: "multiple vulnerabilities"
date: 2025-01-14T17:33:40+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-19450
  - CVE-2020-28463

---

CVE-2019-19450

:   Ravi Prakash Giri discovered a remote code execution vulnerability
    via crafted XML document where `<unichar code="` is followed by
    arbitrary Python code.

    This issue is similar to [CVE-2019-17626](https://deb.freexian.com/extended-lts/tracker/CVE-2019-17626).

CVE-2020-28463

:   Karan Bamal discovered a Server-side Request Forgery (SSRF)
    vulnerability via `<img>` tags.  New settings `trustedSchemes` and
    `trustedHosts` have been added as part of the fix/mitigation: they
    can be used to specify an explicit allowlist for remote sources.
