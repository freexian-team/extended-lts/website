---
title: "ELA-1308-1 activemq security update"
package: activemq
version: 5.6.0+dfsg1-4+deb8u4 (jessie)
version_map: {"8 jessie": "5.6.0+dfsg1-4+deb8u4"}
description: "multiple vulnerabilities"
date: 2025-01-30T10:00:45+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-11775
  - CVE-2020-13920
  - CVE-2021-26117
  - CVE-2023-46604

---

Multiple security issues were discovered in Apache ActiveMQ, a
multi-protocol message broker.

* CVE-2018-11775

    TLS hostname verification was missing which could make the client
    vulnerable to a MITM attack between a Java application using the
    ActiveMQ client and the ActiveMQ server. This is now enabled by
    default.

* CVE-2020-13920

    Apache ActiveMQ uses LocateRegistry.createRegistry() to create the
    JMX RMI registry and binds the server to the "jmxrmi" entry. It is
    possible to connect to the registry without authentication and
    call the rebind method to rebind jmxrmi to something else. If an
    attacker creates another server to proxy the original, and bound
    that, he effectively becomes a man in the middle and is able to
    intercept the credentials when an user connects.

* CVE-2021-26117

    The optional LDAP login module can be configured to use anonymous
    access to the LDAP server. In this case, the anonymous context is
    used to verify a valid users password in error, resulting in no
    check on the password.

* CVE-2023-46604

    The Java OpenWire protocol marshaller is vulnerable to Remote Code
    Execution. This vulnerability may allow a remote attacker with
    network access to either a Java-based OpenWire broker or client to
    run arbitrary shell commands by manipulating serialized class
    types in the OpenWire protocol to cause either the client or the
    broker (respectively) to instantiate any class on the
    classpath.
