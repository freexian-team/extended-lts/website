---
title: "ELA-1296-1 libtar security update"
package: libtar
version: 1.2.20-7+deb10u1 (buster)
version_map: {"10 buster": "1.2.20-7+deb10u1"}
description: "multiple vulnerabilities"
date: 2025-01-20T16:21:53+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-33643
  - CVE-2021-33644
  - CVE-2021-33645
  - CVE-2021-33646

---

Multiple vulnerabilities have been fixed in libtar, a library for manipulating tar archives.

CVE-2021-33643

    out-of-bounds read in gnu_longlink()

CVE-2021-33644

    out-of-bounds read in gnu_longname()

CVE-2021-33645

    memory leak in th_read()

CVE-2021-33646
    memory leak in th_read()
