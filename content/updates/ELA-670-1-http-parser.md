---
title: "ELA-670-1 http-parser security update"
package: http-parser
version: 2.1-2+deb8u1 (jessie), 2.1-2+deb9u1 (stretch)
version_map: {"8 jessie": "2.1-2+deb8u1", "9 jessie": "2.1-2+deb9u1"}
description: "HTTP request smuggling vulnerability"
date: 2022-08-31T10:44:58+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-8287
---

There was a potential HTTP request smuggling vulnerability in http-parser, a
popular library for parsing HTTP messages.
