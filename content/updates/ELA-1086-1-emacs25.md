---
title: "ELA-1086-1 emacs25 security update"
package: emacs25
version: 25.1+1-4+deb9u3 (stretch)
version_map: {"9 stretch": "25.1+1-4+deb9u3"}
description: "multiple vulnerabilities"
date: 2024-05-03T13:35:14+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-30203
  - CVE-2024-30204
  - CVE-2024-30205

---

Multiple problems were discovered in GNU Emacs, the extensible,
customisable, self-documenting display editor.

### CVE-2024-30203 & CVE-2024-30204

In Emacs before 29.3, LaTeX preview is enabled by default for e-mail
attachments in some Emacs MUAs.  This can lead to denial of service.

(A request has been submitted to MITRE to merge these CVE numbers.)

### CVE-2024-30205

In Emacs before 29.3, Org mode considers the contents of remote files to be
trusted.  This affects Org Mode before 9.6.23.
