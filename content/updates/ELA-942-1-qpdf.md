---
title: "ELA-942-1 qpdf security update"
package: qpdf
version: 6.0.0-2+deb9u1 (stretch)
version_map: {"9 stretch": "6.0.0-2+deb9u1"}
description: "multiple vulnerabilities"
date: 2023-08-31T23:57:22+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2015-9252
  - CVE-2017-9208
  - CVE-2017-9209
  - CVE-2017-9210
  - CVE-2017-11624
  - CVE-2017-11625
  - CVE-2017-11626
  - CVE-2017-11627
  - CVE-2017-12595
  - CVE-2017-18183
  - CVE-2017-18186
  - CVE-2018-9918
  - CVE-2021-25786
  - CVE-2021-36978

---
Multiple vulnerabilities were fixed in QPDF, a command-line tool and C++ library that performs content-preserving transformations on PDF files.
