---
title: "ELA-883-1 c-ares security update"
package: c-ares
version: 1.10.0-2+deb8u5 (jessie), 1.12.0-1+deb9u4 (stretch)
version_map: {"8 jessie": "1.10.0-2+deb8u5", "9 stretch": "1.12.0-1+deb9u4"}
description: "multiple vulnerabilities"
date: 2023-06-30T23:05:33+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-31130
  - CVE-2023-32067

---

Two vulnerabilities were discovered in c-ares, an asynchronous name
resolver library:

CVE-2023-31130

    ares_inet_net_pton() is found to be vulnerable to a buffer underflow
    for certain ipv6 addresses, in particular "0::00:00:00/2" was found
    to cause an issue. c-ares only uses this function internally for
    configuration purposes, however external usage for other purposes may
    cause more severe issues.


CVE-2023-32067

    Target resolver may erroneously interprets a malformed UDP packet
    with a length of 0 as a graceful shutdown of the connection, which
    could cause a denial of service.
