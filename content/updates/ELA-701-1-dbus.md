---
title: "ELA-701-1 dbus security update"
package: dbus
version: 1.8.22-0+deb8u5 (jessie), 1.10.32-0+deb9u2 (stretch)
version_map: {"8 jessie": "1.8.22-0+deb8u5", "9 stretch": "1.10.32-0+deb9u2"}
description: "denial of service"
date: 2022-10-10T14:08:14+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-42010
  - CVE-2022-42011
  - CVE-2022-42012

---

Evgeny Vereshchagin discovered multiple vulnerabilities in D-Bus, a
simple interprocess messaging system, which may result in denial of
service by an authenticated user.
