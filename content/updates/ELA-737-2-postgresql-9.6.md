---
title: "ELA-737-2 postgresql-9.6 security update"
package: postgresql-9.6
version: 9.6.24-0+deb9u3 (stretch)
version_map: {"9 stretch": "9.6.24-0+deb9u3"}
description: "address build failure in prior update"
date: 2022-11-27T08:27:49-05:00
draft: false
type: updates
tags:
- update
cvelist:

---

The postgresql-9.6 packages announced in ELA-737-1 failed to build as a
result of a configuration setting in the build environment.  The
packages announced in this follow-up update have been modified so that
they will build properly.  The CVEs referenced in ELA-737-1 remain
properly patched in this release and this release contains no changes
outside of those necessary to address the build failure.
