---
title: "ELA-1271-1 linux-6.1 new linux version"
package: linux-6.1
version: 6.1.112-1~deb9u1 (stretch), 6.1.112-1~deb10u1 (buster)
version_map: {"9 stretch": "6.1.112-1~deb9u1", "10 buster": "6.1.112-1~deb10u1"}
description: "new linux kernel backport"
date: 2024-12-12T12:29:20+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update introduces Linux kernel 6.1 to Debian 9 stretch and Debian 10 buster.
This kernel will be supported along with 5.10, but for a longer period. Linux 4.19
was discontinued as announced in [ELA-1116-1](https://deb.freexian.com/extended-lts/updates/ela-1116-1-linux-4.19/).
Instructions on how to update to 6.1 and support periods can be found
[in the kernel backports page](https://deb.freexian.com/extended-lts/docs/kernel-backport/).
