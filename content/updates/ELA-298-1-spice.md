---
title: "ELA-298-1 spice security update"
package: spice
version: 0.12.5-1+deb8u8
distribution: "Debian 8 jessie"
description: "multiple buffer overflow"
date: 2020-10-12T05:23:22+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-14355

---

Multiple buffer overflow vulnerabilities were found in the QUIC image
decoding process of the SPICE remote display system, before spice-0.14.2-1.

Both the SPICE client (spice-gtk) and server are affected by these flaws.
These flaws allow a malicious client or server to send specially crafted
messages that, when processed by the QUIC image compression algorithm,
result in a process crash or potential code execution. 
