---
title: "ELA-445-1 bluez security update"
package: bluez
version: 5.43-2+deb9u2~deb8u3
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-06-27T01:19:21+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-26558
  - CVE-2021-0129

---

Two issues have been found in bluez, a package with Bluetooth tools and daemons.
One issue is about a man-in-the-middle attack during secure pairing, the other is about information disclosure due to improper access control.

In order to completely fix both issues, you need an updated kernel as well!
