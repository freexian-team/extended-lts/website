---
title: "ELA-939-1 unrar-nonfree security update"
package: unrar-nonfree
version: 1:5.6.6-1+deb9u2 (stretch)
version_map: {"9 stretch": "1:5.6.6-1+deb9u2"}
description: "arbitrary code execution"
date: 2023-08-29T00:28:12+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-40477

---

A specific flaw within the processing of recovery volumes exists in UnRAR,
an unarchiver for rar files. It allows remote attackers to execute arbitrary
code on affected installations. User interaction is required to exploit this
vulnerability. The target must visit a malicious page or open a malicious rar
file.
