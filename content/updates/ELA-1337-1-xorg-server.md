---
title: "ELA-1337-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u18 (jessie), 2:1.19.2-1+deb9u21 (stretch), 2:1.20.4-1+deb10u16 (buster)
version_map: {"8 jessie": "2:1.16.4-1+deb8u18", "9 stretch": "2:1.19.2-1+deb9u21", "10 buster": "2:1.20.4-1+deb10u16"}
description: "privilege escalation"
date: 2025-03-01T00:23:25+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2025-26594
  - CVE-2025-26595
  - CVE-2025-26596
  - CVE-2025-26597
  - CVE-2025-26598
  - CVE-2025-26599
  - CVE-2025-26600
  - CVE-2025-26601

---

Jan-Niklas Sohn discovered several vulnerabilities in the Xorg X server,
which may result in privilege escalation if the X server is running
privileged.

