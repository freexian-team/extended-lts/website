---
title: "ELA-1064-1 wpa security update"
package: wpa
version: 2.3-1+deb8u14 (jessie), 2:2.4-1+deb9u10 (stretch)
version_map: {"8 jessie": "2.3-1+deb8u14", "9 stretch": "2:2.4-1+deb9u10"}
description: "authentication bypass"
date: 2024-03-24T19:01:24Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-52160

---

The implementation of PEAP in wpa_supplicant allowed authentication bypass. For a successful attack, wpa_supplicant must be configured to not verify the network's TLS certificate during Phase 1 authentication, and an eap_peap_decrypt vulnerability can then be abused to skip Phase 2 authentication. The attack vector is sending an EAP-TLV Success packet instead of starting Phase 2. This allows an adversary to impersonate Enterprise Wi-Fi networks.
