---
title: "ELA-936-1 ruby-rack security update"
package: ruby-rack
version: 1.6.4-4+deb9u5 (stretch)
version_map: {"9 stretch": "1.6.4-4+deb9u5"}
description: "denial of service"
date: 2023-08-28T09:06:31+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-27539

---

It was found out that a carefully crafted input can cause header parsing in
Rack, a modular Ruby webserver interface, to take an unexpected amount of time,
possibly resulting in a denial of service attack vector. Any applications that
parse headers using Rack (virtually all Rails applications) are impacted.
