---
title: "ELA-861-1 emacs24 security update"
package: emacs24
version: 24.4+1-5+deb8u2 (jessie), 24.5+1-11+deb9u2 (stretch)
version_map: {"8 jessie": "24.4+1-5+deb8u2", "9 stretch": "24.5+1-11+deb9u2"}
description: "arbitrary shell command execution"
date: 2023-06-03T02:33:03+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-48339
  - CVE-2023-28617

---

Xi Lu discovered that missing input sanitizing in Emacs could result in the
execution of arbitrary shell commands.
