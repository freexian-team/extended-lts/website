---
title: "ELA-1284-1 fastnetmon security update"
package: fastnetmon
version: 1.1.3+dfsg-8.1+deb10u1 (buster)
version_map: {"10 buster": "1.1.3+dfsg-8.1+deb10u1"}
description: "denial of service"
date: 2024-12-30T13:25:47+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-56073

---

A potential security issue has been discovered in FastNetMon, a fast DDoS
analyzer: Malformed Netflow traffic could result in denial of service.

