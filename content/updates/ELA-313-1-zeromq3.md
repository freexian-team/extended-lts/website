---
title: "ELA-313-1 zeromq3 security update"
package: zeromq3
version: 4.0.5+dfsg-2+deb8u3
distribution: "Debian 8 jessie"
description: "DOS on CURVE/ZAP-protected servers"
date: 2020-11-10T19:15:39+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-15166

---

It was discovered that ZeroMQ, a lightweight messaging kernel library
does not properly handle connecting peers before a handshake is
completed. A remote, unauthenticated client connecting to an application
using the libzmq library, running with a socket listening with CURVE
encryption/authentication enabled can take advantage of this flaw to
cause a denial of service affecting authenticated and encrypted clients.
