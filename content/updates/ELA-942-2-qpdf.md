---
title: "ELA-942-2 qpdf regression update"
package: qpdf
version: 6.0.0-2+deb9u2 (stretch)
version_map: {"9 stretch": "6.0.0-2+deb9u2"}
description: "regression update"
date: 2023-09-30T15:08:14+03:00
draft: false
type: updates
tags:
- update
cvelist:

---

Two patches were dropped that caused compatibility issues after backport,
reopening the following CVEs:
     CVE-2015-9252, CVE-2017-9209, CVE-2017-11625, CVE-2017-11627

