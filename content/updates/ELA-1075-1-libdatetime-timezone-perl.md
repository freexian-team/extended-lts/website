---
title: "ELA-1075-1 libdatetime-timezone-perl new timezone database"
package: libdatetime-timezone-perl
version: 1:1.75-2+2024a (jessie), 1:2.09-1+2024a (stretch)
version_map: {"8 jessie": "1:1.75-2+2024a", "9 stretch": "1:2.09-1+2024a"}
description: "update to tzdata 2024a"
date: 2024-04-24T16:05:23+02:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update includes the changes in tzdata 2024a for the Perl bindings.
