---
title: "ELA-869-1 php-phpseclib security update"
package: php-phpseclib
version: 2.0.4-1 (stretch)
version_map: {"9 stretch": "2.0.4-1"}
description: "improper verification of cryptographic signature"
date: 2023-06-17T19:19:13Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-30130

---

It was discovered that php-phpseclib, a pure-PHP implementation of various cryptographic and arithmetic algorithms, mishandles RSA PKCS#1 v1.5 signature verification. An attacker may get invalid signatures accepted, bypassing authorization control in specific situations.
