---
title: "ELA-693-1 snakeyaml security update"
package: snakeyaml
version: 1.12-2+deb8u1 (jessie), 1.17-1+deb9u1 (stretch)
version_map: {"8 jessie": "1.12-2+deb8u1", "9 stretch": "1.17-1+deb9u1"}
description: "denial of service"
date: 2022-10-03T00:55:31+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-25857
  - CVE-2022-38749
  - CVE-2022-38750
  - CVE-2022-38751

---

Several security vulnerabilities have been discovered in SnakeYaml, a YAML
parser for Java, which could facilitate a denial of service attack whenever
maliciously crafted input files are processed by SnakeYaml.
