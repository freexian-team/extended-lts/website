---
title: "ELA-787-1 ruby-sinatra security update"
package: ruby-sinatra
version: 1.4.7-5+deb9u2 (stretch)
version_map: {"9 stretch": "1.4.7-5+deb9u2"}
description: "reflected file download attack"
date: 2023-01-31T04:15:45+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-45442

---

Sinatra is a domain-specific language for creating web applications in Ruby.
An application is vulnerable to a reflected file download (RFD) attack that
sets the Content-Disposition header of a response when the filename is derived
from user-supplied input.
