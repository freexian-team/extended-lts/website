---
title: "ELA-692-1 exim4 security update"
package: exim4
version: 4.84.2-2+deb8u9 (jessie), 4.89-2+deb9u9 (stretch)
version_map: {"8 jessie": "4.84.2-2+deb8u9", "9 stretch": "4.89-2+deb9u9"}
description: "heap-based buffer overflow"
date: 2022-10-01T18:45:05+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-37452

---

It was discovered that in Exim, a mail transport agent, handling an e-mail can
cause a heap-based buffer overflow in some situations. An attacker can cause a
denial-of-service (DoS) and possibly execute arbitrary code.
