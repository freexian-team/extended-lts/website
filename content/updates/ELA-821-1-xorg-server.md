---
title: "ELA-821-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u11 (jessie), 2:1.19.2-1+deb9u14 (stretch)
version_map: {"8 jessie": "2:1.16.4-1+deb8u11", "9 stretch": "2:1.19.2-1+deb9u14"}
description: "use after free"
date: 2023-03-29T15:32:14+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-1393

---

Jan-Niklas Sohn discovered that a use-after-free flaw in the Composite
extension of the X.org X server may result in privilege escalation if
the X server is running under the root user.
