---
title: "ELA-659-1 mod-wsgi security update"
package: mod-wsgi
version: 4.5.11-1+deb9u1 (stretch)
version_map: {"9 stretch": "4.5.11-1+deb9u1"}
description: "X-Client-IP header is not removed"
date: 2022-08-07T01:21:21+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-2255

---

An issue has been found in mod-wsgi, a Python WSGI adapter module for Apache.
A request from an untrusted proxy does not remove the X-Client-IP header and thus allowing this header to be passed to the target WSGI application.
