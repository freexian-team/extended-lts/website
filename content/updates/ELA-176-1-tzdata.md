---
title: "ELA-176-1 tzdata new upstream version"
package: tzdata
version: 2019c-0+deb7u1
distribution: "Debian 7 Wheezy"
description: "new version update"
date: 2019-10-14T13:27:49+02:00
draft: false
type: updates
cvelist:

---

This update brings the timezone changes from the upstream 2019c release.
