---
title: "ELA-154-1 openjdk-7 security update"
package: openjdk-7
version: 7u231-2.6.19-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2019-08-17T00:06:07+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-2745
  - CVE-2019-2762
  - CVE-2019-2769
  - CVE-2019-2816

---

Several vulnerabilities have been discovered in OpenJDK, an
implementation of the Oracle Java platform, resulting in denial of
service, sandbox bypass, information disclosure or the execution
of arbitrary code.

