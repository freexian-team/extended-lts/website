---
title: "ELA-1076-1 tomcat7 security update"
package: tomcat7
version: 7.0.56-3+really7.0.109-1+deb8u6 (jessie)
version_map: {"8 jessie": "7.0.56-3+really7.0.109-1+deb8u6"}
description: "request smuggling"
date: 2024-04-26T06:30:17+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-46589

---

Norihito Aimoto of OSSTech Corporation discovered a security vulnerability in
the Tomcat servlet and JSP engine.

A trailer header that exceeded the header size limit could cause Tomcat to
treat a single request as multiple requests leading to the possibility of
request smuggling when behind a reverse proxy.
