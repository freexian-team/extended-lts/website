---
title: "ELA-458-1 linux-4.9 security update"
package: linux-4.9
version: 4.9.272-1~deb8u2
distribution: "Debian 8 jessie"
description: "linux kernel update"
date: 2021-07-15T16:44:15+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-24586
  - CVE-2020-24587
  - CVE-2020-24588
  - CVE-2020-25670
  - CVE-2020-25671
  - CVE-2020-25672
  - CVE-2020-26139
  - CVE-2020-26147
  - CVE-2020-26558
  - CVE-2020-29374
  - CVE-2020-36322
  - CVE-2021-0129
  - CVE-2021-0512
  - CVE-2021-3428
  - CVE-2021-3483
  - CVE-2021-3564
  - CVE-2021-3573
  - CVE-2021-3587
  - CVE-2021-20292
  - CVE-2021-23133
  - CVE-2021-23134
  - CVE-2021-28660
  - CVE-2021-28688
  - CVE-2021-28950
  - CVE-2021-28964
  - CVE-2021-28971
  - CVE-2021-29154
  - CVE-2021-29265
  - CVE-2021-29647
  - CVE-2021-29650
  - CVE-2021-30002
  - CVE-2021-31916
  - CVE-2021-32399
  - CVE-2021-33034

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to the execution of arbitrary code, privilege escalation,
denial of service, or information leaks.

This update is not yet available for the armel (ARM EABI soft-float)
architecture.

CVE-2020-24586, CVE-2020-24587, CVE-2020-26147

    Mathy Vanhoef discovered that many Wi-Fi implementations,
    including Linux's mac80211, did not correctly implement reassembly
    of fragmented packets.  In some circumstances, an attacker within
    range of a network could exploit these flaws to forge arbitrary
    packets and/or to access sensitive data on that network.

CVE-2020-24588

    Mathy Vanhoef discovered that most Wi-Fi implementations,
    including Linux's mac80211, did not authenticate the "is
    aggregated" packet header flag.  An attacker within range of a
    network could exploit this to forge arbitrary packets on that
    network.

CVE-2020-25670, CVE-2020-25671, CVE-2021-23134

    kiyin (尹亮) of TenCent discovered several reference counting bugs
    in the NFC LLCP implementation which could lead to use-after-free.
    A local user could exploit these for denial of service (crash or
    memory corruption) or possibly for privilege escalation.

    Nadav Markus and Or Cohen of Palo Alto Networks discovered that
    the original fixes for these introduced a new bug that could
    result in use-after-free and double-free.  This has also been
    fixed.

CVE-2020-25672

    kiyin (尹亮) of TenCent discovered a memory leak in the NFC LLCP
    implementation.  A local user could exploit this for denial of
    service (memory exhaustion).

CVE-2020-26139

    Mathy Vanhoef discovered that a bug in some Wi-Fi implementations,
    including Linux's mac80211.  When operating in AP mode, they would
    forward EAPOL frames from one client to another while the sender
    was not yet authenticated.  An attacker within range of a network
    could use this for denial of service or as an aid to exploiting
    other vulnerabilities.

CVE-2020-26558, CVE-2021-0129

    Researchers at ANSSI discovered vulnerabilities in the Bluetooth
    Passkey authentication method, and in Linux's implementation of
    it.  An attacker within range of two Bluetooth devices while they
    pair using Passkey authentication could exploit this to obtain the
    shared secret (Passkey) and then impersonate either of the devices
    to each other.

CVE-2020-29374

    Jann Horn of Google reported a flaw in Linux's virtual memory
    management.  A parent and child process initially share all their
    memory, but when either writes to a shared page, the page is
    duplicated and unshared (copy-on-write).  However, in case an
    operation such as vmsplice() required the kernel to take an
    additional reference to a shared page, and a copy-on-write occurs
    during this operation, the kernel might have accessed the wrong
    process's memory.  For some programs, this could lead to an
    information leak or data corruption.

CVE-2020-36322, CVE-2021-28950

    The syzbot tool found that the FUSE (filesystem-in-user-space)
    implementation did not correctly handle a FUSE server returning
    invalid attributes for a file.  A local user permitted to run a
    FUSE server could use this to cause a denial of service (crash).

    The original fix for this introduced a different potential denial
    of service (infinite loop in kernel space), which has also been
    fixed.

CVE-2021-3428

    Wolfgang Frisch reported a potential integer overflow in the ext4
    filesystem driver. A user permitted to mount arbitrary filesystem
    images could use this to cause a denial of service (crash).

CVE-2021-3483

    马哲宇 (Zheyu Ma) reported a bug in the "nosy" driver for TI
    PCILynx FireWire controllers, which could lead to list corruption
    and a use-after-free.  On a system that uses this driver, local
    users granted access to /dev/nosy could exploit this to cause a
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.

CVE-2021-3564, CVE-2021-3573, CVE-2021-32399

    The BlockSec team discovered several race conditions in the
    Bluetooth subsystem that could lead to a use-after-free or
    double-free.  A local user could exploit these to caue a denial of
    service (crash or memory corruption) or possibly for privilege
    escalation.

CVE-2021-3587

    Active Defense Lab of Venustech discovered a potential null
    pointer dereference in the NFC LLCP implementation.  A local user
    could use this to cause a denial of service (crash).

CVE-2021-20292

    It was discovered that the TTM buffer allocation API used by GPU
    drivers did not handle allocation failures in the way that most
    drivers expected, resulting in a double-free on failure.  A local
    user on a system using one of these drivers could possibly exploit
    this to cause a denial of service (crash or memory corruption) or
    for privilege escalation.  The API has been changed to match
    driver expectations.

CVE-2021-23133

    Or Cohen of Palo Alto Networks discovered a race condition in the
    SCTP implementation, which can lead to list corruption.  A local
    user could exploit this to cause a denial of service (crash or
    memory corruption) or possibly for privilege escalation.

CVE-2021-28660

    It was discovered that the rtl8188eu WiFi driver did not correctly
    limit the length of SSIDs copied into scan results. An attacker
    within WiFi range could use this to cause a denial of service
    (crash or memory corruption) or possibly to execute code on a
    vulnerable system.

CVE-2021-28688 (XSA-371)

    It was discovered that the original fix for CVE-2021-26930
    (XSA-365) introduced a potential resource leak.  A malicious guest
    could presumably exploit this to cause a denial of service
    (resource exhaustion) within the host.

CVE-2021-28964

    Zygo Blaxell reported a race condition in the Btrfs driver which
    can lead to an assertion failure.  On systems using Btrfs, a local
    user could exploit this to cause a denial of service (crash).

CVE-2021-28971

    Vince Weaver reported a bug in the performance event handler for
    Intel PEBS.  A workaround for a hardware bug on Intel CPUs
    codenamed "Haswell" and earlier could lead to a null pointer
    dereference.  On systems with the affected CPUs, if users are
    permitted to access performance events, a local user may exploit
    this to cause a denial of service (crash).

    By default, unprivileged users do not have access to performance
    events, which mitigates this issue.  This is controlled by the
    kernel.perf_event_paranoid sysctl.

CVE-2021-29154

    It was discovered that the Extended BPF (eBPF) JIT compiler
    for x86_64 generated incorrect branch instructions in some
    cases.  On systems where eBPF JIT is enabled, users could
    exploit this to execute arbitrary code in the kernel.

    By default, eBPF JIT is disabled, mitigating this issue.  This is
    controlled by the net.core.bpf_jit_enable sysctl.

CVE-2021-29265

    The syzbot tool found a race condition in the USB/IP host
    (server) implementation which can lead to a null pointer
    dereference.  On a system acting as a USB/IP host, a client
    can exploit this to cause a denial of service (crash).

CVE-2021-29647

    The syzbot tool found an information leak in the Qualcomm IPC
    Router (qrtr) implementation.

    This protocol is not enabled in Debian's official kernel
    configurations.

CVE-2021-29650

    It was discovered that a data race in the netfilter subsystem
    could lead to a null pointer dereference during replacement of a
    table.  A local user with CAP_NET_ADMIN capability in any user
    namespace could use this to cause a denial of service (crash).

    By default, unprivileged users cannot create user namespaces,
    which mitigates this issue.  This is controlled by the
    kernel.unprivileged_userns_clone sysctl.

CVE-2021-30002

    Arnd Bergmann and the syzbot tool found a memory leak in the
    Video4Linux (v4l) subsystem.  A local user permitted to access
    video devices (by default, any member of the "video" group) could
    exploit this to cause a denial of service (memory exhaustion).

CVE-2021-31916

    Dan Carpenter reported incorrect parameter validation in the
    device-mapper (dm) subsystem, which could lead to a heap buffer
    overrun.  However, only users with CAP_SYS_ADMIN capability
    (i.e. root-equivalent) could trigger this bug, so it did not
    have any security impact in this kernel version.

CVE-2021-33034

    The syzbot tool found a bug in the Bluetooth subsystem that could
    lead to a use-after-free.  A local user could use this to cause a
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.
