---
title: "ELA-851-1 uwsgi security update"
package: uwsgi
version: 2.0.14+20161117-3+deb9u6 (stretch)
version_map: {"9 stretch": "2.0.14+20161117-3+deb9u6"}
description: "inconsistent interpretation of HTTP Requests (HTTP Response Smuggling) vulnerability"
date: 2023-05-20T08:08:27Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-27522

---

A HTTP Response Smuggling vulnerability was fixed mod_proxy_uwsgi
apache module included in uwsgi package. Special characters in the
origin response header can truncate/split the response forwarded to
the client.
