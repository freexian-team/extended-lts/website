---
title: "ELA-996-1 request-tracker4 security update"
package: request-tracker4
version: 4.4.1-3+deb9u6 (stretch)
version_map: {"9 stretch": "4.4.1-3+deb9u6"}
description: "multiple vulnerabilities"
date: 2023-11-02T11:11:55-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-41259
  - CVE-2023-41260

---

Multiple vulnerabilities have been discovered in Request Tracker, an extensible trouble-ticket tracking system.

CVE-2023-41259

    Tom Wolters reported that Request Tracker is vulnerable to accepting
    unvalidated RT email headers in incoming email and the mail-gateway REST
    interface.

CVE-2023-41260

    Tom Wolters reported that Request Tracker is vulnerable to information
    leakage via response messages returned from requests sent via the
    mail-gateway REST interface

Even if these issues have been fixed, it is strongly recommended to ensure
that `.../REST/1.0/NoAuth` is only accessible for host(s) that run `rt-mailgate`
for submitting email to RT.  This is often the system which has
request-tracker4 installed. The sample configurations supplied by these
packages for Apache2 and Nginx restrict access to localhost only.
