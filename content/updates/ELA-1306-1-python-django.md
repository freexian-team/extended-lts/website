---
title: "ELA-1306-1 python-django security update"
package: python-django
version: 1.7.11-1+deb8u18 (jessie), 1:1.10.7-2+deb9u24 (stretch), 1:1.11.29-1+deb10u13 (buster)
version_map: {"8 jessie": "1.7.11-1+deb8u18", "9 stretch": "1:1.10.7-2+deb9u24", "10 buster": "1:1.11.29-1+deb10u13"}
description: "multiple vulnerabilities"
date: 2025-01-28T11:17:55+00:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-53907
  - CVE-2024-56374
---

Two vulnerabilities were discovered in [Django](https://www.djangoproject.com/),
a Python-based web development framework:

* `CVE-2024-53907`: Prevent a potential Denial of Service (DoS) attack. The
  `strip_tags` method and `striptags` template filter were subject to a
  potential denial-of-service attack via certain inputs containing large
  sequences of nested incomplete HTML entities.

* `CVE-2024-56374`: Prevent another potential Denial of Service (DoS) attack.
  Lack of upper-bound limit enforcement in strings passed when performing IPv6
  validation could have led to a potential denial-of-service attack. The
  `clean_ipv6_address` and `is_valid_ipv6_address` functions were vulnerable as
  was the `GenericIPAddressField` form field. The `GenericIPAddressField` model
  field was not affected.
