---
title: "ELA-739-1 nginx security update"
package: nginx
version: 1.6.2-5+deb8u10 (jessie), 1.10.3-1+deb9u8 (stretch)
version_map: {"8 jessie": "1.6.2-5+deb8u10", "9 stretch": "1.10.3-1+deb9u8"}
description: "denial of service"
date: 2022-11-23T22:54:35+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-3618
  - CVE-2022-41741
  - CVE-2022-41742

---

It was discovered that parsing errors in the mp4 module of Nginx, a
high-performance web and reverse proxy server, could result in denial
of service, memory disclosure or potentially the execution of arbitrary
code when processing a malformed mp4 file.

This module is only enabled in the nginx-extras binary package.

In addition the following vulnerability has been fixed.

CVE-2021-3618

    ALPACA is an application layer protocol content confusion attack,
    exploiting TLS servers implementing different protocols but using
    compatible certificates, such as multi-domain or wildcard certificates.
    A MiTM attacker having access to victim's traffic at the TCP/IP layer can
    redirect traffic from one subdomain to another, resulting in a valid TLS
    session. This breaks the authentication of TLS and cross-protocol attacks
    may be possible where the behavior of one protocol service may compromise
