---
title: "ELA-135-1 python2.6 security update"
package: python2.6
version: 2.6.8-1.1+deb7u3
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2019-06-25T03:25:02Z
draft: false
type: updates
cvelist:
  - CVE-2019-9636
  - CVE-2019-9740
  - CVE-2019-9947
  - CVE-2019-9948

---

Multiple vulnerabilities were discovered in Python, an interactive
high-level object-oriented language, including 

CVE-2019-9636

    Improper Handling of Unicode Encoding (with an incorrect netloc)
    during NFKC normalization resulting in information disclosure
    (credentials, cookies, etc. that are cached against a given
    hostname).  A specially crafted URL could be incorrectly parsed to
    locate cookies or authentication data and send that information to
    a different host than when parsed correctly.

CVE-2019-9740

    An issue was discovered in urllib2 where CRLF injection is possible
    if the attacker controls a url parameter, as demonstrated by the
    first argument to urllib.request.urlopen with \r\n (specifically in
    the query string after a ? character) followed by an HTTP header or
    a Redis command.

CVE-2019-9947

    An issue was discovered in urllib2 where CRLF injection is possible
    if the attacker controls a url parameter, as demonstrated by the
    first argument to urllib.request.urlopen with \r\n (specifically in
    the path component of a URL that lacks a ? character) followed by an
    HTTP header or a Redis command. This is similar to the CVE-2019-9740
    query string issue.

CVE-2019-9948

    urllib supports the local_file: scheme, which makes it easier for
    remote attackers to bypass protection mechanisms that blacklist
    file: URIs, as demonstrated by triggering a
    urllib.urlopen('local_file:///etc/passwd') call.

