---
title: "ELA-1079-1 pillow security update"
package: pillow
version: 2.6.1-2+deb8u10 (jessie), 4.0.0-4+deb9u6 (stretch)
version_map: {"8 jessie": "2.6.1-2+deb8u10", "9 stretch": "4.0.0-4+deb9u6"}
description: "buffer overflow"
date: 2024-04-28T15:48:30+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-28219

---

A buffer overflow in _imagingcms.c was fixed in Pillow, an image processing library for Python.

