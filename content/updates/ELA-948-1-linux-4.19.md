---
title: "ELA-948-1 linux-4.19 security update"
package: linux-4.19
version: 4.19.289-2~deb8u1 (jessie), 4.19.289-2~deb9u1 (stretch)
version_map: {"8 jessie": "4.19.289-2~deb8u1", "9 stretch": "4.19.289-2~deb9u1"}
description: "linux kernel update"
date: 2023-09-20T13:25:15+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-40982

---

Daniel Moghimi discovered Gather Data Sampling (GDS), a hardware
vulnerability for Intel CPUs which allows unprivileged speculative
access to data which was previously stored in vector registers.

This mitigation requires updated CPU microcode provided in the
intel-microcode package and released as ELA-935-1.

For details please refer to <https://downfall.page/> and
<https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html>.
