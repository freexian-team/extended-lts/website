---
title: "ELA-302-1 poppler security update"
package: poppler
version: 0.26.5-2+deb8u14
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2020-10-25T23:53:57+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-10018
  - CVE-2019-14494

---

This update corrects several security vulnerabilities in poppler, a PDF
rendering library, a regression introduced by the patch for CVE-2018-13988
(Debian bug #942391), and two flaws which could lead to a denial-of-service.

CVE-2019-10018

    Floating point exception in the function PostScriptFunction::exec at
    Function.cc

CVE-2019-14494

    There is a divide-by-zero error in the function
    SplashOutputDev::tilingPatternFill at SplashOutputDev.cc.
