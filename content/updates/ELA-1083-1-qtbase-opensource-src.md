---
title: "ELA-1083-1 qtbase-opensource-src security update"
package: qtbase-opensource-src
version: 5.7.1+dfsg-3+deb9u4 (stretch)
version_map: {"9 stretch": "5.7.1+dfsg-3+deb9u4"}
description: "buffer overflow, infinite loop, application crash"
date: 2024-05-01T00:53:01+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-24607
  - CVE-2023-32763
  - CVE-2023-33285
  - CVE-2023-37369
  - CVE-2023-38197

---

Several issues have been found in qtbase-opensource-src, a collection of
several Qt modules/libraries.
The issues are related to buffer overflows, infinite loops or application
crashes due to processing of crafted input files.

