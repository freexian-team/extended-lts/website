---
title: "ELA-1218-1 asterisk security update"
package: asterisk
version: 1:13.14.1~dfsg-2+deb9u10 (stretch)
version_map: {"9 stretch": "1:13.14.1~dfsg-2+deb9u10"}
description: "privilege escalation"
date: 2024-10-27T19:03:39+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-42365

---

One issue has been found in asterisk, an Open Source Private Branch Exchange.

CVE-2024-42365

    Due to a privilege escalation, remote code execution and/or
    blind server-side request forgery with arbitrary protocol are
    possible.


Thanks to Niels Galjaard, a minor privilege escalation has been fixed. More information about ths can be found at:
https://alioth-lists.debian.net/pipermail/pkg-voip-maintainers/2024-July/038664.html

Please be aware that this fix explicitly sets the gid of the asterisk process to "asterisk".
In case you added the user asterisk to other groups, please update your systemd service file accordingly.
~                                                                                                    
