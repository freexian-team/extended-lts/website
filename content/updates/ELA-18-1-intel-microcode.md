---
title: "ELA-18-1 intel-microcode security update"
package: intel-microcode
version: 3.20180703.2~bpo8+1~deb7u1
distribution: "Debian 7 Wheezy"
description: "mitigations for Spectre v2"
date: 2018-07-19T22:30:53+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-5715
  - CVE-2018-3639
  - CVE-2018-3640
---
This update is required to mitigate against the so called Spectre variant 2 (branch
target injection) vulnerability which requires an update to the processors
microcode, which is non-free.

For instance you can find more information about this topic at

https://meltdownattack.com/

For recent Intel processors, the update is included in the intel-microcode
package from version 3.20180703.2~bpo8+1~deb7u1. It is available via the
wheezy-lts-kernel repository. For other processors, it may be included in an
update to the system BIOS or UEFI firmware, or in a later update to the
amd64-microcode package.
