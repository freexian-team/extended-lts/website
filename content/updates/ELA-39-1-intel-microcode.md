---
title: "ELA-39-1 intel-microcode security update"
package: intel-microcode
version: 3.20180807a.1~deb7u1
distribution: "Debian 7 Wheezy"
description: "mitigations for Spectre"
date: 2018-09-17T14:54:41+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-3615
  - CVE-2018-3620
  - CVE-2018-3646
  - CVE-2018-3639
  - CVE-2018-3640
  - CVE-2017-5715
---

Security researchers identified speculative execution side-channel
methods which have the potential to improperly gather sensitive data
from multiple types of computing devices with different vendors’
processors and operating systems.

In order to fix those issues an update to the intel-microcode package is
required, which is non-free. It is related to ELA-18-1 and adds more
mitigations for additional types of Intel processors.

For more information please also read the official Intel security
advisories at:


 - https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00088.html
 - https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00115.html
 - https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00161.html
