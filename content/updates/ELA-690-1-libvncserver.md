---
title: "ELA-690-1 libvncserver security update"
package: libvncserver
version: 0.9.11+dfsg-1.3~deb9u7 (stretch)
version_map: {"9 stretch": "0.9.11+dfsg-1.3~deb9u7"}
description: "memory leak"
date: 2022-09-30T00:01:51+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-29260

---


An issue has been found in libvncserver, a library to write one's own VNC server.
Due to a memory leak in function rfbClientCleanup() a remote attacker might be able to cause a denial of service.
