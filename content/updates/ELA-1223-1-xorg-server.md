---
title: "ELA-1223-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u17 (jessie), 2:1.19.2-1+deb9u20 (stretch), 2:1.20.4-1+deb10u15 (buster)
version_map: {"8 jessie": "2:1.16.4-1+deb8u17", "9 stretch": "2:1.19.2-1+deb9u20", "10 buster": "2:1.20.4-1+deb10u15"}
description: "local privilege escalation"
date: 2024-10-31T19:53:25+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-9632

---

Jan-Niklas Sohn working with Trend Micro Zero Day Initiative found an
issue in the X server and Xwayland implementations published by X.Org.
CVE-2024-9632 can be triggered by providing a modified bitmap to the X.Org
server. This may lead to local privilege escalation if the server is run
as root or remote code execution (e.g. x11 over ssh).

