---
title: "ELA-1320-1 openjdk-8 new java update"
package: openjdk-8
version: 8u442-ga-1~deb8u1 (jessie), 8u442-ga-1~deb9u1 (stretch)
version_map: {"8 jessie": "8u442-ga-1~deb8u1", "9 stretch": "8u442-ga-1~deb9u1"}
description: "new java update"
date: 2025-02-12T10:51:49+01:00
draft: false
type: updates
tags:
- update
cvelist:

---

This update brings OpenJDK 8u442, which comes with stability and bug fixes.
