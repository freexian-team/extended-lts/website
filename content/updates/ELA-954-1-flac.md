---
title: "ELA-954-1 flac security update"
package: flac
version: 1.3.0-3+deb8u3 (jessie), 1.3.2-2+deb9u3 (stretch)
version_map: {"8 jessie": "1.3.0-3+deb8u3", "9 stretch": "1.3.2-2+deb9u3"}
description: "arbitrary code execution"
date: 2023-09-22T12:52:45+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-22219

---

A buffer overflow was discovered in flac, a library handling Free
Lossless Audio Codec media, which could potentially result in the
execution of arbitrary code.
