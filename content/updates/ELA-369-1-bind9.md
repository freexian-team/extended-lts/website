---
title: "ELA-369-1 bind9 security update"
package: bind9
version: 1:9.9.5.dfsg-9+deb8u21
distribution: "Debian 8 Jessie"
description: "buffer overflow vulnerability"
date: 2021-02-19T08:50:26+00:00
draft: false
type: updates
cvelist:
  - CVE-2020-8625
---

It was discovered that there was a buffer overflow attack in the `bind9` DNS
server caused by an issue in the GSSAPI ("Generic Security Services") security
policy negotiation.
