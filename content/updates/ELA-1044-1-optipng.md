---
title: "ELA-1044-1 optipng security update"
package: optipng
version: 0.7.5-1+deb8u3 (jessie), 0.7.6-1+deb9u2 (stretch)
version_map: {"8 jessie": "0.7.5-1+deb8u3", "9 stretch": "0.7.6-1+deb9u2"}
description: "multiple vulnerabilities"
date: 2024-02-23T13:09:56Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2015-7802
  - CVE-2023-43907

---

Optipng, a tool for optimizing image files, by recompressesing image files to a smaller size,
without losing any information, was vulnerable.

CVE-2015-7802

    Under Debian 8 (jessie), optipng allowed remote attackers to cause a denial of service (uninitialized memory read) via a crafted GIF file. Debian 9, stretch, was already fixed.

CVE-2023-43907

    A global buffer overflow via the 'buffer' variable at gifread.c, was found.
