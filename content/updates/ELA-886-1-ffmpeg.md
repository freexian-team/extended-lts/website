---
title: "ELA-886-1 ffmpeg security update"
package: ffmpeg
version: 7:3.2.19-0+deb9u2 (stretch)
version_map: {"9 stretch": "7:3.2.19-0+deb9u2"}
description: "null pointer dereferences"
date: 2023-06-30T23:52:05+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-3109
  - CVE-2022-3341

---

Two null pointer dereferences have been fixed in the FFmpeg multimedia framework.

CVE-2022-3109

    Null pointer dereference in vp3_decode_frame()

CVE-2022-3341

    Null pointer dereference in nutdec.c

