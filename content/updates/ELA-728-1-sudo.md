---
title: "ELA-728-1 sudo security update"
package: sudo
version: 1.8.19p1-2.1+deb9u4 (stretch)
version_map: {"9 stretch": "1.8.19p1-2.1+deb9u4"}
description: "information disclosure vulnerability"
date: 2022-11-09T08:26:57+00:00
draft: false
cvelist:
  - CVE-2021-23239
---

It was discovered that there was a information disclosure vulnerability in
`sudo`, a tool used to provide limited superuser privileges to specific users.

A local unprivileged user may have been able to perform arbitrary
directory-existence tests by exploiting a race condition in `sudoedit`.
