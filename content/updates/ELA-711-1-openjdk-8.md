---
title: "ELA-711-1 openjdk-8 security update"
package: openjdk-8
version: 8u352-ga-1~deb8u1 (jessie), 8u352-ga-1~deb9u1 (stretch)
version_map: {"8 jessie": "8u352-ga-1~deb8u1", "9 stretch": "8u352-ga-1~deb9u1"}
description: "multiple vulnerabilities"
date: 2022-10-26T19:38:12+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-21619
  - CVE-2022-21624
  - CVE-2022-21626
  - CVE-2022-21628

---

Several vulnerabilities have been discovered in the OpenJDK Java
runtime, which may result in information disclosure or denial of service.
