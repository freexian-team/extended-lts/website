---
title: "ELA-224-2 ntp security update"
package: ntp
version: 1:4.2.6.p5+dfsg-2+deb7u9
distribution: "Debian 7 Wheezy"
description: "denial of service vulnerability update"
date: 2020-04-30T11:49:21+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-11868
---

A Denial of Service (DoS) vulnerability was discovered in the network time
protocol server/client, ntp.

ntp allowed an "off-path" attacker to block unauthenticated synchronisation via
a server mode packet with a spoofed source IP address because transmissions
were rescheduled even if a packet lacked a valid "origin timestamp".

Whilst this was initially addressed in version `1:4.2.6.p5+dfsg-2+deb7u8`,
lhis update adds further protection that was not present before.
