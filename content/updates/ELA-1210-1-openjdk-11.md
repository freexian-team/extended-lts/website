---
title: "ELA-1210-1 openjdk-11 security update"
package: openjdk-11
version: 11.0.25+9-1~deb10u1 (buster)
version_map: {"10 buster": "11.0.25+9-1~deb10u1"}
description: "multiple vulnerabilities"
date: 2024-10-22T17:19:50+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-21208
  - CVE-2024-21210
  - CVE-2024-21217
  - CVE-2024-21235

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
which may result in denial of service, information disclosure or bypass
of Java sandbox restrictions.
