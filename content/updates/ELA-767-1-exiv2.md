---
title: "ELA-767-1 exiv2 security update"
package: exiv2
version: 0.24-4.1+deb8u7 (jessie)
version_map: {"8 jessie": "0.24-4.1+deb8u7"}
description: "several issues with untrusted input"
date: 2023-01-11T10:35:15+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-9239
  - CVE-2017-11591
  - CVE-2017-14859
  - CVE-2017-14862
  - CVE-2017-14864
  - CVE-2017-17669
  - CVE-2017-18005
  - CVE-2019-13110
  - CVE-2019-13112
  - CVE-2020-18771
  - CVE-2021-29458
  - CVE-2021-32815
  - CVE-2021-34334
  - CVE-2021-37620
  - CVE-2021-37622

---

This update fixes a number of memory access violations and other input
validation failures that can be triggered by passing specially crafted files to
exiv2.                                                                                                                                                                                  

CVE-2017-11591

    There is a Floating point exception in the Exiv2::ValueType function
    in Exiv2 0.26 that will lead to a remote denial of service attack via
    crafted input.

CVE-2017-14859

    An Invalid memory address dereference was discovered in
    Exiv2::StringValueBase::read in value.cpp in Exiv2 0.26. The
    vulnerability causes a segmentation fault and application crash, which
    leads to denial of service.

CVE-2017-14862

    An Invalid memory address dereference was discovered in
    Exiv2::DataValue::read in value.cpp in Exiv2 0.26. The vulnerability
    causes a segmentation fault and application crash, which leads to
    denial of service.

CVE-2017-14864

    An Invalid memory address dereference was discovered in
    Exiv2::getULong in types.cpp in Exiv2 0.26. The vulnerability causes a
    segmentation fault and application crash, which leads to denial of
    service.

CVE-2017-17669

    There is a heap-based buffer over-read in the
    Exiv2::Internal::PngChunk::keyTXTChunk function of pngchunk_int.cpp in
    Exiv2 0.26. A crafted PNG file will lead to a remote denial of service
    attack.

CVE-2017-18005

    Exiv2 0.26 has a Null Pointer Dereference in the
    Exiv2::DataValue::toLong function in value.cpp, related to crafted
    metadata in a TIFF file.

CVE-2017-9239

    An issue was discovered in Exiv2 0.26. When the data structure of the
    structure ifd is incorrect, the program assigns pValue_ to 0x0, and
    the value of pValue() is 0x0. TiffImageEntry::doWriteImage will use
    the value of pValue() to cause a segmentation fault. To exploit this
    vulnerability, someone must open a crafted tiff file.

CVE-2019-13110

    A CiffDirectory::readDirectory integer overflow and out-of-bounds read
    in Exiv2 through 0.27.1 allows an attacker to cause a denial of
    service (SIGSEGV) via a crafted CRW image file.

CVE-2019-13112

    A PngChunk::parseChunkContent uncontrolled memory allocation in Exiv2
    through 0.27.1 allows an attacker to cause a denial of service (crash
    due to an std::bad_alloc exception) via a crafted PNG image file.

CVE-2020-18771

    Exiv2 0.27.99.0 has a global buffer over-read in
    Exiv2::Internal::Nikon1MakerNote::print0x0088 in nikonmn_int.cpp which
    can result in an information leak.

CVE-2021-29458

    Exiv2 is a command-line utility and C++ library for reading, writing,
    deleting, and modifying the metadata of image files. An out-of-bounds
    read was found in Exiv2 versions v0.27.3 and earlier. The out-of-
    bounds read is triggered when Exiv2 is used to write metadata into a
    crafted image file. An attacker could potentially exploit the
    vulnerability to cause a denial of service by crashing Exiv2, if they
    can trick the victim into running Exiv2 on a crafted image file. Note
    that this bug is only triggered when writing the metadata, which is a
    less frequently used Exiv2 operation than reading the metadata. For
    example, to trigger the bug in the Exiv2 command-line application, you
    need to add an extra command-line argument such as insert. The bug is
    fixed in version v0.27.4.

CVE-2021-32815

    Exiv2 is a command-line utility and C++ library for reading, writing,
    deleting, and modifying the metadata of image files. The assertion
    failure is triggered when Exiv2 is used to modify the metadata of a
    crafted image file. An attacker could potentially exploit the
    vulnerability to cause a denial of service, if they can trick the
    victim into running Exiv2 on a crafted image file. Note that this bug
    is only triggered when modifying the metadata, which is a less
    frequently used Exiv2 operation than reading the metadata. For
    example, to trigger the bug in the Exiv2 command-line application, you
    need to add an extra command-line argument such as `fi`. ### Patches
    The bug is fixed in version v0.27.5. ### References Regression test
    and bug fix: #1739 ### For more information Please see our [security
    policy](https://github.com/Exiv2/exiv2/security/policy) for
    information about Exiv2 security.

CVE-2021-34334

    Exiv2 is a command-line utility and C++ library for reading, writing,
    deleting, and modifying the metadata of image files. An infinite loop
    is triggered when Exiv2 is used to read the metadata of a crafted
    image file. An attacker could potentially exploit the vulnerability to
    cause a denial of service, if they can trick the victim into running
    Exiv2 on a crafted image file. The bug is fixed in version v0.27.5.

CVE-2021-37620

    Exiv2 is a command-line utility and C++ library for reading, writing,
    deleting, and modifying the metadata of image files. An out-of-bounds
    read was found in Exiv2 versions v0.27.4 and earlier. The out-of-
    bounds read is triggered when Exiv2 is used to read the metadata of a
    crafted image file. An attacker could potentially exploit the
    vulnerability to cause a denial of service, if they can trick the
    victim into running Exiv2 on a crafted image file. The bug is fixed in
    version v0.27.5.

CVE-2021-37622

    Exiv2 is a command-line utility and C++ library for reading, writing,
    deleting, and modifying the metadata of image files. An infinite loop
    was found in Exiv2 versions v0.27.4 and earlier. The infinite loop is
    triggered when Exiv2 is used to modify the metadata of a crafted image
    file. An attacker could potentially exploit the vulnerability to cause
    a denial of service, if they can trick the victim into running Exiv2
    on a crafted image file. Note that this bug is only triggered when
    deleting the IPTC data, which is a less frequently used Exiv2
    operation that requires an extra command line option (`-d I rm`). The
    bug is fixed in version v0.27.5.
