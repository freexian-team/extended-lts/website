---
title: "ELA-1047-1 bind9 security update"
package: bind9
version: 1:9.9.5.dfsg-9+deb8u30 (jessie), 1:9.10.3.dfsg.P4-12.3+deb9u15 (stretch)
version_map: {"8 jessie": "1:9.9.5.dfsg-9+deb8u30", "9 stretch": "1:9.10.3.dfsg.P4-12.3+deb9u15"}
description: "denial of service due to stack exhaustion"
date: 2024-02-25T09:34:45+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-3341

---

An issue has been discovered in BIND, a DNS server implementation.

A stack exhaustion flaw was discovered in the control channel code
which may result in denial of service (named daemon crash).
