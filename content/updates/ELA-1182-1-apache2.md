---
title: "ELA-1182-1 apache2 security update"
package: apache2
version: 2.4.59-1~deb10u3 (buster)
version_map: {"10 buster": "2.4.59-1~deb10u3"}
description: "multiple vulnerabilities"
date: 2024-09-24T21:48:30Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-38474
  - CVE-2024-38475

---

Apache2, a popular webserver, was vulnerable.

CVE-2024-38474

	Substitution encoding issue in mod_rewrite in Apache HTTP Server
	allowed and attacker to execute scripts in directories permitted
	by the configuration but not directly reachable by any URL or
	source disclosure of scripts meant to only to be executed as CGI.
	Some RewriteRules that capture and substitute unsafely will
	now fail unless rewrite flag "UnsafeAllow3F" is specified.


CVE-2024-38475

	Improper escaping of output in mod_rewrite allowed an attacker
	to map URLs to filesystem locations that are permitted
	to be served by the server but are not intentionally/directly
	reachable by any URL, resulting in code execution
	or source code disclosure.
	Substitutions in server context that use a backreferences
	or variables as the first segment of the substitution are affected.
	Some unsafe RewiteRules will be broken by this change
	and the rewrite flag "UnsafePrefixStat" can be used
	to opt back in once ensuring the substitution is
	appropriately constrained.
