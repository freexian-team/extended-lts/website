---
title: "ELA-737-1 postgresql-9.6 security update"
package: postgresql-9.6
version: 9.6.24-0+deb9u2 (stretch)
version_map: {"9 stretch": "9.6.24-0+deb9u2"}
description: "arbitrary code execution"
date: 2022-11-23T15:38:41-05:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-2625
  - CVE-2022-1552
---

* CVE-2022-2625

    Sven Klemm found that some extensions in the PostgreSQL database
    system could replace objects not belonging to the extension. An
    attacker could leverage this to run arbitrary commands as another
    user.

* CVE-2022-1552

    Alexander Lakhin discovered that the autovacuum feature and multiple
    commands could escape the "security-restricted operation" sandbox.
