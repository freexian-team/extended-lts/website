---
title: "ELA-442-1 squid3 security update"
package: squid3
version: 3.5.23-5+deb8u4
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-06-09T18:18:02+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-28651
  - CVE-2021-28652
  - CVE-2021-31806
  - CVE-2021-31807
  - CVE-2021-31808
  - CVE-2021-33620

---

Joshua Rogers discovered several vulnerabilities in Squid, a proxy
caching server. An attacker could cause Denial of Service (DoS).

* CVE-2021-28651

    Denial of Service in URN processing.

* CVE-2021-28652

    Denial of Service issue in Cache Manager.

* CVE-2021-31806, CVE-2021-31807, CVE-2021-31808

    Multiple Issues in HTTP Range header.

* CVE-2021-33620

    Denial of Service in HTTP Response processing.
