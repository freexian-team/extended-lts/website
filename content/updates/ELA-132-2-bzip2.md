---
title: "ELA-132-2 bzip2 regression update"
package: bzip2
version: 
distribution: "Debian 7 Wheezy"
description: "upstream regression"
date: 2019-07-18T19:49:44+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-12900

---

The original fix for CVE-2019-12900 introduces regressions when extracting certain lbzip2 files which were created with a buggy libzip2: https://bugs.debian.org/931278

