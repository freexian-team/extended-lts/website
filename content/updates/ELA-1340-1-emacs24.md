---
title: "ELA-1340-1 emacs24 security update"
package: emacs24
version: 24.4+1-5+deb8u6 (jessie), 24.5+1-11+deb9u6 (stretch)
version_map: {"8 jessie": "24.4+1-5+deb8u6", "9 stretch": "24.5+1-11+deb9u6"}
description: "multiple vulnerabilities"
date: 2025-03-05T18:16:25+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-45939
  - CVE-2024-53920
  - CVE-2025-1244

---

Multiple problems were discovered in GNU Emacs, the extensible, customisable,
self-documenting real-time display editor.

### CVE-2022-45939

Improper use of the `system` C library function in Emacs's implementation of
the `ctags` program could permit shell metacharcater injection when used on
untrusted input source code.

### CVE-2024-53920

Several ways to trigger arbitrary code execution were discovered in Emacs's
support for editing files in its own dialect of Lisp. These include arbitrary
code execution upon opening an otherwise innocent-looking file, with any (or
no) file extension, for editing.

### CVE-2025-1244

Improper handling of custom 'man' URI schemes could allow an attacker to
execute arbitrary shell commands by tricking users into visiting a specially
crafted website, or an HTTP URL with a redirect.

