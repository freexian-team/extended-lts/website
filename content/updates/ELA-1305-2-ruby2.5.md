---
title: "ELA-1305-2 ruby2.5 regression update"
package: ruby2.5
version: 2.5.5-3+deb10u9 (buster)
version_map: {"10 buster": "2.5.5-3+deb10u9"}
description: "regression with xml: namespace of REXML gem"
date: 2025-02-12T23:49:11Z
draft: false
type: updates
tags:
- update
cvelist:

---

A regression was found in the REXML gem shipped with ruby2.5.

Some valid XML file were wrongly considered invalid for some namespace
corner case (particularly XML file using the xml: namespace).
