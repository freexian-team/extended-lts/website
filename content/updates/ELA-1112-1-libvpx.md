---
title: "ELA-1112-1 libvpx security update"
package: libvpx
version: 1.3.0-3+deb8u5 (jessie), 1.6.1-3+deb9u6 (stretch)
version_map: {"8 jessie": "1.3.0-3+deb8u5", "9 stretch": "1.6.1-3+deb9u6"}
description: "multiple vulnerabilities"
date: 2024-06-18T23:39:23+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2016-6711
  - CVE-2017-0393
  - CVE-2024-5197

---

Multiple vulnerabilities have been fixed in libvpx, a library for decoding and encoding VP8 and VP9 videos.

CVE-2016-6711 (vulnerability was not present in stretch)

    VP8 decoder crash with invalid leading keyframes

CVE-2017-0393 (vulnerability was not present in stretch)

    VP8 threading issues

CVE-2024-5197

    Integer overflows
