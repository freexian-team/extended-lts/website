---
title: "ELA-774-1 linux-5.10 security update"
package: linux-5.10
version: 5.10.158-2~deb9u1 (stretch)
version_map: {"9 stretch": "5.10.158-2~deb9u1"}
description: "linux kernel update"
date: 2023-01-23T09:52:16+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-3759
  - CVE-2022-3169
  - CVE-2022-3435
  - CVE-2022-3521
  - CVE-2022-3524
  - CVE-2022-3564
  - CVE-2022-3565
  - CVE-2022-3594
  - CVE-2022-3628
  - CVE-2022-3640
  - CVE-2022-3643
  - CVE-2022-4139
  - CVE-2022-4378
  - CVE-2022-41849
  - CVE-2022-41850
  - CVE-2022-42328
  - CVE-2022-42329
  - CVE-2022-42895
  - CVE-2022-42896
  - CVE-2022-47518
  - CVE-2022-47519
  - CVE-2022-47520
  - CVE-2022-47521

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2021-3759

    It was discovered that the memory cgroup controller did not
    account for kernel memory allocated for IPC objects.  A local user
    could use this for denial of service (memory exhaustion).

CVE-2022-3169

    It was discovered that the NVMe host driver did not prevent a
    concurrent reset and subsystem reset.  A local user with access to
    an NVMe device could use this to cause a denial of service (device
    disconnect or crash).

CVE-2022-3435

    Gwangun Jung reported a flaw in the IPv4 forwarding subsystem
    which would lead to an out-of-bounds read.  A local user with
    CAP_NET_ADMIN capability in any user namespace could possibly
    exploit this to cause a denial of service (crash).

CVE-2022-3521

    The syzbot tool found a race condition in the KCM subsystem
    which could lead to a crash.

    This subsystem is not enabled in Debian's official kernel
    configurations.

CVE-2022-3524

    The syzbot tool found a race condition in the IPv6 stack which
    could lead to a memory leak.  A local user could exploit this to
    cause a denial of service (memory exhaustion).

CVE-2022-3564

    A flaw was discovered in the Bluetooh L2CAP subsystem which
    would lead to a use-after-free.  This might be exploitable
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.

CVE-2022-3565

    A flaw was discovered in the mISDN driver which would lead to a
    use-after-free.  This might be exploitable to cause a denial of
    service (crash or memory corruption) or possibly for privilege
    escalation.

CVE-2022-3594

    Andrew Gaul reported that the r8152 Ethernet driver would log
    excessive numbers of messages in response to network errors.  A
    remote attacker could possibly exploit this to cause a denial of
    service (resource exhaustion).

CVE-2022-3628

    Dokyung Song, Jisoo Jang, and Minsuk Kang reported a potential
    heap-based buffer overflow in the brcmfmac Wi-Fi driver.  A user
    able to connect a malicious USB device could exploit this to cause
    a denial of service (crash or memory corruption) or possibly for
    privilege escalation.

CVE-2022-3640

    A flaw was discovered in the Bluetooh L2CAP subsystem which
    would lead to a use-after-free.  This might be exploitable
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.

CVE-2022-3643 (XSA-423)

    A flaw was discovered in the Xen network backend driver that would
    result in it generating malformed packet buffers.  If these
    packets were forwarded to certain other network devices, a Xen
    guest could exploit this to cause a denial of service (crash or
    device reset).

CVE-2022-4139

    A flaw was discovered in the i915 graphics driver.  On gen12 "Xe"
    GPUs it failed to flush TLBs when necessary, resulting in GPU
    programs retaining access to freed memory.  A local user with
    access to the GPU could exploit this to leak sensitive
    information, cause a denial of service (crash or memory
    corruption) or likely for privilege escalation.

CVE-2022-4378

    Kyle Zeng found a flaw in procfs that would cause a stack-based
    buffer overflow.  A local user permitted to write to a sysctl
    could use this to cause a denial of service (crash or memory
    corruption) or possibly for privilege escalation.

CVE-2022-41849

    A race condition was discovered in the smscufx graphics driver,
    which could lead to a use-after-free.  A user able to remove the
    physical device while also accessing its device node could exploit
    this to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.

CVE-2022-41850

    A race condition was discovered in the hid-roccat input driver,
    which could lead to a use-after-free.  A local user able to access
    such a device could exploit this to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.

CVE-2022-42328, CVE-2022-42329 (XSA-424)

    Yang Yingliang reported that the Xen network backend driver did
    not use the proper function to free packet buffers in one case,
    which could lead to a deadlock.  A Xen guest could exploit this to
    cause a denial of service (hang).

CVE-2022-42895

    Tamás Koczka reported a flaw in the Bluetooh L2CAP subsystem
    that would result in reading uninitialised memory.  A nearby
    attacker able to make a Bluetooth connection could exploit
    this to leak sensitive information.

CVE-2022-42896

    Tamás Koczka reported flaws in the Bluetooh L2CAP subsystem that
    can lead to a use-after-free.  A nearby attacker able to make a
    Bluetooth SMP connection could exploit this to cause a denial of
    service (crash or memory corruption) or possibly for remote code
    execution.

CVE-2022-47518, CVE-2022-47519, CVE-2022-47521

    Several flaws were discovered in the wilc1000 Wi-Fi driver which
    could lead to a heap-based buffer overflow.  A nearby attacker
    could exploit these for denial of service (crash or memory
    corruption) or possibly for remote code execution.

CVE-2022-47520

    A flaw was discovered in the wilc1000 Wi-Fi driver which could
    lead to a heap-based buffer overflow.  A local user with
    CAP_NET_ADMIN capability over such a Wi-Fi device could exploit
    this for denial of service (crash or memory corruption) or
    possibly for privilege escalation.
