---
title: "ELA-888-1 yajl security update"
package: yajl
version: 2.1.0-2+deb8u1 (jessie), 2.1.0-2+deb9u1 (stretch)
version_map: {"8 jessie": "2.1.0-2+deb8u1", "9 stretch": "2.1.0-2+deb9u1"}
description: "memory leak"
date: 2023-07-01T11:58:07+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-33460

---

A memory leak has been found in yajl, a JSON parser / small validating JSON generator
written in ANSI C, which might allow an attacker to cause an out of memory situation 
and potentially causing a crash.

