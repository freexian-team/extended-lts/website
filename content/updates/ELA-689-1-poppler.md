---
title: "ELA-689-1 poppler security update"
package: poppler
version: 0.26.5-2+deb8u15 (jessie), 0.48.0-2+deb9u5 (stretch)
version_map: {"8 jessie": "0.26.5-2+deb8u15", "9 stretch": "0.48.0-2+deb9u5"}
description: "denial of service"
date: 2022-09-29T18:34:50+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-13988
  - CVE-2018-18897
  - CVE-2019-10873
  - CVE-2020-27778
  - CVE-2022-27337
  - CVE-2022-38784

---

Several security vulnerabilities have been discovered in Poppler, a PDF
rendering library, that could lead to denial of service or possibly other
unspecified impact when processing maliciously crafted documents.
