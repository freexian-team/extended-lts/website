---
title: "ELA-362-1 intel-microcode security update"
package: intel-microcode
version: 3.20201118.1~deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-02-13T12:24:04+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-8695
  - CVE-2020-8696
  - CVE-2020-8698

---

CVE-2020-8695

    Observable discrepancy in the RAPL interface for some
    Intel(R) Processors may allow a privileged user to
    potentially enable information disclosure via local access.

CVE-2020-8696

    Improper removal of sensitive information before storage
    or transfer in some Intel(R) Processors may allow an
    authenticated user to potentially enable information
    disclosure via local access.

CVE-2020-8698

    Improper isolation of shared resources in some
    Intel(R) Processors may allow an authenticated user to
    potentially enable information disclosure via local access.
