---
title: "ELA-801-1 clamav security update"
package: clamav
version: 0.103.8+dfsg-0+deb8u1 (jessie), 0.103.8+dfsg-0+deb9u1 (stretch)
version_map: {"8 jessie": "0.103.8+dfsg-0+deb8u1", "9 stretch": "0.103.8+dfsg-0+deb9u1"}
description: "arbitrary code execution"
date: 2023-02-20T18:54:40+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-20032
  - CVE-2023-20052

---

Two vulnerabilities have been found in the ClamAV antivirus toolkit,
which could result in arbitrary code execution or information disclosure
when parsing maliciously crafted HFS+ or DMG files.
