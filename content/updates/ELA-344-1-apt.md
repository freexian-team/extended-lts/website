---
title: "ELA-344-1 apt security update"
package: apt
version: 1.0.9.8.7
distribution: "Debian 8 jessie"
description: "multiple integer overflows and underflows"
date: 2021-01-11T19:09:33+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-27350

---

It was discovered that missing input validation in the ar/tar
implementations of APT, the high level package manager, could cause
out-of-bounds reads or infinite loops, resulting in denial of service
when processing malformed deb files.
