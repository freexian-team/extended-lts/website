---
title: "ELA-1066-1 python3.5 security update"
package: python3.5
version: 3.5.3-1+deb9u9 (stretch)
version_map: {"9 stretch": "3.5.3-1+deb9u9"}
description: "quoted-overlap zipbomb DoS"
date: 2024-03-24T23:55:11+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-0450

---

The zipfile module was vulnerable to “quoted-overlap” zip-bombs
in the Python 3 interpreter.
