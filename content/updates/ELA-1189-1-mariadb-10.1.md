---
title: "ELA-1189-1 mariadb-10.1 security update"
package: mariadb-10.1
version: 10.1.48-0+deb9u4 (stretch)
version_map: {"9 stretch": "10.1.48-0+deb9u4"}
description: "multiple vulnerabilities"
date: 2024-09-30T10:30:30Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-46659
  - CVE-2022-21427
  - CVE-2022-24048
  - CVE-2022-24050
  - CVE-2022-24051
  - CVE-2022-24052
  - CVE-2022-27380
  - CVE-2022-27383
  - CVE-2022-27384
  - CVE-2022-27387
  - CVE-2022-27448
  - CVE-2022-31622
  - CVE-2022-32083

---

Several vulnerabilities have been fixed in MariaDB, a popular database server.

CVE-2022-21427
 
    An easily exploitable vulnerability allowed high
    privileged attacker with network access via multiple protocols
    to compromise MariaDB Server. Successful attacks of this vulnerability
    can result in unauthorized ability to cause a hang
    or frequently repeatable crash (complete DOS). Certain UTF8 combining
    marks cause MariaDB to crash when doing Full-Text searches.

CVE-2022-24048, CVE-2022-24051, CVE-2022-24052

    MariaDB CONNECT Storage Engine Stack-based Buffer
    Overflow Privilege Escalation Vulnerability. This vulnerability allows
    local attackers to escalate privileges on affected installations
    of MariaDB. Authentication is required to exploit this vulnerability.
    The specific flaw exists within the processing of SQL queries.
    The issue results from the lack of proper validation of the length
    of user-supplied data prior to copying it to a fixed-length stack-based
    buffer. An attacker can leverage this vulnerability to escalate
    privileges and execute arbitrary code in the context of the
    service account. Concerned Storage Engines are JSON, XML and MYSQL.
    
CVE-2022-24050
 
    MariaDB CONNECT Storage Engine use-after-free
    privilege escalation vulnerability. This vulnerability allows local
    attackers to escalate privileges on affected installations of MariaDB.
    Authentication is required to exploit this vulnerability.
    The specific flaw exists within the processing of SQL queries.
    The issue results from the lack of validating the existence of an object
    prior to performing operations on the object.
    An attacker can leverage this vulnerability to escalate privileges and
    execute arbitrary code in the context of the service account.
    
CVE-2022-27380
 
    An issue in the component my_decimal::operator=
    of MariaDB Server was discovered that makes it possible for attackers to cause
    a Denial of Service (DoS) via specially crafted SQL statements.

CVE-2022-27383

    An use-after-free was found in the component
    my_strcasecmp_8bit, which may be exploited via specially crafted
    SQL statements.
    
CVE-2022-27384, CVE-2022-32083

    An issue in the component
    Item_subselect::init_expr_cache_tracker allows attackers to cause
    a Denial of Service (DoS) via specially crafted SQL statements.

CVE-2022-27387

    A global buffer overflow in the component
    decimal_bin_size was found, which is exploited via specially
    crafted SQL statements.

CVE-2022-27448

    An issue was found in multi-update and implicit
    grouping handling, which is exploited via specially
    crafted SQL statements.  An attacker can leverage
	this vulnerability to cause a Denial of Service (DoS)

CVE-2022-31622

    Incorrect handling of errors while executing the
    method create_worker_threads could lead to a Denial of Service (DoS).





