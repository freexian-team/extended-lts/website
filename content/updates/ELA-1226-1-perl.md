---
title: "ELA-1226-1 perl security update"
package: perl
version: 5.20.2-3+deb8u14 (jessie), 5.24.1-3+deb9u8 (stretch), 5.28.1-6+deb10u2 (buster)
version_map: {"8 jessie": "5.20.2-3+deb8u14", "9 stretch": "5.24.1-3+deb9u8", "10 buster": "5.28.1-6+deb10u2"}
description: "multiple vulnerabilities"
date: 2024-11-02T18:08:01Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-16156
  - CVE-2023-31484

---

Perl a popular script language was affected by multiple vulnerabilities.

CVE-2020-16156:

     An attacker can prepend checksums for modified
     packages to the beginning of CHECKSUMS files,
     before the cleartext PGP headers. This makes
     the Module::Signature::_verify() checks
     in both cpan and cpanm pass.
     Without the sigtext and plaintext arguments
     to _verify(), the _compare() check is bypassed.
     This results in _verify() only checking that
     valid signed cleartext is present somewhere
     in the file.

CVE-2023-31484:

    CPAN.pm does not verify TLS certificates
    when downloading distributions over HTTPS.
