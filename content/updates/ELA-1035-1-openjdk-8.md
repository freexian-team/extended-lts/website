---
title: "ELA-1035-1 openjdk-8 security update"
package: openjdk-8
version: 8u402-ga-1~deb8u1 (jessie), 8u402-ga-1~deb9u1 (stretch)
version_map: {"8 jessie": "8u402-ga-1~deb8u1", "9 stretch": "8u402-ga-1~deb9u1"}
description: "multiple vulnerabilities"
date: 2024-01-26T10:10:37+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-20918
  - CVE-2024-20919
  - CVE-2024-20921
  - CVE-2024-20926
  - CVE-2024-20945
  - CVE-2024-20952

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
which may result in side channel attacks, leaking sensitive data to log
files, denial of service or bypass of sandbox restrictions.
