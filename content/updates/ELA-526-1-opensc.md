---
title: "ELA-526-1 opensc security update"
package: opensc
version: 0.16.0-3+deb8u3
distribution: "Debian 8 Jessie"
description: "multiple vulnerabilities"
date: 2021-12-07T10:00:52-08:00
draft: false
type: updates
cvelist:
  - CVE-2020-26570
  - CVE-2020-26571
  - CVE-2020-26572
---

Multiple vulnerabilities were discovered in opensc, a set of utilities to
interact with smartcard devices:

* CVE-2020-26570: Heap-based buffer overflow in `sc_oberthur_read_file`.
* CVE-2020-26571: Stack-based buffer overflow in `sc_pkcs15emu_gemsafeGPK_init`.
* CVE-2020-26572: Stack-based buffer overflow in `tcos_decipher`.
