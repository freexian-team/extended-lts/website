---
title: "ELA-734-1 tomcat8 security update"
package: tomcat8
version: 8.0.14-1+deb8u23 (jessie), 8.5.54-0+deb9u9 (stretch)
version_map: {"8 jessie": "8.0.14-1+deb8u23", "9 stretch": "8.5.54-0+deb9u9"}
description: "request smuggling"
date: 2022-11-20T23:35:26+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-23181
  - CVE-2022-29885
  - CVE-2022-42252

---

Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine. The version of Tomcat 8 in Jessie was only affected by
CVE-2022-23181.

CVE-2022-42252

    If Apache Tomcat was configured to ignore invalid HTTP headers via setting
    rejectIllegalHeader to false (the default for 8.5.x only), Tomcat did not
    reject a request containing an invalid Content-Length header making a
    request smuggling attack possible if Tomcat was located behind a reverse
    proxy that also failed to reject the request with the invalid header.


CVE-2022-23181

    The fix for bug CVE-2020-9484 introduced a time of check, time of use
    vulnerability into Apache Tomcat that allowed a local attacker to perform
    actions with the privileges of the user that the Tomcat process is using.
    This issue is only exploitable when Tomcat is configured to persist
    sessions using the FileStore.

CVE-2022-29885

    The documentation of Apache Tomcat for the EncryptInterceptor incorrectly
    stated it enabled Tomcat clustering to run over an untrusted network. This
    was not correct. While the EncryptInterceptor does provide confidentiality
    and integrity protection, it does not protect against all risks associated
    with running over any untrusted network, particularly DoS risks.
