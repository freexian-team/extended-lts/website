---
title: "ELA-1003-1 postgresql-9.4 security update"
package: postgresql-9.4
version: 9.4.26-0+deb8u8 (jessie)
version_map: {"8 jessie": "9.4.26-0+deb8u8"}
description: "arbitrary code execution"
date: 2023-11-19T11:41:42+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-5869
  - CVE-2023-39417

---

Several security vulnerabilities have been found in PostgreSQL, an advanced
open source database.

CVE-2023-5869

    While modifying certain SQL array values, missing overflow checks let
    authenticated database users write arbitrary bytes to a memory area that
    facilitates arbitrary code execution. Missing overflow checks also let
    authenticated database users read a wide area of server memory. The
    CVE-2021-32027 fix covered some attacks of this description, but it missed
    others.

CVE-2023-39417

    In the EXTENSION SCRIPT, a SQL Injection vulnerability was found in
    PostgreSQL if it uses @extowner@, @extschema@, or @extschema:...@ inside a
    quoting construct (dollar quoting, '', or ""). If an administrator has
    installed files of a vulnerable, trusted, non-bundled extension, an
    attacker with database-level CREATE privilege can execute arbitrary code as
    the bootstrap superuser.
