---
title: "ELA-447-1 tiff security update"
package: tiff
version: 4.0.3-12.3+deb8u11
distribution: "Debian 8 jessie"
description: "denial of service or arbitrary code execution"
date: 2021-06-28T00:44:09+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-35523
  - CVE-2020-35524

---

Two vulnerabilities have been discovered in the libtiff library and the
included tools, which may result in denial of service or the execution
of arbitrary code if malformed image files are processed.
