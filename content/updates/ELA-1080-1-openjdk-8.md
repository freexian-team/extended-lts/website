---
title: "ELA-1080-1 openjdk-8 security update"
package: openjdk-8
version: 8u412-ga-1~deb8u1 (jessie), 8u412-ga-1~deb9u1 (stretch)
version_map: {"8 jessie": "8u412-ga-1~deb8u1", "9 stretch": "8u412-ga-1~deb9u1"}
description: "multiple vulnerabilities"
date: 2024-04-29T11:26:18+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-21011
  - CVE-2024-21068
  - CVE-2024-21085
  - CVE-2024-21094

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
which may result in denial of service or information disclosure.
