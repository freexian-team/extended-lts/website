---
title: "ELA-853-1 python2.7 security update"
package: python2.7
version: 2.7.9-2-ds1-1+deb8u10 (jessie), 2.7.13-2+deb9u7 (stretch)
version_map: {"8 jessie": "2.7.9-2-ds1-1+deb8u10", "9 stretch": "2.7.13-2+deb9u7"}
description: "multiple vulnerabilities"
date: 2023-05-25T11:42:02+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2015-20107
  - CVE-2020-8492
  - CVE-2020-26116
  - CVE-2021-3733
  - CVE-2021-3737
  - CVE-2022-45061

---

Multiple security issues were discovered in Python, an interactive
high-level object-oriented language. An attacker may cause command
injection, denial of service (DoS) and request smuggling.

* CVE-2015-20107

    The mailcap module does not add escape characters into commands
    discovered in the system mailcap file. This may allow attackers to
    inject shell commands into applications that call
    mailcap.findmatch with untrusted input (if they lack validation of
    user-provided filenames or arguments).
    Note: this CVE was really issued in 2022.

* CVE-2020-8492

    Python allows an HTTP server to conduct Regular Expression Denial
    of Service (ReDoS) attacks against a client because of
    urllib.request.AbstractBasicAuthHandler catastrophic backtracking.

* CVE-2020-26116

    http.client allows CRLF injection if the attacker controls the
    HTTP request method, as demonstrated by inserting CR and LF
    control characters in the first argument of
    HTTPConnection.request.

* CVE-2021-3733

    There's a flaw in urllib's AbstractBasicAuthHandler class. An
    attacker who controls a malicious HTTP server that an HTTP client
    (such as web browser) connects to, could trigger a Regular
    Expression Denial of Service (ReDOS) during an authentication
    request with a specially crafted payload that is sent by the
    server to the client.

* CVE-2021-3737

    An improperly handled HTTP response in the HTTP client code of
    python may allow a remote attacker, who controls the HTTP server,
    to make the client script enter an infinite loop, consuming CPU
    time.

* CVE-2022-45061

    An unnecessary quadratic algorithm exists in one path when
    processing some inputs to the IDNA (RFC 3490) decoder, such that a
    crafted, unreasonably long name being presented to the decoder
    could lead to a CPU denial of service.

This update also brings improved fixes for CVE-2019-10160
(ELA-134-1,DLA-2280-1) and CVE-2021-3177 (ELA-598-1,DLA-2919-1), and
drop the patch for CVE-2019-9740/CVE-2019-9947 (DLA-1834-1,DLA-2337-1)
whose issue was introduced later in the 2.7.x series.
