---
title: "ELA-1045-1 phpseclib security update"
package: phpseclib
version: 1.0.19-1~deb9u2 (stretch)
version_map: {"9 stretch": "1.0.19-1~deb9u2"}
description: " Terrapin-Attack"
date: 2024-02-23T20:24:41Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-48795

---

phpseclib, a library used for secure communication written in PHP language, was
vulnerable to so called Terrapin-Attack. The SSH transport protocol, with
certain OpenSSH extensions, allows remote attackers to bypass
integrity checks such that some packets are omitted
(from the extension negotiation message), and a client and
server may consequently end up with a connection for which some security
features have been downgraded or disabled.
