---
title: "ELA-517-1 atftp security update"
package: atftp
version: 0.7.git20120829-1+deb8u2
distribution: "Debian 8 jessie"
description: "denial-of-service due to crafted requests"
date: 2021-11-17T01:32:41+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-6097
  - CVE-2021-41054

---

Two issues have been found in atftp, an advanced TFTP client.
Both are related to sending crafted requests to the server and triggering
a denial-of-service due to for example a buffer overflow.

