---
title: "ELA-25 libcgroup security update"
package: libcgroup
version: 0.38-1+deb7u1
distribution: "Debian 7 Wheezy"
description: "insecure permission of logfile"
date: 2018-08-05T16:56:26+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-14348
---

Due to a wrong umask, access permissions of log files could have been insecure.
