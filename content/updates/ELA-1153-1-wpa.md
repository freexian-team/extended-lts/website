---
title: "ELA-1153-1 wpa security update"
package: wpa
version: 2.3-1+deb8u15 (jessie), 2:2.4-1+deb9u11 (stretch), 2:2.7+git20190128+0c1e29f-6+deb10u5 (buster)
version_map: {"8 jessie": "2.3-1+deb8u15", "9 stretch": "2:2.4-1+deb9u11", "10 buster": "2:2.7+git20190128+0c1e29f-6+deb10u5"}
description: "local root exploit"
date: 2024-08-14T21:42:19+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-5290

---

Local privilege escalation by loading libraries from untrusted paths has been fixed in wpasupplicant, a commonly used tool for connection and authentication in wireless and wired networks.
