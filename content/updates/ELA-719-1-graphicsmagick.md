---
title: "ELA-719-1 graphicsmagick security update"
package: graphicsmagick
version: 1.3.20-3+deb8u13 (jessie), 1.3.30+hg15796-1~deb9u6 (stretch)
version_map: {"8 jessie": "1.3.20-3+deb8u13", "9 stretch": "1.3.30+hg15796-1~deb9u6"}
description: "heap buffer overflow"
date: 2022-10-30T02:07:46+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-1270

---

An issue has been found in graphicsmagick, a collection of image processing tools.
Due to missing checks, a crafted MIFF file could result in a heap buffer overflow when parsing it.
