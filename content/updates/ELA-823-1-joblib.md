---
title: "ELA-823-1 joblib security update"
package: joblib
version: 0.8.3-1+deb8u1 (jessie), 0.10.3+git55-g660fe5d-1+deb9u1 (stretch)
version_map: {"8 jessie": "0.8.3-1+deb8u1", "9 stretch": "0.10.3+git55-g660fe5d-1+deb9u1"}
description: "code execution vulnerability"
date: 2023-03-30T19:59:31+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-21797

---

It was discovered that joblib did not properly sanitize arguments to pre_dispatch, allowing arbitrary code execution.
