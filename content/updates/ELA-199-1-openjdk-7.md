---
title: "ELA-199-1 openjdk-7 security update"
package: openjdk-7
version: 7u241-2.6.20-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2019-12-11T01:23:31+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-2894
  - CVE-2019-2933
  - CVE-2019-2945
  - CVE-2019-2949
  - CVE-2019-2958
  - CVE-2019-2962
  - CVE-2019-2964
  - CVE-2019-2973
  - CVE-2019-2978
  - CVE-2019-2981
  - CVE-2019-2983
  - CVE-2019-2987
  - CVE-2019-2988
  - CVE-2019-2989
  - CVE-2019-2992
  - CVE-2019-2999

---

Several vulnerabilities have been discovered in OpenJDK, an
implementation of the Oracle Java platform, resulting in denial of
service, sandbox bypass, information disclosure or the execution
of arbitrary code.

