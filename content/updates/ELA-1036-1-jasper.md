---
title: "ELA-1036-1 jasper security update"
package: jasper
version: 1.900.1-debian1-2.4+deb8u12 (jessie)
version_map: {"8 jessie": "1.900.1-debian1-2.4+deb8u12"}
description: "invalid memory write"
date: 2024-01-30T19:30:03+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-51257

---

An issue has been found in jasper, a library and programs for manipulating JPEG-2000 files.
The issue is about an invalid memory write which might allow a local attacker to execute arbitrary code.
