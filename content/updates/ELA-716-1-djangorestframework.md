---
title: "ELA-716-1 djangorestframework security update"
package: djangorestframework
version: 3.4.0-2+deb9u1 (stretch)
version_map: {"9 stretch": "3.4.0-2+deb9u1"}
description: "cross-site scripting vulnerabilities"
date: 2022-10-28T09:17:21+05:30
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-25045
  - CVE-2020-25626

---

Two cross-site scripting vulnerabilities were discovered in the Django
Rest Framework, a toolkit to build web APIs.
