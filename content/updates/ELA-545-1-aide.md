---
title: "ELA-545-1 aide security update"
package: aide
version: 0.16~a2.git20130520-3+deb8u1
distribution: "Debian 8 jessie"
description: "heap-based buffer overflow"
date: 2022-01-24T02:26:23+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-45417

---

David Bouman discovered a heap-based buffer overflow vulnerability in
the base64 functions of aide, an advanced intrusion detection system,
which can be triggered via large extended file attributes or ACLs. This
may result in denial of service or privilege escalation.
