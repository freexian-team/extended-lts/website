---
title: "ELA-1081-1 ruby-rack security update"
package: ruby-rack
version: 1.6.4-4+deb9u6 (stretch)
version_map: {"9 stretch": "1.6.4-4+deb9u6"}
description: "multiple vulnerabilities"
date: 2024-04-29T12:27:30+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-26141
  - CVE-2024-26146

---

Multiple vulnerabilities were fixed in ruby-rack,
an interface for developing web applications in Ruby.

CVE-2024-26141

    Reject Range headers which are too large

CVE-2024-26146

    ReDoS in Accept header parsing
