---
title: "ELA-1140-1 imagemagick security update"
package: imagemagick
version: 8:6.8.9.9-5+deb8u27 (jessie)
version_map: {"8 jessie": "8:6.8.9.9-5+deb8u27"}
description: "multiple vulnerabilities"
date: 2024-07-26T10:53:26Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-11752
  - CVE-2017-12566
  - CVE-2017-18022
  - CVE-2018-11655
  - CVE-2021-3596
  - CVE-2022-28463
  - CVE-2022-48541
  - CVE-2023-1289
  - CVE-2023-5341
  - CVE-2023-34151

---

Imagemagick, an image processing toolking was vulnerable.

CVE-2017-11752

    The ReadMAGICKImage function allows remote attackers to cause
    a denial of service (memory leak) via a crafted file.

CVE-2017-12566

    A memory leak vulnerability was found in the function ReadMVGImage
    in mvg coder, which allows attackers to cause a denial of service.

CVE-2017-18022

    A memory leak vulnerability was found in MontageImageCommand.

CVE-2018-11655

    A memory leak vulnerability was found in the function GetImagePixelCache
    which allows attackers to cause a denial of service via a crafted
    CALS image file.

CVE-2021-3596

    A NULL pointer dereference flaw was found in ReadSVGImage(). This issue
    is due to not checking the return value from libxml2's xmlCreatePushParserCtxt()
    and uses the value directly, which leads to a crash and segmentation fault.

CVE-2022-28463

    A buffer overflow was found in Imagemagick in cin file coder.

CVE-2022-48541

    A memory leak was found that allows a remote attackers to perform
    a denial of service via the "identify -help" command.

CVE-2023-1289

    Loading a specially created SVG file may cause a segmentation fault.
    When ImageMagick crashes, it generates a lot of trash files. These trash
    files can be large if the SVG file contains many render actions, and could
    result in a denial of service.

CVE-2023-5341

    A heap use-after-free flaw was found in coders/bmp.c

CVE-2023-34151

    Undefined behaviors of casting double to size_t in svg, mvg and other
    coders.
