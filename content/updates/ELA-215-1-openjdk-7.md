---
title: "ELA-215-1 openjdk-7 security update"
package: openjdk-7
version: 7u251-2.6.21-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2020-02-29T13:20:53+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-2583
  - CVE-2020-2590
  - CVE-2020-2593
  - CVE-2020-2601
  - CVE-2020-2604
  - CVE-2020-2654
  - CVE-2020-2659

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
resulting in denial of service, incorrect implementation of Kerberos
GSSAPI and TGS requests or incorrect TLS handshakes.
