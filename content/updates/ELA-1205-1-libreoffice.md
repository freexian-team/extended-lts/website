---
title: "ELA-1205-1 libreoffice security update"
package: libreoffice
version: 1:6.1.5-3+deb9u5 (stretch), 1:6.1.5-3+deb10u14 (buster)
version_map: {"9 stretch": "1:6.1.5-3+deb9u5", "10 buster": "1:6.1.5-3+deb10u14"}
description: "improper digital signature invalidation"
date: 2024-10-19T14:25:54Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-7788

---

A vulnerability was found in libreoffice a popular office productivity suite.

CVE-2024-7788:
	
	Various file formats are based on the zip file format. In cases of corruption of the underlying zip's central directory, LibreOffice offers a "repair mode" which will attempt to recover the zip file structure by scanning for secondary local file headers in the zip to reconstruct the document.
	
	Prior to this fix, in the case of digitally signed zip files, an attacker could construct a document which, when repaired, reported a signature status not valid for the recovered file.
	
	Previously if verification failed the user could choose to ignore the failure and enable the macros anyway.
	
	Repair document mode has to be inherently tolerant, so now in fixed versions all signatures are implied to be invalid in recovery mode.
