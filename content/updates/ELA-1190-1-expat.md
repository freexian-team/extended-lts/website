---
title: "ELA-1190-1 expat security update"
package: expat
version: 2.1.0-6+deb8u12 (jessie), 2.2.0-2+deb9u9 (stretch), 2.2.6-2+deb10u8 (buster)
version_map: {"8 jessie": "2.1.0-6+deb8u12", "9 stretch": "2.2.0-2+deb9u9", "10 buster": "2.2.6-2+deb10u8"}
description: "multiple vulnerabilities"
date: 2024-09-30T13:51:14+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-45490
  - CVE-2024-45491
  - CVE-2024-45492

---

Multiple vulnerabilities were found in expat, an XML parsing C library,
which could lead to Denial of Service, memory corruption or arbitrary
code execution.

  * CVE-2024-45490: TaiYou discovered that xmlparse.c does not reject a
    negative length for `XML_ParseBuffer()`, which may cause memory
    corruption or code execution.

  * CVE-2024-45491: TaiYou discovered that xmlparse.c has an integer
    overflow for `nDefaultAtts` on 32-bit platforms, which may cause
    denial of service or code execution.

  * CVE-2024-45492: TaiYou discovered that xmlparse.c has an integer
    overflow for `m_groupSize` on 32-bit platforms, which may cause
    denial of service or code execution.

---


