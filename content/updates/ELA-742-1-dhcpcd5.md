---
title: "ELA-742-1 dhcpcd5 security update"
package: dhcpcd5
version: 6.10.1-1+deb9u1 (stretch)
version_map: {"9 stretch": "6.10.1-1+deb9u1"}
description: "latency attack"
date: 2022-11-25T23:16:54+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2019-11578
  - CVE-2019-11579

---

Several security vulnerabilities have been discovered in dhcpcd5, a DHCPv4 and
DHCPv6 dual-stack client.

CVE-2019-11579:

    dhcp.c in dhcpcd contains a 1-byte read overflow with DHO_OPTSOVERLOADED.

CVE-2019-11578:

    auth.c in dhcpcd allowed attackers to infer secrets by performing latency attacks.
