---
title: "ELA-1031-1 xerces-c security update"
package: xerces-c
version: 3.1.1-5.1+deb8u6 (jessie), 3.1.4+debian-2+deb9u3 (stretch)
version_map: {"8 jessie": "3.1.1-5.1+deb8u6", "9 stretch": "3.1.4+debian-2+deb9u3"}
description: "integer overflow"
date: 2024-01-21T18:53:45+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-37536

---

Even Rouault discovered that xerces-c, a validating XML parser library
for C++, was vulnerable to integer overflow via crafted .xsd files,
which can lead to out-of-bounds access.
