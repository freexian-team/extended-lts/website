---
title: "ELA-606-1 ghostscript security update"
package: ghostscript
version: 9.26a~dfsg-0+deb8u9
distribution: "Debian 8 jessie"
description: "restriction bypass"
date: 2022-05-09T09:56:30+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-25059

---

A security vulnerability was found in Ghostscript, the GPL PostScript/PDF
interpreter. It was discovered that some privileged Postscript operators
remained accessible from various places. For instance a specially crafted
PostScript file could use this flaw in order to have access to the file
system outside of the constrains imposed by -dSAFER.
