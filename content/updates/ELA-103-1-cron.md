---
title: "ELA-103-1 cron security update"
package: cron
version: 3.0pl1-124+deb7u1
distribution: "Debian 7 Wheezy"
description: "several DoS fixes, root escalation fix"
date: 2019-04-01T13:10:43+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-9525
  - CVE-2019-9704
  - CVE-2019-9705
  - CVE-2019-9706

---

Various security problems have been discovered in Debian's CRON scheduler.

CVE-2017-9525: Fix group crontab to root escalation via the Debian
package's postinst script as described by Alexander Peslyak (Solar
Designer) in http://www.openwall.com/lists/oss-security/2017/06/08/3

CVE-2019-9704: DoS: Fix unchecked return of calloc(). Florian Weimer
discovered that a missing check for the return value of calloc() could
crash the daemon, which could be triggered by a very large crontab
created by a user.

CVE-2019-9705: Enforce maximum crontab line count of 1000 to prevent a
malicious user from creating an excessivly large crontab. The daemon will
log a warning for existing files, and crontab(1) will refuse to create
new ones.


CVE-2019-9706: A user reported a use-after-free condition in the cron
daemon, leading to a possible Denial-of-Service scenario by crashing the
daemon.
