---
title: "ELA-402-1 python-django security update"
package: python-django
version: 1.7.11-1+deb8u12
distribution: "Debian 8 Jessie"
description: "directory traversal vulnerability"
date: 2021-04-09T16:18:08+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-28658
---

It was discovered that there was a potential directory traversal issue in
Django, a Python-based web development framework.

The vulnerability could have been exploited by maliciously crafted filenames.
However, the upload handlers built into Django itself were not affected.
