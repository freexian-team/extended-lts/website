---
title: "ELA-82-1 libarchive security update"
package: libarchive
version: 3.0.4-3+wheezy6+deb7u3
distribution: "Debian 7 Wheezy"
description: "denial of service vulnerabilities"
date: 2019-02-07T16:24:53+01:00
draft: false
type: updates
cvelist:
 - CVE-2019-1000019
 - CVE-2019-1000020
---

Two vulnerabilities were discovered and corrected in the
[libarchive](http://libarchive.github.com/) multi-format compression library,
first fixing an issue where a specially-crafted [.z7ip](https://www.7-zip.org/)
file could cause a denial-of-service attack via a crash (CVE-2019-1000019) in
addition to an endless-loop vulnerability where a malicious
[ISO9660](https://en.wikipedia.org/wiki/ISO_9660) image could cause an infinite
loop (CVE-2019-1000020).
