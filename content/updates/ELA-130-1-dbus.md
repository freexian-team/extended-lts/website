---
title: "ELA-130-1 dbus security update"
package: dbus
version: 1.6.8-1+deb7u7
distribution: "Debian 7 Wheezy"
description: "authentication bypass"
date: 2019-06-14T13:49:00+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-12749

---

Joe Vennix discovered an authentication bypass vulnerability in dbus, an
asynchronous inter-process communication system. The implementation of
the DBUS_COOKIE_SHA1 authentication mechanism was susceptible to a
symbolic link attack. A local attacker could take advantage of this flaw
to bypass authentication and connect to a DBusServer with elevated
privileges.
