---
title: "ELA-852-1 cups-filters security update"
package: cups-filters
version: 1.11.6-3+deb9u2 (stretch)
version_map: {"9 stretch": "1.11.6-3+deb9u2"}
description: "RCE due to missing input sanitising"
date: 2023-05-22T11:18:06+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-24805

---

It was discovered that missing input sanitising in cups-filters, when
using the Backend Error Handler (beh) backend to create an accessible
network printer, may result in the execution of arbitrary commands.
