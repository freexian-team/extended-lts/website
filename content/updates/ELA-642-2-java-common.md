---
title: "ELA-642-2 java-common regression update"
package: java-common
version: 0.52+deb8u2 (jessie)
version_map: {"8 jessie": "0.52+deb8u2"}
description: "regression update"
date: 2023-09-22T13:35:47-03:00
draft: false
type: updates
tags:
- update
cvelist:

---

The java-common update of ELA-642-1 introduced a bug in
`/usr/share/java/java_defaults.mk`, that made several actually supported
architectures were not included in the `java_architectures` variable.
