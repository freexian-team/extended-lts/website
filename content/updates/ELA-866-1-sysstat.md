---
title: "ELA-866-1 sysstat security update"
package: sysstat
version: 11.0.1-1+deb8u2 (jessie), 11.4.3-2+deb9u2 (stretch)
version_map: {"8 jessie": "11.0.1-1+deb8u2", "9 stretch": "11.4.3-2+deb9u2"}
description: "remote code execution"
date: 2023-06-08T13:37:07+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-33204

---

It was discovered that sysstat, a system performance tools for Linux,
incompletely fixed CVE-2022-39377 (as published in ELA-731-1), which
could lead to crashes and possibly remote code execution.

* CVE-2023-33204

    sysstat allows a multiplication integer overflow in check_overflow
    in common.c. NOTE: this issue exists because of an incomplete fix
    for CVE-2022-39377.

For reference, the initial vulnerability was:

* CVE-2022-39377

    On 32 bit systems, allocate_structures contains a size_t overflow
    in sa_common.c. The allocate_structures function insufficiently
    checks bounds before arithmetic multiplication, allowing for an
    overflow in the size allocated for the buffer representing system
    activities. This issue may lead to Remote Code Execution (RCE).
