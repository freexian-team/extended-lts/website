---
title: "ELA-14-1 linux security update"
package: linux
version: 3.16.56-1+deb8u1~deb7u1
distribution: "Debian 7 Wheezy"
description: "linux kernel 3.16 backport"
date: 2018-07-08T18:28:19+02:00
draft: false
---

The latest Linux Kernel 3.16 from Jessie was backported to Wheezy. It is now
available via the wheezy-lts-kernel repository. Please refer to the
[documentation] how to enable it. This update serves particularly as a means to
test the compatibility with your system environment. Please do not hesitate to
contact us if you discover any issues. Future updates will follow security
releases for Jessie closely.


[documentation] https://deb.freexian.com/extended-lts/docs/how-to-use-extended-lts/
