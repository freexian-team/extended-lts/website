---
title: "ELA-72-2 jasper regression update"
package: jasper
version: 1.900.1-13+deb7u9
distribution: "Debian 7 Wheezy"
description: "denial-of-service"
date: 2019-04-20T16:40:49+02:00
draft: false
type: updates
cvelist:
 - CVE-2018-19542

---

The update of jasper issued as ELA-72-1 caused a regression due to
the fix for CVE-2018-19542, a NULL pointer dereference in the function
jp2_decode, which could lead to a denial-of-service. In some cases not
only invalid jp2 files but also valid jp2 files were rejected.
