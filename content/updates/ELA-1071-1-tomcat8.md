---
title: "ELA-1071-1 tomcat8 security update"
package: tomcat8
version: 8.5.54-0+deb9u15 (stretch)
version_map: {"9 stretch": "8.5.54-0+deb9u15"}
description: "denial of service"
date: 2024-04-11T11:41:06+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-23672
  - CVE-2024-24549

---

Two security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.

CVE-2024-24549

     Denial of Service due to improper input validation vulnerability for
     HTTP/2. When processing an HTTP/2 request, if the request exceeded any of
     the configured limits for headers, the associated HTTP/2 stream was not
     reset until after all of the headers had been processed.

CVE-2024-23672

     Denial of Service via incomplete cleanup vulnerability. It was possible
     for WebSocket clients to keep WebSocket connections open leading to
     increased resource consumption.
