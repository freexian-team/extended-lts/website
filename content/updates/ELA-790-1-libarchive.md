---
title: "ELA-790-1 libarchive security update"
package: libarchive
version: 3.1.2-11+deb8u10 (jessie), 3.2.2-2+deb9u4 (stretch)
version_map: {"8 jessie": "3.1.2-11+deb8u10", "9 stretch": "3.2.2-2+deb9u4"}
description: "null pointer dereference"
date: 2023-01-31T23:17:56+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-36227

---

An issue has been found in libarchive, a multi-format archive and compression library.
Due to missing checks after calloc, null pointer dereferences might happen. 
