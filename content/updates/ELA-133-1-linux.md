---
title: "ELA-133-1 linux security update"
package: linux
version: 3.16.68-2~deb7u1
distribution: "Debian 7 Wheezy"
description: "linux kernel update"
date: 2019-06-23T18:12:23+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-3846
  - CVE-2019-5489
  - CVE-2019-10126
  - CVE-2019-11477
  - CVE-2019-11478
  - CVE-2019-11479
  - CVE-2019-11810
  - CVE-2019-11833
  - CVE-2019-11884

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2019-3846, CVE-2019-10126

    huangwen reported multiple buffer overflows in the Marvell wifi
    (mwifiex) driver, which a local user could use to cause denial of
    service or the execution of arbitrary code.

CVE-2019-5489

    Daniel Gruss, Erik Kraft, Trishita Tiwari, Michael Schwarz, Ari
    Trachtenberg, Jason Hennessey, Alex Ionescu, and Anders Fogh
    discovered that local users could use the mincore() system call to
    obtain sensitive information from other processes that access the
    same memory-mapped file.

CVE-2019-11477

    Jonathan Looney reported that a specially crafted sequence of TCP
    selective acknowledgements (SACKs) allows a remotely triggerable
    kernel panic.

CVE-2019-11478

    Jonathan Looney reported that a specially crafted sequence of TCP
    selective acknowledgements (SACKs) will fragment the TCP
    retransmission queue, allowing an attacker to cause excessive
    resource usage.

CVE-2019-11479

    Jonathan Looney reported that an attacker could force the Linux
    kernel to segment its responses into multiple TCP segments, each of
    which contains only 8 bytes of data, drastically increasing the
    bandwidth required to deliver the same amount of data.

    This update introduces a new sysctl value to control the minimal MSS
    (net.ipv4.tcp_min_snd_mss), which by default uses the formerly hard-
    coded value of 48.  We recommend raising this to 512 unless you know
    that your network requires a lower value.  (This value applies to
    Linux 3.16 only.)

CVE-2019-11810

    It was discovered that the megaraid_sas driver did not correctly
    handle a failed memory allocation during initialisation, which
    could lead to a double-free.  This might have some security
    impact, but it cannot be triggered by an unprivileged user.

CVE-2019-11833

    It was discovered that the ext4 filesystem implementation writes
    uninitialised data from kernel memory to new extent blocks.  A
    local user able to write to an ext4 filesystem and then read the
    filesystem image, for example using a removable drive, might be
    able to use this to obtain sensitive information.

CVE-2019-11884

    It was discovered that the Bluetooth HIDP implementation did not
    ensure that new connection names were null-terminated.  A local
    user with CAP_NET_ADMIN capability might be able to use this to
    obtain sensitive information from the kernel stack.


