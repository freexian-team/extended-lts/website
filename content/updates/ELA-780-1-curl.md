---
title: "ELA-780-1 curl security update"
package: curl
version: 7.38.0-4+deb8u24 (jessie), 7.52.1-5+deb9u18 (stretch)
version_map: {"8 jessie": "7.38.0-4+deb8u24", "9 stretch": "7.52.1-5+deb9u18"}
description: "multiple vulnerabilities"
date: 2023-01-28T16:29:20-05:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-27774
  - CVE-2022-27782
  - CVE-2022-32221
  - CVE-2022-35252
  - CVE-2022-43552

---

Several vulnerabilities were discovered in Curl, an easy-to-use client-side
URL transfer library, which could result in denial of service or
information disclosure.

This update also revises the fix for CVE-2022-27782 released for stretch in
ELA-664-1.
