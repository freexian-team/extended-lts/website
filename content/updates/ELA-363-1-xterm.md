---
title: "ELA-363-1 xterm security update"
package: xterm
version: 312-2+deb8u1
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-02-13T23:18:44+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-27135

---

xterm through Patch #365 allows remote attackers to cause a
denial of service (segmentation fault) or possibly have
unspecified other impact via a crafted UTF-8 character sequence.
