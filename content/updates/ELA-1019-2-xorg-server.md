---
title: "ELA-1019-2 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u14 (jessie), 2:1.19.2-1+deb9u17 (stretch)
version_map: {"8 jessie": "2:1.16.4-1+deb8u14", "9 stretch": "2:1.19.2-1+deb9u17"}
description: "integer overflow and OOB reads/writes"
date: 2023-12-17T19:53:30+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-6377

---

The initial fix for CVE-2023-6377 as applied in ELA 1019-1 did not fully
fix the vulnerability. Updated packages correcting this issue including
the upstream merged commit are now available.
