---
title: "ELA-586-1 tzdata new upstream version"
package: tzdata
version: 2021a-0+deb8u3
distribution: "Debian 8 jessie"
description: "timezone database update"
date: 2022-03-29T19:19:07+02:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2022a. Notable
changes are:

- Adjusted DST rules for Palestine, already in effect.
