---
title: "ELA-551-1 policykit-1 security update"
package: policykit-1
version: 0.105-15~deb8u5
distribution: "Debian 8 jessie"
description: "root privilege escalation"
date: 2022-01-25T19:35:45+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-4034

---

The Qualys Research Labs discovered a local privilege escalation in
PolicyKit's pkexec.

Details can be found in the Qualys advisory at
https://www.qualys.com/2022/01/25/cve-2021-4034/pwnkit.txt
