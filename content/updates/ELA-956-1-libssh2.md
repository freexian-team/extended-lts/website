---
title: "ELA-956-1 libssh2 security update"
package: libssh2
version: 1.4.3-4.1+deb8u7 (jessie), 1.7.0-1+deb9u3 (stretch)
version_map: {"8 jessie": "1.4.3-4.1+deb8u7", "9 stretch": "1.7.0-1+deb9u3"}
description: "out of bound memory"
date: 2023-09-23T16:10:28+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2020-22218

---

An issue has been found in libssh2, an SSH2 client-side library, in function _libssh2_packet_add(), which could allow attackers to access out of bounds memory.
