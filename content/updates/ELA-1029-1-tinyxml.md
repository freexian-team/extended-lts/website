---
title: "ELA-1029-1 tinyxml security update"
package: tinyxml
version: 2.6.2-2+deb8u2 (jessie), 2.6.2-4+deb9u2 (stretch)
version_map: {"8 jessie": "2.6.2-2+deb8u2", "9 stretch": "2.6.2-4+deb9u2"}
description: "denial of service"
date: 2024-01-16T20:14:18Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-34194

---

TinyXML, a small and simple XML parser library, was vulnerable.
A specially crafted	XML document with a NUL character (<kbd>\0</kbd>) 
located after a whitespace character, could trigger a crash.

