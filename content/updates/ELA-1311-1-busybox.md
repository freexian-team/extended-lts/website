---
title: "ELA-1311-1 busybox security update"
package: busybox
version: 1:1.22.0-9+deb8u6 (jessie), 1:1.22.0-19+deb9u3 (stretch), 1:1.30.1-4+deb10u1 (buster)
version_map: {"8 jessie": "1:1.22.0-9+deb8u6", "9 stretch": "1:1.22.0-19+deb9u3", "10 buster": "1:1.30.1-4+deb10u1"}
description: "multiple vulnerabilities"
date: 2025-01-31T09:25:03+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-20679
  - CVE-2021-28831
  - CVE-2021-42378
  - CVE-2021-42379
  - CVE-2021-42380
  - CVE-2021-42381
  - CVE-2021-42382
  - CVE-2021-42384
  - CVE-2021-42385
  - CVE-2021-42386
  - CVE-2022-48174
  - CVE-2023-42364
  - CVE-2023-42365

---

Multiple vulnerabilities have been found in BusyBox, a lightweight
single-executable containing various Unix utilities, which potentially
allow attackers to cause denial of service, information leakage, or
arbitrary code execution through malformed gzip data, crafted LZMA
input or crafted awk patterns.

CVE-2018-20679 (Jessie and Stretch only)

    An issue was discovered in BusyBox before 1.30.0. An out of bounds read
    in udhcp components (consumed by the DHCP server, client, and relay)
    allows a remote attacker to leak sensitive information from the stack by
    sending a crafted DHCP message. This is related to verification in
    udhcp_get_option() in networking/udhcp/common.c that 4-byte options are
    indeed 4 bytes.

CVE-2021-28831 (Buster only)

    decompress_gunzip.c in BusyBox through 1.32.1 mishandles the error bit
    on the huft_build result pointer, with a resultant invalid free or
    segmentation fault, via malformed gzip data.

CVE-2021-42374

    An out-of-bounds heap read in Busybox's unlzma applet leads to
    information leak and denial of service when crafted LZMA-compressed
    input is decompressed. This can be triggered by any applet/format that

CVE-2021-42378

    A use-after-free in Busybox's awk applet leads to denial of service and
    possibly code execution when processing a crafted awk pattern in the
    getvar_i function

CVE-2021-42379

    A use-after-free in Busybox's awk applet leads to denial of service and
    possibly code execution when processing a crafted awk pattern in the
    next_input_file function

CVE-2021-42380

    A use-after-free in Busybox's awk applet leads to denial of service and
    possibly code execution when processing a crafted awk pattern in the
    clrvar function

CVE-2021-42381

    A use-after-free in Busybox's awk applet leads to denial of service and
    possibly code execution when processing a crafted awk pattern in the
    hash_init function

CVE-2021-42382

    use-after-free in Busybox's awk applet leads to denial of service and
    possibly code execution when processing a crafted awk pattern in the
    getvar_s function

CVE-2021-42384

    A use-after-free in Busybox's awk applet leads to denial of service and
    possibly code execution when processing a crafted awk pattern in the
    handle_special function

CVE-2021-42385

    A use-after-free in Busybox's awk applet leads to denial of service and
    possibly code execution when processing a crafted awk pattern in the
    evaluate function

CVE-2021-42386

    A use-after-free in Busybox's awk applet leads to denial of service and
    possibly code execution when processing a crafted awk pattern in the
    nvalloc function

CVE-2022-48174

    There is a stack overflow vulnerability in ash.c:6030 in busybox before
    1.35. In the environment of Internet of Vehicles, this vulnerability can
    be executed from command to arbitrary code execution.

CVE-2023-42364

    A use-after-free vulnerability in BusyBox v.1.36.1 allows attackers to
    cause a denial of service via a crafted awk pattern in the awk.c
    evaluate function.

CVE-2023-42365

    A use-after-free vulnerability was discovered in BusyBox v.1.36.1 via a
    crafted awk pattern in the awk.c copyvar function.

