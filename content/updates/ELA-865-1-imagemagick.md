---
title: "ELA-865-1 imagemagick security update"
package: imagemagick
version: 8:6.8.9.9-5+deb8u26 (jessie), 8:6.9.7.4+dfsg-11+deb9u19 (stretch)
version_map: {"8 jessie": "8:6.8.9.9-5+deb8u26", "9 stretch": "8:6.9.7.4+dfsg-11+deb9u19"}
description: "several security vulnerabilities"
date: 2023-06-07T10:31:18Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2017-12670
  - CVE-2018-10804
  - CVE-2021-20309
  - CVE-2022-32545
  - CVE-2022-32546
  - CVE-2022-32547

---

Several security vulnerabilities have been addressed in imagemagick, an image processing toolkit.

CVE-2017-12670

    A missing validation was found in coders/mat.c, leading to an assertion failure in the function DestroyImage in MagickCore/image.c, which allows attackers to cause a denial of service. This fix was only applied for  Debian 9 stretch. Debian 8 jessie was previously fixed.

CVE-2018-10804

    A memory leak in WriteTIFFImage (coders/tiff.c) was fixed.

CVE-2021-20309

    A division by zero in WaveImage() was fixed.

CVE-2022-32545

    An undefined behavior due to conversion to outside the range of long was fixed.

CVE-2022-32546

    An unaligned access in magick/property.c was fixed.

CVE-2022-32547

    An undefined behavior due to conversion to outside the range of representable values of type 'unsigned char'.

