---
title: "ELA-1138-1 python3.4 security update"
package: python3.4
version: 3.4.2-1+deb8u18 (jessie)
version_map: {"8 jessie": "3.4.2-1+deb8u18"}
description: "multiple vulnerabilities"
date: 2024-07-23T16:21:43+03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-4032
  - CVE-2024-5642

---

Multiple vulnerabilities have been fixed in the Python3 interpreter.

CVE-2024-4032

    Incorrect information about private addresses in the ipaddress module

CVE-2024-5642

    NPN buffer overread when using empty list in SSLContext.set_npn_protocols()

Note that the CVE-2024-5642 fix disables NPN (Next Protocol Negotiation) in the ssl module, NPN is a TLS extension for the obsolete SPDY protocol (HTTP/2 is the successor to SPDY).
