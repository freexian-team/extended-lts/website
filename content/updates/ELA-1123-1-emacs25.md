---
title: "ELA-1123-1 emacs25 security update"
package: emacs25
version: 25.1+1-4+deb9u5 (stretch)
version_map: {"9 stretch": "25.1+1-4+deb9u5"}
description: "arbitrary code execution"
date: 2024-07-05T10:00:21+08:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-39331

---

A vulnerability was discovered in GNU Emacs, the extensible, customisable,
self-documenting display editor.

The org-link-expand-abbrev function expanded a %(...) link abbrev even when
the abbrev specified an unsafe function, such as shell-command-to-string.
This could lead to arbitrary code execution as soon as an Org-mode format file
was opened.

