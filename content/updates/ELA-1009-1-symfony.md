---
title: "ELA-1009-1 symfony security update"
package: symfony
version: 2.8.7+dfsg-1.3+deb9u5 (stretch)
version_map: {"9 stretch": "2.8.7+dfsg-1.3+deb9u5"}
description: "cross-site-scripting"
date: 2023-11-27T20:05:19+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-46734

---

Pierre Rudloff discovered a potential XSS vulnerability in Symfony, a PHP
framework. Some Twig filters in CodeExtension use `is_safe=html` but do not
actually ensure their input is safe. Symfony now escapes the output of the
affected filters.

