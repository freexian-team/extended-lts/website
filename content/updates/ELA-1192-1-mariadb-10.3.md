---
title: "ELA-1192-1 mariadb-10.3 security update"
package: mariadb-10.3
version: 1:10.3.39-0+deb10u3 (buster)
version_map: {"10 buster": "1:10.3.39-0+deb10u3"}
description: "unauthorized read access"
date: 2024-10-03T19:51:28Z
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-21096

---

Several vulnerabilities have been fixed in MariaDB, a popular database server.

CVE-2024-21096

    A difficult to exploit vulnerability allows unauthenticated
    attacker with logon to the infrastructure where MariaDB Server
    executes to compromise MariaDB Server.
    Successful attacks of this vulnerability can result in
    unauthorized update, insert or delete access to some of
    MariaDB Server accessible data as well as unauthorized
    read access to a subset of MariaDB Server accessible
    data and unauthorized ability to cause a partial
    denial of service (partial DoS)

Note that fixes related to CVE-2024-21096 may break forwards and backwards
compatibility in certain situations when doing logical backup and restore
with plain SQL files (e.g. when using `mariadb-dump` or `mysqldump`).

The MariaDB client now has the command-line option `--sandbox` and the
MariaDB client database prompt command `\-`. This enables sandbox mode for
the rest of the session, until disconnected. Once in sandbox mode, any
command that could do something on the shell is disabled.

Additionally `mysqldump` now adds the following command inside a comment
 at the very top of the logical SQL file to trigger sandbox mode:

    /*M!999999\- enable the sandbox mode */

Newer version of MariaDB clients strip away the backslash and dash (\-), and
then tries to execute the internal command with a dash.

Older versions of MariaDB client and all versions of MySQL client considers
this a comment, and will ignore it. There may however be situations where
importing logical SQL dump files may fail due to this, so users should be
advised.

Users are best protected from both security issues and interoperability
issues by using the latest `mariadb-dump` shipped in MariaDB 11.4.3, 10.11.9,
10.6.19 and 10.5.26. The CVE-2024-21096 was officially fixed already in
11.4.2, but the latest batch of MariaDB minor maintenance releases include
further improvements on the sandbox mode.

Note that the `mariadb-dump` can be used to make the logical backups from
both MariaDB and MySQL servers. Also the `mariadb` client program can connect
to both MariaDB and MySQL servers and import those SQL dump files.
