---
title: "ELA-146-1 nss security update"
package: nss
version: 2:3.26-1+debu7u7
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2019-07-20T16:54:18Z
draft: false
type: updates
cvelist:
  - CVE-2019-11719
  - CVE-2019-11729

---

Vulnerabilities have been discovered in nss, the Mozilla Network
Security Service library.

CVE-2019-11719: Out-of-bounds read when importing curve25519 private key

    When importing a curve25519 private key in PKCS#8format with leading
    0x00 bytes, it is possible to trigger an out-of-bounds read in the
    Network Security Services (NSS) library. This could lead to
    information disclosure.

CVE-2019-11729: Empty or malformed p256-ECDH public keys may trigger a
    segmentation fault

    Empty or malformed p256-ECDH public keys may trigger a segmentation
    fault due values being improperly sanitized before being copied into
    memory and used.
