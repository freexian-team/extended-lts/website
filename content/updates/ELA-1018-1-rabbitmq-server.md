---
title: "ELA-1018-1 rabbitmq-server security update"
package: rabbitmq-server
version: 3.6.6+really3.8.9-0+deb9u2 (stretch)
version_map: {"9 stretch": "3.6.6+really3.8.9-0+deb9u2"}
description: "denial of service"
date: 2023-12-11T12:44:16+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-46118

---

RabbitMQ is a multi-protocol messaging and streaming broker. The HTTP API did
not enforce an HTTP request body limit, making it vulnerable for denial of
service (DoS) attacks with very large messages by an authenticated user with
sufficient credentials.
