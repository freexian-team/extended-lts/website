---
title: "ELA-502-1 phpldapadmin security update"
package: phpldapadmin
version: 1.2.2-5.2+deb8u2
distribution: "Debian 8 jessie"
description: "cross-site scripting"
date: 2021-10-25T11:32:51+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-35132

---

An XSS security vulnerability has been discovered in phpLDAPadmin, a web based
interface for administering LDAP servers, that allows users to store malicious
values that may be executed by other users at a later time.
