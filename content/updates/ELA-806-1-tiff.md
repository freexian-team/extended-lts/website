---
title: "ELA-806-1 tiff security update"
package: tiff
version: 4.0.3-12.3+deb8u15 (jessie), 4.0.8-2+deb9u10 (stretch)
version_map: {"8 jessie": "4.0.3-12.3+deb8u15", "9 stretch": "4.0.8-2+deb9u10"}
description: "denial of service"
date: 2023-02-22T00:04:20+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-0795
  - CVE-2023-0796
  - CVE-2023-0797
  - CVE-2023-0798
  - CVE-2023-0799
  - CVE-2023-0800
  - CVE-2023-0801
  - CVE-2023-0802
  - CVE-2023-0803
  - CVE-2023-0804

---

Several flaws were found in tiffcrop, a program distributed by tiff, a library
and tools providing support for the Tag Image File Format (TIFF). A specially
crafted tiff file can lead to an out-of-bounds write or read resulting in a
denial of service.
