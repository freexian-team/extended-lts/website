---
title: "ELA-688-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb8u19 (jessie), 1.1.0l-1~deb9u7 (stretch)
version_map: {"8 jessie": "1.0.1t-1+deb8u19", "9 stretch": "1.1.0l-1~deb9u7"}
description: "command injection"
date: 2022-09-29T18:08:27+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-2068
  - CVE-2022-2097

---

It was discovered that the c_rehash script included in OpenSSL did not
sanitise shell meta characters which could result in the execution of
arbitrary commands.

In addition, the stretch package addresses CVE-2022-2097, an information
disclosure issue in the AES OCB assembly implementation for the x86
architecture.
