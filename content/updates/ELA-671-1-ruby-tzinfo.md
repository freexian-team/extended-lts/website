---
title: "ELA-671-1 ruby-tzinfo security update"
package: ruby-tzinfo
version: 1.2.2-2+deb9u1 (stretch)
version_map: {"9 stretch": "1.2.2-2+deb9u1"}
description: "directory traversal"
date: 2022-09-04T02:31:35+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-31163

---

It was discovered that there was a potential directory traversal
vulnerablilty in ruby-tzinfo, a timezone library for the Ruby
programming language.
