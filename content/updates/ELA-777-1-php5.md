---
title: "ELA-777-1 php5 security update"
package: php5
version: 5.6.40+dfsg-0+deb8u16 (jessie)
version_map: {"8 jessie": "5.6.40+dfsg-0+deb8u16"}
description: "multiple vulnerabilities"
date: 2023-01-24T15:22:00+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2021-21707
  - CVE-2022-31625
  - CVE-2022-31626
  - CVE-2022-31628
  - CVE-2022-31629

---

Multiple security issues were discovered in PHP, a widely-used open
source general purpose scripting language which could result in denial
of service, information disclosure, insecure cookie handling or
potentially the execution of arbitrary code.
