---
title: "ELA-951-1 beep security update"
package: beep
version: 1.3-4+deb9u2 (stretch)
version_map: {"9 stretch": "1.3-4+deb9u2"}
description: "denial of service"
date: 2023-09-20T23:58:53+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2018-1000532

---

It was found that beep, an advanced PC-speaker beeper, contains an External
Control of File Name or Path vulnerability in the `--device` option that can allow a
local unprivileged user to inhibit execution of arbitrary programs by other
users, allowing DoS.
