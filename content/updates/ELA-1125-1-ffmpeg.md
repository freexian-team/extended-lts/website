---
title: "ELA-1125-1 ffmpeg security update"
package: ffmpeg
version: 7:3.2.19-0+deb9u4 (stretch)
version_map: {"9 stretch": "7:3.2.19-0+deb9u4"}
description: "buffer overflows"
date: 2024-07-06T23:34:50+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-48434
  - CVE-2023-50010
  - CVE-2023-51793
  - CVE-2023-51794
  - CVE-2023-51798
  - CVE-2024-31578

---

Several buffer overflow vulnerabilities were discovered in ffmpeg, tools for
transcoding, streaming and playing of multimedia files. An attacker may use
these flaws to create specially crafted multimedia files and cause a denial of
service or arbitrary code execution when they are processed by ffmpeg.
