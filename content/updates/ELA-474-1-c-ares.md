---
title: "ELA-474-1 c-ares security update"
package: c-ares
version: 1.10.0-2+deb8u3
distribution: "Debian 8 jessie"
description: "missing input validation"
date: 2021-08-10T08:41:54+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3672

---

An issue has been found in c-ares, an asynchronous name resolver.
Missing input validation of host names returned by Domain Name Servers can lead to output of wrong hostnames.

