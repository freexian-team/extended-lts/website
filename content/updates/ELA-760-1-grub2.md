---
title: "ELA-760-1 grub2 security update"
package: grub2
version: 2.02~beta3-5+deb9u3 (stretch)
version_map: {"9 stretch": "2.02~beta3-5+deb9u3"}
description: "execution of arbitrary code"
date: 2022-12-30T14:41:28+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2022-2601
  - CVE-2022-3775

---

Several issues were found in GRUB2's font handling code, which could
result in crashes and potentially execution of arbitrary code.
