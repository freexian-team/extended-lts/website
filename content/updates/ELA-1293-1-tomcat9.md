---
title: "ELA-1293-1 tomcat9 security update"
package: tomcat9
version: 9.0.31-1~deb10u13 (buster)
version_map: {"10 buster": "9.0.31-1~deb10u13"}
description: "denial of service"
date: 2025-01-15T22:28:53+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-21733
  - CVE-2024-38286
  - CVE-2024-50379
  - CVE-2024-52316
  - CVE-2024-56337

---

Several problems have been addressed in Tomcat 9, a Java based web server,
servlet and JSP engine.

CVE-2024-21733

    Generation of Error Message Containing Sensitive Information vulnerability
    in Apache Tomcat.

CVE-2024-38286

    Apache Tomcat, under certain configurations, allows an attacker to cause an
    OutOfMemoryError by abusing the TLS handshake process.

CVE-2024-52316

    Unchecked Error Condition vulnerability in Apache Tomcat. If Tomcat is
    configured to use a custom Jakarta Authentication (formerly JASPIC)
    ServerAuthContext component which may throw an exception during the
    authentication process without explicitly setting an HTTP status to
    indicate failure, the authentication may not fail, allowing the user to
    bypass the authentication process. There are no known Jakarta
    Authentication components that behave in this way.

CVE-2024-50379 / CVE-2024-56337

    Time-of-check Time-of-use (TOCTOU) Race Condition vulnerability during JSP
    compilation in Apache Tomcat permits an RCE on case insensitive file
    systems when the default servlet is enabled for write (non-default
    configuration).
    Some users may need additional configuration to fully mitigate
    CVE-2024-50379 depending on which version of Java they are using with
    Tomcat. For Debian 10 "buster" the system property
    sun.io.useCanonCaches must be explicitly set to false (it defaults to
    true). Most Debian users will not be affected because Debian uses case
    sensitive file systems by default.
