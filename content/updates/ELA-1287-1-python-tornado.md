---
title: "ELA-1287-1 python-tornado security update"
package: python-tornado
version: 4.4.3-1+deb9u1 (stretch), 5.1.1-4+deb10u1 (buster)
version_map: {"9 stretch": "4.4.3-1+deb9u1", "10 buster": "5.1.1-4+deb10u1"}
description: "multiple vulnerabilities"
date: 2025-01-11T17:20:45+01:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-28370
  - CVE-2024-52804

---

Multiple vulnerabilities were discovered in python-tornado, a scalable,
non-blocking Python web framework and asynchronous networking library.

CVE-2023-28370

    An open redirect vulnerability in Tornado versions 6.3.1 and earlier allows
    a remote unauthenticated attacker to redirect a user to an arbitrary web
    site and conduct a phishing attack by having the user access a specially
    crafted URL.

CVE-2024-52804

    The algorithm used for parsing HTTP cookies in Tornado versions prior to
    6.4.2 sometimes has quadratic complexity, leading to excessive CPU
    consumption when parsing maliciously-crafted cookie headers. This
    parsing occurs in the event loop thread and may block the processing of
    other requests.
