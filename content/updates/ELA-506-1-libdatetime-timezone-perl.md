---
title: "ELA-506-1 libdatetime-timezone-perl new upstream version"
package: libdatetime-timezone-perl
version: 1:1.75-2+2021e
distribution: "Debian 8 jessie"
description: "update to tzdata 2021e"
date: 2021-10-29T12:08:10+02:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2021e for the
Perl bindings.
