---
title: "ELA-1238-1 needrestart security update"
package: needrestart
version: 1.2-8+deb8u3 (jessie), 2.11-3+deb9u3 (stretch), 3.4-5+deb10u2 (buster)
version_map: {"8 jessie": "1.2-8+deb8u3", "9 stretch": "2.11-3+deb9u3", "10 buster": "3.4-5+deb10u2"}
description: "multiple vulnerabilities"
date: 2024-11-20T15:23:01-03:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2024-11003
  - CVE-2024-48990
  - CVE-2024-48991
  - CVE-2024-48992

---

The Qualys Threat Research Unit discovered several local privilege
escalation vulnerabilities in needrestart, a utility to check which
daemons need to be restarted after library upgrades.

CVE-2024-11003

```
  Local attackers can trick needrestart to call the Perl module
  Module::ScanDeps with attacker-controlled files.
```

CVE-2024-48990

```
  Local attackers can execute arbitrary code as root by tricking needrestart
  into running the Python interpreter with an attacker-controlled PYTHONPATH
  environment variable.
```

CVE-2024-28991

```
  Local attackers can execute arbitrary code as root by winning a race
  condition and tricking needrestart into running their own, fake Python
  interpreter (instead of the system's real Python interpreter).
```

CVE-2024-28992

```
  Local attackers can also execute arbitrary code as root by tricking
  needrestart into running the Ruby interpreter with an attacker-controlled
  RUBYLIB environment variable.
```

Details can be found in the Qualys advisory at
https://www.qualys.com/2024/11/19/needrestart/needrestart.txt
