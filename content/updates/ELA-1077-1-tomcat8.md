---
title: "ELA-1077-1 tomcat8 security update"
package: tomcat8
version: 8.0.14-1+deb8u28 (jessie)
version_map: {"8 jessie": "8.0.14-1+deb8u28"}
description: "request smuggling"
date: 2024-04-26T06:31:17+02:00
draft: false
type: updates
tags:
- update
cvelist:
  - CVE-2023-46589

---

Norihito Aimoto of OSSTech Corporation discovered a security vulnerability in
the Tomcat servlet and JSP engine.

A trailer header that exceeded the header size limit could cause Tomcat to
treat a single request as multiple requests leading to the possibility of
request smuggling when behind a reverse proxy.
