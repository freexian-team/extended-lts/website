---
title: "ELA-697-1 libraw security update"
package: libraw
version: 0.17.2-6+deb9u3 (stretch)
version_map: {"9 stretch": "0.17.2-6+deb9u3"}
description: "multiple file format vulnerabilities"
date: 2022-10-04T14:30:37+02:00
draft: false
cvelist:
  - CVE-2018-5816
  - CVE-2018-10528
  - CVE-2018-10529
  - CVE-2020-35530
  - CVE-2020-35531
  - CVE-2020-35532
  - CVE-2020-35533

---

Multiple file format vulnerabilities have been fixed in libraw.

CVE-2018-5816

    An integer overflow error within the "identify()"
    function (internal/dcraw_common.cpp) in LibRaw versions
    prior to 0.18.12 can be exploited to trigger a division by
    zero via specially crafted NOKIARAW file (Note: This
    vulnerability is caused due to an incomplete fix of
    CVE-2018-5804).

CVE-2018-10528

    There is a stack-based buffer overflow in the utf2char
    function in libraw_cxx.cpp.

CVE-2018-10529

    There is an out-of-bounds read affecting the X3F
    property table list implementation in libraw_x3f.cpp and
    libraw_cxx.cpp.

CVE-2020-35530

    In LibRaw, there is an out-of-bounds write vulnerability
    within the "new_node()" function
    (libraw\src\x3f\x3f_utils_patched.cpp) that can be triggered
    via a crafted X3F file.

CVE-2020-35531

    In LibRaw, an out-of-bounds read vulnerability exists
    within the get_huffman_diff() function
    (libraw\src\x3f\x3f_utils_patched.cpp) when reading data
    from an image file.

CVE-2020-35532

    In LibRaw, an out-of-bounds read vulnerability exists
    within the "simple_decode_row()" function
    (libraw\src\x3f\x3f_utils_patched.cpp) which can be
    triggered via an image with a large row_stride field.

CVE-2020-35533

    In LibRaw, an out-of-bounds read vulnerability exists
    within the "LibRaw::adobe_copy_pixel()" function
    (libraw\src\decoders\dng.cpp) when reading data from the
    image file.
