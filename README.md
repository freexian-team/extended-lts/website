# Extended LTS Advisories (ELA)

This repository only hosts the content of the advisories.

`content/updates/` is the important directory, it is included
dynamically in the main Freexian website during build.

The rest is not used in any way but it is kept so that you
can do some local rendering with `hugo server`.

The main freexian website is managed in this Gitlab repository
which is accessible to Freexian collaborators only:
https://gitlab.com/freexian/organization/website

This repository triggers an update of the main Freexian website
through its CI (see .gitlab-ci.yml) and a private token. So any
new ELA is made available almost instantly.
